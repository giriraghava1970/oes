	<!-- ====================================================
	================= CONTENT ===============================
	===================================================== -->
	<section id="content">	
		<div class="page page-tables-datatables">	
			<div class="pageheader">	
				<h2><?php echo $pageName;?> <span>// You can place subtitle here</span></h2>	
				<div class="page-bar">	
					<ul class="page-breadcrumb">
						<li>
							<a href="<?php echo base_url();?>"><i class="fa fa-home"></i> Dashboard</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>Masters"><i class="fa"></i> Masters</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>Masters/Users"><i class="fa"></i> Users</a>
						</li>
					</ul>	
				</div>	
			</div>
			<!-- row -->
			<div class="row">
				<!-- col -->
				<div class="col-md-12">
					<!-- tile -->
					<section class="tile">
						<!-- tile header -->
						<div class="tile-header dvd dvd-btm">
							<h1 class="custom-font"><strong>Users</strong>(Total : <?php isset($totalRecs) ? $totalRecs : 0?>)</h1>
							<ul class="controls ">
								<li class="dropdown">
									<a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
										<i class="fa fa-cog"></i>
										<i class="fa fa-spinner fa-spin"></i>
									</a>

									<ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
										<li>
											<a role="button" tabindex="0" class="tile-toggle">
												<span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
												<span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
											</a>
										</li>
										<li>
											<a role="button" tabindex="0" class="tile-refresh">
												<i class="fa fa-refresh"></i> Refresh
											</a>
										</li>
										<li>
											<a role="button" tabindex="0" class="tile-fullscreen">
												<i class="fa fa-expand"></i> Fullscreen
											</a>
										</li>
									</ul>
								</li>
								<li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
							</ul>
						</div>
						<!-- /tile header -->
						<!-- tile body -->
						<div class="tile-body">
							<table class="table table-custom dt-responsive" id="responsive-usage" width="100%">
								<thead>
									<tr>
										<th>ID</th>
										<th>First Name</th>
										<th>Last Name</th>
										<th class="none">Phone</th>
										<th class="none">Street Address</th>
										<th class="none">City</th>
										<th class="none">State</th>
										<th class="none">ZIP</th>
									</tr>
								</thead>
							</table>
						</div>
						<!-- /tile body -->
					</section>
					<!-- /tile -->
				</div>
				<!-- /col -->
			</div>
			<!-- /row -->
		</div>
	</section>
	<!--/ CONTENT -->