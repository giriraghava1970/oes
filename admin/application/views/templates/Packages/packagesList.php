<div class="card" id="showPackageForm" style="display:none;">
    <div class="card-header"><h4>Packages Add/Edit </h4></div>
        <div class="card-body">
            <form name="formCom" id="formCom" method="POST" action="" enctype="multipart/form-data">
                <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="<?php echo base_url();?>"/>
                <input type="hidden" name="hidRecID" id="hidRecID" value=""/>
                    <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                        <span class="text-red">*</span>
                            <label for="Name">Course ID:</label>
                            <select id="courseId" name="courseId" class="form-control" required>
                                <option value="">Select Course</option> 
                        <?php
                        $courses =   $this->Courses_model->get();
                        $data['courses'] =   $courses;
                        $sele   =   '';

                        if(!(empty($courses))) {
                            foreach ($courses as $cid) {
                                /*if($course_id == $cid->id) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }*/
                                echo '<option value="'.$cid->id.'"'.$sele.'">'.$cid->course_name.'</option>';
                            }
                        }
                    ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <span class="text-red">*</span>
                            <label for="Name">Batch ID:</label>
                            <select id="batchId" name="batchId" class="form-control" required>
                                <option value="">Select Batch</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                        <span class="text-red">*</span>
                            <label for="Name">Institute ID:</label>
                            <select id="instituteId" name="instituteId" class="form-control" required>
                                <option value="">Select Institute</option>
                        <?php 
                            $institutes  =   $this->Institutes_model->getInstitute();
                            $data['institutes']   =   $institutes;
                            $sele   =   '';

                            if(!(empty($institutes))) {
                                foreach ($institutes as $iid) {
                                    /*if($institute_id == $iid->id) {
                                        $sele   =   'selected="SELECTED"';
                                    } else {
                                        $sele   =   '';
                                    }*/

                                    echo '<option value="'.$iid->id.'"'.$sele.'">'.$iid->institute_name.'</option>';
                                }
                            }?>
                        </select>
                        </div>
                    </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <span class="text-red">*</span>
                                <label for="Name">Package Name:</label>
                                <input autofocus="" type="text" class="form-control" required name="packageName" id="packageName" 
                                placeHolder="Enter Package Name" value=" ">
                                <span class="text-red Error_msg" id="username_error"></span>
                            </div>
                        </div>
        
                        <div class="col-md-12">
                            <div class="form-group">
                            <span class="text-red">*</span>
                                <label for="Name">Package Description:</label>
                                <textarea name="packageDesc" id="packageDesc" placeHolder="Enter Package Description" 
                                class="form-control" ></textarea>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                            <span class="text-red">*</span>
                                <label for="Name">Start Date:</label>
                                <input type="DATE" class="form-control" required name="startDate" id="startDate"
                                value=" ">
                            </div>
                        </div>

                        <div class="col-md-4">
                                <div class="form-group">
                                <span class="text-red">*</span>
                                    <label for="Name">End Date:</label>
                                    <input type="DATE" class="form-control" required name="endDate" id="endDate"
                                    value=" ">
                                </div>
                        </div>

                    <!-- <div class="col-md-4">
                        <div class="form-group">
                        <span class="text-red">*</span>
                            <label for="Name">Course ID:</label>
                            <select id="courseId" name="courseId" class="form-control" required>
                                <option value="">Select Course</option> 

    <?php
                        $courses =   $this->Courses_model->get();
                        $data['courses'] =   $courses;

                        if(!(empty($courses))) {
                            foreach ($courses as $cid) {
                                if($course_id == $cid->id) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }

                                echo '<option value="'.$cid->id.'"'.$sele.'">'.$cid->course_name.'</option>';
                            }
                        }
                    ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <span class="text-red">*</span>
                            <label for="Name">Batch ID:</label>
                            <select id="batchId" name="batchId" class="form-control" required>
                                <option value="">Select Batch</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                        <span class="text-red">*</span>
                            <label for="Name">Institute ID:</label>
                            <select id="instituteId" name="instituteId" class="form-control" required>
                                <option value="">Select Institute</option>
                        <?php 
                            $institutes  =   $this->Institutes_model->getInstitute();
                            $data['institutes']   =   $institutes;

                            if(!(empty($institutes))) {
                                foreach ($institutes as $iid) {
                                    if($institute_id == $iid->id) {
                                        $sele   =   'selected="SELECTED"';
                                    } else {
                                        $sele   =   '';
                                    }

                                    echo '<option value="'.$iid->id.'"'.$sele.'">'.$iid->institute_name.'</option>';
                                }
                            }
                        ?>

                        </select>
                        </div>
                    </div> -->

                    <div class="col-md-4">
                    <div class="form-group">
                    <span class="text-red">*</span>
                        <label for="name">Free Package:</label><br>
                        <input type="radio" name="freePackage" value="yes">Yes
                        <input type="radio" name="freePackage" value="no">No
                    </div>
                </div>

                    <div class="col-md-4">
                    <div class="form-group">
                    <span class="text-red">*</span>
                        <label for="Name">Package Cost:</label>
                        <input type="text" class="form-control" required name="packageCost" id="packageCost"
                        value=" ">
                    </div>
                </div>


                    <div class="col-md-4">
                    <div class="form-group">
                    <span class="text-red">*</span>
                        <label for="Name">Package MRP:</label>
                        <input type="text" class="form-control" required name="packageMrp" id="packageMrp"
                        value=" ">
                    </div>
                </div>

                    <div class="col-md-4">
                        <div class="form-group">
                        <label for="photo">Select Image</label>
                        <input class="form-control" type="file" name="photo" id="photo" multiple accept='image/*' size="20">
                        </div>
                    </div>
            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="Name">Status:</label>
                                    <select id="status" name="status" class="form-control">
                                        <option value="'.$status.'">Select Status</option>

                                        <option value="Active">Active</option>
                                        <option value="InActive">InActive</option>
                                        <option value="None">NONE</option>
                                    }

                            </select>
                        </div>
                    </div>
                </div><!-- class="row"--->
            </form>
          
            <!-- <script>
            CKEDITOR.replace('packageDesc');
            </script> -->

            
            <button type="button" class="btn btn-primary" name="btnBack" id="btnBack" value="">Back To Listing</button>&nbsp;
            <button type="button" class="btn btn-primary" name="btnSave" id="btnSave" value="">Save</button>&nbsp;
            <button type="button" class="btn btn-primary" name="btnChoose" id="btnChoose" value="">Choose Test</button>&nbsp;
            <button type="button" class="btn btn-primary" name="btnCancel" id="btnCancel" value="">Cancel</button>
            <!-- <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button> -->
            </div>
            <!-- </div> -->
        </div>
        

<div class="card" id="showPackageListing">
    <div class="card-body">
        <table id="packageTable" class="table datatable-button-html5-basic">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Package</th>
                    <!-- <th>Package Description</th> -->
                    <th>StartDate</th>
                    <th>EndDate</th>
                    <th>Course/Batch</th>
                    <!-- <th>Batch</th> -->
                    <th>PublishOnline</th>
                    <th>FreePackage</th>
                    <th>PackageCost</th>
                    <th>PackageMRP</th>
                    <th>Institute</th>
                    <th>PackageImage</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<!-- THEME RELATED JQUERY FILES -->


<!-- <script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/spin.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/ladda.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/ui/prism.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/jqueryui_components.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/loaders/progressbar.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/bootstrapvalidator.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/form_validation.js"></script> 
    <script src="<?php echo base_url();?>theme/assets/js/main/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?php echo base_url();?>theme/assets/js/demo_pages/extra_sweetalert.js"></script>
    <script src="<?php echo base_url();?>theme/assets/js/plugins/editors/ckeditor/ckeditor.js"></script>  -->

    <script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/spin.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/ladda.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/components_buttons.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/bootbox.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/ui/prism.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/extra_sweetalert.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/jqueryui_components.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/loaders/progressbar.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/bootstrapvalidator.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/form_validation.js"></script>

<!-- THEME RELATED JQUERY FILES -->

<!--Your Page Wise Jquery Script here-->
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Packages/packagesListing.js"></script>
<!--Your Page Wise Jquery Script here-->

