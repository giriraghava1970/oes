	<!-- ====================================================
	================= CONTENT ===============================
	===================================================== -->
	<section id="content">
		<div class="page page-forms-common">
			<div class="pageheader">
				<h2><?php echo $pageName;?><span>// You can place subtitle here</span></h2>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="<?php echo base_url();?>"><i class="fa fa-home"></i> Dashboard</a>
						</li>
						<li>
							<a href="#"><?php echo $pageName;?></a>
						</li>
					</ul>				
				</div>
			</div>
	
			<!-- row -->
			<div class="row">
				<!-- col -->
				<div class="col-md-6">
					<!-- tile -->
					<section class="tile">
						<!-- tile header -->
						<div class="tile-header dvd dvd-btm">
							<h1 class="custom-font"><strong>Horizontal </strong>Form</h1>
							<ul class="controls">
								<li class="dropdown">
									<a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
										<i class="fa fa-cog"></i>
										<i class="fa fa-spinner fa-spin"></i>
									</a>
	
									<ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
										<li>
											<a role="button" tabindex="0" class="tile-toggle">
												<span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
												<span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
											</a>
										</li>
										<li>
											<a role="button" tabindex="0" class="tile-refresh">
												<i class="fa fa-refresh"></i> Refresh
											</a>
										</li>
										<li>
											<a role="button" tabindex="0" class="tile-fullscreen">
												<i class="fa fa-expand"></i> Fullscreen
											</a>
										</li>
									</ul>
	
								</li>
								<li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
							</ul>
						</div>
						<!-- /tile header -->
	
						<!-- tile body -->
						<div class="tile-body">
							<form class="form-horizontal" role="form">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
									<div class="col-sm-10">
										<input type="email" class="form-control" id="inputEmail3" placeholder="Email">
										<p class="help-block mb-0">Example block-level help text here.</p>
									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
									<div class="col-sm-10">
										<input type="password" class="form-control" id="inputPassword3" placeholder="Password">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<label class="checkbox checkbox-custom">
											<input type="checkbox" checked><i></i>
											Remember me
										</label>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<button type="submit" class="btn btn-rounded btn-primary btn-sm">Sign in</button>
									</div>
								</div>
							</form>
						</div>
						<!-- /tile body -->
					</section>
					<!-- /tile -->
				</div>
				<!-- /col -->
	
				<!-- col -->
				<div class="col-md-6">
					<!-- tile -->
					<section class="tile">
						<!-- tile header -->
						<div class="tile-header dvd dvd-btm">
							<h1 class="custom-font"><strong>Vertical </strong>Form</h1>
							<ul class="controls">
								<li class="dropdown">
									<a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
										<i class="fa fa-cog"></i>
										<i class="fa fa-spinner fa-spin"></i>
									</a>
	
									<ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
										<li>
											<a role="button" tabindex="0" class="tile-toggle">
												<span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
												<span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
											</a>
										</li>
										<li>
											<a role="button" tabindex="0" class="tile-refresh">
												<i class="fa fa-refresh"></i> Refresh
											</a>
										</li>
										<li>
											<a role="button" tabindex="0" class="tile-fullscreen">
												<i class="fa fa-expand"></i> Fullscreen
											</a>
										</li>
									</ul>
	
								</li>
								<li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
							</ul>
						</div>
						<!-- /tile header -->
	
						<!-- tile body -->
						<div class="tile-body">
							<form role="form">
								<div class="form-group">
									<label for="exampleInputEmail1">Email address</label>
									<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Password</label>
									<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
								</div>
								<label class="checkbox checkbox-custom">
									<input type="checkbox" checked disabled><i></i>
									Check me out
								</label>
								<button type="submit" class="btn btn-rounded btn-success btn-sm">Submit</button>
							</form>
						</div>
						<!-- /tile body -->
					</section>
					<!-- /tile -->
				</div>
				<!-- /col -->
			</div>
			<!-- /row -->
	
			<!-- row -->
			<div class="row">
				<!-- col -->
				<div class="col-md-12">
					<!-- tile -->
					<section class="tile">
						<!-- tile header -->
						<div class="tile-header dvd dvd-btm">
							<h1 class="custom-font"><strong>Inline </strong>Form</h1>
							<ul class="controls">
								<li class="dropdown">
									<a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
										<i class="fa fa-cog"></i>
										<i class="fa fa-spinner fa-spin"></i>
									</a>
	
									<ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
										<li>
											<a role="button" tabindex="0" class="tile-toggle">
												<span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
												<span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
											</a>
										</li>
										<li>
											<a role="button" tabindex="0" class="tile-refresh">
												<i class="fa fa-refresh"></i> Refresh
											</a>
										</li>
										<li>
											<a role="button" tabindex="0" class="tile-fullscreen">
												<i class="fa fa-expand"></i> Fullscreen
											</a>
										</li>
									</ul>
								</li>
								<li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
							</ul>
						</div>
						<!-- /tile header -->
	
						<!-- tile body -->
						<div class="tile-body">
							<form class="form-inline" role="form">
								<div class="form-group">
									<label class="sr-only" for="exampleInputEmail2">Email address</label>
									<input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
								</div>
								<div class="form-group">
									<label class="sr-only" for="exampleInputPassword2">Password</label>
									<input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password">
								</div>
								<label class="checkbox checkbox-custom mr-10">
									<input type="checkbox"><i></i> Remember me
								</label>
								<button type="submit" class="btn btn-default">Sign in</button>
							</form>
						</div>
						<!-- /tile body -->
					</section>
					<!-- /tile -->
	
					<!-- tile -->
					<section class="tile">
						<!-- tile header -->
						<div class="tile-header dvd dvd-btm">
							<h1 class="custom-font"><strong>Form</strong> Elements</h1>
							<ul class="controls">
								<li class="dropdown">
									<a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
										<i class="fa fa-cog"></i>
										<i class="fa fa-spinner fa-spin"></i>
									</a>
	
									<ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
										<li>
											<a role="button" tabindex="0" class="tile-toggle">
												<span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
												<span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
											</a>
										</li>
										<li>
											<a role="button" tabindex="0" class="tile-refresh">
												<i class="fa fa-refresh"></i> Refresh
											</a>
										</li>
										<li>
											<a role="button" tabindex="0" class="tile-fullscreen">
												<i class="fa fa-expand"></i> Fullscreen
											</a>
										</li>
									</ul>
								</li>
								<li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
							</ul>
						</div>
						<!-- /tile header -->
	
						<!-- tile body -->
						<div class="tile-body">
							<form class="form-horizontal" role="form">
								<div class="form-group">
									<label for="input01" class="col-sm-2 control-label">Normal</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="input01">
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label for="input02" class="col-sm-2 control-label">Password</label>
									<div class="col-sm-10">
										<input type="password" class="form-control" id="input02">
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label for="input03" class="col-sm-2 control-label">Help text</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="input03">
										<span class="help-block mb-0">A block of help text that breaks onto a new line and may extend beyond one line.</span>
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label for="input04" class="col-sm-2 control-label">Placeholder</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="input04" placeholder="This is placeholder...">
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label for="input05" class="col-sm-2 control-label">Rounded</label>
									<div class="col-sm-10">
										<input type="text" class="form-control rounded" id="input05">
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label for="input06" class="col-sm-2 control-label">Line</label>
									<div class="col-sm-10">
										<input type="text" class="form-control underline-input" id="input06" placeholder="This is placeholder...">
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label class="col-sm-2 control-label">Label not focus</label>
									<div class="col-sm-10">
										<input type="text" class="form-control">
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label for="input07" class="col-sm-2 control-label">Disabled</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="input07" placeholder="This is disabled input..." disabled>
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label class="col-sm-2 control-label">Static control</label>
									<div class="col-sm-10">
										<p class="form-control-static">email@example.com</p>
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label class="col-sm-2 control-label">Checkboxes & radios</label>
									<div class="col-sm-10">
										<div class="checkbox">
											<label>
												<input type="checkbox" value="">
												Option one is this and that�be sure to include why it's great
											</label>
										</div>
										<div class="checkbox disabled">
											<label>
												<input type="checkbox" value="" disabled>
												Option two is disabled
											</label>
										</div>
	
										<div class="radio">
											<label>
												<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
												Option one is this and that�be sure to include why it's great
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
												Option two can be something else and selecting it will deselect option one
											</label>
										</div>
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label class="col-sm-2 control-label">Inline checkboxes</label>
									<div class="col-sm-10">
	
										<label class="checkbox-inline">
											<input type="checkbox" id="inlineCheckbox1" value="option1"> 1
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" id="inlineCheckbox2" value="option2"> 2
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" id="inlineCheckbox3" value="option3"> 3
										</label>
	
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label class="col-sm-2 control-label">Custom Checkboxes & radios</label>
									<div class="col-sm-2">
										<label class="checkbox checkbox-custom">
											<input type="checkbox"><i></i> Option one
										</label>
										<label class="checkbox checkbox-custom">
											<input type="checkbox" checked><i></i> Option two checked
										</label>
										<label class="checkbox checkbox-custom">
											<input type="checkbox" checked disabled><i></i> Option three checked/disabled
										</label>
										<label class="checkbox checkbox-custom">
											<input type="checkbox" disabled><i></i> Option four disabled
										</label>
										<label class="checkbox checkbox-custom checkbox-custom-sm">
											<input type="checkbox"><i></i> Small checkbox
										</label>
										<label class="checkbox checkbox-custom checkbox-custom-lg">
											<input type="checkbox"><i></i> Large checkbox
										</label>
									</div>
	
									<div class="col-sm-2">
										<label class="checkbox checkbox-custom-alt">
											<input type="checkbox"><i></i> Option one
										</label>
										<label class="checkbox checkbox-custom-alt">
											<input type="checkbox" checked><i></i> Option two checked
										</label>
										<label class="checkbox checkbox-custom-alt">
											<input type="checkbox" checked disabled><i></i> Option three checked/disabled
										</label>
										<label class="checkbox checkbox-custom-alt">
											<input type="checkbox" disabled><i></i> Option four disabled
										</label>
										<label class="checkbox checkbox-custom-alt checkbox-custom-sm">
											<input type="checkbox"><i></i> Small checkbox
										</label>
										<label class="checkbox checkbox-custom-alt checkbox-custom-lg">
											<input type="checkbox"><i></i> Large checkbox
										</label>
	
									</div>
	
									<div class="col-sm-2">
	
										<label class="checkbox checkbox-custom">
											<input name="customRadio" type="radio"><i></i> Option one
										</label>
										<label class="checkbox checkbox-custom">
											<input name="customRadio" type="radio"><i></i> Option two
										</label>
										<label class="checkbox checkbox-custom">
											<input type="radio" checked disabled><i></i> Option three checked/disabled
										</label>
										<label class="checkbox checkbox-custom">
											<input type="radio" disabled><i></i> Option four disabled
										</label>
										<label class="checkbox checkbox-custom checkbox-custom-sm">
											<input name="customRadio" type="radio"><i></i> Small radio
										</label>
										<label class="checkbox checkbox-custom checkbox-custom-lg">
											<input name="customRadio" type="radio"><i></i> Large radio
										</label>
	
									</div>
	
									<div class="col-sm-2">
										<label class="checkbox checkbox-custom-alt">
											<input name="customRadioAlt" type="radio"><i></i> Option one
										</label>
										<label class="checkbox checkbox-custom-alt">
											<input name="customRadioAlt" type="radio"><i></i> Option two
										</label>
										<label class="checkbox checkbox-custom-alt">
											<input type="radio" checked disabled><i></i> Option three checked/disabled
										</label>
										<label class="checkbox checkbox-custom-alt">
											<input type="radio" disabled><i></i> Option four disabled
										</label>
										<label class="checkbox checkbox-custom-alt checkbox-custom-sm">
											<input name="customRadioAlt" type="radio"><i></i> Small radio
										</label>
										<label class="checkbox checkbox-custom-alt checkbox-custom-lg">
											<input name="customRadioAlt" type="radio"><i></i> Large radio
										</label>
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label class="col-sm-2 control-label">Inline checkboxes</label>
									<div class="col-sm-10">
										<label class="checkbox-inline checkbox-custom">
											<input type="checkbox" id="inlineCheckbox4" value="option1"><i></i> 1
										</label>
										<label class="checkbox-inline checkbox-custom">
											<input type="checkbox" id="inlineCheckbox5" value="option2"><i></i> 2
										</label>
										<label class="checkbox-inline checkbox-custom">
											<input type="checkbox" id="inlineCheckbox6" value="option3"><i></i> 3
										</label>
	
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label class="col-sm-2 control-label">Switches</label>
									<div class="col-sm-10">
										<div class="onoffswitch greensea inline-block">
											<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch01" checked="">
											<label class="onoffswitch-label" for="switch01">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
	
										<div class="onoffswitch primary inline-block">
											<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch02" checked="">
											<label class="onoffswitch-label" for="switch02">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
	
										<div class="onoffswitch lightred inline-block">
											<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch03" checked="">
											<label class="onoffswitch-label" for="switch03">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
	
										<div class="onoffswitch drank inline-block">
											<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch04" checked="">
											<label class="onoffswitch-label" for="switch04">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label class="col-sm-2 control-label">Radio Switch</label>
									<div class="col-sm-10">
										<div class="onoffswitch dutch inline-block">
											<input type="radio" name="onoffswitch-radio" class="onoffswitch-checkbox" id="switch05" checked="">
											<label class="onoffswitch-label" for="switch05">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
	
										<div class="onoffswitch danger inline-block">
											<input type="radio" name="onoffswitch-radio" class="onoffswitch-checkbox" id="switch06" checked="">
											<label class="onoffswitch-label" for="switch06">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label class="col-sm-2 control-label">Switch size</label>
									<div class="col-sm-10">
										<div class="onoffswitch hotpink inline-block small">
											<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch07" checked="">
											<label class="onoffswitch-label" for="switch07">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
	
										<div class="onoffswitch green inline-block medium">
											<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch08" checked="">
											<label class="onoffswitch-label" for="switch08">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label class="col-sm-2 control-label">Switch label</label>
									<div class="col-sm-10">
										<div class="onoffswitch labeled inline-block">
											<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch09" checked="">
											<label class="onoffswitch-label" for="switch09">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
	
										<div class="onoffswitch labeled blue inline-block">
											<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch10" checked="">
											<label class="onoffswitch-label" for="switch10">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
	
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
	
								<div class="form-group">
									<label class="col-sm-2 control-label">Select</label>
									<div class="col-sm-10">
										<select class="form-control mb-10">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
	
										<select multiple class="form-control">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
	
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group has-success">
									<label class="col-sm-2 control-label" for="inputSuccess1">Input success</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="inputSuccess1">
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group has-warning">
									<label class="col-sm-2 control-label" for="inputWarning1">Input warning</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="inputWarning1">
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group has-error">
									<label class="col-sm-2 control-label" for="inputError1">Input error</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="inputError1">
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Control sizing</label>
									<div class="col-sm-10">
										<input class="form-control input-lg mb-10" type="text" placeholder=".input-lg">
										<input class="form-control mb-10" type="text" placeholder="Default input">
										<input class="form-control input-sm" type="text" placeholder=".input-sm">
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Column sizing</label>
									<div class="col-sm-10">
										<div class="row">
											<div class="col-xs-2">
												<input type="text" class="form-control" placeholder=".col-xs-2">
											</div>
											<div class="col-xs-3">
												<input type="text" class="form-control" placeholder=".col-xs-3">
											</div>
											<div class="col-xs-4">
												<input type="text" class="form-control" placeholder=".col-xs-4">
											</div>
										</div>
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Input groups</label>
									<div class="col-sm-10">
										<div class="input-group mb-10">
											<span class="input-group-addon">@</span>
											<input type="text" class="form-control" placeholder="Username">
										</div>

										<div class="input-group mb-10">
											<input type="text" class="form-control">
											<span class="input-group-addon">.00</span>
										</div>

										<div class="input-group mb-10">
											<span class="input-group-addon">$</span>
											<input type="text" class="form-control">
											<span class="input-group-addon">.00</span>
										</div>

										<div class="input-group mb-10">
											<span class="input-group-addon">
												<input type="checkbox">
											</span>
											<input type="text" class="form-control">
										</div>

										<div class="input-group">
											<span class="input-group-addon">
												<input type="radio">
											</span>
											<input type="text" class="form-control">
										</div>
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Button addons</label>
									<div class="col-sm-10">
										<div class="input-group mb-10">
											<span class="input-group-btn">
												 <button class="btn btn-default" type="button">Go!</button>
											</span>
											<input type="text" class="form-control">
										</div>
	
										<div class="input-group">
											<input type="text" class="form-control">
											<span class="input-group-btn">
												<button class="btn btn-default" type="button">Go!</button>
											</span>
										</div>
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Dropdown addons</label>
									<div class="col-sm-10">
										<div class="input-group mb-10">
											<div class="input-group-btn">
												<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
												<ul class="dropdown-menu" role="menu">
													<li><a href="#">Action</a></li>
													<li><a href="#">Another action</a></li>
													<li><a href="#">Something else here</a></li>
													<li class="divider"></li>
													<li><a href="#">Separated link</a></li>
												</ul>
											</div><!-- /btn-group -->
											<input type="text" class="form-control">
										</div>

										<div class="input-group">
											<input type="text" class="form-control">
											<div class="input-group-btn">
												<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
												<ul class="dropdown-menu dropdown-menu-right" role="menu">
													<li><a href="#">Action</a></li>
													<li><a href="#">Another action</a></li>
													<li><a href="#">Something else here</a></li>
													<li class="divider"></li>
													<li><a href="#">Separated link</a></li>
												</ul>
											</div><!-- /btn-group -->
										</div>
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Segmented</label>
									<div class="col-sm-10">
										<div class="input-group mb-10">
											<div class="input-group-btn">
												<button type="button" class="btn btn-default" tabindex="-1">Action</button>
												<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
													<span class="caret"></span>
													<span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
													<li><a href="#">Action</a></li>
													<li><a href="#">Another action</a></li>
													<li><a href="#">Something else here</a></li>
													<li class="divider"></li>
													<li><a href="#">Separated link</a></li>
												</ul>
											</div>
											<input type="text" class="form-control">
										</div>

										<div class="input-group">
											<input type="text" class="form-control">
											<div class="input-group-btn">
												<button type="button" class="btn btn-default" tabindex="-1">Action</button>
												<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
													<span class="caret"></span>
													<span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu dropdown-menu-right" role="menu">
													<li><a href="#">Action</a></li>
													<li><a href="#">Another action</a></li>
													<li><a href="#">Something else here</a></li>
													<li class="divider"></li>
													<li><a href="#">Separated link</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Radio buttons</label>
									<div class="col-sm-10">
										<div class="btn-group">
											<label class="btn btn-default toggle-class" data-toggle="active" data-type="radio">Left</label>
											<label class="btn btn-default toggle-class" data-toggle="active" data-type="radio">Middle</label>
											<label class="btn btn-default toggle-class" data-toggle="active" data-type="radio">Right</label>
										</div>
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Checkbox buttons</label>
									<div class="col-sm-10">
										<div class="btn-group">
											<label class="btn btn-default toggle-class" data-toggle="active">Left</label>
											<label class="btn btn-default toggle-class" data-toggle="active">Middle</label>
											<label class="btn btn-default toggle-class" data-toggle="active">Right</label>
										</div>
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Slider</label>
									<div class="col-sm-10">
										<input id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="50" data-slider-step="1" data-slider-value="14"/>
										<span id="ex1CurrentSliderValLabel"><span id="ex1SliderVal">14</span></span>
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Vertical Slider</label>
									<div class="col-sm-10">
										<input id="ex2" type="text" data-slider-max="30" data-slider-step="1" data-slider-value="5" data-slider-orientation="vertical"/>
										<input id="ex3" type="text" data-slider-max="30" data-slider-step="1" data-slider-value="15" data-slider-orientation="vertical"/>
										<input id="ex4" type="text" data-slider-max="30" data-slider-step="1" data-slider-value="10" data-slider-orientation="vertical"/>
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Range Slider</label>
									<div class="col-sm-10">
										<input id="ex5" type="text" class="span2" value="" data-slider-max="1000" data-slider-step="10" data-slider-value="[120,780]"/>
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Color Picker HEX</label>
									<div class="col-sm-10">
										<input type="text" class="form-control w-md colorpicker" value="#e32882" data-format="hex">
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Color Picker RGB</label>
									<div class="col-sm-10">
										<input type="text" class="form-control w-md mb-10 colorpicker" value="rgb(99,56,56)" data-format="rgb">
										<input type="text" class="form-control w-md colorpicker" value="rgba(33,113,207,0.71)" data-format="rgba" data-align="left">
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">TouchSpin</label>
									<div class="col-sm-10">
										<div class="mb-10">
											<input type="text" value="55" class="form-control touchspin" data-min='0' data-max="100" data-step="0.1" data-decimals="2" data-boostat="5" data-maxboostedstep="10" data-postfix="%">
										</div>
										<div class="mb-10">
											<input type="text" value="0" class="form-control touchspin" data-min='-100000' data-max="100000" data-stepinterval="50" data-maxboostedstep="100000" data-prefix="$">
										</div>
										<div class="mb-10">
											<input type="text" value="0" class="form-control touchspin" data-verticalbuttons="true">
										</div>
										<div>
											<input type="text" value="0" class="form-control touchspin" data-verticalbuttons="true" data-verticalupclass="fa fa-plus" data-verticaldownclass="fa fa-minus">
										</div>
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Chosen</label>
									<div class="col-sm-10">
										<select tabindex="3" class="chosen-select" style="width: 240px;">
											<optgroup label="Alaskan/Hawaiian Time Zone">
												<option value="AK">Alaska</option>
												<option value="HI">Hawaii</option>
											</optgroup>
											<optgroup label="Pacific Time Zone">
												<option value="CA">California</option>
												<option value="NV">Nevada</option>
												<option value="OR">Oregon</option>
												<option value="WA">Washington</option>
											</optgroup>
											<optgroup label="Mountain Time Zone">
												<option value="AZ">Arizona</option>
												<option value="CO">Colorado</option>
												<option value="ID">Idaho</option>
												<option value="MT">Montana</option><option value="NE">Nebraska</option>
												<option value="NM">New Mexico</option>
												<option value="ND">North Dakota</option>
												<option value="UT">Utah</option>
												<option value="WY">Wyoming</option>
											</optgroup>
											<optgroup label="Central Time Zone">
												<option value="AL">Alabama</option>
												<option value="AR">Arkansas</option>
												<option value="IL">Illinois</option>
												<option value="IA">Iowa</option>
												<option value="KS">Kansas</option>
												<option value="KY">Kentucky</option>
												<option value="LA">Louisiana</option>
												<option value="MN">Minnesota</option>
												<option value="MS">Mississippi</option>
												<option value="MO">Missouri</option>
												<option value="OK">Oklahoma</option>
												<option value="SD">South Dakota</option>
												<option value="TX">Texas</option>
												<option value="TN">Tennessee</option>
												<option value="WI">Wisconsin</option>
											</optgroup>
											<optgroup label="Eastern Time Zone">
												<option value="CT">Connecticut</option>
												<option value="DE">Delaware</option>
												<option value="FL">Florida</option>
												<option value="GA">Georgia</option>
												<option value="IN">Indiana</option>
												<option value="ME">Maine</option>
												<option value="MD">Maryland</option>
												<option value="MA">Massachusetts</option>
												<option value="MI">Michigan</option>
												<option value="NH">New Hampshire</option><option value="NJ">New Jersey</option>
												<option value="NY">New York</option>
												<option value="NC">North Carolina</option>
												<option value="OH">Ohio</option>
												<option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option>
												<option value="VT">Vermont</option><option value="VA">Virginia</option>
												<option value="WV">West Virginia</option>
											</optgroup>
										</select>
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Chosen Multiple</label>
									<div class="col-sm-10">
										<select multiple="" tabindex="3" class="chosen-select" style="width: 240px;">
											<option value="AK">Alaska</option>
											<option value="HI">Hawaii</option>
											<option value="CA">California</option>
											<option value="NV">Nevada</option>
											<option value="OR">Oregon</option>
											<option value="WA">Washington</option>
										</select>
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">Datepicker</label>
									<div class="col-sm-10">
										<div class='input-group datepicker w-360'>
											<input type='text' class="form-control" />
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>

										<div class='input-group datepicker w-360 mt-10' data-format="LT">
											<input type='text' class="form-control" />
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>

										<div class='input-group datepicker w-360 mt-10' data-format="L">
											<input type='text' class="form-control" />
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>
									</div>
								</div>

								<hr class="line-dashed line-full"/>

								<div class="form-group">
									<label class="col-sm-2 control-label">File Input</label>
									<div class="col-sm-10">
										<input type="file" class="filestyle" data-buttonText="Find file" data-iconName="fa fa-inbox">
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
								<div class="form-group">
									<label class="col-sm-2 control-label">WYSIWYG</label>
									<div class="col-sm-10">
										<div id="summernote">Hello Summernote</div>
									</div>
								</div>
	
								<hr class="line-dashed line-full"/>
								<div class="form-group">
									<div class="col-sm-4 col-sm-offset-2">
										<button type="reset" class="btn btn-lightred">Cancel</button>
										<button type="submit" class="btn btn-default">Save changes</button>
									</div>
								</div>
							</form>
						</div>
						<!-- /tile body -->
					</section>
					<!-- /tile -->
				</div>
				<!-- /col -->
			</div>
			<!-- /row -->
		</div>
	</section>
	<!--/ CONTENT -->