<!DOCTYPE html>
<html lang="en">
    <head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    	<title>:: Online Examination System[OES]::</title>
    	<!-- Global stylesheets -->
    	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    	<link href="<?php echo base_url();?>theme/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    	<link href="<?php echo base_url();?>theme/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    	<link href="<?php echo base_url();?>theme/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    	<link href="<?php echo base_url();?>theme/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    	<link href="<?php echo base_url();?>theme/assets/css/components.min.css" rel="stylesheet" type="text/css">
    	<link href="<?php echo base_url();?>theme/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    	<!-- /global stylesheets -->
    	<!-- Core JS files -->
    	<script src="<?php echo base_url();?>theme/assets/js/main/jquery.min.js"></script>
    	<script src="<?php echo base_url();?>theme/assets/js/main/bootstrap.bundle.min.js"></script>
    	<script src="<?php echo base_url();?>theme/assets/js/plugins/loaders/blockui.min.js"></script>
    	<!-- /core JS files -->
    	<!-- Theme JS files -->
    	<script src="<?php echo base_url();?>theme/assets/js/plugins/forms/validation/validate.min.js"></script>
    	<script src="<?php echo base_url();?>theme/assets/js/plugins/forms/styling/uniform.min.js"></script>
    	<script src="<?php echo base_url();?>theme/assets/js/app.js"></script>
    	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/login_validation.js"></script>
    	<!-- /theme JS files -->
    </head>
    <body class="bg-slate-800">
    	<!-- Page content -->
    	<div class="page-content">
    		<!-- Main content -->
    		<div class="content-wrapper">
    			<!-- Content area -->
    			<div class="content d-flex justify-content-center align-items-center">
    				<!-- Login card -->
    				<form class="login-form" method="post" action="<?php echo base_url().'Login/Verifylogin';?>">
    					<div class="card mb-0">
    						<div class="card-body">
    							<div class="text-center mb-3">
    								<i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
    								<h5 class="mb-0">Login to your account</h5>
    								<span class="d-block text-muted">Your credentials</span>
    							</div>
    							<div class="form-group form-group-feedback form-group-feedback-left">
    								<input type="text" class="form-control" id="userName" name="userName" placeholder="Username" required>
    								<div class="form-control-feedback">
    									<i class="icon-user text-muted"></i>
    								</div>
    							</div>
    
    							<div class="form-group form-group-feedback form-group-feedback-left">
    								<input type="password" class="form-control" id="userPwd" name="userPassword" placeholder="Password" required>
    								<div class="form-control-feedback">
    									<i class="icon-lock2 text-muted"></i>
    								</div>
    							</div>
    							<div class="form-group d-flex align-items-center">
    								<div class="form-check mb-0">
    									<label class="form-check-label">
    										<input type="checkbox" name="remember" class="form-input-styled" checked data-fouc>
    										Remember
    									</label>
    								</div>
    								<a href="login_password_recover.html" class="ml-auto">Forgot password?</a>
    							</div>
    							<div class="form-group">
    								<button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
    							</div>
    							<div class="form-group text-center text-muted content-divider">
    								<span class="px-2">Don't have an account?</span>
    							</div>
    							<div class="form-group">
    								<a href="#" class="btn btn-light btn-block">Sign up</a>
    							</div>
    							<span class="form-text text-center text-muted">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a> and <a href="#">Cookie Policy</a></span>
    						</div>
    					</div>
    				</form>
    				<!-- /login card -->
    			</div>
    			<!-- /content area -->
    		</div>
    		<!-- /main content -->
    	</div>
    	<!-- /page content -->
    	<!-- Footer -->
		<div class="navbar navbar-expand-lg navbar-light">
			<div class="text-center d-lg-none w-100">
				<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
					<i class="icon-unfold mr-2"></i>
					Footer
				</button>
			</div>

			<div class="navbar-collapse collapse" id="navbar-footer">
				<span class="navbar-text">
					&copy; <?php echo date('Y');?>. <a href="#"> Online Examination System[OES]</a> Developed by <a href="http://www.icetsolutions.com/" target="_blank"> ICET Solutions Private Limited</a>
				</span>
			</div>
		</div>
		<!-- /footer -->
    </body>
</html>