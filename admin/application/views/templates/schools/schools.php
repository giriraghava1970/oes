	<div class="card">
		<div class="card-body">
			<!--  PAGE LOADER ANIMATION
        	Content loading spinner 3
        	PAGE LOADER ANIMATION -->
        	<div class="btn btn-light" id="spinner-light-8"></div>
			<table id="myTable" class="table datatable-button-html5-basic" cellspacing="0" width="100%" style="display:none;">
		   		<thead>
		    		<tr>
		    			<th>SNo</th>
		    			<th>Code</th>
		    			<th>Name</th>
		    			<!-- <th>Address</th> -->
                        <th>Principal</th>
                        <th>Students</th>
                        <th>Employees</th>
                        <th class="no-sort">Actions</th>
                    </tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</form>
<!-- Theme JS files -->
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/spin.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/ladda.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/components_buttons.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/dataTables.checkboxes.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/bootbox.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/ui/prism.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/extensions/jquery_ui/widgets.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/extensions/jquery_ui/effects.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/jqueryui_components.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/loaders/progressbar.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/bootstrapvalidator.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/form_validation.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/forms/styling/switchery.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/extra_sweetalert.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Schools/schoolsListing.js"></script>