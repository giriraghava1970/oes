<!-- Form Section -->
<div class="card" id="showCoursesForm" style="display:none;">
	<div class="card-body">
		<div class="card-header">
			<h4>Course Add / Edit <div style="float:right"><a class="nav-link" id="showBackToListing" href="javascript:void(0);">Back To Listing</a></div></h4>
		</div>
		
<?php   $formCont   =   '';
        $hidRecID   =  0;
        $courseCode =   $courseName =   $courseDesc =   $status =   '';
        $classIDs   =   $qBankID   =   $qCatIDs    =   $subjIDs =   $levelID   =   $qTypeID   =   '';
        $formCont   .=  '<form id="frmCommon" post="frmCommon" method="post" action="">
                            <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
                            <input type="hidden" name="hidRecID" id="hidRecID" value="0"/>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="courseCode">Course Code:</label>
                                        <input type="text" class="form-control" required name="courseCode" id="courseCode" value="'.$courseCode.'">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="courseName">Course Name:&nbsp;<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" required name="courseName" id="courseName" value="'.$courseName.'">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="courseCode">Class:&nbsp;<span class="text-danger">*</span></label>
                                        <select d1ata-placeholder="Select Class" class="form-control form-input-styled" data-fouc name="lsClasses[]" id="lsClasses" multiple style="height:100px !important;">
                                            <option value="">--Choose One--</option>';
        if(!(empty($classRes))) {
            foreach($classRes as $cl) {
                if(!(empty($classIDs))) {
                    if(in_array($cl->id, $classIDs)) {
                        $sele   =   'selected="SELECTED"';
                    } else {
                        $sele   =   '';
                    }
                } else {
                    $sele   =   '';
                }

                $formCont   .=  '<option value="'.$cl->id.'" ' . $sele.'>'.$cl->class_name.'</option>';
            }
        }

        $formCont   .=  '               </select>
                                    </div>
                                </div>
                            </div><!-- Top Row-->
                            <div class="card"><!--class="card"-->
    							<div class="card-header header-elements-inline">
                                    <div class="form-group">
									   <h6 class="card-title">Parameters</h6>
                                    </div>

    								<div class="header-elements">
    									<div class="list-icons">
    				                		<a class="list-icons-item" data-action="collapse"></a>
    				                		<!--<a class="list-icons-item" data-action="reload"></a>
    				                		<a class="list-icons-item" data-action="remove"></a>-->
    				                	</div>
    			                	</div>
    		                	</div>

						        <div class="card-body pb-0"><!--class="card-body pb-0" for Parameters-->
							        <div class="row">
                                        <div class="col-md-12">';
        if(!(empty($questionBankRes))) {
            $cnt    =   0;

            foreach($questionBankRes as $qB) {
                $formCont   .=  '           <div class="card"><!--class="card"-->
                    							<div class="card-header header-elements-inline">
                                                    <div class="form-group">
                									   <label>&nbsp;</label><BR/>
                									   <div class="form-check mb-0">
                										  <label class="form-check-label">
                										      <input type="checkbox" id="cboQBank-'.$cnt.'" name="courses[questionBank]" class="form-input-styled" value="'.$qB->id.'">
                                                              <h6 class="card-title">'.$qB->questionbank_type_name.'</h6>
                										</label>
                									   </div>
                                                    </div>
            
                    								<div class="header-elements">
                    									<div class="list-icons">
                    				                		<a class="list-icons-item" data-action="collapse"></a>
                    				                		<!--<a class="list-icons-item" data-action="reload"></a>
                    				                		<a class="list-icons-item" data-action="remove"></a>-->
                    				                	</div>
                    			                	</div>
                    		                	</div>
            
            							        <div class="card-body pb-0"><!--class="card-body pb-0"-->
            								        <div class="row">
                                                        <div class="col-md-3" nowrap="nowrap"><!-- class="col-md-4" for Level-->';
                if(!empty($levelRes)) {
                    $lCnt   =   0;
                    foreach($levelRes as $l) {
                        $formCont   .=  '                   <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                    									<label>&nbsp;</label><BR/>
                                    									<div class="form-check mb-0">
                                    										<label class="form-check-label">
                                    										<input type="checkbox" id="cboLevel-'.$lCnt.'" name="courses[questionBank][levels]" class="form-input-styled" value="'.$l->id.'">'.
                                    										  $l->level_name.'
                                    										</label>
                                    									</div>
                                    								</div>
                                                                </div>
                                                            </div>';
                        $lCnt++;
                    }
                }

                $formCont   .=  '                       </div><!-- class="col-md-4" for Level-->
                                                        <div class="col-md-3"><!-- class="col-md-4" for % -->';
                $lCnt   =   0;

                for($i=0; $i < count($levelRes);$i++) {
                    $formCont   .=          '               <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>Percentage</label>
                                                                        <input class="form-control" type="text" name="courses[questionBank][Levels][Percent]" id="percent-'.$i.'" style="width:20%" placeHolder="%" value="">
                                                                    </div>
                                                                </div>
                                                            </div>';
                }

                $formCont   .=  '                       </div><!-- class="col-md-4" for %-->
                                                        <div class="col-md-6"><!-- class="col-md-4" for qType-->';

                if(!empty($questionTypesRes)) {
                    $qCnt   =   0;

                    foreach($questionTypesRes as $qT) {
                        $formCont   .=          '           <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                    									<label>&nbsp;</label><BR/>
                                    									<div class="form-check mb-0">
                                    										<label class="form-check-label">
                                    										<input type="checkbox" id="cboQuestionTypes-'.$qCnt.'" name="courses[questionBank][questionTypes]" class="form-input-styled" value="'.$qT->id.'">'.
                                    										$qT->question_type_name.'
                                    										</label>
                                    									</div>
                                    								</div>
                                                                </div>
                                                            </div>';
                        $qCnt++;
                    }
                }

                $formCont   .=  '                       </div><!-- class="col-md-4" for qType-->
                                                    </div>
                                                </div><!--class="card-body pb-0"-->
                                             </div><!--class="card"-->';
                $cnt++;
            }
        }

        $formCont   .='                 </div><!-- class="col-md-12"-->
                                    </div><!-- class="row"-->
                                </div><!--class="card-body pb-0" for Parameters-->
                            </div><!-- class="card" for Parameters"-->
                            <div class="row"><!-- class="row"-->
                                <div class="col-md-3">
                                     <div class="form-group">
                                        <label for="subject">Subjects:&nbsp;<span class="text-danger">*</span></label></label>
                                        <select data-placeholder="Select Subjects" class="form-control form-input-styled" data-fouc name="courses[Subjects]" id="lsSubjects">
                                            <option value="">--Choose One--</option>';
                    if(!(empty($subjRes))) {
                        foreach($subjRes as $s) {
                            if(!(empty($subjIDs))) {
                                if(in_array($$s->id, $subjIDs)) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }
                            } else {
                                $sele   =   '';
                            }

                            $formCont   .=  '<option value="'.$s->id.'" '. $sele.'>'.$s->subject_name.'</option>';
                        }
                    }

                    $formCont   .=  '   </select>
                                    </div>
                                </div>
                            </div><!-- class="row"-->
                            <div class="row" id="showSubjectSections" style="display:none;">
                                <div class="col-md-4" id="showChapters"> <!-- col-md-4" for Chapters-->
                                </div> <!-- col-md-4" for Chapters-->
                                <div class="col-md-4" id="showTopics" style="display:none;"> <!-- col-md-4" for Topics-->
                                </div> <!-- col-md-4" for Topics-->
                                <div class="col-md-4" id="showSubTopics" style="display:none;"> <!-- col-md-4" for Sub Topics-->
                                </div> <!-- col-md-4" for Sub Topics-->
                            </div><!-- class="row"-->
                        </form>';
        echo $formCont;?>
	</div>
</div>
<!--  Form Section -->
<div class="card"  id="showCoursesListing"> 
    <div class="card-body">
        <table id="coursesTable" class="table datatable-button-html5-basic">
            <thead>
                <tr>
                    <th>SNo</th>
                    <th>Course Code</th>
                    <th>Course Name</th>
                    <th>Course Description </th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table> 
    </div>
</div>
<input type="hidden" name="hidProjectURL" id="hidProjectURL" value="<?php echo base_url();?>"/>
<!-- THEME RELATED JQUERY FILES -->
<!-- <script src="<?php echo base_url();?>/theme/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/buttons/spin.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/buttons/ladda.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/components_buttons.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/dataTables.checkboxes.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/notifications/bootbox.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/ui/prism.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/extensions/jquery_ui/widgets.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/extensions/jquery_ui/effects.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/jqueryui_components.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/loaders/progressbar.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/bootstrapvalidator.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/form_validation.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/styling/switchery.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/extra_sweetalert.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/picker_date.js"></script> -->

<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/spin.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/ladda.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/components_buttons.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/bootbox.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/ui/prism.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/extra_sweetalert.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/jqueryui_components.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/loaders/progressbar.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/bootstrapvalidator.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/form_validation.js"></script>


<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Academics/Courses/coursesListing.js"></script>