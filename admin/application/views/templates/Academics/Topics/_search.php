<?php   $chkLastArr =   null?>
	<form name="frmFilters" id="frmFilters" method="post" action ="">
		<input type="hidden" name="hidProjectURL" id="hidProjectURL" value="<?php echo base_url(); ?>"/>
		<input type="hidden" name="hidJSONPath" id="hidJSONPath" value="<?php echo base_url(); ?>"/>
		<input type="hidden" name="hidSearchFlag" id="hidSearchFlag" value=""/>
		<input type="hidden" name="hidSearchConcat" id="hidSearchConcat" value=""/>
		<input type="hidden" name="hidShowAll" id="hidShowAll" value="0"/>
		<input type="hidden" name="hidStatusSearch" id="hidStatusSearch" value=""/>
		<!-- Student Listing -->
		<div class="card">
			<!-- <div class="card-body" style="padding:0.5rem!important;overflow:auto;"> -->
				<div class="navbar navbar-expand-xl navbar-dark bg-indigo-400 navbar-component rounded-top mb-0"> <!-- class="navbar navbar-expand-xl navbar-light navbar-component rounded-top mb-0" -->
					<div class="d-xl-none">
						<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-demo-dark">
							<i class="icon-menu"></i>
						</button>
					</div>
					<div class="navbar-collapse collapse" id="navbar-demo-dark" style="border:0px !important;">
						<span>Filter Topics :</span>
						<ul class="nav navbar-nav">
							<li class="nav-item dropdown">
								<a id="nav-name" href="#tab-dark-1" class="navbar-nav-link active show" data-toggle="tab">
									Chapter
								</a>
							</li>

							<li class="nav-item dropdown">
<?php	if( (isset($chkLastArr)) && ($chkLastArr == 'subjectName') ) { ?>
								<a id="nav-admNo" href="#tab-dark-2" class="navbar-nav-link active show" data-toggle="tab">
<?php	} else {?>
								<a id="nav-admNo" href="#tab-dark-2" class="navbar-nav-link" data-toggle="tab">
<?php	}?>
									Topic Name
								</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="card card-body border-top-0 rounded-0 rounded-bottom tab-content mb-0">
					<div class="tab-pane fade active show" id="tab-dark-1">
						<div class="row">
							<div class="col-md-3">
								<label for="subject">Subject:<span class="text-danger">*</span></label>
								<select class="form-control form-control-select2" name="lsSubjectsForFilter" id="lsSubjectsForFilter" required>
									<option value="">--Choose One--</option>
<?php   if(!(empty($subjRes))) {
            foreach($subjRes as $s) {
                echo '<option value="'.$s->id.'$$##$$'.$s->subject_name.'">'.$s->subject_name.'</option>';
            }
        }?>
								</select>
							</div>
							<div class="col-md-3">
								<label for="module">Module:<span class="text-danger">*</span></label>
								<select class="form-control form-control-select2" name="lsModulesForFilter" id="lsModulesForFilter" required>
									<option value="">--Choose One--</option>
								</select>
							</div>
							<div class="col-md-3">
								<label for="chapter">Chapter:<span class="text-danger">*</span></label>
								<select class="form-control form-control-select2" name="lsChaptersForFilter" id="lsChaptersForFilter" required>
									<option value="">--Choose One--</option>
								</select>
							</div>
							<div class="col-md-3">
								<label for="none">&nbsp;</label><Br/>
								<button type="button" name="btnSubject" id="btnSubject" data-initial-text="<i class='icon-spinner4 mr-2'></i> Search" data-loading-text="<i class='icon-spinner4 spinner mr-2'></i> Loading..." class="btn btn-light btn-loading">
									<i class="icon-spinner4 mr-2"></i> Search
								</button>
								<button type="button" name="btnReset" id="btnReset" data-initial-text="<i class='icon-spinner4 mr-2'></i> Reset" data-loading-text="<i class='icon-spinner4 spinner mr-2'></i> Loading..." class="btn btn-light btn-loading">
									<i class="icon-spinner4 mr-2"></i> Reset
								</button>
							</div>
						</div>
					</div>
<?php	if( (isset($chkLastArr)) && ($chkLastArr == 'topicName') ) { ?>
					<div class="tab-pane fade active show" id="tab-dark-2">
<?php	} else {?>
					<div class="tab-pane fade" id="tab-dark-2">
<?php	}?>
						<div class="row">
							<div class="col-md-4">
								<input type="text" id="topicName" name="topicName" required class="form-control" placeholder="Topic Name">
							</div>
							<div class="col-md-4">
								<button type="button" name="btnName" id="btnName" data-initial-text="<i class='icon-spinner4 mr-2'></i> Search" data-loading-text="<i class='icon-spinner4 spinner mr-2'></i> Loading..." class="btn btn-light btn-loading">
									<i class="icon-spinner4 mr-2"></i> Search
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-body" style="padding:8px !important">
					<ul id="activeFilters" class="breadcrumb">
						<li><span class="text-semibold text-uppercase" style="valign:middle;">Active Filter&nbsp;:&nbsp;</span>
					</ul>
				</div>
			</div>