    <!-- START HTML Code Section -->
    <form name="frmFilters" id="frmFilters" method="post" action="">
      <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="http://localhost:8280/akshara/SchoolERP/akshara/">
      <input type="hidden" name="hidJSONPath" id="hidJSONPath" value="http://localhost:8280/akshara/SchoolERP/akshara/">
      <input type="hidden" name="hidSearchFlag" id="hidSearchFlag" value="">
      <input type="hidden" name="hidDelFormAction" id="hidDelFormAction" value="">
      <input type="hidden" name="hidDelIds" id="hidDelIds" value="">
      <input type="hidden" name="hidSearchConcat" id="hidSearchConcat" value="">
      <input type="hidden" name="hidShowAll" id="hidShowAll" value="0">
      <input type="hidden" name="hidAppTitle" id="hidAppTitle" value="">
      <input type="hidden" name="hidSaveFilterRecID" id="hidSaveFilterRecID" value="">
      <input type="hidden" name="hidStatusSearch" id="hidStatusSearch" value="">
      <!-- Student Listing -->
      <div class="card">
          <div class="navbar navbar-expand-xl navbar-dark bg-indigo-400 navbar-component rounded-top mb-0"> 
            <div class="d-xl-none">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-demo-dark">
                <i class="icon-menu"></i>
              </button>
            </div>
            <div class="navbar-collapse collapse" id="navbar-demo-dark" style="border:0px !important;">
              <ul class="nav navbar-nav">
                <li class="nav-item dropdown">
                  <a id="nav-purpose" href="#tab-dark-1" class="navbar-nav-link active show" data-toggle="tab">
                    Country
                  </a>
                </li>
                <li class="nav-item dropdown">
                  <a id="nav-complaintType" href="#tab-dark-2" class="navbar-nav-link" data-toggle="tab">
                    State
                  </a>
                </li>
                <li class="nav-item dropdown">
                  <a id="nav-source" href="#tab-dark-3" class="navbar-nav-link" data-toggle="tab">
                    City
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="card card-body border-top-0 rounded-0 rounded-bottom tab-content mb-0">
            <div class="tab-pane active show" id="tab-dark-1"  >
              <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                      <!--  PAGE LOADER ANIMATION
                      Content loading spinner 3
                      PAGE LOADER ANIMATION -->
                      <div class="btn btn-light" id="purposes-spinner-light-8" style="display: none;"></div>
                      <div id="visitorPurposesTable_wrapper" class="dataTables_wrapper no-footer"><div class="datatable-header"><div class="dt-buttons" style="float: left;"> </div></div><div class="datatable-scroll-wrap"><div class="dataTables_scroll"><div class="dataTables_scrollHead" style="overflow: hidden; position: relative; border: 0px none; width: 100%;"><div class="dataTables_scrollHeadInner" style="box-sizing: content-box; width: 953px; padding-right: 0px;"><table id="usersTable" class="table datatable-button-html5-basic no-footer dataTable" style="margin-left: 0px; width: 953px;" role="grid" aria-describedby="visitorPurposesTable_info" width="100%" cellspacing="0"><thead>
                          <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="SNO: activate to sort column descending" style="width: 165.167px;" aria-sort="ascending">SNo</th><th class="sorting" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="Visitor Purpose: activate to sort column ascending" style="width: 282.867px;">Country Code</th><th class="sorting" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="Description: activate to sort column ascending" style="width: 294.25px;">Country Name</th>
                            <th class="sorting_asc" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="SNO: activate to sort column descending" style="width: 165.167px;" aria-sort="ascending">Status</th><th class="no-sort sorting_disabled" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="Action" style="width: 210.717px;">Action</th></tr>
                        </thead></table></div></div><div class="dataTables_scrollBody" style="position: relative; width: 100%;">
                        </div></div></div> </div>
                  </div>
                </div>
              </div>
            </div>
                        
          </div>
          <div class="tab-pane fade" id="tab-dark-2">
              <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                      <!--  PAGE LOADER ANIMATION
                      Content loading spinner 3
                      PAGE LOADER ANIMATION -->
                      <div class="btn btn-light" id="purposes-spinner-light-8" style="display: none;"></div>
                      <div id="visitorPurposesTable_wrapper" class="dataTables_wrapper no-footer"><div class="datatable-header"><div class="dt-buttons" style="float: left;"> </div></div><div class="datatable-scroll-wrap"><div class="dataTables_scroll"><div class="dataTables_scrollHead" style="overflow: hidden; position: relative; border: 0px none; width: 100%;"><div class="dataTables_scrollHeadInner" style="box-sizing: content-box; width: 953px; padding-right: 0px;"><table id="statesTable" class="table datatable-button-html5-basic no-footer dataTable" style="margin-left: 0px; width: 953px;" role="grid" aria-describedby="visitorPurposesTable_info" width="100%" cellspacing="0"><thead>
                          <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="SNO: activate to sort column descending" style="width: 165.167px;" aria-sort="ascending">SNo</th><th class="sorting" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="Visitor Purpose: activate to sort column ascending" style="width: 282.867px;">State Code</th><th class="sorting" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="Description: activate to sort column ascending" style="width: 294.25px;">State Name</th>
                            <th class="no-sort sorting_disabled" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="Action" style="width: 210.717px;">Status</th><th class="no-sort sorting_disabled" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="Action" style="width: 210.717px;">Action</th></tr>
                        </thead></table></div></div><div class="dataTables_scrollBody" style="position: relative; width: 100%;">
                        </div></div></div> </div>
                  </div>
                </div>
              </div>
            </div>
                        
          </div>

          <div class="tab-pane fade" id="tab-dark-3">
              <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                      <!--  PAGE LOADER ANIMATION
                      Content loading spinner 3
                      PAGE LOADER ANIMATION -->
                      <div class="btn btn-light" id="purposes-spinner-light-8" style="display: none;"></div>
                      <div id="visitorPurposesTable_wrapper" class="dataTables_wrapper no-footer"><div class="datatable-header"><div class="dt-buttons" style="float: left;"> </div></div><div class="datatable-scroll-wrap"><div class="dataTables_scroll"><div class="dataTables_scrollHead" style="overflow: hidden; position: relative; border: 0px none; width: 100%;"><div class="dataTables_scrollHeadInner" style="box-sizing: content-box; width: 953px; padding-right: 0px;"><table id="citiesTable" class="table datatable-button-html5-basic no-footer dataTable" style="margin-left: 0px; width: 953px;" role="grid" aria-describedby="visitorPurposesTable_info" width="100%" cellspacing="0"><thead>
                          <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="SNO: activate to sort column descending" style="width: 165.167px;" aria-sort="ascending">SNo</th><th class="sorting" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="Visitor Purpose: activate to sort column ascending" style="width: 282.867px;">City Code</th><th class="sorting" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="Description: activate to sort column ascending" style="width: 294.25px;">City Name</th>
                            <th class="no-sort sorting_disabled" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="Action" style="width: 210.717px;">Status</th><th class="no-sort sorting_disabled" tabindex="0" aria-controls="visitorPurposesTable" rowspan="1" colspan="1" aria-label="Action" style="width: 210.717px;">Action</th></tr>
                        </thead></table></div></div><div class="dataTables_scrollBody" style="position: relative; width: 100%;">
                        </div></div></div> </div>
                  </div>
                </div>
              </div>
            </div>   
          </div>
        </div>
      </div>
      </form>
    <!-- Scripts Goes here -->
    <!-- START HTML Code Section -->
<input type="hidden" name="hidProjectURL" id="hidProjectURL" value="<?php echo base_url();?>"/>
<!-- THEME RELATED JQUERY FILES -->
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/spin.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/ladda.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/bootbox.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/ui/prism.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/extra_sweetalert.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/jqueryui_components.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/loaders/progressbar.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/bootstrapvalidator.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/form_validation.js"></script>
<!-- THEME RELATED JQUERY FILES -->
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Settings/Regions/regionsListing.js"></script>