<div class="row" style="height:18px !important">
</div>

<div class="card">
    <div class="card-header">Add/Edit Settings Type</div><br>
<div class="card-body">
    <form name="formCom" id="formCom" method="POST" action="">
        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="<?php echo base_url();?>"/>
        <input type="hidden" name="hidRecID" id="hidRecID" value="0"/>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    <span class="text-red">*</span>
                        <label for="Name">Company Name:</label>
                        <input required autofocus="" type="text" name="cname" id="cname" placeHolder="Enter Company Name" value="" class="form-control" >
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                    <span class="text-red">*</span>
                        <label for="User Name">Company Logo:</label>
                        <input required class="form-control" type="text" name="clogo" id="clogo" placeHolder="Place Logo Here." value="" >
                    </div>
                </div>
                </div><!-- class="row"--->
                <div>
                   
                   <button type="button" class="btn btn-info" name="btnSave" id="btnSave">SAVE</button>
                   <button type="button" class="btn btn-info" name="btnReset" id="btnReset">RESET</button>
                </div>
            </form>
        </div>
    </div>


<div class="card">
    <div class="card-body">
        <table id="clientsTable" class="table datatable-button-html5-basic">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Company Name</th>
                    <th>Company Logo</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<!-- THEME RELATED JQUERY FILES -->


<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/spin.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/ladda.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/ui/prism.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/jqueryui_components.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/loaders/progressbar.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/bootstrapvalidator.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/form_validation.js"></script> 
    <script src="<?php echo base_url();?>theme/assets/js/main/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?php echo base_url();?>theme/assets/js/demo_pages/extra_sweetalert.js"></script>

<!-- THEME RELATED JQUERY FILES -->

<!--Your Page Wise Jquery Script here-->
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Institute/institute.js"></script>
<!--Your Page Wise Jquery Script here-->
