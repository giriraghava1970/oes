    <div class="row" style="height:15px !important"></div>
    <div class="navbar-collapse collapse" id="navbar-demo-dark" style="border:0px !important;display:block;">
<?php   if(!(empty($settingTypes))) { ?>
            <ul class="nav navbar-nav">
<?php       foreach($settingTypes as $sT) {?>
                <li class="nav-item dropdown">
                    <a id="nav-setting-<?php echo $sT->id;?>" href="<?php echo base_url();?>" class="navbar-nav-link active show" data-toggle="tab">
                        <?php echo $sT->setting_type_name;?>
                    </a>
                </li>
<?php       }
            echo '</ul>';
        }?>
        </div>

<!-- <div class="card">
    <div class="card-header">Add/Edit Settings</div><br>
    
<div class="card-body">
    <form name="formCom" id="formCom" method="POST" action="">
        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="<?php echo base_url();?>"/>
        <input type="hidden" name="hidRecID" id="hidRecID" value="0"/>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <span class="text-red">*</span>
                        <label for="Setting Name">Setting Name:</label>
                        <input required class="form-control" type="text" name="settingName" id="settingName" placeHolder="Enter Setting Name" value="">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                    <span class="text-red">*</span>
                        <label for="Setting Value">Setting Value:</label>
                        <input required class="form-control" type="text" name="settingValue" id="settingValue" placeHolder="Enter Setting Value" value="">
                    </div>
                </div> -->

            <!-- </div>class="row"- -->
            <!-- <div class="modal-footer"> -->
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                    <!-- <button type="button" class="btn btn-info" name="btnReset" id="btnReset">RESET</button>
                   <button type="button" class="btn btn-info" name="btnSave" id="btnSave">INSERT</button>
                </div> -->
                <!-- <div>
                   
                   <button type="button" class="btn btn-info" name="btnSave" id="btnSave">SAVE</button>
                   <button type="button" class="btn btn-info" name="btnReset" id="btnReset">RESET</button>
                </div> -->
            <!-- </form>
        </div>
    </div> -->



<div class="card">
    <div class="card-body">
        <table id="clientsTable" class="table datatable-button-html5-basic">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Setting Type ID</th>
                    <th>Setting Sub Type ID</th>
                    <th>Setting Name</th>
                    <th>Setting Value</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<!-- THEME RELATED JQUERY FILES -->


<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/spin.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/ladda.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/ui/prism.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/jqueryui_components.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/loaders/progressbar.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/bootstrapvalidator.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/form_validation.js"></script> 
    <script src="<?php echo base_url();?>theme/assets/js/main/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?php echo base_url();?>theme/assets/js/demo_pages/extra_sweetalert.js"></script>

<!-- THEME RELATED JQUERY FILES -->

<!--Your Page Wise Jquery Script here-->
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Settings/setting_type.js"></script>
<!--Your Page Wise Jquery Script here-->
