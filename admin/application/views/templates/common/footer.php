				</div>
				<!-- /content area -->

				<!-- Footer -->
				<div class="navbar navbar-expand-lg navbar-light">
					<div class="text-center d-lg-none w-100">
						<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
							<i class="icon-unfold mr-2"></i>
							Footer
						</button>
					</div>

					<div class="navbar-collapse collapse" id="navbar-footer">
						<span class="navbar-text">
							&copy; <?php echo date('Y');?>. <a href="#"> Online Examination System [OES]</a> Developed by <a href="http://www.icetsolutions.com/" target="_blank"> ICET Solutions Private Limited</a>
						</span>
					</div>
				</div>
				<!-- /footer -->
			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->
		<script src="<?php echo base_url();?>theme/assets/js/core/app.js"></script>
    	<script src="<?php echo base_url();?>theme/assets/js/core/custom.js"></script>
    </body>
</html>