<!DOCTYPE html>
<html lang="en">
    <head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>:: Online Examination System [OES] ::</title>
    	<!-- Global stylesheets -->
		<!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"> -->
		<link href="<?php echo base_url();?>theme/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url();?>theme/assets/css/icons/material/icons.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url();?>theme/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url();?>theme/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url();?>theme/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url();?>theme/assets/css/layout.min.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url();?>theme/assets/css/components.min.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url();?>theme/assets/css/colors.min.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url();?>theme/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		
		<!-- /global stylesheets -->

		<!--  Custom CSS  -->
		<link href="<?php echo base_url();?>theme/assets/css/awesome-bootstrap-checkbox.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url();?>theme/assets/css/dataTables.checkboxes.css" rel="stylesheet" type="text/css">
		<!-- <link href="<?php echo base_url();?>theme/assets/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"> -->
		<!--  Custom CSS -->

		<!-- Core JS files -->
		<!-- <script src="//cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script> -->
		 <!-- <script src="<?php echo base_url();?>theme/assets/js/demo_pages/ckeditor.js"></script>  -->
		<script src="<?php echo base_url();?>theme/assets/js/core/libraries/jquery.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/main/bootstrap.bundle.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/loaders/blockui.min.js"></script>
		<!-- /core JS files -->

		<!-- Theme JS files -->
		<script src="<?php echo base_url();?>theme/assets/js/plugins/visualization/d3/d3.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/ui/moment/moment.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/pickers/daterangepicker.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>

		<script src="<?php echo base_url();?>theme/assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/extensions/jquery_ui/widgets.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/extensions/jquery_ui/effects.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/extensions/mousewheel.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/extensions/jquery_ui/globalize/globalize.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/extensions/jquery_ui/globalize/cultures/globalize.culture.de-DE.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/extensions/jquery_ui/globalize/cultures/globalize.culture.ja-JP.js"></script>

		<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/datatables.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/forms/selects/select2.min.js"></script>

		<script src="<?php echo base_url();?>theme/assets/js/plugins/forms/wizards/steps.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/forms/selects/select2.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/forms/styling/uniform.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/forms/inputs/inputmask.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/extensions/cookie.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/forms/validation/validate.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/demo_pages/form_inputs.js"></script>

		<script src="<?php echo base_url();?>theme/assets/js/plugins/forms/inputs/touchspin.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/internationalization/i18next.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/internationalization/jquery-i18next.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/internationalization/i18nextXHRBackend.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/internationalization/i18nextBrowserLanguageDetector.min.js"></script>
		<!-- <script src="<?php echo base_url();?>theme/assets/js/demo_pages/internationalization_callbacks.js"></script> -->
		<script src="<?php echo base_url();?>theme/assets/js/plugins/ui/prism.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/ui/moment/moment.min.js"></script>
		<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/styling/switch.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/pickers/daterangepicker.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/pickers/anytime.min.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/pickers/pickadate/picker.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/pickers/pickadate/legacy.js"></script>
		<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/noty.min.js"></script>
		<!-- /theme JS files -->
    </head>
    
    <body>
    	<input type="hidden" name="hidProjectURL" id="hidProjectURL" value="<?php echo base_url();?>"/>
    	<!-- Main navbar -->
    	<div class="navbar navbar-expand-md navbar-dark">
    		<div class="navbar-brand">
    			<a href="<?php echo base_url();?>Dashboard" class="d-inline-block">
    				<img src="<?php echo base_url();?>theme/assets/images/logo_light.png" alt="">
    			</a>
    		</div>
    		<div class="d-md-none">
    			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
    				<i class="icon-tree5"></i>
    			</button>
    			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
    				<i class="icon-paragraph-justify3"></i>
    			</button>
    		</div>
    		<div class="collapse navbar-collapse" id="navbar-mobile">
    			<ul class="navbar-nav">
    				<li class="nav-item">
    					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
    						<i class="icon-paragraph-justify3"></i>
    					</a>
    				</li>
    				<li class="nav-item dropdown"> <a href="<?php echo base_url();?>/admin/admin/dashboard/" class="nav-link" style="color:#fff; font-weight:bold;">Online Examination System</a></li>
    			</ul>
    			<span class="badge ml-md-3 mr-md-auto">&nbsp;</span>
    			<ul class="navbar-nav">
    				<li class="nav-item dropdown dropdown-user">
    					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
    						<img src="<?php echo base_url();?>theme/assets/images/placeholders/placeholder.jpg" class="rounded-circle mr-2" height="34" alt="">
    						<span style="text-transform: capitalize;">User</span>
    					</a>

    					<div class="dropdown-menu dropdown-menu-right">
    						<a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a>
    						<a href="<?php echo base_url();?>Logout" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
    					</div>
    				</li>
    			</ul>
    		</div>
    	</div>
    	<!-- /main navbar -->

    	<!-- Page content -->
    	<div class="page-content">