<form name="frmQuestionsUpload" id="frmQuestionsUpload" method="post" action="" enctype="multipart/form-data">
    <noscript>
    	<input type="hidden" name="redirect" value="<?php echo base_url();?>"/>
   	</noscript>
	<div class="card">
		<div class="card-body">
        	<div class="row">
        		<div class="col-md-3">
        			<label for="questionBank">Question Bank:<span class="text-danger">*</span></label>
        			<select class="form-control form-control-select2" name="lsQuestionBankTypes" id="lsQuestionBankTypes">
        				<option value="">--Choose One--</option>
<?php   if(!(empty($questionBankTypeRes))) {
            foreach($questionBankTypeRes as $qB) {
                echo '<option value="'.$qB->id.'">'.$qB->questionbank_type_name.'</option>';
            }
        }?>
        			</select>
        		</div>
        		<div class="col-md-3">
        			<label for="subject">Subject:<span class="text-danger">*</span></label>
        			<select class="form-control form-control-select2" name="lsSubjects" id="lsSubjects">
        				<option value="">--Choose One--</option>
<?php   if(!(empty($subjRes))) {
            foreach($subjRes as $s) {
                echo '<option value="'.$s->id.'">'.$s->subject_name.'</option>';
            }
        }?>
        			</select>
        		</div>
        		<div class="col-md-3">
        			<label for="chapter">Chapter:<span class="text-danger">*</span></label>
        			<select class="form-control form-control-select2" name="lsChapters" id="lsChapters">
        				<option value="">--Choose One--</option>
        			</select>
        		</div>
        		<div class="col-md-3">
        			<label for="topic">Topic:<span class="text-danger">*</span></label>
        			<select class="form-control form-control-select2" name="lsTopics" id="lsTopics">
        				<option value="">--Choose One--</option>
        			</select>
        		</div>
        	</div>
        	<div class="row" style="height:18px !important"></div>
        	<div class="row">
        		<div class="col-md-3">
        			<label for="topic">Sub Topic:</label>
        			<select class="form-control form-control-select2" name="lsSubTopics" id="lsSubTopics">
        				<option value="">--Choose One--</option>
        			</select>
        		</div>
        		<div class="col-md-6">
        			<label for="questionUpload">Question:<span class="text-danger">*</span></label>
        			<input type="file" class="form-input-styled" data-fouc name="questionUpload" id="questionUpload">
        			<div class="row">
        				<div class="col-md-2">&nbsp;</div>
        				<div class="col-md-6">
        	                <div id="image_size_error" style="color:#F00;"></div>                
        	     	 		<div>Maximum file size is 5MB. Allowed file types are .doc, .docx</div>
             	 		</div>
             	 		<div class="col-md-4">&nbsp;</div>
                    </div>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-md-12">
        			<label>&nbsp;</label><br/>
					<button type="button" name="btnUpload" id="btnUpload" data-initial-text="<i class='icon-spinner4 mr-2'></i> Upload" data-loading-text="<i class='icon-spinner4 spinner mr-2'></i> Loading..." class="btn btn-light btn-loading">
						<i class="icon-spinner4 mr-2"></i> Upload
					</button>
					&nbsp;
					<button type="button" name="btnUpload" id="btnUpload" data-initial-text="<i class='icon-spinner4 mr-2'></i> Cancel" data-loading-text="<i class='icon-spinner4 spinner mr-2'></i> Loading..." class="btn btn-light btn-loading">
						<i class="icon-spinner4 mr-2"></i> Cancel
					</button>
				</div>
        	</div>
        </div>
    </div>
</form>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/validation/validate.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/Questions/form_validation.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/inputmask.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/autosize.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/formatter.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/typeahead/handlebars.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/passy.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/maxlength.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/form_controls_extended.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/form_input_groups.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/extra_sweetalert.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/picker_date_rtl.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Questions/questionUploads.js"></script>