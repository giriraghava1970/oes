<!-- Saving state -->
<div class="row">
	<div class="col-md-10">&nbsp;</div>
	<div class="col-md-2">
		<button type="button" name="btnBack" id="btnBack" data-initial-text="<i class='icon-spinner4 mr-2'></i> Back To Listing" data-loading-text="<i class='icon-spinner4 spinner mr-2'></i> Loading..." class="btn btn-light btn-loading">
			<i class="icon-spinner4 mr-2"></i> Back To Listing
		</button>
	</div>
</div>
<div class="row" style="height:18px !important"></div>
<div class="card">
	<div class="card-header bg-white header-elements-inline">
		<h6 class="card-title">Create Question</h6>
	</div>

	<!--  debug : wizard-form steps-validation clearfix wizard-form steps-enable-all wizard clearfix  >> classs -->
	<!--  PAGE LOADER ANIMATION
	Content loading spinner 3
	PAGE LOADER ANIMATION -->
    <div class="btn btn-light" id="spinner-light-questFormWizard"></div>
	<form enctype="multipart/form-data" class="wizard-form steps-validation clearfix" name="questionForm" id="questionForm" method="post" action="<?php echo base_url();?>Questions/ajaxResultsForQuestions/" data-fouc>
	 	<input type="hidden" name="hidStudForm" id="hidStudForm" value="1"/>
	 	<input type="hidden" name="question_id" id="question_id" value=""/>
	 	<input type="hidden" name="hidReqType" id="hidReqType" value=""/>
	 	<h6>Question Entry Options</h6>
		<fieldset>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Question Bank:<span class="text-danger">*</span></label>
						<select tabindex="1" name="questions[questionBankType_id]" id="lsQuestionBankTypes" required data-placeholder="Select Question Bank Type" class="form-control form-input-styled" data-fouc>
							<option value="">--Choose One--</option>
<?php   if(!(empty($questionBankTypeRes))) {
            foreach($questionBankTypeRes as $qB) {
                echo '<option value="'.$qB->id.'">'.$qB->questionbank_type_name.'</option>';
            }
        }?>
        				</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Class:<span class="text-danger">*</span></label>
						<select tabindex="2" name="questions[classID]" id="lsClasses" required data-placeholder="Select Class" class="form-control form-input-styled" data-fouc>
							<option value="">--Choose One--</option>
<?php   if(!(empty($classRes))) {
            foreach($classRes as $cl) {
                echo '<option value="'.$cl->id.'">'.$cl->class_name.'</option>';
            }
        }?>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Subject:<span class="text-danger">*</span></label>
						<select tabindex="3" name="questions[subjectID]" id="lsSubjects" required data-placeholder="Select Subject" class="form-control form-input-styled" data-fouc>
							<option value="">--Choose One--</option>
<?php   if(!(empty($subjRes))) {
            foreach($subjRes as $s) {
                echo '<option value="'.$s->id.'">'.$s->subject_name.'</option>';
            }
        }?>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Chapter:<span class="text-danger">*</span></label>
						<select tabindex="5" name="questions[chapterID]" id="lsChapters" required data-placeholder="Select Chapter" class="form-control form-input-styled" data-fouc>
							<option value="">--Choose One--</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Topic:<span class="text-danger">*</span></label>
						<select tabindex="6" name="questions[topicID]" id="lsTopics" required data-placeholder="Select Topic" class="form-control form-input-styled" data-fouc>
							<option value="">--Choose One--</option>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Question Category:<span class="text-danger">*</span></label>
						<select tabindex="6" name="questions[questionCategoryID]" id="lsQuestionCategories" required data-placeholder="Select Question Category" class="form-control form-input-styled" data-fouc>
							<option value="">--Choose One--</option>
<?php   if(!(empty($questionCatRes))) {
            foreach($questionCatRes as $qC) {
                echo '<option value="'.$qC->id.'">'.$qC->category_name.'</option>';
            }
        }?>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Question Type:<span class="text-danger">*</span></label>
						<select tabindex="6" name="questions[questionTypeID]" id="lsQuestionTypes" required data-placeholder="Select Question Type" class="form-control form-input-styled" data-fouc>
							<option value="">--Choose One--</option>
<?php   if(!(empty($qTypeRes))) {
            foreach($qTypeRes as $qT) {
                echo '<option value="'.$qT->id.'">'.$qT->question_type_name.'</option>';
            }
        }?>
						</select>
					</div>
				</div>
			</div>
		</fieldset>

		<!-- STEP 2  [ PARENT DETAILS] -->
		<h6>Question Details</h6>
		<fieldset>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Exam Type:<span class="text-danger">*</span></label>
						<select tabindex="6" name="questions[examTypeID]" id="lsExamTypes" required data-placeholder="Select Question Type" class="form-control form-input-styled" data-fouc>
							<option value="">--Choose One--</option>
<?php   if(!(empty($examTypesRes))) {
            foreach($examTypesRes as $eT) {
                echo '<option value="'.$eT->id.'">'.$eT->exam_type_name.'</option>';
            }
        }?>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Level:<span class="text-danger">*</span></label>
						<select tabindex="6" name="questions[levelID]" id="lslevels" required data-placeholder="Select Level" class="form-control form-input-styled" data-fouc>
							<option value="">--Choose One--</option>
<?php   if(!(empty($levelRes))) {
            foreach($levelRes as $le) {
                echo '<option value="'.$le->id.'">'.$le->level_name.'</option>';
            }
        }?>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<textarea class="form-control editor-full" name="editor-full" id="editor-full" rows="4" cols="4" style="height:100px !important;"></textarea>
					<script>
                    // Replace the <textarea id="editor1"> with a CKEditor
                    // instance, using default configuration.
                    CKEDITOR.replace( 'editor-full' );
                </script>
				</div>
			</div>
			<div class="row" style="height:15px !important;"></div>
			<div class="row" id="questionOptions" style="display:none;"></div>
		</fieldset>
	 </form>
</div>
<!-- /saving state -->
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/validation/validate.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/Questions/question_formWizard.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/Questions/form_validation.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/inputmask.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/autosize.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/formatter.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/typeahead/handlebars.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/passy.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/maxlength.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/form_controls_extended.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/form_input_groups.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/extra_sweetalert.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/Questions/questionRadios.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/Questions/questionUI.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/picker_date_rtl.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/editors/ckeditor/ckeditor.js"></script>

<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/form_checkboxes_radios.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/editors/ckeditor/plugins/eqneditor/dialogs/eqneditor.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/editors/ckeditor/plugins/eqneditor/plugin.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/editors/ckeditor/plugins/mathjax/dialogs/mathjax.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/Questions/editor_ckeditor.js"></script>