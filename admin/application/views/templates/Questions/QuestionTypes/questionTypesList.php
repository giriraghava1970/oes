<div class="card">
    <div class="card-body">
        <table id="usersTable" class="table datatable-button-html5-basic">
            <thead>
                <tr>
                    <th>SNo</th>
                    <th>Question Type</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<input type="hidden" name="hidProjectURL" id="hidProjectURL" value="<?php echo base_url();?>"/>
<!-- THEME RELATED JQUERY FILES -->
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/spin.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/ladda.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/bootbox.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/ui/prism.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/extra_sweetalert.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/jqueryui_components.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/loaders/progressbar.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/bootstrapvalidator.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/form_validation.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Questions/QuestionTypes/questionTypesListing.js"></script>