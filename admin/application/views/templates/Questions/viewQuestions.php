<div class="card">
	<div class="card-body">
		<!-- Search field -->
		<div class="card">
			<div class="card-body">
				<h5 class="mb-3">Questions</h5>
				<form action="#">
					<div class="input-group mb-3">
						<div class="form-group-feedback form-group-feedback-left">
							<input type="text" class="form-control form-control-lg alpha-grey" value="Limitless interface kit" placeholder="Search">
							<div class="form-control-feedback form-control-feedback-lg">
								<i class="icon-search4 text-muted"></i>
							</div>
						</div>
						<div class="input-group-append">
							<button type="submit" class="btn btn-primary btn-lg">Search</button>
						</div>
					</div>

					<div class="d-md-flex align-items-md-center flex-md-wrap text-center text-md-left">
						<ul class="list-inline list-inline-condensed mb-0">
							<li class="list-inline-item dropdown">
								<a href="#" class="btn btn-link text-default dropdown-toggle" data-toggle="dropdown">
									<i class="icon-stack2 mr-2"></i>
									Subject
								</a>
								<div class="dropdown-menu">
<?php   if(!(empty($subjRes))) {
            foreach($subjRes as $s) {
                //echo '<option value="'.$s->id.'$$##$$'.$s->subject_name.'">'.$s->subject_name.'</option>';
                echo '<a id="subject-'.$s->id.'" href="#" class="dropdown-item populateListing"><i class="icon-question7"></i> '. $s->subject_name.'</a>';
            }
        }?>
								</div>
							</li>
							<li class="list-inline-item dropdown" id="showChatpersListing" style="display:none;">
								<a href="#" class="btn btn-link text-default dropdown-toggle" data-toggle="dropdown">
									<i class="icon-stack2 mr-2"></i>
									Chapters
								</a>
								<div class="dropdown-menu">
								</div>
							</li>
							<li class="list-inline-item dropdown" id="showTopicsListing" style="display:none;">
								<a href="#" class="btn btn-link text-default dropdown-toggle" data-toggle="dropdown">
									<i class="icon-stack2 mr-2"></i>
									Topics
								</a>
								<div class="dropdown-menu">
								</div>
							</li>
							<li class="list-inline-item dropdown" id="showSubTopicsListing" style="display:none;">
								<a href="#" class="btn btn-link text-default dropdown-toggle" data-toggle="dropdown">
									<i class="icon-stack2 mr-2"></i>
									Sub Topics
								</a>
								<div class="dropdown-menu">
								</div>
							</li>
							<li class="list-inline-item"><a href="#" class="btn btn-link text-default"><i class="icon-reload-alt mr-2"></i> Refine your search</a></li>
						</ul>
					</div>
				</form>
			</div>
		</div>
		<!-- /search field -->

		<!-- Tabs -->
		<!-- /tabs -->
		<!-- List view -->
		<div class="pt-2 mb-3">
			<h6 class="mb-0 font-weight-semibold">
				List view
			</h6>
			<span class="text-muted d-block">Single card with rows</span>
		</div>

		<div class="card">
			<div class="card-body">
				<ul class="media-list">
					<li class="media font-weight-semibold py-1">Team leaders</li>

					<li class="media">
						<div class="mr-3">
							<a href="#">
								<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" width="40" height="40" alt="">
							</a>
						</div>

						<div class="media-body">
							<div class="media-title font-weight-semibold">James Alexander</div>
							<span class="text-muted">Development</span>
						</div>

						<div class="align-self-center ml-3">
							<div class="list-icons list-icons-extended">
		                    	<a href="#" class="list-icons-item" data-popup="tooltip" title="Call" data-toggle="modal" data-trigger="hover" data-target="#call"><i class="icon-phone2"></i></a>
		                    	<a href="#" class="list-icons-item" data-popup="tooltip" title="Chat" data-toggle="modal" data-trigger="hover" data-target="#chat"><i class="icon-comment"></i></a>
		                    	<a href="#" class="list-icons-item" data-popup="tooltip" title="Video" data-toggle="modal" data-trigger="hover" data-target="#video"><i class="icon-video-camera"></i></a>
	                    	</div>
						</div>
					</li>					
				</ul>
			</div>
		</div>
		<!-- /list view -->

		<!-- Pagination -->
		<div class="d-flex justify-content-center pt-3 mb-3">
			<ul class="pagination">
				<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-right"></i></a></li>
				<li class="page-item active"><a href="#" class="page-link">1</a></li>
				<li class="page-item"><a href="#" class="page-link">2</a></li>
				<li class="page-item"><a href="#" class="page-link">3</a></li>
				<li class="page-item"><a href="#" class="page-link">4</a></li>
				<li class="page-item"><a href="#" class="page-link">5</a></li>
				<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-left"></i></a></li>
			</ul>
		</div>
		<!-- /pagination -->
	</div>
</div>
<!-- THEME RELATED JQUERY FILES -->
<script src="<?php echo base_url();?>/theme/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/buttons/spin.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/buttons/ladda.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/components_buttons.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/notifications/bootbox.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/ui/prism.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/extensions/jquery_ui/widgets.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/extensions/jquery_ui/effects.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/jqueryui_components.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/loaders/progressbar.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/bootstrapvalidator.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/form_validation.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/styling/switchery.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/extra_sweetalert.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/picker_date.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Questions/questionsListing.js"></script>