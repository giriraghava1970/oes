<div class="row" style="height:28px !important"></div>
<form action="" method="post" name="frmStudent" id="frmStudent">
<div class="row" style="height:28px !important"></div>
    <div class="card">
        <div class="card-body">
            <table id="studentsTable" class="table datatable-button-html5-basic">
                <thead>
                    <tr>
                        <th class="select-checkbox noExport" nowrap="nowrap">
							<input type="checkbox" name="select-all" id="select-all" class="form-input-styled">
				         </th>
                        <th>SNo</th>
                        <th>Student Name</th>
                        <th>Course </th>
                        <th>Batch</th>
                        <th>Institute</th>
                        <th>Register Date</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="<?php echo base_url();?>"/>
</form>
<!-- THEME RELATED JQUERY FILES -->
<script src="<?php echo base_url();?>/theme/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/buttons/spin.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/buttons/ladda.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/components_buttons.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/dataTables.checkboxes.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/notifications/bootbox.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/ui/prism.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/extensions/jquery_ui/widgets.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/extensions/jquery_ui/effects.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/jqueryui_components.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/loaders/progressbar.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/bootstrapvalidator.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/form_validation.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/styling/switchery.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/extra_sweetalert.js"></script>
<script src="<?php echo base_url();?>/theme/assets/js/demo_pages/picker_date.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Students/studentsListing.js"></script>
