<div class="card">
    <div class="card-body">
    <table id="usersTable" class="table datatable-button-html5-basic">
            <thead>
                <tr>
                    <th>SNo</th>
                    <th>Course Name</th>
                    <th>Batch Name</th>
                    <th>Start Date </th>
                    <th>End Date</th>
                    <th>Action</th>
                </tr> 
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<input type="hidden" name="hidProjectURL" id="hidProjectURL" value="<?php echo base_url();?>"/>
<!-- THEME RELATED JQUERY FILES -->
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/spin.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/ladda.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/bootbox.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/ui/prism.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/extra_sweetalert.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/jqueryui_components.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/plugins/loaders/progressbar.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/bootstrapvalidator.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/form_validation.js"></script>
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->
<!-- THEME RELATED JQUERY FILES -->
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Students/Batches/batchesListing.js"></script>