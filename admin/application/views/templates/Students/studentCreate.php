<form enctype="multipart/form-data" class="wizard-form steps-validation clearfix" name="studentForm" id="studentForm" method="post" action="/minerva/index.php?r=students/students/ajaxResults" data-fouc>
	 	<input type="hidden" name="hidStudForm" id="hidStudForm" value="1"/>
	 	<input type="hidden" name="hidPageReferred" id="hidPageReferred" value="//"/>
	 	<input type="hidden" name="student_id" id="student_id" value=""/>
	 	<input type="hidden" name="guardian_id" id="guardian_id" value=""/>
	 	<input type="hidden" name="guardian_email" id="guardian_email" value=""/>
		<h6>Student Details</h6>
		<fieldset>
			<div class="row">
				<div class="col-md-12">
					<h4 class="card-title">Personal Details</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>First Name:<span class="text-danger">*</span></label>
						<input tabindex="1" type="text" name="Students[first_name]" id="fName" class="form-control" required placeholder="First Name" data-i18n="form.firstName" data-fouc>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Middle Name:</label>
						<input tabindex="2" type="text" name="Students[middle_name]" id="mName" class="form-control" placeholder="Middle Name" data-i18n="form.MiddleName" data-fouc>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Last Name:<span class="text-danger">*</span></label>
						<input tabindex="3" type="text" name="Students[last_name]" id="lName" class="form-control" required placeholder="Last Name" data-i18n="form.LastName" data-fouc>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Date Of Birth:<span class="text-danger">*</span></label>	
						<div class="input-group">
							<span class="input-group-prepend">
								<span class="input-group-text"><i class="icon-calendar22"></i></span>
							</span>
							<input tabindex="6" type="text" id="txtDOB" name="Students[date_of_birth]" required placeholder="Date Of Birth" class="form-control daterange-single" value="09/02/2019">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Blood Group:</label>
						<select tabindex="8" class="form-control form-input-styled" data-fouc name="Students[blood_group]" id="lsBloodGroups">
                            <option value="">--Choose One--</option>
                        </select>
					</div>
				</div>
			</div>
			<div class="row">
                <div class="col-md-4">
        				<div class="form-group">
                        <label for="Students_aadhar_card">Aadhar Card</label>
                        <input type="text" tabindex="" name="Students[aadhar_card]" id="Aadhar Card" value="" class="form-control" placeholder="Aadhar Card">
		        </div>
            </div>
			<div class="row">
				<div class="col-md-12">
					<h4 class="card-title">Contact Details</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Address Line 1:</label>
						<input type="text" name="Students[address_line1]" id="txtAddress1" class="form-control" placeholder="Address 1">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Address Line 2:</label>
						<input type="text" name="Students[address_line2]" id="txtAddress2" class="form-control" placeholder="Address 2">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>City:</label>
						<input type="text" name="Students[city]" id="txtCity" class="form-control" placeholder="City">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>PinCode:</label>
						<input type="text" name="Students[pin_code]" id="txtPinCode" class="form-control" placeholder="PinCode">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>State:</label>
						<input type="text" name="Students[state]" id="txtState" class="form-control" placeholder="State">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Country:</label>
						<select class="form-control form-input-styled" data-fouc name="Students[country_id]" id="lsCountries">
                            <option value="">--Choose One--</option>
                        </select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Mobile:<span class="text-danger">*</span></label>
						<input type="text" required data-mask="9999999999" name="Students[phone1]" id="txtPhone" class="form-control" placeholder="9999999999">
					</div>
				</div>	
				<div class="col-md-4">
					<div class="form-group">
						<label>Phone 1:</label>
						<input type="text" data-mask="(999) 99999999" name="Students[phone2]" id="txtPhone1" class="form-control" placeholder="Phone 1">
					</div>
				</div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Email:</label>
                        <input type="email" name="Students[email]" id="txtEmail" class="form-control" placeholder="you@email.com">
                    </div>
                </div>
			</div>
			<div class="row">
			</div>
			<h4 class="card-title">Upload Photo</h4>
			<div class="form-group row">
				<label class="col-form-label col-lg-2">Upload Photo:<span class="text-danger">*</span></label>
				<div id="showUploadFile" class="col-lg-10">
					<input id="photoUpload" type="file" required name="Students[photo_data]" class="form-input-styled" data-fouc>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">&nbsp;</div>
				<div class="col-md-6">
	                <div id="image_size_error" style="color:#F00;"></div>                
	     	 		<div>Maximum file size is 1MB. Allowed file types are png, gif, jpeg, jpg</div>
     	 		</div>
     	 		<div class="col-md-4">&nbsp;</div>
            </div>
		</fieldset>

		<!-- STEP 2  [ PARENT DETAILS] -->
	
     </form>
    
     <script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/validation/validate.min.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/demo_pages/Students/student_formWizard.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/inputmask.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/demo_pages/form_validation.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/autosize.min.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/formatter.min.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/typeahead/handlebars.min.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/passy.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/inputs/maxlength.min.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/demo_pages/form_controls_extended.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/demo_pages/form_input_groups.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/demo_pages/extra_sweetalert.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/demo_pages/picker_date_rtl.js"></script>
    <script src="<?php echo base_url();?>/theme/assets/js/plugins/forms/wizards/steps.min.js"></script>
    
   

    


    <!-- Add Your JS files related to student Create -->
