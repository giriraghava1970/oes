 <div class="row" style="height:18px !important">
</div>
<div class="card">
    <div class="card-body">
        <table id="clientsTable" class="table datatable-button-html5-basic">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Institute Name</th>
                    <th>Address Line 1</th>
                    <th>Address Line 2</th>
                    <th>Pincode</th>
                    <th>City ID</th>
                    <th>State ID</th>
                    <th>Country ID</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<!-- THEME RELATED JQUERY FILES -->


<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/spin.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/ladda.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/ui/prism.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/jqueryui_components.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/plugins/loaders/progressbar.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/bootstrapvalidator.min.js"></script>
	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/form_validation.js"></script> 
    <script src="<?php echo base_url();?>theme/assets/js/main/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?php echo base_url();?>theme/assets/js/demo_pages/extra_sweetalert.js"></script>

<!-- THEME RELATED JQUERY FILES -->

<!--Your Page Wise Jquery Script here-->
<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Institute/institute.js"></script>
<!--Your Page Wise Jquery Script here-->
