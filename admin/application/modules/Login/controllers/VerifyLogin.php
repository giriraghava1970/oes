<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
		ini_set('error_reporting', '1');

Class VerifyLogin extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model("Users_model");
        $this->load->model("Userroles_model");
        $this->load->library('Auth');
        $this->load->library('Enc_lib');
        $this->generalFn    =   new GeneralFunctions();
    }

    function index() {
        $login_post =   array('email' =>  $this->input->post('userName'), 'password'  =>  $this->input->post('userPassword') );
        $result =   $this->Users_model->checkLogin($login_post);
        //echo "\r\n <br/> results : \r\n <br/><pre>"; print_r($result);

        if ($result) {
            if($result->status == 'Active'){
                $session_data   =   array(
                    'id'    =>  $result->id,
                    'username'  =>  $result->name,
                    'email' =>  $result->email,
                    'roles' =>  $result->user_role_id,);
                $this->session->set_userdata('admin', $session_data);
                $role   =   $this->generalFn->getUserRole();
                $role_name  =   json_decode($role)->name;
                $this->generalFn->setUserLog($this->input->post('username'), $role_name);

                if (isset($_SESSION['redirect_to']))
                    redirect($_SESSION['redirect_to']);
                else
                    redirect('Dashboard');
            }else{
                $data['error_message']  =   'Your account is disabled please contact to administrator';
                redirect('Login', $data);
            }
        } else {
            $data['error_message']  =   'Invalid Username or Password';
            redirect('Login', $data);
        }
    }
}?>