<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_type extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Setting_type_model'); 
        // $this->load->model('Submodules_model');
    }

    function index() {
        $data['pageName'] =   'Settings_type';
        $this->load->template('Settings_type', 'templates/', 'settings/', 'setting_type', $data);
    
    }

    function ajaxResultsForValidateSettingsJSON(){
        $res    =   array();
        // echo "\r\n <br/> POST VALS : \r\n$o <Br/><pre>"; print_r($_POST);

        if( ($_POST['name']) && ($_POST['userName']) && ($_POST['password']) && ($_POST['conformPassword']) && ($_POST['email']) && ($_POST['contactNo']) && ($_POST['selectProfile']) && ($_POST['selectInstitute'])) {
            //Connect to model by passing the username and pwd values.. Validate whether true or false.
            $data   =   array(
                        'company_information'   =>  $_POST['companyName'], 
                        'comapany_'    =>  $_POST['userName'],
                        'password'  =>  $_POST['password'],
                        'conform_password'  =>  $_POST['conformPassword'], 
                        'contact_no' => $_POST['contactNo'],
                        'email' => $_POST['email'],
                        'select_profile' => $_POST['selectProfile'],
                        'select_institute' => $_POST['selectInstitute']
                    );
            $settingRes   =   $this->Settings_model->add($data);

            if($settingRes != FALSE) {
                if($settingRes > 0) {
                    // Set Session for the logged in user later.
                    $res['data']    =   'success';
                } else {
                    $res['data']   =    'failure';
                }
            } else {
                $res['data']   =    'Client Already exists.';
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForFiltersJSONGeneration(){
        $posts  =   $this->Settings_model->getSettings();
        $writeJSONData  =   $this->fetchDataForSettings($posts);
        $results    =   array();
        $results['data']['success'] =   1;
        $results['data']['writeJSONData']    =   $writeJSONData;
        //echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
    }

    function fetchDataForSettings($posts) {
        $newPosts   =   array();
        $cnt    =   0;

        foreach($posts as $post) {
            $newPosts[$cnt]['SNo']  =   ($cnt + 1);
            $newPosts[$cnt]['User Name']   =   $post->user_name;
            $newPosts[$cnt]['Name']   =   $post->name;
            $newPosts[$cnt]['Contact No']   =   $post->contact_no;
            $newPosts[$cnt]['Email']   =   $post->email;
            $newPosts[$cnt]['Profile']   =   $post->select_profile;
            // $newPosts[$cnt]['Settings Data']   =   $post->settings_data;
            $newPosts[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$post->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$post->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }
        return $newPosts;
    }
    
    function ajaxResultsForSettingsAction(){
        $res    =   array();
        // echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($_POST);
        // exit();

        if($_POST['reqType'] != '') {
            switch($_POST['reqType']) {

                case 'create' :
                // Do the update... 
                // call the same function which u do for adding new entry with slight modifications.
                $data   =   array(
                            'name'    =>  $_POST['name'], 
                            'user_name'   =>  $_POST['userName'], 
                            'password' => $_POST['password'],
                            'conform_password' => $_POST['conformPassword'],
                            'email' => $_POST['email'],
                            'contact_no' => $_POST['contactNo'],
                            'select_profile' => $_POST['selectProfile'],
                            'select_institute' => $_POST['selectInstitute']
                        );
                $settingRes  =   $this->Settings_model->add($data);
                if($settingRes == FALSE) {
                    $res['data']   =    'Already exists.';
                } 
                else{
                    if($settingRes > 0) {
                        $res['data']    =   'success';
                    } else {   
                            $res['data']    =   'failure';
                    }
                }

                break;

                case 'edit' :
                    // $hidRecID   =   0;
                    $newSettingsRes   =   array();
                    $formContent    =   '';
                    $name  =   $user_name    =   $password  =   $conform_password   =   $email  =   $contact_no    =   $select_profile  =   $select_institute    =   '';

                    if($_POST['hidRecID'] != 0) {
                        $hidRecID   =   $_POST['hidRecID'];
                        $settingRes  =   $this->Settings_model->getSettings($hidRecID);

                        if(!(empty($settingRes))) {
                            $res['data'] = 'success';

                            foreach($settingRes as $s) {
                                $newSettingsRes['name'] =   $s->name;
                                $newSettingsRes['user_name'] =   $s->user_name;
                                $newSettingsRes['password'] =   $s->password;
                                $newSettingsRes['conform_password'] =   $s->conform_password;
                                $newSettingsRes['email'] =   $s->email;
                                $newSettingsRes['contact_no'] =   $s->contact_no;
                                $newSettingsRes['select_profile'] =   $s->select_profile;
                                $newSettingsRes['select_institute'] =   $s->select_institute;
                                //assiign client record data here in an array
                            }
                        }
                        $res['settingsRecord']    =   $newSettingsRes;
                    }

                    break;

                case 'update' :
                    //echo "\r\n <br/> post vals for update : \r\n <br/><pre>"; print_r($_POST);
                    if($_POST['hidRecID'] != '') {
                        // Do the update... 
                        // call the same function which u do for adding new entry with slight modifications.
                        $newSettingsRes   =   array();
                        $postData   =   array(
                                        'id'    =>  $_POST['hidRecID'],
                                        'user_name'    =>  $_POST['userName'], 
                                        'name'   =>  $_POST['name'], 
                                        'contact_no' => $_POST['contactNo'],
                                        'email' => $_POST['email'],
                                        'select_profile' => $_POST['selectProfile'],
                                        'select_institute' => $_POST['selectInstitute']);
                        $settingRes  =   $this->Settings_model->add($postData);

                        if($settingRes == 1) {                   
                            $res['data']    =   'success';     
                        } else {        
                            $res['data']    =   'failure';
                        }
                    }

                    break;
                

        case 'del':
        $newSettingsRes   =   array();

        if($_POST['hidRecID'] != 0) {
            $settingRes  =   $this->Settings_model->delete($_POST['hidRecID']);

            if($settingRes == 1) {
                $res['data']    =   'success';        
            } else {
                $res['data']    =   'failure';
            }
        }

        break;
}
}
 
        echo json_encode($res);
    }
}