<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Users_model');
    }
	
	public function index() {
        $data  =   array();
        $data['pageName'] =   'Users List';
        $roles  =   $this->Users_model->getRoles();
        $data['roles']  =   $roles; 
	    $this->load->template('Users', 'templates/', 'Users/', 'usersList', $data);
    }
    
    function ajaxResultsForUsersJSON(){
        $res    =   array();

        if( ($_POST['name']) && ($_POST['email']) && ($_POST['image']) && ($_POST['role'])) {
            //Connect to model by passing the username and pwd values.. Validate whether true or false.
            $data   =   array(
                        'name'    =>  $_POST['name'], 
                        'email'   =>  $_POST['email'], 
                        'image' => $_POST['image'],
                        'role_id'   =>  $_POST['role']);
                        // 'settings_data' =>  $_POST['settingsData']);
            $usersRes   =   $this->Users_model->add($data);

            if($usersRes != FALSE) {
                if($usersRes > 0) {
                    // Set Session for the logged in user later.
                    $res['data']    =   'success';
                } else {
                    $res['data']   =    'failure';
                }
            } else {
                $res['data']   =    'User Already exists.';
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForFiltersJSONGeneration(){
        $users  =   $this->Users_model->getUsers();
        $writeJSONData  =   $this->fetchDataForUsers($users);
        $results    =   array();
        $results['data']['success'] =   1;
        $results['data']['writeJSONData']    =   $writeJSONData;
        //echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
    }

    function fetchDataForUsers($users) {
        $newUser   =   array();
        $cnt    =   0;

        foreach($users as $user) {
            $newUser[$cnt]['SNo']  =   ($cnt + 1);
            $newUser[$cnt]['Name']  =   $user->name;
            $newUser[$cnt]['Email']   =   $user->email;

            if(empty($user->image)) {
                $user->image    =   'default.png';
            }

            $newUser[$cnt]['Image']   = '<img src="'.base_url().'/uploads/users/'. $user->image.'" border="0" alt="'.$user->name.'" title="'.$user->name.'" style="height:50px;width:50px;"/>';
            $newUser[$cnt]['Role']   =  $this->Users_model->getRoleName($user->role_id);

            if($user->status    ==  'Active') {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-success">
                Active</span>';//'<i class="fa fa-check text-success"></i>';
            } else if ($user->status    ==  'Inactive') {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-grey">InActive</span>';
            } else {
                $newUser[$cnt]['Status']   =   '<i class="fa fa-times text-dark"></i>';
            }

            $newUser[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$user->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$user->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }
        return $newUser;
    }
    function checkFieldExists() {
        $res    =   array();
   
        if( (!(empty($_POST['name']))) && (!(empty($_POST['value'])))) {
            $userRes    =   $this->Users_model->checkField($_POST['name'], $_POST['value']);

            if($userRes == 1) {
                $res['data']    =   'failure';
            } else {
                $res['data']    =   'success'; 
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForUsersAction(){
        $res    =   array();
        // echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($_POST);exit();

        if($_POST['hidReqType'] != '') {
            // echo "\r\n <br/> hidReqType : ";$_POST['hidReqType'];exit();
            switch($_POST['hidReqType']) {
                case 'create' :
                // echo "\r\n <br/> Post Value: <pre>";print_r($_POST);
                // echo "\r\n <br/> Image Value: <pre>";print_r($_FILES);exit();
                    // echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($_POST);exit();
                    $data   =   array(
                                'name'    =>  $_POST['name'], 
                                'username'   =>  $_POST['uname'], 
                                'email' => $_POST['email'],
                                'password'  =>  SHA1($_POST['pass']),
                                'role_id'   =>  $_POST['role_id'],
                                'phone' =>  $_POST['phone'],
                    );
                    $usersRes  =   $this->Users_model->add($data);
                    $imgRes =   0;
                    if($usersRes > 0 ){
                        if (isset($_FILES["photo"]) && !empty($_FILES['photo']['name'])) {
                            $fileInfo   =   pathinfo($_FILES["photo"]["name"]);
                            $ext = explode(".",$_FILES["photo"]["name"]);
                            $img_name = $usersRes . '.' .end($ext);
                            move_uploaded_file($_FILES["photo"]["tmp_name"], "./uploads/users/" . $img_name);
                            $imgRes =   $this->Users_model->image_update($usersRes,$img_name);
                        }else{
                            $data_img = 'default.png';
                            $imgRes =   $this->Users_model->image_update($usersRes,$data_img);
                        }
                    }

                    if($usersRes > 0 && $imgRes >   0) {
                        $res['data']    =   'success';
                    } else {
                        $res['data']    =   'failure';
                    }

                    break;

                case 'edit' :
                    $newUsersRes   =   array();

                    if($_POST['hidRecID'] != 0) {
                        $usersRes  =   $this->Users_model->getUsers($_POST['hidRecID']);

                        if(!(empty($usersRes))) {
                            $res['data'] = 'success';

                            foreach($usersRes as $s) {
                                $newUsersRes['name']   =   $s->name;
                                $newUsersRes['username']   =   $s->username;
                                $newUsersRes['email']   =   $s->email;
                                $newUsersRes['password']   =   $s->password;    
                                $newUsersRes['role_id']   =   $s->role_id; 
                                $newUsersRes['phone']   =   $s->phone;    

                            }
                        }

                        $res['userRecord']    =   $newUsersRes;
                    }

                    break;

                case 'update' :
                    // echo "\r\n <br/> post vals for update : \r\n <br/><pre>"; print_r($_POST);
                    if($_POST['hidRecID'] != '') {
                        $data   =   array(
                            'id'    =>  $_POST['hidRecID'],
                            'name'    =>  $_POST['name'], 
                            'username'   =>  $_POST['uname'], 
                            'email' => $_POST['email'],
                            'password'  =>  SHA1($_POST['pass']),
                            'role_id'   =>  $_POST['role_id'],
                            'phone' =>  $_POST['phone'] );
                        $usersRes  =   $this->Users_model->add($data);

                        // echo "\r\n <br/> FILES : \r\n <br/><pre>"; print_r($_FILES);
                        //File upload handling..
                        if($usersRes    ==  1){
                            if (isset($_FILES["photo"]) && !empty($_FILES['photo']['name'])) {
                                $fileInfo   =   pathinfo($_FILES["photo"]["name"]);
                                $ext = explode(".",$_FILES["photo"]["name"]);
                                $img_name = $_POST['hidRecID'] . '.' .end($ext);
                                // echo "\r\n <br/> Image name :";print_r($img_name);exit();
                                move_uploaded_file($_FILES["photo"]["tmp_name"], "./uploads/users/" . $img_name);
                                // echo "\r\n <br/> Data Image: <pre>";print_r($data_img);exit();
                                $imgRes =   $this->Users_model->image_update($_POST['hidRecID'],$img_name);
                            }else{
                                $data_img = 'default.png';
                                $imgRes =   $this->Users_model->image_update($_POST['hidRecID'],$data_img);
                            }
                        }
                        if($usersRes == 1  && $imgRes   ==  1) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;

                case 'dele':
                    if($_POST['hidRecID'] != 0) {
                        $userRes  =   $this->Users_model->delete($_POST['hidRecID']);

                        if($userRes == 1) {
                            $res['data']    =   'success';        
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
            }
        }

        echo json_encode($res);
    }
}