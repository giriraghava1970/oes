<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends MX_Controller {
    function __construct() {
        parent::__construct(); 
        $this->load->model('Students_model');
        $this->load->model('Courses_model');
        $this->load->model('Batches_model');
        $this->load->model('Institutes_model');
        $this->load->model('Document_model');
        $this->load->model('States_model');
        $this->load->model('Countries_model');
        $this->load->model('Cities_model');
    }
	
	public function index() {
        $data  =   array();
        $data['pageName'] =   'Students List';
	    $this->load->template('Batch', 'templates/', 'Students/', 'studentsListing', $data);
    }

    public function Create() {
        $data['pageName'] =   'Student Create';
        $data['courses']   =   $this->Courses_model->get();
        $data['bacthes']    =   $this->Batches_model->get();
        $data['institutes'] =   $this->Institutes_model->getInstitute();
        $data['countryRes']    =   $this->Countries_model->get();
        $data['documentRes']    =   $this->Document_model->get();
        // echo "<pre> Data: <br/>";print_r($data);exit();
	    $this->load->template('Student Create', 'templates/', 'Students/', 'studentCreate', $data);
    }

    function ajaxResultsForUsersJSON(){
        $res    =   array();

        if( ($_POST['name']) && ($_POST['email']) && ($_POST['image']) && ($_POST['role'])) {
            //Connect to model by passing the username and pwd values.. Validate whether true or false.
            $data   =   array(
                        'name'    =>  $_POST['name'], 
                        'email'   =>  $_POST['email'], 
                        'image' => $_POST['image'],
                        'role_id'   =>  $_POST['role']);
                        // 'settings_data' =>  $_POST['settingsData']);
            $usersRes   =   $this->Students_model->add($data);

            if($usersRes != FALSE) {
                if($usersRes > 0) {
                    // Set Session for the logged in user later.
                    $res['data']    =   'success';
                } else {
                    $res['data']   =    'failure';
                }
            } else {
                $res['data']   =    'User Already exists.';
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForFiltersJSONGeneration(){
        $Students  =   $this->Students_model->get();
        //  echo "\r\n <br/> Result Of Students :<pre>";print_r($Students);exit();
        $writeJSONData  =   $this->fetchDataForStudents($Students);
        $results    =   array();
        $results['data']['success'] =   1;
        $results['data']['writeJSONData']    =   $writeJSONData;
        //echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
    }

    function fetchDataForStudents($Students) {
        $newUser   =   array();
        $cnt    =   0;
        foreach($Students as $user) {
            $courseName =   $instituteName    = $batchName  =   '';
            $courseRes  =   $this->Courses_model->get($user->course_id);
            if(!(empty( $courseRes)))
            {
                foreach($courseRes as $res)
                {
                    $courseName =   $res->course_name;
                }
            }
            $batchRes   =   $this->Batches_model->getBatchByID($user->batch_id);
            if(!(empty( $batchRes)))
            {
                foreach($batchRes as $res)
                {
                    $batchName =   $res->batch_name;
                }
            }
            $instituteRes   =   $this->Institutes_model->getInstitute($user->institute_id);
            if(!(empty( $instituteRes)))
            {
                foreach($instituteRes as $res)
                {
                    $instituteName =   $res->institute_name;
                }
            }
            
            // echo "<pre> Batch Id  : <br/>";print_r($user->institute_id);exit();
            $newUser[$cnt]['ID']  = '<div class="checkbox">
                                        <input class="dt-checkbox" type="checkbox" name="studentIDS[]" id="'.$user->id.'" value="'.$user->id.'"/>
                                        <label></label></div>'; 
            $newUser[$cnt]['SNo']  =   ($cnt + 1);
            $newUser[$cnt]['Student Name']  =   '<a href="">'.$user->first_name." ".$user->last_name.'</a>'; 
            $newUser[$cnt]['Course']  =  $courseName;
            $newUser[$cnt]['Batch']   =  $batchName;
            $newUser[$cnt]['Institute']   =  $instituteName;
            $newUser[$cnt]['Register Date']   =   $user->admission_date;
            // $newUser[$cnt]['End Date']   =   $user->end_date;
        
            if($user->status    ==  'Active') {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-success">
                Active</span>';//'<i class="fa fa-check text-success"></i>';
            } else if ($user->status    ==  'InActive') {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-grey">InActive</span>';
            } else {
                $newUser[$cnt]['Status']   =   '<i class="fa fa-times text-dark"></i>';
            }

            $newUser[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$user->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$user->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }
        return $newUser;
    }

    function checkFieldExists() {
        $res    =   array();
   
        if( (!(empty($_POST['name']))) && (!(empty($_POST['value'])))) {
            $userRes    =   $this->Students_model->checkField($_POST['name'], $_POST['value']);

            if($userRes == 1) {
                $res['data']    =   'failure';
            } else {
                $res['data']    =   'success';
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForStatesDropBox() {
        $res    =   array();
        $states =   array();
        $cnt    =   0;

        if($_POST['countryID'] != '') {
            $stateRes   =   $this->States_model->getStatesByCountry($_POST['countryID']);

            if(!(empty($stateRes))) {
                $res['data']    =   'success';

                foreach($stateRes as $s) {
                    $states[$cnt]['id'] =   $s->id;
                    $states[$cnt]['state_name'] =   $s->state_name;
                    $cnt++;
                }

                $res['states']  =   $states;
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForcityDropBox(){
        $res    =   array();
        $cities =   array();
        $cnt    =   0;

        if($_POST['stateId'] != '') {
            $cityRes   =   $this->Cities_model->getCityByState($_POST['stateId']);

            if(!(empty($cityRes))) {
                $res['data']    =   'success';

                foreach($cityRes as $s) {
                    $cities[$cnt]['id'] =   $s->id;
                    $cities[$cnt]['city_name'] =   $s->city_name;
                    $cnt++;
                }

                $res['cities']  =   $cities;
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForStudentsAction(){
        $res    =   array();
        $formCont   =   '';

        if($_POST['hidReqType'] != '') {
            // echo "\r\n <br/> hidReqType : ";$_POST['hidReqType'];exit();
            switch($_POST['hidReqType']) {
                case 'create' :
                // echo "\r\n <br/> Post Value: <pre>";print_r($_POST);
                // echo "\r\n <br/> Image Value: <pre>";print_r($_FILES);exit();
                    // echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($_POST);exit();
                    $data   =   array(
                                'course_id'   =>  $_POST['courseName'], 
                                'batch_name' => $_POST['batchName'],
                                'start_date'  =>  $_POST['startDate'],
                                'end_date'  =>  $_POST['endDate'],
                    );
                    // echo "\r\n <br/> Data :";print_r($data);exit();
                    $usersRes  =   $this->Students_model->add($data);
                  
                    if($usersRes > 0 ) {
                        $res['data']    =   'success';
                    } else {
                        $res['data']    =   'failure';
                    }

                    break;

                case 'update' :
                    // echo "\r\n <br/> post vals for update : \r\n <br/><pre>"; print_r($_POST);exit();
                    if($_POST['hidRecID'] != '') {
                        $data   =   array(
                            'id'    =>  $_POST['hidRecID'],
                            'course_id'    =>  $_POST['courseName'], 
                            'course_id'   =>  $_POST['courseName'], 
                            'batch_name' => $_POST['batchName'],
                            'start_date'  =>  $_POST['startDate'],
                            'end_date'  =>  $_POST['endDate']);
                            // 'status'  =>  $_POST['status']);
                        $usersRes  =   $this->Students_model->add($data);
                        
                        if($usersRes > 0) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }

                    }

                    break;

                case 'dele':
                    if($_POST['hidRecID'] != 0) {
                        $userRes  =   $this->Students_model->delete($_POST['hidRecID']);
                        if($userRes == 1) {
                            $res['data']    =   'success';        
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
            }
        }
        echo json_encode($res);
    }
    


    function ajaxResultsForStudents(){
        $res    =   array();
        $admission_date =   date('Y-m-d');
        $data   =   array(
            'first_name'    => $_POST['Students']['first_name'],
            'middle_name'   =>  $_POST['Students']['middle_name'],
            'Last_name'   =>  $_POST['Students']['last_name'],
            'dob'   =>  $_POST['Students']['dob'],
            'gender'   =>  $_POST['Students']['gender'],
            'blood_group'   =>  $_POST['Students']['blood'],
            'aadhar_no'  =>  $_POST['Students']['aadhar_no'],
            'course_id'   =>  $_POST['Students']['course'],
            'batch_id'  =>  $_POST['Students']['batch'],
            'institute_id'  =>  $_POST['Students']['institute'],
            'admission_date'    =>  $admission_date,
            'guardian_fname'  =>  $_POST['Students']['pfName'],
            'guardian_mname'  =>  $_POST['Students']['pmName'],
            'guardian_lname'  =>  $_POST['Students']['plName'],
            'guardian_mobile'  =>  $_POST['Students']['guardian_phone1'],
            'photo' =>  'default.png',
        );
        $insertID   =   $this->Students_model->add($data);

        $imgRes =   0;
                    if($insertID > 0 ){
                        if (isset($_FILES["Students[photo_data]"]) && !empty($_FILES['Students[photo_data]']['name'])) {
                            $fileInfo   =   pathinfo($_FILES["Students[photo_data]"]["name"]);
                            $ext = explode(".",$_FILES["Students[photo_data]"]["name"]);
                            $img_name = $insertID . '.' .end($ext);
                            move_uploaded_file($_FILES["Students[photo_data]"]["tmp_name"], "./uploads/students/" . $img_name);
                            $imgRes =   $this->Students_model->image_update($insertID,$img_name);
                        }else{
                            $data_img = 'default.png';
                            $imgRes =   $this->Students_model->image_update($insertID,$data_img);
                        }
                    }

        if($insertID    >   0   &&    $imgRes >   0)
        {
           $status  =   $this->addContact($insertID);
           if($status == 1)
           {    
               $this->index();
                // $docRes =   $this->addDocument($insertID);
                // if($docRes  ==  1){
                //         $res['data']    =   'success';
                // }
                // else {
                //     $res['data']    =   'failure';
                // }
           }
        }

        // if( ($_POST['name']) && ($_POST['email']) && ($_POST['image']) && ($_POST['role'])) {
        //     //Connect to model by passing the username and pwd values.. Validate whether true or false.
        //     $data   =   array(
        //                 'name'    =>  $_POST['name'], 
        //                 'email'   =>  $_POST['email'], 
        //                 'image' => $_POST['image'],
        //                 'role_id'   =>  $_POST['role']);
        //                 // 'settings_data' =>  $_POST['settingsData']);
        //     $usersRes   =   $this->Students_model->add($data);

        //     if($usersRes != FALSE) {
        //         if($usersRes > 0) {
        //             // Set Session for the logged in user later.
        //             $res['data']    =   'success';
        //         } else {
        //             $res['data']   =    'failure';
        //         }
        //     } else {
        //         $res['data']   =    'User Already exists.';
        //     }
        // }

        echo json_encode($res);
    }
    public function addContact($insertID)
    {
        $contactDetails =   array(
            'student_id'    =>   $insertID,
            'adress_line1'  =>   $_POST['StudentContacts']['address1'],
            'adress_line2'  =>   $_POST['StudentContacts']['address2'],
            'mobile'    =>  $_POST['StudentContacts']['phone1'],
            'email'    =>  $_POST['StudentContacts']['email'],
            'city_id'    =>  $_POST['StudentContacts']['cityId'],
            'state_id'    =>  $_POST['StudentContacts']['stateId'],
            'country_id'    =>  $_POST['StudentContacts']['countryId'],
        );
        $res    =   $this->Students_model->addContact($contactDetails);
        return $res;
    }

    public function addDocument($insertID)
    {
        $docDetails =   array(
            'student_id'    =>   $insertID,

        );
        $res    =   $this->Students_model->addDocument($docDetails);
        return $res;
    }
}