<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class QuestionTypes extends MX_Controller {
    function __construct() {
        parent::__construct(); 
        $this->load->model('QuestionTypeMaster_model');
        $this->generalFn    =   new Generalfunctions();
    }

    //question type master//
    public function Index() {
        $data  =   array();
        $data['pageName'] =   'Question Type';
        $this->load->template('QuestionTypes', 'templates/', 'Questions/QuestionTypes/', 'questionTypesList', $data);
    }

    function ajaxQuestionTypesResultsForFiltersJSONGeneration(){
        $posts  =   $this->QuestionTypeMaster_model->getList();
        $writeJSONData  =   $this->fetchDataForQuestionTypes($posts);
        $results    =   array();
        $results['data']['success'] =   1;
        $results['data']['writeJSONData']    =   $writeJSONData;
        echo json_encode($results['data']);
    }

    function fetchDataForQuestionTypes($posts) {
        $newPosts   =   array();
        $cnt    =   0;

        foreach($posts as $post) {
            $newPosts[$cnt]['SNo']  =   ($cnt + 1);
            $newPosts[$cnt]['Question Type']  =  $post->question_type_name;

            if($post->status    ==  'Active') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-success">Active</span>';
            } else if ($post->status    ==  'InActive') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-secondary">InActive</span>';
            } else {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-warning">None</span>';
            }

            $newPosts[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$post->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$post->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }

        return $newPosts;
    }

    function ajaxResultsForQuestionTypeAction(){
        $res    =   array();
        $formCont   =   '';

        if($_POST['reqType'] != '') {
          switch($_POST['reqType']) {
                case 'formList' :
                    $hidRecID   =   0;
                    $questionType =    $statusFlag =   '';

                    if($_POST['hidRecID'] != '') {
                        $hidRecID   =   $_POST['hidRecID'];
                        $qRes  =   $this->QuestionTypeMaster_model->get($hidRecID);

                        if(!(empty($qRes))) {
                            foreach($qRes as $qT) {
                                $questionType =   $qT->question_type_name;
                                $statusFlag =   $qT->status;
                            }
                        }
                    }

                    $formCont   =   '<form id="frmCommon" post="frmCommon" method="post" action="">
                                        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
                                        <input type="hidden" name="hidRecID" id="hidRecID" value="'.$hidRecID.'"/> 
                                        <div class="card">
                                			<div class="row"><!-- class="row" -->
                                				<div class="col-lg-12"><!-- class="col-lg-12" -->
                                					<div class="navbar navbar-expand-xl navbar-dark bg-indigo-400 navbar-component rounded-top mb-0"> <!-- class="navbar navbar-expand-xl navbar-light navbar-component rounded-top mb-0" -->
                                						<div class="d-xl-none">
                                							<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-demo-dark">
                                								<i class="icon-menu"></i>
                                							</button>
                                						</div>
                                						<div class="navbar-collapse collapse" id="navbar-demo-dark" style="border:0px !important;">
                                							<h6 style="color:#ff;">Question Types Add / Edit</h6>
                                	                   	</div>
                                	                </div>
                                	            </div>
                                	        </div>
                                			<div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="QuestionType">Question Type:<span class="text-red">*</span></label>
                                                            <input type="text" class="form-control" required name="question_type" id="question_type" value="'.$questionType.'">
                                                        </div>
                                                    </div>';
                    if($hidRecID != 0) {
                        $formCont   .=  '           <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="status">Status:<span class="text-red">*</span></label>
                                                            <select data-placeholder="Select Status" class="form-control form-input-styled" data-fouc name="lsStatus" id="lsStatus">
                                                                <option value="">--Choose One--</option>';
                        $fEnums =   $this->generalFn->field_enums(' question_types_mst', 'status');

                        if(!(empty($fEnums))) {
                            foreach($fEnums as $fe){
                                if($statusFlag == $fe) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }

                                $formCont   .=  '<option value="'.$fe.'" '. $sele.'>'.$fe.'</option>';
                            }
                        }

                        $formCont   .=  '                   </select>
                                                         </div>
                                                     </div>';
                    }

                    $formCont   .=   '          </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>';
                    $res['data']    =   'success';
                    $res['formCont']    =   $formCont;
                    break;

                case 'create' :
                    $chkRes =   $this->QuestionTypeMaster_model->checkExists('question_type_name', $_POST['question_type']);

                    if($chkRes == 0) {
                        $data   =   array(
                                    'question_type_name'   =>  $_POST['question_type'], 
                                    'status' => 'Active', 
                                    'created_at' => date('Y-m-d h:i:s'),
                                    'created_by'    =>  1,);
                        $qtRes  =   $this->QuestionTypeMaster_model->add($data);

                        if($qtRes > 0 ) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }
                    } else {
                        $res['data']    =   'Question Type already exists.';
                    }

                    break;

                case 'update' :
                    if($_POST['hidRecID'] != '') {
                        $data   =   array(
                                        'id'    =>  $_POST['hidRecID'], 
                                        'question_type_name'   =>  $_POST['question_type'], 
                                        'status'    =>  $_POST['lsStatus'],
                                        'updated_at'    =>  date('Y-m-d h:i:s'),);
                        $qtRes  =   $this->QuestionTypeMaster_model->add($data);

                        if($qtRes == 1) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;

                case 'dele':
                    if($_POST['hidRecID'] != 0) {
                        $qtRes  =   $this->QuestionTypeMaster_model->delete($_POST['hidRecID']);

                        if($qtRes == 1) {
                            $res['data']    =   'success';        
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
            }
        }

        echo json_encode($res);
    }
}