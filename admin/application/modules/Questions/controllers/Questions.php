<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Questions extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Classes_model');
        $this->load->model('Examtypes_model');
        $this->load->model('Levels_model');
        $this->load->model('Subjects_model');
        $this->load->model('Topics_model');
        $this->load->model('Questionbanktypes_model');
        $this->load->model('Questioncategories_model');
        $this->load->model('Questions_model');
        $this->load->model('Questiontypemaster_model');
        // $this->generalFn    =   new GeneralFunctions();
    }
    
    public function index() {
        $data  =   array();
        $data['pageName'] =   'Questions';
        $data['subjRes']    =   $this->Subjects_model->get();
        $dataForFilter  =   array();
        $dataForFilter['chapterID'] =   $dataForFilter['subjectID'] =   $dataForFilter['moduleID'] =    $dataForFilter['topicID'] = 0;
        $dataForFilter['questionName'] =   null;
        $activeFilters	=	'';

        if(!(empty($_POST['hidSearchFlag']))) {
            $newSearchArr	=	json_decode($_POST['hidSearchConcat']);
            $activeFilters	=	'';
            
            if(!(empty($newSearchArr))) {
                foreach ($newSearchArr as $key => $val) {
                    foreach ($val as $key1 =>  $val1) {
                        switch($key1) {
                            case 'subjectCode' :
                                $dataForFilter['subjectCode'] =   trim($val1);
                                $activeFilters	.=	'<li>
														<button type="button" id="btnRemSubjectCode" class="btn remBtn" style="width:16%;background:none;padding:0!important;">
															<i class="fa fa-remove mr-3" style="color:red;align-self:center;"></i>
														</button>'. ucfirst($key1). ' : <b>' . $val1.'</b>&nbsp;</li>';
                                break;
                                
                            case 'subjectName' :
                                $dataForFilter['subjectName'] =   trim($val1);
                                $activeFilters	.=	'<li>
														<button type="button" id="btnRemSubjectName" class="btn remBtn" style="width:16%;background:none;padding:0!important;">
															<i class="fa fa-remove mr-3" style="color:red;align-self:center;"></i>
														</button>'. ucfirst($key1). ' : <b>' . $val1.'</b>&nbsp;</li>';
                                break;
                        }
                    }
                }
            }
        }
        
        $questions  =   $this->Questions_model->getList($dataForFilter);
        $questionArr    =   array();
        $qOptions   =   array();

        if(!(empty($questions))) {
            $cnt    =   0;

            foreach($questions as $q) {
                $questionArr[$cnt]['questionbank_type_id']  =   $q->questionbank_type_id;
                $questionArr[$cnt]['class_id']  =   $q->class_id;
                $questionArr[$cnt]['subject_id']  =   $q->subject_id;
                $questionArr[$cnt]['chapter_id']  =   $q->chapter_id;
                $questionArr[$cnt]['topic_id']  =   $q->topic_id;

                $questionArr[$cnt]['topic_id']  =   $q->topic_id;
                $questionArr[$cnt]['questioncategory_id']  =   $q->questioncategory_id;
                $questionArr[$cnt]['question_type_id']  =   $q->question_type_id;
                $questionArr[$cnt]['exam_type_id']  =   $q->exam_type_id;
                $questionArr[$cnt]['level_id']  =   $q->level_id;
                $questionArr[$cnt]['question_details_text']  =   $q->question_details_text;
                $qOptDtls   =   $this->Questions_model->getQuestionOptions($q->id);

                if(!(empty($qOptDtls))) {
                    $cnt1   =   0;

                    foreach($qOptDtls as $qOpt) {
                        $questionArr[$cnt]['Options'][$cnt1]['question_id']   =   $qOpt->question_id;
                        $questionArr[$cnt]['Options'][$cnt1]['question_details_id']   =   $qOpt->question_details_id;
                        $questionArr[$cnt]['Options'][$cnt1]['option1']   =   $qOpt->option1;
                        $questionArr[$cnt]['Options'][$cnt1]['option2']   =   $qOpt->option2;
                        $questionArr[$cnt]['Options'][$cnt1]['option3']   =   $qOpt->option3;
                        $questionArr[$cnt]['Options'][$cnt1]['option4']   =   $qOpt->option4;
                        $questionArr[$cnt]['Options'][$cnt1]['option_true']   =   $qOpt->option_true;
                        $questionArr[$cnt]['Options'][$cnt1]['option_false']   =   $qOpt->option_false;
                        $cnt1++;
                    }
                }

                $cnt++;
            }
        }

        //$data['qOptions']   =   $qOptions;
        //echo "\r\n <br/> qAr : \r\n <br/><pre>"; print_r($questionArr);
        $data['questions']  =   $questionArr;
        $this->load->template('ViewQuestions', 'templates/', 'Questions/', 'questionsList', $data);
    }

    public function Create() {
        $data  =   array();
        $data['pageName'] =   'Questions';
        $data['classRes'] =   $this->Classes_model->get();
        $data['questionBankTypeRes']    =   $this->Questionbanktypes_model->get();
        $data['questionCatRes']    =   $this->Questioncategories_model->get();
        $data['subjRes']    =   $this->Subjects_model->get();
        $data['qTypeRes']   =   $this->Questiontypemaster_model->get();
        $data['examTypesRes']   =   $this->Examtypes_model->get();
        $data['levelRes']   =   $this->Levels_model->get();
        $this->load->template('QuestionsCreate', 'templates/', 'Questions/', 'questionForm', $data);
    }

    function Upload() {
        $data  =   array();
        $data['pageName'] =   'UploadQuestions';
        $data['questionBankTypeRes']    =   $this->Questionbanktypes_model->get();
        $data['questionCatRes']    =   $this->Questioncategories_model->get();
        $data['subjRes']    =   $this->Subjects_model->get();
        $this->load->template('UploadQuestions', 'templates/', 'Questions/', 'uploadQuestions', $data);
    }

    function ajaxResultsForFiltersJSONGeneration(){
        $dataForFilter  =   array();
        $dataForFilter['chapterID'] =   $dataForFilter['subjectID'] =   $dataForFilter['moduleID'] =    $dataForFilter['topicID'] = 0;
        $dataForFilter['questionName'] =   null;
        $activeFilters	=	'';

        if(!(empty($_POST['hidSearchFlag']))) {
            $newSearchArr	=	json_decode($_POST['hidSearchConcat']);
            $activeFilters	=	'';

            if(!(empty($newSearchArr))) {
                foreach ($newSearchArr as $key => $val) {
                    foreach ($val as $key1 =>  $val1) {
                        switch($key1) {
                            case 'subjectCode' :
                                $dataForFilter['subjectCode'] =   trim($val1);
                                $activeFilters	.=	'<li>
														<button type="button" id="btnRemSubjectCode" class="btn remBtn" style="width:16%;background:none;padding:0!important;">
															<i class="fa fa-remove mr-3" style="color:red;align-self:center;"></i>
														</button>'. ucfirst($key1). ' : <b>' . $val1.'</b>&nbsp;</li>';
                                break;
                                
                            case 'subjectName' :
                                $dataForFilter['subjectName'] =   trim($val1);
                                $activeFilters	.=	'<li>
														<button type="button" id="btnRemSubjectName" class="btn remBtn" style="width:16%;background:none;padding:0!important;">
															<i class="fa fa-remove mr-3" style="color:red;align-self:center;"></i>
														</button>'. ucfirst($key1). ' : <b>' . $val1.'</b>&nbsp;</li>';
                                break;
                        }
                    }
                }
            }
        }

        $questions  =   $this->Questions_model->getList($dataForFilter);
        $writeJSONData  =   $this->fetchDataForQuestions($questions);
        $results    =   array();
        $results['data']['success'] =   1;
        
        if($activeFilters == '') {
            $results['data']['activeFilters']	=	'<li><span class="text-semibold text-uppercase" style="valign:middle;">Active Filter&nbsp;:&nbsp;</span></li><li><span class="text-semibold text-uppercase" style="valign:middle;"> No Active Filters </span></li>';
        } else {
            $results['data']['activeFilters']	=	'<li><span class="text-semibold text-uppercase" style="valign:middle;">Active Filter&nbsp;:&nbsp;</span></li>'.$activeFilters;
        }

        $results['data']['writeJSONData']    =   $writeJSONData;
        //echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
    }

    function fetchDataForQuestions($posts) {
        $newPosts   =   array();
        $cnt    =   0;

        foreach($posts as $post) {
            $newPosts[$cnt]['SNo']  =   ($cnt + 1);
            $newPosts[$cnt]['Subject']  =   $post->subjectName;
            //$newPosts[$cnt]['Module']  =   $post->moduleName;
            $newPosts[$cnt]['Chapter']  =   $post->chapterName;
            $newPosts[$cnt]['Topic']    =   $post->topicName;
            $qDtls  =   $this->Questions_model->getTotalQuestions($post->id);
            $totQuestions   =   0;

            if(!(empty($qDtls))) {
                foreach($qDtls as $q) {
                    $totQuestions   =   $q->totalQuestions;
                }
            }

            $newPosts[$cnt]['Total Questions'] =   $totQuestions;

            if($post->status    ==  'Active') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-success">Active</span>';
            } else if ($post ->status    ==  'InActive') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-secondary">InActive</span>';
            } else {
                $newPosts[$cnt]['Status']   =   '<i class="fa fa-times text-dark"></i>';
            }

            $newPosts[$cnt]['Action']   =   '<a href="javascript:void(0);" id="view-'.base64_encode($post->id).'" class="logAction">
                                                <span class="badge badge-primary"> View Questions </span>
                                            </a>
                                            <a href="javascript:void(0);" id="edit-'.base64_encode($post->id).'" class="logAction">
                                                <span class="badge badge-info"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.base64_encode($post->id).'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }

        return $newPosts;
    }

    function ajaxResultsForQuestions() {
        $statusFlag =   0;

        if ( ($_POST['hidStudForm'] == 1) && ($_POST['hidReqType'] == 'create') ) {
            //echo "\r\n <br/> post : \r\n <br/><pre>"; print_r($_POST);

            if(is_array($_POST['questions'])) {
                //STEP 1 : Insert to Questions table --
                $qData  =   array(
                    'questionbank_type_id'  =>  $_POST['questions']['questionBankType_id'],
                    'class_id'  =>  $_POST['questions']['classID'],
                    'subject_id' =>  $_POST['questions']['subjectID'],
                    'module_id' =>   0,
                    'chapter_id'    =>  $_POST['questions']['chapterID'],
                    'topic_id'  =>  $_POST['questions']['topicID'],
                    'questioncategory_id'   =>  $_POST['questions']['questionCategoryID'],
                    'question_type_id'  =>  $_POST['questions']['questionTypeID'],
                    'exam_type_id'  =>  $_POST['questions']['examTypeID'],
                    'level_id'  =>  $_POST['questions']['levelID'],
                    'status'    =>  'Active',
                    //'lang_type' =>  'English', //Default
                    'created_at'    =>  date('Y-m-d h:i:s'),
                    'created_by'    =>  1,);
                $qRes   =   $this->Questions_model->add($qData);

                if($qRes > 0) {
                    $statusFlag =   1;
                    //STEP 2 : Insert to Question Details table --
                    $qData  =   array(
                                    'question_id'   =>  $qRes,
                                    'question_details_text' =>  $_POST['editor-full'],
                                    'status'    =>  'Active',
                                    'created_at'    =>  date('Y-m-d h:i:s'),
                                    'created_by'    =>  1, );
                    $qDtlsRes   =   $this->Questions_model->addQuestionDetails($qData);

                    if($qDtlsRes > 0) {
                        $statusFlag =   1;
                        //STEP 3 : Insert to Question Options Details table --
                        $qOptDtls   =   array(
                            'question_id'   =>  $qRes,
                            'question_details_id'   =>  $qDtlsRes,);

                        switch($_POST['questions']['questionTypeID']) {
                            case 1 ://Single Choice [Looping to max 4 -- need to pick only 1 option]
                            case 2 ://Multi Choice [Looping to max 4 -- need to pick only 1 option]
                                for($i = 0; $i < 4; $i++) {
                                    $qOptDtls['option'.($i+1)]  =   $_POST['single-editor-half'][$i];
                                }

                                break;
                            case 3 ://Integer or decimal
                                for($i = 0; $i < 4; $i++) {
                                    $qOptDtls['option'.($i+1)]  =   $_POST['intDecOptions'][$i];
                                }

                                break;
                            case 4 ://True or Falseay
                                $qOptDtls['option_true']    =   $_POST['trueFalseOptions'][0];
                                $qOptDtls['option_false']    =   $_POST['trueFalseOptions'][1];
                                break;

                            case 5 ://Match the Following
                                for($i = 0; $i < 4; $i++) {
                                    $qOptDtls['option'.($i+1)]  =   $_POST['matchOptions'][$i];
                                }

                                break;

                            case 6 ://Paragraph
                                break;

                            case 7  ://Fill In the blanks
                                break;
                        }

                        $qOptDtls['status'] =   'Active';
                        //$qOptDtls['lang_type']  =   'English'; //Default
                        $qOptDtls['created_at'] =   date('Y-m-d h:i:s');
                        $qOptDtls['created_by'] =   1;
                        //echo "\r\n <br/> final qOptDtls : \r\n <br/><pre>"; print_r($qOptDtls);
                        $qOptRes    =   $this->Questions_model->addQuestionOptions($qOptDtls);

                        if($qOptRes > 0) {
                            $statusFlag =   1;
                        } else {
                            $statusFlag =   0;
                        }
                    } else {
                        $statusFlag =   0;
                    }
                } else {
                    $statusFlag =   0;
                }
            }//if(is_array($_POST['questions'])) {

            /*if($statusFlag == 1) {
                //echo "\r\n <br/> redirect back to Questions Listing screen..";
                redirect('Questions/');
            } else {
                //echo "\r\n <br/> throw appropriate errors.";
            }*/
            redirect('Questions/');
        }
    }

    function ajaxResultsForQuestionUploadsFiltersJSONGeneration() {
        $dataForFilter  =   array();
        $questions  =   $this->Questions_model->getList($dataForFilter);
        $writeJSONData  =   $this->fetchDataForQuestionsUplaod($questions);
        $results    =   array();
        $results['data']['success'] =   1;
        $results['data']['writeJSONData']    =   $writeJSONData;
        echo json_encode($results['data']);
    }

    function fetchDataForQuestionsUplaod($posts) {
        $newPosts   =   array();
        $cnt    =   0;

        foreach($posts as $post) {
            $cnt++;
        }

        return $newPosts;
    }
}