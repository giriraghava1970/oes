<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Packages extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Packages_model');
        $this->load->model('Courses_model');
        $this->load->model('Batches_model');
        $this->load->model('Institutes_model');
    }
	
	public function index() {
        $data  =   array();
        $data['pageName'] =   'Packages';
	    $this->load->template('Packages', 'templates/', 'Packages/', 'packagesList', $data);
    }

    function ajaxResultsForValidatePackagesJSON(){
        $res    =   array();

        if( ($_POST['packageName']) && ($_POST['packageImg'])) {
            //Connect to model by passing the username and pwd values.. Validate whether true or false.
            $data   =   array(
                        'course_id'   =>  $_POST['courseId'],
                        'batch_id'   =>  $_POST['batchId'],
                        'institute_id'   =>  $_POST['instituteId'],
                        'package_name'   =>  $_POST['packageName'],
                        // 'package_description'   =>  $_POST['packageDesc'],
                        'package_start_date'  =>  $_POST['startDate'],
                        'package_end_date'    =>  $_POST['endDate'],
                        'package_cost'    =>  $_POST['packageCost'],
                        'package_mrp'    =>  $_POST['packageMrp'],
                        'package_is_published'    =>  $_POST['packageIsPublished'],
                        'package_is_free'    =>  $_POST['packageIsFree'],
                        'package_image_link'    =>  $_POST['packageImg'],
                        'status'    =>  $_POST['status'],
                        'created_at'    =>  $_POST['createdAt'],
                        'created_by'    =>  $_POST['createdBy'],
                        'updated_at'    =>  $_POST['updatedAt'],
                        'status'    =>  $_POST['status']);
            $packageRes   =   $this->Packages_model->add($data);

            if($packageRes != FALSE) {
                if($packageRes > 0) {
                    // Set Session for the logged in user later.
                    $res['data']    =   'success';
                } else {
                    $res['data']   =    'failure';
                }
            } else {
                $res['data']   =    'Institute Already exists.';
            }
        }
        echo json_encode($res);
    }

    function ajaxResultsForFiltersPackageJSONGeneration(){
        $posts  =   $this->Packages_model->getPackages();
        $writeJSONData  =   $this->fetchDataForPackages($posts);
        $results    =   array();
        $results['data']['success'] =   1;
        $results['data']['writeJSONData']    =   $writeJSONData;
        // echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
    }

    function fetchDataForPackages($posts) {
        $newUser   =   array();
        $cnt    =   0;

        foreach($posts as $p) {
            $courseInfo =   '';
            $instituteInfo  =   '';
            $courseRes  =   $this->Courses_model->get($p->course_id);

            if(!(empty($courseRes))) {
                foreach($courseRes as $c) {
                    $courseInfo =   $c->course_name;
                }
            }

            $batchRes  =   $this->Batches_model->getBatchByID($p->batch_id);

            if(!(empty($batchRes))) {
                foreach($batchRes as $b) {
                    $courseInfo .=   ' / ' . $b->batch_name;
                }
            }

            $instituteRes  =   $this->Institutes_model->getInstitute($p->institute_id);

            if(!(empty($instituteRes))) {
                foreach($instituteRes as $i) {
                    $instituteInfo .=   $i->institute_name;
                }
            }

            $newUser[$cnt]['SNo']  =   ($cnt + 1);
            $newUser[$cnt]['Package']  =   $p->package_name;
            $newUser[$cnt]['Package Description']   =   $p->package_description;
            $newUser[$cnt]['Start Date']   =   $p->package_start_date;
            $newUser[$cnt]['End Date']   =   $p->package_end_date;
            $newUser[$cnt]['Course / Batch']   =   $courseInfo;
            $newUser[$cnt]['Publish Online']   =   $p->package_is_published;
            $newUser[$cnt]['Free Package']   =   $p->package_is_free;
            $newUser[$cnt]['Package Cost']   =   $p->package_cost;
            $newUser[$cnt]['Package MRP']   =   $p->package_mrp;
            $newUser[$cnt]['Institute']   =   $instituteInfo;
            $newUser[$cnt]['Package Image']   = '<img src="'.base_url().'/uploads/packages/'. $p->package_image_link.'" border="0" alt="'.$p->package_name.'" title="'.$p->package_name.'" style="height:50px;width:50px;"/>';
            // $newUser[$cnt]['Package Image']   =   $p->package_image_link;
            // $newUser[$cnt]['Status']   =   $p->status;
            if($p->status    ==  'Active') {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-success">
                Active</span>';//'<i class="fa fa-check text-success"></i>';
            } else if ($p->status    ==  'InActive') {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-gray" style="background-color: burlywood;">InActive</span>';
            } else {
                $newUser[$cnt]['Status']   =   '<i class="fa fa-times text-dark"></i>';
            }

            if(empty($p->package_image_link)) {
                $p->package_image_link    =   'default.png';
            }

            $newUser[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$p->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="dele-'.$p->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }
        return $newUser;
    }

    function ajaxResultsForPackagesAction(){
        $res    =   array();
        // echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($res);
        //  exit();

        if($_POST['reqType'] != '') {
            switch($_POST['reqType']) {
                
                case 'create' :
                // Do the update... 
                // call the same function which u do for adding new entry with slight modifications.
                    $data   =   array(
                        'course_id' =>  $_POST['courseId'],
                        'batch_id' =>  $_POST['batchId'],
                        'institute_id' =>  $_POST['instituteId'],
                        'package_name'   =>  $_POST['packageName'],
                        'package_description'   =>  $_POST['packageDesc'],
                        'package_start_date'   =>  $_POST['startDate'],
                        'package_end_date'   =>  $_POST['endDate'],
                        'package_cost'   =>  $_POST['packageCost'],
                        'package_mrp'  =>  $_POST['packageMrp'],
                        'status'    =>  $_POST['status'],
                        'created_at'    =>  date('Y-m-d h:i:s'));
                        // Need to include created_by field after Session workflow is implemented.
                       
                    $packageRes  =   $this->Packages_model->add($data);

                    // $imgRes =   0;
                    // if($packageRes > 0 ){
                    //     if (isset($_FILES["file"]) && !empty($_FILES['file']['name'])) {
                    //         $fileInfo = pathinfo($_FILES["file"]["name"]);
                    //         $img_name = $packageRes . '.' . $fileInfo['extension'];
                    //         move_uploaded_file($_FILES["file"]["tmp_name"], "./uploads/packages/" . $img_name);
                    //         $data_img = array('id' => $packageRes, 'photo' => 'uploads/packages/' . $img_name);
                    //         $imgRes =   $this->Packages_model->image_update($packageRes,$data_img);
                    //     }else{
                    //         $data_img = array('id' => $packageRes, 'photo' => 'uploads/packages/default.png');
                    //         $imgRes =   $this->Packages_model->image_update($packageRes,$data_img);
                    //     }
                    // }
                    //  echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($data);
                    //  exit();

                    if($packageRes == FALSE) {
                        $res['data']   =    'Already exists.';
                    } else{
                        if($packageRes > 0 && $imgRes >   0) {
                            $res['data']    =   'success';
                        } else {   
                                $res['data']    =   'failure';
                        }
                    }
                break;


                case 'edit' :
                    $newPackageRes   =   array();

                    if($_POST['hidRecID'] != 0) {
                        $hidRecID   =   $_POST['hidRecID'];
                        $packageRes  =   $this->Packages_model->getPackages($hidRecID);

                        if(!(empty($packageRes))) {
                            $res['data'] = 'success';

                            foreach($packageRes as $p) {
                                $newPackageRes['course_id'] =   $p->course_id;
                                $newPackageRes['batch_id'] =   $p->batch_id;
                                $newPackageRes['institute_id'] =   $p->institute_id;
                                $newPackageRes['package_name'] =   $p->package_name;
                                $newPackageRes['package_description'] =   $p->package_description;
                                $newPackageRes['package_start_date'] =   $p->package_start_date;
                                $newPackageRes['package_end_date'] =   $p->package_end_date;
                                $newPackageRes['package_cost'] =   $p->package_cost;
                                $newPackageRes['package_mrp'] =   $p->package_mrp;
                                $newPackageRes['package_is_free'] =   $p->package_is_free;
                                $newPackageRes['package_image_link'] =   $p->package_image_link;
                                $newPackageRes['status'] =   $p->status;
                            }
                        }
                        $res['packageRecord']    =   $newPackageRes;
                    }

                    break;

                 case 'update' :
                    //  echo "\r\n <br/> post vals for update : \r\n <br/><pre>"; print_r($_POST);
                    if($_POST['hidRecID'] != '') {
                        // Do the update... 
                        // call the same function which u do for adding new entry with slight modifications.
                        $newPackageRes   =   array();
                        $postData   =   array(
                            'id'    =>  $_POST['hidRecID'],
                            'course_id' =>  $_POST['courseId'],
                            'batch_id' =>  $_POST['batchId'],
                            'institute_id' =>  $_POST['instituteId'],
                            'package_name'   =>  $_POST['packageName'],
                            'package_description'   =>  $_POST['packageDesc'],
                            'package_start_date'   =>  $_POST['startDate'],
                            'package_end_date'   =>  $_POST['endDate'],
                            'package_cost'   =>  $_POST['packageCost'],
                            'package_mrp'  =>  $_POST['packageMrp'],
                            'status'    =>  $_POST['status'],
                            'updated_at'    =>  date('Y-m-d h:i:s'));
                        $packageRes  =   $this->Packages_model->add($postData);
                    //       echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($packageRes);
                    //  exit();

                        if($packageRes == 1) {                   
                            $res['data']    =   'success';     
                        } else {        
                            $res['data']    =   'failure';
                        }
                    }

                    break;
                

                case 'dele':
                    $newPackageRes   =   array();

                    if($_POST['hidRecID'] != 0) {
                        $packageRes  =   $this->Packages_model->delete($_POST['hidRecID']);

                        if($packageRes == 1) {
                            $res['data']    =   'success';        
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
                
                }   
            }
                
        echo json_encode($res);
    }
}
