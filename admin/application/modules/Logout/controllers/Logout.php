<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('Auth');
        $this->load->library('Enc_lib');
    }

	public function index() {
	    $this->auth->logout();
	    redirect('Login');
	}
}
