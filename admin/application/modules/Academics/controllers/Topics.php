<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Topics extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Chapters_model');
        $this->load->model('Modules_model');
        $this->load->model('Subjects_model');
        $this->load->model('Topics_model');
        $this->generalFn    =   new Generalfunctions();
    }

	public function index() {
        $data  =   array();
        $data['pageName'] =   'Topics List';
        $data['subjRes']    =   $this->Subjects_model->getList();
	    $this->load->template('Topics', 'templates/', 'Academics/Topics/', 'topicsList', $data);
    }

    function ajaxResultsForFiltersJSONGeneration(){
        $dataForFilter  =   array();
        $dataForFilter['subjectID'] =   $dataForFilter['moduleID'] =   $dataForFilter['chapterID']   =   $dataForFilter['topicName'] =   null;
        $activeFilters	=	'';

        if(!(empty($_POST['hidSearchFlag']))) {
            $newSearchArr	=	json_decode($_POST['hidSearchConcat']);
            $activeFilters	=	'';

            if(!(empty($newSearchArr))) {
                foreach ($newSearchArr as $key => $val) {
                    foreach ($val as $key1 =>  $val1) {
                        switch($key1) {
                            case 'subject' :
                                $subjDtl    =   explode('$$##$$', $val1);
                                $dataForFilter['subjectID'] =   $subjDtl[0];
                                break;

                            case 'module':
                                $modDtl    =   explode('$$##$$', $val1);
                                $dataForFilter['moduleID'] =   $modDtl[0];
                                break;
                            case 'chapter':
                                $chDtl    =   explode('$$##$$', $val1);
                                $dataForFilter['chapterID'] =   $chDtl[0];
                                $activeFilters	.=	'<li style="width:50% !important;">
														<button type="button" id="btnRemSubject" class="btn remBtn" style="width:16%;background:none;padding:0!important;">
															<i class="fa fa-remove mr-3" style="color:red;align-self:center;"></i>
														</button>'. ucfirst($key1). ' : <b>' . $subjDtl[1]. ' / ' . $modDtl[1].' / ' . $chDtl[1].'</b>&nbsp;</li>';
                                break;

                            case 'topicName' :
                                $dataForFilter['topicName'] =   trim($val1);
                                $activeFilters	.=	'<li>
														<button type="button" id="btnRemName" class="btn remBtn" style="width:16%;background:none;padding:0!important;">
															<i class="fa fa-remove mr-3" style="color:red;align-self:center;"></i>
														</button>'. ucfirst($key1). ' : <b>' . $val1.'</b>&nbsp;</li>';
                                break;
                        }
                    }
                }
            }
        }

        $toapters  =   $this->Topics_model->getList();
        $writeJSONData  =   $this->fetchDataForTopics($toapters);
        $results    =   array();
        $results['data']['success'] =   1;

        if($activeFilters == '') {
            $results['data']['activeFilters']	=	'<li><span class="text-semibold text-uppercase" style="valign:middle;">Active Filter&nbsp;:&nbsp;</span></li><li><span class="text-semibold text-uppercase" style="valign:middle;"> No Active Filters </span></li>';
        } else {
            $results['data']['activeFilters']	=	'<li><span class="text-semibold text-uppercase" style="valign:middle;">Active Filter&nbsp;:&nbsp;</span></li>'.$activeFilters;
        }

        $results['data']['writeJSONData']    =   $writeJSONData;
        echo json_encode($results['data']);
    }

    function fetchDataForTopics($toapters) {
        $newPosts   =   array();
        $cnt    =   0;

        foreach($toapters as $to) {
            $newPosts[$cnt]['SNo']  =   ($cnt + 1);
            $newPosts[$cnt]['Subject']  =  $to->subjectName;
            //$newPosts[$cnt]['Module']  =  $to->moduleName;
            $newPosts[$cnt]['Chapter']  =  $to->chapterName;
            $newPosts[$cnt]['Topic']  =  $to->topic_name;

            if($to->status    ==  'Active') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-success">Active</span>';
            } else if ($to->status    ==  'InActive') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-secondary">InActive</span>';
            } else {
                $newPosts[$cnt]['Status']   =   '<i class="fa fa-times text-dark"></i>';
            }

            $newPosts[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$to->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$to->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }

        return $newPosts;
    }

    function ajaxResultsForTopicsAction(){
        $res    =   array();
        $formCont   =   '';

        if($_POST['hidReqType'] != '') {
            // echo "\r\n <br/> hidReqType : ";$_POST['hidReqType'];exit();
            switch($_POST['hidReqType']) {
                case 'formList' :
                    $hidRecID   =   $chapterID  =   $moduleID  =   $subjectID  =   0;
                    $toName =   '';

                    if($_POST['hidRecID'] != '') {
                        $hidRecID   =   $_POST['hidRecID'];
                        $topicRes  =   $this->Topics_model->get($hidRecID);

                        if(!(empty($topicRes))) {
                            foreach($topicRes as $to) {
                                $subjectID  =   $to->subject_id;
                                $moduleID  =   $to->module_id;
                                $chapterID  =   $to->chapter_id;
                                $toName    =   $to->topic_name;
                                $statusFlag =   $to->status;
                            }
                        }
                    }

                    $subjRes    =   $this->Subjects_model->getList();
                    $formCont   =   '<form id="frmCommon" post="frmCommon" method="post" action="">
                                        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
                                        <input type="hidden" name="hidRecID" id="hidRecID" value="'.$hidRecID.'"/> 
                                        <div class="card">
                                            <div class="card-header"><h4>Topic Add / Edit </h4></div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="subject">Subject:&nbsp;<span class="text-danger">*</span></label>
                                                                <select data-placeholder="Select Subject" class="form-control form-input-styled" data-fouc name="lsSubjects" id="lsSubjects">
                                                                    <option value="">--Choose One--</option>';
                    if(!(empty($subjRes))) {
                        foreach($subjRes as $s) {
                            if($subjectID == $s->id) {
                                $sele   =   'selected="SELECTED"';
                            } else {
                                $sele   =   '';
                            }

                            $formCont   .=  '<option value="'.$s->id.'" ' . $sele.'>'.$s->subject_name.'</option>';
                        }
                    }

                    $formCont   .=  '                           </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="chapter">Chapter:&nbsp;<span class="text-danger">*</span></label>
                                                                <select data-placeholder="Select Chapter" class="form-control form-input-styled" data-fouc name="lsChapters" id="lsChapters">
                                                                    <option value="">--Choose One--</option>';
                    if($hidRecID != 0) {
                        $chapterRes =   $this->Chapters_model->get();

                        if(!(empty($chapterRes))) {
                            foreach($chapterRes as $ch) {
                                if($chapterID == $ch->id) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }

                                $formCont   .=  '<option value="'.$ch->id.'" ' . $sele.'>'.$ch->chapter_name.'</option>';
                            }
                        }
                    }

                    $formCont   .=  '                           </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="topicName">Topic Name:&nbsp;<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control" required name="topicName" id="topicName" value="'.$toName.'">
                                                            </div>
                                                        </div>';
                    if($hidRecID != 0) {
                        $formCont   .=  '               <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="status">Status:&nbsp;<span class="text-danger">*</span></label>
                                                                <select data-placeholder="Select Status" class="form-control form-input-styled" data-fouc name="lsStatus" id="lsStatus">
                                                                    <option value="">--Choose One--</option>';
                        $fEnums =   $this->generalFn->field_enums('chapters', 'status');

                        if(!(empty($fEnums))) {
                            foreach($fEnums as $fe){
                                if($statusFlag == $fe) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }

                                $formCont   .=  '<option value="'.$fe.'" '. $sele.'>'.$fe.'</option>';
                            }
                        }

                        $formCont   .=  '                       </select>
                                                            </div>
                                                        </div>';
                    }

                    $formCont   .=  '               </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>';
                    $res['data']    =   'success';
                    $res['formCont']    =   $formCont;
                    break;

                case 'create' :
                    if ( ($_POST['lsSubjects'] != '')  && ($_POST['lsChapters'] != '') &&  ($_POST['topicName'] != '') ) {
                        $tokRes =   $this->Topics_model->checkExists('topic_name', $_POST['lsSubjects'], 0, $_POST['lsChapters'], $_POST['topicName']);

                        if($tokRes == 0) {
                            $data   =   array(
                                        'subject_id'  =>  $_POST['lsSubjects'], 
                                        'module_id'  =>  0, //$_POST['lsModules'],
                                        'chapter_id'  =>  $_POST['lsChapters'],
                                        'topic_name'   =>  $_POST['topicName'],
                                        'created_at'    =>  date('Y-m-d h:i:s'),
                                        'status'    =>  'Active',);
                            $toRes  =   $this->Topics_model->add($data);

                            if($toRes > 0 ) {
                                $res['data']    =   'success';
                            } else {
                                $res['data']    =   'failure';
                            }
                        } else {
                            $res['data']    =   'Chapter already exists.';
                        }
                    }

                    break;

                case 'update' :
                    if ( ($_POST['hidRecID'] != '') && ($_POST['lsSubjects'] != '') && ($_POST['lsChapters'] != '') 
                        &&  ($_POST['topicName'] != '') ) {
                        $data   =   array(
                                        'id'    =>  $_POST['hidRecID'], 
                                        'subject_id'  =>  $_POST['lsSubjects'],
                                        'module_id'  =>  0, //$_POST['lsModules'],
                                        'chapter_id'  =>  $_POST['lsChapters'],
                                        'topic_name'   =>  $_POST['topicName'],
                                        'updated_at'    =>  date('Y-m-d h:i:s'),
                                        'status'    =>  $_POST['lsStatus'],);
                        $toRes  =   $this->Topics_model->add($data);

                        if($toRes > 0) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;

                case 'dele':
                    if($_POST['hidRecID'] != 0) {
                        $cRes  =   $this->Topics_model->delete($_POST['hidRecID']);

                        if($cRes == 1) {
                            $res['data']    =   'success';        
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
            }
        }

        echo json_encode($res);
    }    
}