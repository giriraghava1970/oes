<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Subjects extends MX_Controller {
    function __construct() {
        parent::__construct(); 
        $this->load->model('Subjects_model');
        $this->generalFn    =   new GeneralFunctions();
    }
	
	public function index() {
        $data  =   array();
        $data['pageName'] =   'Batches List';
	    $this->load->template('Subjects', 'templates/', 'Academics/Subjects/', 'subjectsList', $data);
    }

    function ajaxResultsForFiltersJSONGeneration(){
        $dataForFilter  =   array();
        $dataForFilter['subjectCode'] =   $dataForFilter['subjectName'] =   null;
        $activeFilters	=	'';

        if(!(empty($_POST['hidSearchFlag']))) {
            $newSearchArr	=	json_decode($_POST['hidSearchConcat']);
            $activeFilters	=	'';

            if(!(empty($newSearchArr))) {
                foreach ($newSearchArr as $key => $val) {
                    foreach ($val as $key1 =>  $val1) {
                        switch($key1) {
                            case 'subjectCode' :
                                $dataForFilter['subjectCode'] =   trim($val1);
                                $activeFilters	.=	'<li>
														<button type="button" id="btnRemSubjectCode" class="btn remBtn" style="width:16%;background:none;padding:0!important;">
															<i class="fa fa-remove mr-3" style="color:red;align-self:center;"></i>
														</button>'. ucfirst($key1). ' : <b>' . $val1.'</b>&nbsp;</li>';
                                break;

                            case 'subjectName' :
                                $dataForFilter['subjectName'] =   trim($val1);
                                $activeFilters	.=	'<li>
														<button type="button" id="btnRemSubjectName" class="btn remBtn" style="width:16%;background:none;padding:0!important;">
															<i class="fa fa-remove mr-3" style="color:red;align-self:center;"></i>
														</button>'. ucfirst($key1). ' : <b>' . $val1.'</b>&nbsp;</li>';
                                break;
                        }
                    }
                }
            }
        }

        $subjects  =   $this->Subjects_model->getList($dataForFilter);
        $writeJSONData  =   $this->fetchDataForSubjects($subjects);
        $results    =   array();
        $results['data']['success'] =   1;

        if($activeFilters == '') {
            $results['data']['activeFilters']	=	'<li><span class="text-semibold text-uppercase" style="valign:middle;">Active Filter&nbsp;:&nbsp;</span></li><li><span class="text-semibold text-uppercase" style="valign:middle;"> No Active Filters </span></li>';
        } else {
            $results['data']['activeFilters']	=	'<li><span class="text-semibold text-uppercase" style="valign:middle;">Active Filter&nbsp;:&nbsp;</span></li>'.$activeFilters;
        }

        $results['data']['writeJSONData']    =   $writeJSONData;
        //echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
    }

    function fetchDataForSubjects($subjects) {
        $newPosts   =   array();
        $cnt    =   0;

        foreach($subjects as $s) {
            $newPosts[$cnt]['SNo']  =   ($cnt + 1);
            $newPosts[$cnt]['Code']  =  $s->subject_code;
            $newPosts[$cnt]['Subject']  =  $s->subject_name;

            if($s->status    ==  'Active') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-success">Active</span>';
            } else if ($s->status    ==  'InActive') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-secondary">InActive</span>';
            } else {
                $newPosts[$cnt]['Status']   =   '<i class="fa fa-times text-dark"></i>';
            }

            $newPosts[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$s->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$s->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }

        return $newPosts;
    }

    function ajaxResultsForSubjectsAction(){
        $res    =   array();
        $formCont   =   '';

        if($_POST['hidReqType'] != '') {
            // echo "\r\n <br/> hidReqType : ";$_POST['hidReqType'];exit();
            switch($_POST['hidReqType']) {
                case 'formList' :
                    $hidRecID   =   $isCore  =   0;
                    $subjectName =    $subjectCode  =   $statusFlag =   '';

                    if($_POST['hidRecID'] != '') {
                        $hidRecID   =   $_POST['hidRecID'];
                        $subjecteRes  =   $this->Subjects_model->get($hidRecID);

                        if(!(empty($subjecteRes))) {
                            foreach($subjecteRes as $s) {
                                $subjectCode    =   $s->subject_code;
                                $subjectName    =   $s->subject_name;
                                $isCore =   $s->is_core;
                                $statusFlag =   $s->status;
                            }
                        }
                    }

                    $formCont   =   '<form id="frmCommon" post="frmCommon" method="post" action="">
                                        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
                                        <input type="hidden" name="hidRecID" id="hidRecID" value="'.$hidRecID.'"/> 
                                        <div class="card">
                                            <div class="card-header"><h4>Subject Add / Edit </h4></div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="subjectCode">Subject Code:<span class="text-red">*</span></label>
                                                            <input type="text" class="form-control" required name="subjectCode" id="subjectCode" value="'.$subjectCode.'">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="subjectName">Subject Name:<span class="text-red">*</span></label>
                                                            <input type="text" class="form-control" required name="subjectName" id="subjectName" value="'.$subjectName.'">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>&nbsp;</label><BR/>
                                                            <div class="form-check mb-0">
                        										<label class="form-check-label" style="left:1%">
                        											<input type="checkbox" id="cboIsCore" name="isCore" class="form-input-styled" value="'.$isCore.'">Core Subject ?
                        										</label>
                        									</div>
                                                        </div>
                                                    </div>';
                    if($hidRecID != 0) {
                        $formCont   .=  '           <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="status">Status:<span class="text-red">*</span></label>
                                                            <select data-placeholder="Select Status" class="form-control form-input-styled" data-fouc name="lsStatus" id="lsStatus">
                                                                <option value="">--Choose One--</option>';
                        $fEnums =   $this->generalFn->field_enums('subjects', 'status');

                        if(!(empty($fEnums))) {
                            foreach($fEnums as $fe){
                                if($statusFlag == $fe) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }

                                $formCont   .=  '<option value="'.$fe.'" '. $sele.'>'.$fe.'</option>';
                            }
                        }

                        $formCont   .=  '                   </select>
                                                        </div>
                                                    </div>';
                    }

                    $formCont   .=  '           </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>';
                    $res['data']    =   'success';
                    $res['formCont']    =   $formCont;
                    break;

                case 'create' :
                    $chkRes =   $this->Subjects_model->checkExists('subject_name', $_POST['subjectName']);

                    if($chkRes == 0) {
                        $data   =   array('subject_name'   =>  $_POST['subjectName'],);
                        $csRes  =   $this->Subjects_model->add($data);

                        if($csRes > 0 ) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }
                    } else {
                        $res['data']    =   'Subject already exists.';
                    }

                    break;

                case 'update' :
                    if($_POST['hidRecID'] != '') {
                        $data   =   array('id'    =>  $_POST['hidRecID'], 'subject_name'   =>  $_POST['subjectName'],);
                        $csRes  =   $this->Subjects_model->add($data);

                        if($csRes > 0) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;

                case 'dele':
                    if($_POST['hidRecID'] != 0) {
                        $cRes  =   $this->Subjects_model->delete($_POST['hidRecID']);

                        if($cRes == 1) {
                            $res['data']    =   'success';        
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
            }
        }

        echo json_encode($res);
    }    
}