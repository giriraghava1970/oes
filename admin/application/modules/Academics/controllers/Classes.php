<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Classes extends MX_Controller {
    function __construct() {
        parent::__construct(); 
        $this->load->model('Classes_model');
        $this->generalFn    =   new Generalfunctions();
    }

	public function index() {
        $data  =   array();
        $data['pageName'] =   'Batches List';
	    $this->load->template('Classes', 'templates/', 'Academics/Classes/', 'classesList', $data);
    }

    function ajaxResultsForFiltersJSONGeneration(){
        $dataForFilter  =   array();
        $dataForFilter['className'] =   null;

        if(!(empty($_POST['hidSearchFlag']))) {
            $newSearchArr	=	json_decode($_POST['hidSearchConcat']);
            $activeFilters	=	'';

            if(!(empty($newSearchArr))) {
                foreach ($newSearchArr as $key => $val) {
                    foreach ($val as $key1 =>  $val1) {
                        switch($key1) {
                            case 'name' :
                                $dataForFilter['className'] =   trim($val1);
                                break;
                        }
                    }
                }
            }
        }

        //echo "\r\n <br/> dataForFilter :\r\n <br/><pre>"; print_r($dataForFilter);
        $classes  =   $this->Classes_model->getList($dataForFilter);
        
        //  echo "\r\n <br/> Result Of batches :<pre>";print_r($classes);exit();
        $writeJSONData  =   $this->fetchDataForClasses($classes);
        $results    =   array();
        $results['data']['success'] =   1;
        $results['data']['writeJSONData']    =   $writeJSONData;
        //echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
    }

    function fetchDataForClasses($classes) {
        $newPosts   =   array();
        $cnt    =   0;

        foreach($classes as $c) {
            $newPosts[$cnt]['SNo']  =   ($cnt + 1);
            $newPosts[$cnt]['Class Name']  =  $c->class_name;

            if($c->status    ==  'Active') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-success">Active</span>';
            } else if ($c->status    ==  'InActive') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-secondary">InActive</span>';
            } else {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-warning">None</span>';
            }

            $newPosts[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$c->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$c->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }

        return $newPosts;
    }

    function ajaxResultsForClassesAction(){
        $res    =   array();
        $formCont   =   '';

        if($_POST['hidReqType'] != '') {
            // echo "\r\n <br/> hidReqType : ";$_POST['hidReqType'];exit();
            switch($_POST['hidReqType']) {
                case 'formList' :
                    $hidRecID   =   0;
                    $className =    $statusFlag =   '';

                    if($_POST['hidRecID'] != '') {
                        $hidRecID   =   $_POST['hidRecID'];
                        $classRes  =   $this->Classes_model->get($hidRecID);

                        if(!(empty($classRes))) {
                            foreach($classRes as $c) {
                                $className =   $c->class_name;
                                $statusFlag =   $c->status;
                            }
                        }
                    }

                    $formCont   =   '<form id="frmCommon" post="frmCommon" method="post" action="">
                                        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
                                        <input type="hidden" name="hidRecID" id="hidRecID" value="'.$hidRecID.'"/> 
                                        <div class="card">
                                			<div class="row"><!-- class="row" -->
                                				<div class="col-lg-12"><!-- class="col-lg-12" -->
                                					<div class="navbar navbar-expand-xl navbar-dark bg-indigo-400 navbar-component rounded-top mb-0"> <!-- class="navbar navbar-expand-xl navbar-light navbar-component rounded-top mb-0" -->
                                						<div class="d-xl-none">
                                							<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-demo-dark">
                                								<i class="icon-menu"></i>
                                							</button>
                                						</div>
                                						<div class="navbar-collapse collapse" id="navbar-demo-dark" style="border:0px !important;">
                                							<h6 style="color:#ff;">Classes Add / Edit</h6>
                                	                   	</div>
                                	                </div>
                                	            </div>
                                	        </div>
                                			<div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="className">Class Name:<span class="text-red">*</span></label>
                                                            <input type="text" class="form-control" required name="className" id="className" value="'.$className.'">
                                                        </div>
                                                    </div>';
                    if($hidRecID != 0) {
                        $formCont   .=  '           <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="status">Status:<span class="text-red">*</span></label>
                                                            <select data-placeholder="Select Status" class="form-control form-input-styled" data-fouc name="lsStatus" id="lsStatus">
                                                                <option value="">--Choose One--</option>';
                        $fEnums =   $this->generalFn->field_enums('classes', 'status');

                        if(!(empty($fEnums))) {
                            foreach($fEnums as $fe){
                                if($statusFlag == $fe) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }

                                $formCont   .=  '<option value="'.$fe.'" '. $sele.'>'.$fe.'</option>';
                            }
                        }

                        $formCont   .=  '                    </select>
                                                        </div>
                                                    </div>';
                    }

                    $formCont   .=   '          </div>
                                            </div>
                                        </div>
                                    </form>';
                    $res['data']    =   'success';
                    $res['formCont']    =   $formCont;
                    break;

                case 'create' :
                    $chkRes =   $this->Classes_model->checkExists('class_name', $_POST['className']);

                    if($chkRes == 0) {
                        $data   =   array(
                                    'class_name'   =>  $_POST['className'], 
                                    'status' => 'Active', 
                                    'created_at' => date('Y-m-d h:i:s'),
                                    'created_by'    =>  1,);
                        $csRes  =   $this->Classes_model->add($data);

                        if($csRes > 0 ) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }
                    } else {
                        $res['data']    =   'Class already exists.';
                    }

                    break;

                case 'update' :
                    if($_POST['hidRecID'] != '') {
                        $data   =   array(
                                        'id'    =>  $_POST['hidRecID'], 
                                        'class_name'   =>  $_POST['className'], 
                                        'status' => $_POST['lsStatus'],
                                        'updated_at'    =>  date('Y-m-d h:i:s'),);
                        $csRes  =   $this->Classes_model->add($data);

                        if($csRes > 0) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;

                case 'dele':
                    if($_POST['hidRecID'] != 0) {
                        $cRes  =   $this->Classes_model->delete($_POST['hidRecID']);

                        if($cRes == 1) {
                            $res['data']    =   'success';        
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
            }
        }

        echo json_encode($res);
    }    
}