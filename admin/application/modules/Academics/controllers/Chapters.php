<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Chapters extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Chapters_model');
        $this->load->model('Modules_model');
        $this->load->model('Subjects_model');
        $this->generalFn    =   new Generalfunctions();
    }

	public function index() {
        $data  =   array();
        $data['pageName'] =   'Chapters List';
        $data['subjRes']    =   $this->Subjects_model->getList();
	    $this->load->template('Chapters', 'templates/', 'Academics/Chapters/', 'chaptersList', $data);
    }

    function ajaxResultsForFiltersJSONGeneration(){
        $dataForFilter  =   array();
        $dataForFilter['subjectID'] =   $dataForFilter['moduleID'] =   $dataForFilter['chapterName'] =   null;
        $activeFilters	=	'';

        if(!(empty($_POST['hidSearchFlag']))) {
            $newSearchArr	=	json_decode($_POST['hidSearchConcat']);
            $activeFilters	=	'';

            if(!(empty($newSearchArr))) {
                foreach ($newSearchArr as $key => $val) {
                    foreach ($val as $key1 =>  $val1) {
                        switch($key1) {
                            case 'subject' :
                                $subjDtl    =   explode('$$##$$', $val1);
                                $dataForFilter['subjectID'] =   $subjDtl[0];
                                break;

                            case 'module':
                                $modDtl    =   explode('$$##$$', $val1);
                                $dataForFilter['moduleID'] =   $modDtl[0];
                                $activeFilters	.=	'<li style="width:50% !important;">
														<button type="button" id="btnRemSubject" class="btn remBtn" style="width:16%;background:none;padding:0!important;">
															<i class="fa fa-remove mr-3" style="color:red;align-self:center;"></i>
														</button>'. ucfirst($key1). ' : <b>' . $subjDtl[1]. ' / ' . $modDtl[1].'</b>&nbsp;</li>';
                                break;

                            case 'chapterName' :
                                $dataForFilter['chapterName'] =   trim($val1);
                                $activeFilters	.=	'<li>
														<button type="button" id="btnRemName" class="btn remBtn" style="width:16%;background:none;padding:0!important;">
															<i class="fa fa-remove mr-3" style="color:red;align-self:center;"></i>
														</button>'. ucfirst($key1). ' : <b>' . $val1.'</b>&nbsp;</li>';
                                break;
                        }
                    }
                }
            }
        }

        $chapters  =   $this->Chapters_model->getList($dataForFilter);
        $writeJSONData  =   $this->fetchDataForChapters($chapters);
        $results    =   array();
        $results['data']['success'] =   1;

        if($activeFilters == '') {
            $results['data']['activeFilters']	=	'<li><span class="text-semibold text-uppercase" style="valign:middle;">Active Filter&nbsp;:&nbsp;</span></li><li><span class="text-semibold text-uppercase" style="valign:middle;"> No Active Filters </span></li>';
        } else {
            $results['data']['activeFilters']	=	'<li><span class="text-semibold text-uppercase" style="valign:middle;">Active Filter&nbsp;:&nbsp;</span></li>'.$activeFilters;
        }

        $results['data']['writeJSONData']    =   $writeJSONData;
        echo json_encode($results['data']);
    }

    function fetchDataForChapters($chapters) {
        $newPosts   =   array();
        $cnt    =   0;

        foreach($chapters as $ch) {
            $newPosts[$cnt]['SNo']  =   ($cnt + 1);
            $newPosts[$cnt]['Subject']  =  $ch->subjectName;
            //$newPosts[$cnt]['Module']  =  $ch->moduleName;
            $newPosts[$cnt]['Chapter']  =  $ch->chapter_name;

            if($ch->status    ==  'Active') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-success">Active</span>';
            } else if ($ch->status    ==  'InActive') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-secondary">InActive</span>';
            } else {
                $newPosts[$cnt]['Status']   =   '<i class="fa fa-times text-dark"></i>';
            }

            $newPosts[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$ch->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$ch->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }

        return $newPosts;
    }

    function ajaxResultsForChaptersAction(){
        $res    =   array();
        $formCont   =   '';

        if($_POST['hidReqType'] != '') {
            // echo "\r\n <br/> hidReqType : ";$_POST['hidReqType'];exit();
            switch($_POST['hidReqType']) {
                case 'formList' :
                    $hidRecID   =   $moduleID  =   $subjectID  =   0;
                    $chName =   '';

                    if($_POST['hidRecID'] != '') {
                        $hidRecID   =   $_POST['hidRecID'];
                        $chaptersRes  =   $this->Chapters_model->get($hidRecID);

                        if(!(empty($chaptersRes))) {
                            foreach($chaptersRes as $ch) {
                                $subjectID  =   $ch->subject_id;
                                $moduleID  =   $ch->module_id;
                                $chName    =   $ch->chapter_name;
                                $statusFlag =   $ch->status;
                            }
                        }
                    }

                    $subjRes    =   $this->Subjects_model->getList();
                    $formCont   =   '<form id="frmCommon" post="frmCommon" method="post" action="">
                                        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
                                        <input type="hidden" name="hidRecID" id="hidRecID" value="'.$hidRecID.'"/> 
                                        <div class="card">
                                            <div class="card-header"><h4>Chapter Add / Edit </h4></div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="subject">Subject:<span class="text-red">*</span></label>
                                                                <select data-placeholder="Select Subject" class="form-control form-input-styled" data-fouc name="lsSubjects" id="lsSubjects">
                                                                    <option value="">--Choose One--</option>';
                    if(!(empty($subjRes))) {
                        foreach($subjRes as $s) {
                            if($subjectID == $s->id) {
                                $sele   =   'selected="SELECTED"';
                            } else {
                                $sele   =   '';
                            }

                            $formCont   .=  '<option value="'.$s->id.'" ' . $sele.'>'.$s->subject_name.'</option>';
                        }
                    }

                    $formCont   .=  '                           </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="chapterName">Chapter Name:<span class="text-red">*</span></label>
                                                                <input type="text" class="form-control" required name="chapterName" id="chapterName" value="'.$chName.'">
                                                            </div>
                                                        </div>';
                    if($hidRecID != 0) {
                        $formCont   .=  '               <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="status">Status:<span class="text-red">*</span></label>
                                                                <select data-placeholder="Select Status" class="form-control form-input-styled" data-fouc name="lsStatus" id="lsStatus">
                                                                    <option value="">--Choose One--</option>';
                        $fEnums =   $this->generalFn->field_enums('chapters', 'status');

                        if(!(empty($fEnums))) {
                            foreach($fEnums as $fe){
                                if($statusFlag == $fe) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }

                                $formCont   .=  '<option value="'.$fe.'" '. $sele.'>'.$fe.'</option>';
                            }
                        }

                        $formCont   .=  '                       </select>
                                                            </div>
                                                        </div>';
                    }

                    $formCont   .=  '               </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>';
                    $res['data']    =   'success';
                    $res['formCont']    =   $formCont;
                    break;

                case 'create' :
                    if ( ($_POST['lsSubjects'] != '') && ($_POST['chapterName'] != '') ) {
                        $chkRes =   $this->Chapters_model->checkExists('chapter_name', $_POST['chapterName']);

                        if($chkRes == 0) {
                            $data   =   array(
                                        'subject_id'  =>  $_POST['lsSubjects'], 
                                        'module_id'  =>  0,//$_POST['lsModules'], 
                                        'chapter_name'   =>  $_POST['chapterName'],
                                        'created_at'    =>  date('Y-m-d h:i:s'),
                                        'status'    =>  'Active',);
                            $chRes  =   $this->Chapters_model->add($data);

                            if($chRes > 0 ) {
                                $res['data']    =   'success';
                            } else {
                                $res['data']    =   'failure';
                            }
                        } else {
                            $res['data']    =   'Chapter already exists.';
                        }
                    }

                    break;

                case 'update' :
                    if ( ($_POST['hidRecID'] != '')  && ($_POST['lsSubjects'] != '') && ($_POST['chapterName'] != '') ) {
                        $data   =   array(
                                        'id'    =>  $_POST['hidRecID'], 
                                        'subject_id'  =>  $_POST['lsSubjects'],
                                        'module_id'  =>  0, //$_POST['lsModules'],
                                        'chapter_name'   =>  $_POST['chapterName'],
                                        'updated_at'    =>  date('Y-m-d h:i:s'),
                                        'status'    =>  $_POST['lsStatus'],);
                        $chRes  =   $this->Chapters_model->add($data);

                        if($chRes > 0) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;

                case 'dele':
                    if($_POST['hidRecID'] != 0) {
                        $cRes  =   $this->Chapters_model->delete($_POST['hidRecID']);

                        if($cRes == 1) {
                            $res['data']    =   'success';        
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
            }
        }

        echo json_encode($res);
    }    
}