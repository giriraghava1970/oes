<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Batches extends MX_Controller {
    function __construct() {
        parent::__construct(); 
        $this->load->model('Batches_model');
    }
	
	public function index() {
        $data  =   array();
        $data['pageName'] =   'Batches List';
	    $this->load->template('Batch', 'templates/', 'Academics/Batches/', 'batchesList', $data);
    }

    function ajaxResultsForUsersJSON(){
        $res    =   array();

        if( ($_POST['name']) && ($_POST['email']) && ($_POST['image']) && ($_POST['role'])) {
            //Connect to model by passing the username and pwd values.. Validate whether true or false.
            $data   =   array(
                        'name'    =>  $_POST['name'], 
                        'email'   =>  $_POST['email'], 
                        'image' => $_POST['image'],
                        'role_id'   =>  $_POST['role']);
                        // 'settings_data' =>  $_POST['settingsData']);
            $usersRes   =   $this->Batches_model->add($data);

            if($usersRes != FALSE) {
                if($usersRes > 0) {
                    // Set Session for the logged in user later.
                    $res['data']    =   'success';
                } else {
                    $res['data']   =    'failure';
                }
            } else {
                $res['data']   =    'User Already exists.';
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForFiltersJSONGeneration(){
        $batches  =   $this->Batches_model->get();
        //  echo "\r\n <br/> Result Of batches :<pre>";print_r($batches);exit();
        $writeJSONData  =   $this->fetchDataForbatches($batches);
        $results    =   array();
        $results['data']['success'] =   1;
        $results['data']['writeJSONData']    =   $writeJSONData;
        //echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
    }

    function fetchDataForbatches($batches) {
        $newUser   =   array();
        $cnt    =   0;

        foreach($batches as $user) {
            $newUser[$cnt]['SNo']  =   ($cnt + 1);
            $newUser[$cnt]['Course Name']  =  $this->Batches_model->getCourses($user->course_id);
            $newUser[$cnt]['Batch Name']   =   $user->batch_name;
            $newUser[$cnt]['Start Date']   =   $user->start_date;
            $newUser[$cnt]['End Date']   =   $user->end_date;
        
            if($user->status    ==  'Active') {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-success">
                Active</span>';//'<i class="fa fa-check text-success"></i>';
            } else if ($user->status    ==  'InActive') {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-grey">InActive</span>';
            } else {
                $newUser[$cnt]['Status']   =   '<i class="fa fa-times text-dark"></i>';
            }

            $newUser[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$user->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$user->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }
        return $newUser;
    }
    function checkFieldExists() {
        $res    =   array();
   
        if( (!(empty($_POST['name']))) && (!(empty($_POST['value'])))) {
            $userRes    =   $this->Batches_model->checkField($_POST['name'], $_POST['value']);

            if($userRes == 1) {
                $res['data']    =   'failure';
            } else {
                $res['data']    =   'success';
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForBatchesAction(){
        $res    =   array();
        $formCont   =   '';

        if($_POST['hidReqType'] != '') {
            // echo "\r\n <br/> hidReqType : ";$_POST['hidReqType'];exit();
            switch($_POST['hidReqType']) {
                case 'formList' :
                    $hidRecID   =  0;
                    $courseName =   $batchName =   $startDate =   $endDate =   '';

                    if($_POST['hidRecID'] != '') {
                        $hidRecID   =   $_POST['hidRecID'];
                        $batchRes  =   $this->Batches_model->get($hidRecID);

                        if(!(empty($batchRes))) {
                            foreach($batchRes as $c) {
                                //Declare variables atop and assign data from DB to those variables.
                                $courseName =   $c->course_id;
                                $batchName =   $c->batch_name;
                                $startDate =   $c->start_date;
                                $endDate =   $c->end_date;
                            }
                        }
                        $stat  =   $this->Batches_model->getStatus();
                        $data['stat'] =   $stat;
                        // echo "\r\n <br/> Data Status : <pre>";print_r($data);
                        $formCont   = 
'<form id="frmCommon" post="frmCommon" method="post" action="">
    <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
    <input type="hidden" name="hidRecID" id="hidRecID" value="'.$hidRecID.'"/> 
        <div class="card">
            <div class="card-header"><h4>Batch Add / Edit </h4></div>
                <div class="card-body">
                    <div class="row">
                    <div class="col-md-4">
                    <div class="form-group">
                        <label for="status">Select Course<span class="text-red">*</span></label>
                        <select id="courseName" name="courseName" class="form-control" required>
                        <option value="">Select Course</option>';
                        $courseRes  =   $this->Batches_model->getCourses();
                        // echo "<pre> :";print_r($courseRes);exit();
         foreach ($courseRes as $stat) {
            if($courseName == $stat->id){
                $sele   =   'selected="SELECTED"';
            } else {
                $sele   =   '';
            }

            $formCont .= ' <option value="'. $stat->id .'" '. $sele.'>'.$stat->course_name.'</option>';
        }
    $formCont .=         '</select>
                    </div>
                </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="batchName">Batch Name:<span class="text-red">*</span></label>
                                <input type="text" class="form-control" required name="batchName" id="batchName" value="'.$batchName.'">
                            </div>
                        </div>
                         <div class="col-md-4">
                            <div class="form-group">
                                <label for="startDate">Start Date:<span class="text-red">*</span></label>
                                <input type="date" class="form-control" required name="startDate" id="startDate" value="'.$startDate.'">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="batchName">End Date:<span class="text-red">*</span></label>
                                <input type="date" class="form-control" required name="endDate" id="endDate" value="'.$endDate.'">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</form>';
                    }

                    $res['data']    =   'success';
                    $res['formCont']    =   $formCont;
                    break;

                case 'create' :
                // echo "\r\n <br/> Post Value: <pre>";print_r($_POST);
                // echo "\r\n <br/> Image Value: <pre>";print_r($_FILES);exit();
                    // echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($_POST);exit();
                    $data   =   array(
                                'course_id'   =>  $_POST['courseName'], 
                                'batch_name' => $_POST['batchName'],
                                'start_date'  =>  $_POST['startDate'],
                                'end_date'  =>  $_POST['endDate'],
                    );
                    // echo "\r\n <br/> Data :";print_r($data);exit();
                    $usersRes  =   $this->Batches_model->add($data);
                  
                    if($usersRes > 0 ) {
                        $res['data']    =   'success';
                    } else {
                        $res['data']    =   'failure';
                    }

                    break;

                case 'update' :
                    // echo "\r\n <br/> post vals for update : \r\n <br/><pre>"; print_r($_POST);exit();
                    if($_POST['hidRecID'] != '') {
                        $data   =   array(
                            'id'    =>  $_POST['hidRecID'],
                            'course_id'    =>  $_POST['courseName'], 
                            'course_id'   =>  $_POST['courseName'], 
                            'batch_name' => $_POST['batchName'],
                            'start_date'  =>  $_POST['startDate'],
                            'end_date'  =>  $_POST['endDate']);
                            // 'status'  =>  $_POST['status']);
                        $usersRes  =   $this->Batches_model->add($data);
                        
                        if($usersRes > 0) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }

                    }

                    break;

                case 'dele':
                    if($_POST['hidRecID'] != 0) {
                        $userRes  =   $this->Batches_model->delete($_POST['hidRecID']);

                        if($userRes == 1) {
                            $res['data']    =   'success';        
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
            }
        }

        echo json_encode($res);
    }
    
}