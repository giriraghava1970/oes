<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Courses extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Classes_model');
        $this->load->model('Courses_model');
        $this->load->model('CourseDetails_model');
        $this->load->model('Levels_model');
        $this->load->model('QuestionBankTypes_model');
        $this->load->model('QuestionCategories_model');
        $this->load->model('QuestionTypeMaster_model');
    }

	public function index() {
        $data['pageName'] =   'Courses List';
	    $this->load->template('Course', 'templates/', 'Academics/Courses/', 'coursesList', $data);
    }

    function ajaxResultsForUsersJSON(){
        $res    =   array();

        if( ($_POST['name']) && ($_POST['email']) && ($_POST['image']) && ($_POST['role'])) {
            //Connect to model by passing the username and pwd values.. Validate whether true or false.
            $data   =   array(
                        'name'    =>  $_POST['name'], 
                        'email'   =>  $_POST['email'], 
                        'image' => $_POST['image'],
                        'role_id'   =>  $_POST['role']);
                        // 'settings_data' =>  $_POST['settingsData']);
            $courseRes   =   $this->Courses_model->add($data);

            if($courseRes != FALSE) {
                if($courseRes > 0) {
                    // Set Session for the logged in user later.
                    $res['data']    =   'success';
                } else {
                    $res['data']   =    'failure';
                }
            } else {
                $res['data']   =    'User Already exists.';
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForFiltersJSONGeneration(){
        $courses  =   $this->Courses_model->get();
        // echo "\r\n <br/> Result Of Courses :<pre>";print_r($courses);exit();
        $writeJSONData  =   $this->fetchDataForCourses($courses);
        $results    =   array();
        $results['data']['success'] =   1;
        $results['data']['writeJSONData']    =   $writeJSONData;
        //echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
    }

    function fetchDataForCourses($courses) {
        $newPosts   =   array();
        $cnt    =   0;

        foreach($courses as $user) {
            $newPosts[$cnt]['SNo']  =   ($cnt + 1);
            $newPosts[$cnt]['Course Code']  =   $user->course_code;
            $newPosts[$cnt]['Course Name']   =   $user->course_name;
            $newPosts[$cnt]['Course Description']   =   $user->course_desc;

            if($user->status    ==  'Active') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-success">Active</span>';
            } else if ($user->status    ==  'InActive') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-grey">InActive</span>';
            } else {
                $newPosts[$cnt]['Status']   =   '<i class="fa fa-times text-dark"></i>';
            }

            $newPosts[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$user->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$user->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }

        return $newPosts;
    }

    function checkFieldExists() {
        $res    =   array();
   
        if( (!(empty($_POST['name']))) && (!(empty($_POST['value'])))) {
            $courseRes    =   $this->Courses_model->checkField($_POST['name'], $_POST['value']);

            if($courseRes == 1) {
                $res['data']    =   'failure';
            } else {
                $res['data']    =   'success';
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForCoursesAction(){
        $res    =   array();
        $formCont   =   '';

        if($_POST['hidReqType'] != '') {
            // echo "\r\n <br/> hidReqType : ";$_POST['hidReqType'];exit();
            switch($_POST['hidReqType']) {
                case 'formList' :
                    $hidRecID   =  0;
                    $courseCode =   $courseName =   $courseDesc =   $status =   '';
                    $classIDs   =   $qBankIDs   =   $qCatIDs    =   $levelIDs   =   $qTypeIDs   =   '';

                    if($_POST['hidRecID'] != '') {
                        $hidRecID   =   $_POST['hidRecID'];
                        $courseRes  =   $this->Courses_model->get($hidRecID);

                        if(!(empty($courseRes))) {
                            foreach($courseRes as $c) {
                                //Declare variables atop and assign data from DB to those variables.
                                $courseCode =   $c->course_code;
                                $courseName =   $c->course_name;
                                $courseDesc =   $c->course_desc;
                                $status =   $c->status;
                            }

                            //Sub Table Info based on Course Edit.
                            $courseDtlsRes  =   $this->CourseDetails_model->get($hidRecID);

                            if(!(empty($courseDtlsRes))) {
                                foreach($courseDtlsRes as $cD) {
                                    $classIDs   =   json_decode($cD->class_id);
                                    $qBankIDs   =   json_decode($cD->questionbank_types_id);
                                    $qCatIDs    =   json_decode($cD->question_category_id);
                                    $levelIDs   =   json_decode($cD->level_id);
                                    $qTypeIDs   =   json_decode($cD->question_types_id);
                                }
                            }
                        }
                    }

                    $classRes   =   $this->Classes_model->get();
                    $questionBankRes    =   $this->QuestionBankTypes_model->get();
                    $questionCatRes =   $this->QuestionCategories_model->get();
                    $levelRes   =   $this->Levels_model->get();
                    $questionTypesRes   =   $this->QuestionTypeMaster_model->get();
                    $formCont   =   '<form id="frmCommon" post="frmCommon" method="post" action="">
                                        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
                                        <input type="hidden" name="hidRecID" id="hidRecID" value="'.$hidRecID.'"/> 
                                        <div class="card">
                                            <div class="card-header"><h4>Courses Add / Edit </h4></div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="courseCode">Class:<span class="text-red">*</span></label>
                                                                <select d1ata-placeholder="Select Class" class="form-control form-input-styled" data-fouc name="lsClasses[]" id="lsClasses" multiple style="height:100px !important;">
                                                                    <option value="">--Choose One--</option>';
                    if(!(empty($classRes))) {
                        foreach($classRes as $cl) {
                            if(!(empty($classIDs))) {
                                if(in_array($cl->id, $classIDs)) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }
                            } else {
                                $sele   =   '';
                            }

                            $formCont   .=  '<option value="'.$cl->id.'" ' . $sele.'>'.$cl->class_name.'</option>';
                        }
                    }

                    $formCont   .=  '                           </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="questionBankType">Question Bank:<span class="text-red">*</span></label>
                                                                <select data-placeholder="Select Question Bank Type" class="form-control form-input-styled" data-fouc name="lsQuestionBankTypes[]" id="lsQuestionBankTypes" multiple style="height:100px !important;">
                                                                    <option value="">--Choose One--</option>';
                    if(!(empty($questionBankRes))) {
                        foreach($questionBankRes as $qB) {
                            if(!(empty($qBankIDs))) {
                                if(in_array($qB->id, $qBankIDs)) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }
                            } else {
                                $sele   =   '';
                            }

                            $formCont   .=  '<option value="'.$qB->id.'" ' . $sele.'>'.$qB->questionbank_type_name.'</option>';
                        }
                    }

                    $formCont   .=  '                           </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="level">Level:<span class="text-red">*</span></label>
                                                                <select data-placeholder="Select Question Category" class="form-control form-input-styled" data-fouc name="lsLevels[]" id="lsLevels" multiple style="height:100px !important;">
                                                                    <option value="">--Choose One--</option>';
                    if(!(empty($levelRes))) {
                        foreach($levelRes as $lev) {
                            if(!(empty($levelIDs))) {
                                if(in_array($lev->id, $levelIDs)) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }
                            } else {
                                $sele   =   '';
                            }

                            $formCont   .=  '<option value="'.$lev->id.'" '. $sele.'>'.$lev->level_name.'</option>';
                        }
                    }

                    $formCont   .=  '                           </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="questionCategory">Question Category:<span class="text-red">*</span></label>
                                                                <select data-placeholder="Select Question Category" class="form-control form-input-styled" data-fouc name="lsQuestionCategories[]" id="lsQuestionCategories" multiple style="height:100px !important;">
                                                                    <option value="">--Choose One--</option>';
                    if(!(empty($questionCatRes))) {
                        foreach($questionCatRes as $qC) {
                            if(!(empty($qCatIDs))) {
                                if(in_array($qC->id, $qCatIDs)) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }
                            } else {
                                $sele   =   '';
                            }

                            $formCont   .=  '<option value="'.$qC->id.'" '. $sele.'>'.$qC->category_name.'</option>';
                        }
                    }

                    $formCont   .=  '                           </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="questionType">Question Type:<span class="text-red">*</span></label>
                                                                <select data-placeholder="Select Question Type" class="form-control form-input-styled" data-fouc name="lsQuestionTypes[]" id="lsQuestionTypes" multiple style="height:100px !important;">
                                                                    <option value="">--Choose One--</option>';
                    if(!(empty($questionTypesRes))) {
                        foreach($questionTypesRes as $qT) {
                            if(!(empty($qTypeIDs))) {
                                if(in_array($qT->id, $qTypeIDs)) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }
                            } else {
                                $sele   =   '';
                            }

                            $formCont   .=  '<option value="'.$qT->id.'" ' . $sele.'>'.$qT->question_type_name.'</option>';
                        }
                    }

                    $formCont   .=  '                           </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="courseCode">Course Code:<span class="text-red">*</span></label>
                                                                <input type="text" class="form-control" required name="courseCode" id="courseCode" value="'.$courseCode.'">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="courseName">Course Name:<span class="text-red">*</span></label>
                                                                <input type="text" class="form-control" required name="courseName" id="courseName" value="'.$courseName.'">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="courseName">Course Description:</label>
                                                                <textarea name="courseDesc" class="form-control">'.$courseDesc.'</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>';
                    $res['data']    =   'success';
                    $res['formCont']    =   $formCont;
                    break;

                case 'create' :
                    //echo "\r\n <br/> postVals : \r\n <br/><pre>"; print_r($_POST);echo '</pre>';
                    if($_POST['courseName'] != '') {
                        //Check if Course already Exists
                        $chkRes =   $this->Courses_model->checkExists('course_name', $_POST['courseName']);

                        if($chkRes == false) {
                            $data   =   array(
                                        'course_code'    =>  isset($_POST['courseCode']) ? $_POST['courseCode'] : '', 
                                        'course_name'   =>  $_POST['courseName'], 
                                        'course_desc' => $_POST['courseDesc'],
                                        'status'    =>  'Active');
                            $courseRes  =   $this->Courses_model->add($data);

                            if($courseRes > 0 ) {
                                // Add logic for Sub Table with the other fields set and course ID
                                $data   =   array(
                                                'course_id' =>  $courseRes,
                                                'class_id'  =>  json_encode($_POST['lsClasses']),
                                                'questionbank_types_id'  =>  json_encode($_POST['lsQuestionBankTypes']),
                                                'question_category_id'  =>  json_encode($_POST['lsQuestionCategories']),
                                                'level_id'  =>  json_encode($_POST['lsLevels']),
                                                'question_types_id'  =>  json_encode($_POST['lsQuestionTypes']),
                                                'created_at'    =>  date('Y-m-d h:i:s'),
                                                'created_by'=>  1,);
                                $courseDtlsRes  =   $this->CourseDetails_model->add($data);
                                if($courseDtlsRes > 0) {
                                    $res['data']    =   'success';
                                } else {
                                    $res['data']    =   'Error in storing the Details of the course.';
                                }
                            } else {
                                $res['data']    =   'Error in storing the Course information';
                            }
                        } else {
                            $res['data']    =   'Course Name already exists..';
                        }
                    }

                    break;

                case 'update' :
                    // echo "\r\n <br/> post vals for update : \r\n <br/><pre>"; print_r($_POST);exit();
                    if($_POST['hidRecID'] != '') {
                        //Delete the existing record and recreate afresh.
                        $courseDtlsRes  =   $this->CourseDetails_model->delete($_POST['hidRecID']);

                        if($courseDtlsRes == 1) {
                            $data   =   array(
                                'id'    =>  $_POST['hidRecID'],
                                'course_code'    =>  $_POST['courseCode'], 
                                'course_name'   =>  $_POST['courseName'], 
                                'course_desc' => $_POST['courseDesc'],
                                'updated_at'    =>  date('Y-m-d h:i:s'),);
                            $courseRes  =   $this->Courses_model->add($data);
    
                            if($courseRes == 1) {
                                //Delete the existing record(s) based on the course id and recreate the same.
                                // Add logic for Sub Table with the other fields set and course ID
                                $data   =   array(
                                    'course_id' =>  $_POST['hidRecID'],
                                    'class_id'  =>  json_encode($_POST['lsClasses']),
                                    'questionbank_types_id'  =>  json_encode($_POST['lsQuestionBankTypes']),
                                    'question_category_id'  =>  json_encode($_POST['lsQuestionCategories']),
                                    'level_id'  =>  json_encode($_POST['lsLevels']),
                                    'question_types_id'  =>  json_encode($_POST['lsQuestionTypes']),
                                    'created_at'    =>  date('Y-m-d h:i:s'),
                                    'created_by'=>  1,);
                                $courseDtlsRes  =   $this->CourseDetails_model->add($data);

                                if($courseDtlsRes > 0) {
                                    $res['data']    =   'success';
                                } else {
                                    $res['data']    =   'Error in storing the Details of the course.';
                                }
                            } else {
                                $res['data']    =   'failure';
                            }
                        } else {
                            $res['data']    =   'Error in deleting the existing course details and recreate';
                        }
                    }

                    break;

                case 'dele':
                    if($_POST['hidRecID'] != 0) {
                        $courseRes  =   $this->Courses_model->delete($_POST['hidRecID']);

                        if($courseRes == 1) {
                            //Delete the course details sub table
                            $courseDtlsRes  =   $this->CourseDetails_model->delete($_POST['hidRecID']);
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
            }
        }

        echo json_encode($res);
    }
    
}