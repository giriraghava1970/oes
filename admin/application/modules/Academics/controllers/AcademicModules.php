<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class AcademicModules extends MX_Controller {
    function __construct() {
        parent::__construct(); 
        $this->load->model('Modules_model');
        $this->load->model('Subjects_model');
        $this->generalFn    =   new Generalfunctions();
    }

	public function index() {
        $data  =   array();
        $data['subjRes']    =   $this->Subjects_model->getList();
        $data['pageName'] =   'Modules List';
	    $this->load->template('Modules', 'templates/', 'Academics/Modules/', 'modulesList', $data);
    }

    function ajaxResultsForFiltersJSONGeneration(){
        $dataForFilter  =   array();
        $dataForFilter['subjectID'] =   $dataForFilter['moduleName'] =   null;
        $activeFilters	=	'';

        if(!(empty($_POST['hidSearchFlag']))) {
            $newSearchArr	=	json_decode($_POST['hidSearchConcat']);
            $activeFilters	=	'';

            if(!(empty($newSearchArr))) {
                foreach ($newSearchArr as $key => $val) {
                    foreach ($val as $key1 =>  $val1) {
                        switch($key1) {
                            case 'subject' :
                                $subjDtl    =   explode('$$##$$', $val1);
                                $dataForFilter['subjectID'] =   $subjDtl[0];
                                $activeFilters	.=	'<li>
														<button type="button" id="btnRemSubject" class="btn remBtn" style="width:16%;background:none;padding:0!important;">
															<i class="fa fa-remove mr-3" style="color:red;align-self:center;"></i>
														</button>'. ucfirst($key1). ' : <b>' . $subjDtl[1].'</b>&nbsp;</li>';
                                break;
                                
                            case 'moduleName' :
                                $dataForFilter['moduleName'] =   trim($val1);
                                $activeFilters	.=	'<li>
														<button type="button" id="btnRemName" class="btn remBtn" style="width:16%;background:none;padding:0!important;">
															<i class="fa fa-remove mr-3" style="color:red;align-self:center;"></i>
														</button>'. ucfirst($key1). ' : <b>' . $val1.'</b>&nbsp;</li>';
                                break;
                        }
                    }
                }
            }
        }

        $modules  =   $this->Modules_model->getList($dataForFilter);
        $writeJSONData  =   $this->fetchDataForModules($modules);
        $results    =   array();
        $results['data']['success'] =   1;

        if($activeFilters == '') {
            $results['data']['activeFilters']	=	'<li><span class="text-semibold text-uppercase" style="valign:middle;">Active Filter&nbsp;:&nbsp;</span></li><li><span class="text-semibold text-uppercase" style="valign:middle;"> No Active Filters </span></li>';
        } else {
            $results['data']['activeFilters']	=	'<li><span class="text-semibold text-uppercase" style="valign:middle;">Active Filter&nbsp;:&nbsp;</span></li>'.$activeFilters;
        }

        $results['data']['writeJSONData']    =   $writeJSONData;
        //echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
    }

    function fetchDataForModules($modules) {
        $newPosts   =   array();
        $cnt    =   0;

        foreach($modules as $m) {
            $newPosts[$cnt]['SNo']  =   ($cnt + 1);
            $newPosts[$cnt]['Subject']  =  $m->subjectName;
            $newPosts[$cnt]['Module']  =  $m->module_name;

            if($m->status    ==  'Active') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-success">Active</span>';
            } else if ($m->status    ==  'InActive') {
                $newPosts[$cnt]['Status']   =   '<span class="badge badge-secondary">InActive</span>';
            } else {
                $newPosts[$cnt]['Status']   =   '<i class="fa fa-times text-dark"></i>';
            }

            $newPosts[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$m->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$m->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }

        return $newPosts;
    }

    function ajaxResultsForModulesAction(){
        $res    =   array();
        $formCont   =   '';

        if($_POST['hidReqType'] != '') {
            // echo "\r\n <br/> hidReqType : ";$_POST['hidReqType'];exit();
            switch($_POST['hidReqType']) {
                case 'formList' :
                    $hidRecID   =   $subjectID  =   0;
                    $moduleName =    $moduleCode  =   $statusFlag =   '';

                    if($_POST['hidRecID'] != '') {
                        $hidRecID   =   $_POST['hidRecID'];
                        $modulesRes  =   $this->Modules_model->get($hidRecID);

                        if(!(empty($modulesRes))) {
                            foreach($modulesRes as $m) {
                                $subjectID  =   $m->subject_id;
                                $moduleName    =   $m->module_name;
                                $statusFlag =   $m->status;
                            }
                        }
                    }

                    $subjRes    =   $this->Subjects_model->getList();
                    $formCont   =   '<form id="frmCommon" post="frmCommon" method="post" action="">
                                        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
                                        <input type="hidden" name="hidRecID" id="hidRecID" value="'.$hidRecID.'"/> 
                                        <div class="card">
                                            <div class="card-header"><h4>Module Add / Edit </h4></div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="subject">Subject:<span class="text-red">*</span></label>
                                                                <select d1ata-placeholder="Select Subject" class="form-control form-input-styled" data-fouc name="lsSubjects" id="lsSubjects">
                                                                    <option value="">--Choose One--</option>';
                    if(!(empty($subjRes))) {
                        foreach($subjRes as $s) {
                            if($subjectID == $s->id) {
                                $sele   =   'selected="SELECTED"';
                            } else {
                                $sele   =   '';
                            }

                            $formCont   .=  '<option value="'.$s->id.'" ' . $sele.'>'.$s->subject_name.'</option>';
                        }
                    }

                    $formCont   .=  '                           </select>
                                                            </div>
                                                        </div>        
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="moduleName">Module Name:<span class="text-red">*</span></label>
                                                                <input type="text" class="form-control" required name="moduleName" id="moduleName" value="'.$moduleName.'">
                                                            </div>
                                                        </div>';
                    if($hidRecID != 0) {
                        $formCont   .=  '               <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="status">Status:<span class="text-red">*</span></label>
                                                                <select data-placeholder="Select Status" class="form-control form-input-styled" data-fouc name="lsStatus" id="lsStatus">
                                                                    <option value="">--Choose One--</option>';
                        $fEnums =   $this->generalFn->field_enums('modules', 'status');

                        if(!(empty($fEnums))) {
                            foreach($fEnums as $fe){
                                if($statusFlag == $fe) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }
                                
                                $formCont   .=  '<option value="'.$fe.'" '. $sele.'>'.$fe.'</option>';
                            }
                        }
                        
                        $formCont   .=  '                       </select>
                                                            </div>
                                                        </div>';
                    }

                    $formCont   .=  '               </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>';
                    $res['data']    =   'success';
                    $res['formCont']    =   $formCont;
                    break;

                case 'create' :
                    if ( ($_POST['lsSubjects'] != '') && ($_POST['moduleName'] != '') ) {
                        //$chkRes =   $this->Modules_model->checkExists('module_name', $_POST['moduleName']);

                        //if($chkRes == 0) {
                            $data   =   array(
                                        'subject_id'  =>  $_POST['lsSubjects'], 
                                        'module_name'   =>  $_POST['moduleName'],
                                        'status'    =>  'Active',
                                        'created_at'    =>  date('Y-m-d h:i:s'),
                                        'created-by'    =>  1,);
                            $mRes  =   $this->Modules_model->add($data);

                            if($mRes > 0 ) {
                                $res['data']    =   'success';
                            } else {
                                $res['data']    =   'failure';
                            }
                        /*} else {
                            $res['data']    =   'Subject already exists.';
                        }*/
                    }

                    break;

                case 'update' :
                    if ( ($_POST['hidRecID'] != '')  && ($_POST['lsSubjects'] != '') && ($_POST['moduleName'] != '') ) {
                        $data   =   array(
                                        'id'    =>  $_POST['hidRecID'], 
                                        'subject_id'  =>  $_POST['lsSubjects'] , 
                                        'module_name'   =>  $_POST['moduleName'],
                                        'status'    =>  $_POST['lsStatus'],
                                        'updated_at'    =>  date('Y-m-d h:i:s'));
                        $mRes  =   $this->Modules_model->add($data);

                        if($mRes > 0) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;

                case 'dele':
                    if($_POST['hidRecID'] != 0) {
                        $cRes  =   $this->Modules_model->delete($_POST['hidRecID']);

                        if($cRes == 1) {
                            $res['data']    =   'success';        
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
            }
        }

        echo json_encode($res);
    }    
}