<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Chapters_model');
        $this->load->model('Cities_model');
        $this->load->model('Classes_model');
        $this->load->model('Courses_model');
        //$this->load->model('CourseDetails_model');
        $this->load->model('Countries_model');
        $this->load->model('Institutes_model');
        $this->load->model('Modules_model');
        $this->load->model('States_model');
        $this->load->model('Settingsubtype_model');
        $this->load->model('Settingtype_model');
        $this->load->model('Topics_model');
    }

    public function ajaxResultsForQuestionTypeOptions() {
        $res    =   array();
        $formCont   =   '<div class="col-md-12"><!-- Top Col construction-->
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card-header"><h5> Options</h5></div>
                                        </div>
                                    </div>';

        if( ($_POST['reqType'] != '') && ($_POST['questTypeID'] != '') ) {
            switch($_POST['questTypeID']) {
                case 1 : //Single Choice [Looping to max 4 -- need to pick only 1 option]
                case 2 :// Multiple Choices [ max 4 ]
                    $formCont   .=  '<div class="row">';

                    for($i = 1; $i < 5; $i++) {
                        $formCont   .=  '<div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea class="form-control editor-full" name="single-editor-half[]" id="editor-full-'.$i.'" rows="4" cols="4"></textarea>
                                                <script language="javascript" type="text/javascript">
                                                    CKEDITOR.replace("editor-full-'.$i.'", {
                                                        height: 100,
                                                        extraPlugins: "forms"
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                    </div>';
                    }

                    $formCont   .=  '</div><!-- class="row"-->';                    
                    break;

                case 3 ://Integer or decimal
                    for($i = 1; $i < 5; $i++) {
                        $formCont   .=  '<div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="intDecOptions[]" id="intDecOption-'.$i.'" placeHolder="Integer Or Decimal Value '. $i.'" value="">
                                                </div>
                                            </div>
                                        </div>';
                    }

                    break;

                case 4 ://True or False
                    $formCont   .=  '<div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
            									<label class="d-block font-weight-semibold">True Or False</label>
            									<div class="form-check form-check-inline">
            										<label class="form-check-label">
            											<input type="radio" class="form-check-input" name="trueFalseOptions[]" checked>
            											True
            										</label>
            									</div>
                        
            									<div class="form-check form-check-inline">
            										<label class="form-check-label">
            											<input type="radio" class="form-check-input" name="trueFalseOptions[]">
            											False
            										</label>
            									</div>
            								</div>
                                        </div>
                                    </div>';
                    break;

                case 5 ://Match the Following
                    for($i = 1; $i < 5; $i++) {
                        $formCont   .=  '<div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="matchOptions[]" id="matchOptions-'.$i.'" placeHolder="Match Option Value '.$i.'" value="">
                                                </div>
                                            </div>
                                        </div>';
                    }

                    break;

                case 6 ://Paragraph
                    $formCont   .=  '<div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea rows="4" name="paragraphOption" id="paragraphOption"></textarea>
                                            </div>
                                        </div>
                                    </div>';
                    break;

                case 7 ://Fill In the Blanks
                    $formCont   .=  '<div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="fillOption" id="fillOption" placeHolder="Fill In the blanks" value="">
                                            </div>
                                        </div>
                                    </div>';
                    break;
            }

            $formCont   .=  '       </div><!-- class="card-body"-->
                                </div><!-- class="card"-->
                            </div><!-- Top Col construction-->';
            $res['data']    =   'success';
            $res['formCont']    =   $formCont;
        }

        echo json_encode($res);
    }


    public function ajaxResultsForClassesDropBox() {
        $res    =   array();
        $cRes   =   array();
        $classIDs   =   '';

        if ($_POST['courseID'] != '') {
            $courseRes   =   $this->CourseDetails_model->getClassesByCourseID($_POST['courseID']);

            if(!(empty($courseRes))) {
                foreach($courseRes as $co) {
                    $cIds   =   json_decode($co->class_id);
                    //echo "\r\n <br/> cIds : \r\n <br/><pre>"; print_r($cIds);
                    for($i = 0; $i < count($cIds); $i++) {
                        if($classIDs != '') {
                            $classIDs   .=  ', ' . $cIds[$i];
                        } else {
                            $classIDs   .=  $cIds[$i];
                        }
                    }
                }

                if($classIDs != '') {
                    $classRes   =   $this->Classes_model->getClassesByIDs($classIDs);

                    if(!(empty($classRes))) {
                        $res['data']    =   'success';
                        $cnt    =   0;

                        foreach($classRes as $cl) {
                            $cRes[$cnt]['id']   =   $cl->id;
                            $cRes[$cnt]['className']    =   $cl->class_name;
                            $cnt++;
                        }
                    }
                }

                $res['classDtls']    =   $cRes;
            }
        }

        echo json_encode($res);
    }

    public function ajaxResultsForChaptersDropBox() {
        $res    =   array();
        $cRes   =   array();

        if ($_POST['subjectID'] != '') {
            $chapterRes   =   $this->Chapters_model->getChaptersByModuleID($_POST['subjectID']);

            if(!(empty($chapterRes))) {
                $res['data']    =   'success';
                $cnt    =   0;
                $formCont   =   '';

                if($_POST['reqType'] == 'fetchChaptersBySubjectForCourseForm') {
                    foreach($chapterRes as $ch) {
                        $formCont   .=  '<div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                									<label>&nbsp;</label><BR/>
                									<div class="form-check mb-0">
                										<label class="form-check-label">
                										<input type="checkbox" id="cboChapters-'.$cnt.'" name="courses[Subjects][Chapters]" class="form-input-styled" value="'.$ch->id.'">'.
                										$ch->chapter_name.'
                										</label>
                									</div>
                								</div>
                                            </div>
                                        </div>';
                        $cnt++;
                    }

                    $res['chapterDtls']    =   $formCont;
                } else {
                    foreach($chapterRes as $ch) {
                        $cRes[$cnt]['id']   =   $ch->id;
                        $cRes[$cnt]['chapterName']    =   $ch->chapter_name;
                        $cnt++;
                    }

                    $res['chapterDtls']    =   $cRes;
                }
            }
        }

        echo json_encode($res);
    }

    public function ajaxResultsForModulesDropBox() {
        $res    =   array();
        $mRes   =   array();
        
        if($_POST['subjectID'] != '') {
            $moduleRes   =   $this->Modules_model->getModulesBySubjectID($_POST['subjectID']);

            if(!(empty($moduleRes))) {
                $res['data']    =   'success';
                $cnt    =   0;

                foreach($moduleRes as $m) {
                    $mRes[$cnt]['id']   =   $m->id;
                    $mRes[$cnt]['moduleName']    =   $m->module_name;
                    $cnt++;
                }

                $res['modulesDtls']    =   $mRes;
            }
        }

        echo json_encode($res);
    }

    public function ajaxResultsForTopicsDropBox() {
        $res    =   array();
        $cRes   =   array();
        
        if ($_POST['chapterID'] != '') {
            $topicRes   =   $this->Topics_model->getTopicsByChapterID($_POST['chapterID']);

            if(!(empty($topicRes))) {
                $res['data']    =   'success';
                $cnt    =   0;

                foreach($topicRes as $to) {
                    $cRes[$cnt]['id']   =   $to->id;
                    $cRes[$cnt]['topicName']    =   $to->topic_name;
                    $cnt++;
                }

                $res['topicDtls']    =   $cRes;
            }
        }

        echo json_encode($res);
    }

    public function ajaxResultsForSubTopicsDropBox() {
        $res    =   array();
        $cRes   =   array();

        if ($_POST['topicID'] != '') {
            $topicRes   =   $this->Topics_model->getSubTopicsByTopicID($_POST['topicID']);

            if(!(empty($topicRes))) {
                $res['data']    =   'success';
                $cnt    =   0;

                foreach($topicRes as $to) {
                    $cRes[$cnt]['id']   =   $to->id;
                    $cRes[$cnt]['topicName']    =   $to->topic_name;
                    $cnt++;
                }

                $res['topicDtls']    =   $cRes;
            } else {
                $res['data']    =   'error';
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForStatesDropBox() {
        $res    =   array();
        $states =   array();
        $cnt    =   0;

        if($_POST['countryID'] != '') {
            $stateRes   =   $this->States_model->getStatesByCountry($_POST['countryID']);

            if(!(empty($stateRes))) {
                $res['data']    =   'success';

                foreach($stateRes as $s) {
                    $states[$cnt]['id'] =   $s->id;
                    $states[$cnt]['state_name'] =   $s->state_name;
                    $cnt++;
                }

                $res['states']  =   $states;
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForcityDropBox(){
        $res    =   array();
        $cities =   array();
        $cnt    =   0;

        if($_POST['stateId'] != '') {
            $cityRes   =   $this->Cities_model->getCityByState($_POST['stateId']);

            if(!(empty($cityRes))) {
                $res['data']    =   'success';

                foreach($cityRes as $s) {
                    $cities[$cnt]['id'] =   $s->id;
                    $cities[$cnt]['city_name'] =   $s->city_name;
                    $cnt++;
                }

                $res['cities']  =   $cities;
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForSubSettingTypeDropBox() {
        $res    =   array();
        $settingSubTypeId =   array();
        $cnt    =   0;

        if($_POST['settingTypesId'] != '') {
            $settingSubRes   =   $this->Settingsubtype_model->getSubSettingType($_POST['settingTypesId']);

            if(!(empty($settingSubRes))) {
                $res['data']    =   'success';

                foreach($settingSubRes as $s) {
                    $settingSubTypeId[$cnt]['id'] =   $s->id;
                    $settingSubTypeId[$cnt]['setting_sub_type_name'] =   $s->setting_sub_type_name;
                    $cnt++;
                }

                $res['settingSubTypeId']  =   $settingSubTypeId;
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForSettingTypeDropBox(){
        $res    =   array();
        $settingtypeid =   array();
        $cnt    =   0;

        if($_POST['settingTypesId'] != '') {
            $settingRes   =   $this->Settingtype_model->getSettingType($_POST['settingTypesId']);

            if(!(empty($settingRes))) {
                $res['data']    =   'success';

                foreach($settingRes as $s) {
                    $settingtypeid[$cnt]['id'] =   $s->id;
                    $settingtypeid[$cnt]['setting_type_name'] =   $s->setting_type_name;
                    $cnt++;
                }

                $res['settingtypeid']  =   $settingtypeid;
            }
        }

        echo json_encode($res);
    }
}