<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class DocTypes extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Document_model');
    }

    function index() {
        $data['pageName'] =   'Document';
        $this->load->template('Document', 'templates/', 'Settings/', 'docTypes', $data);
    
    }

    function ajaxResultsForValidateSettingsJSON(){
        $res    =   array();
        // echo "\r\n <br/> POST VALS : \r\n$o <Br/><pre>"; print_r($_POST);

        if( ($_POST['userName']) && ($_POST['Name']) && ($_POST['description'])) {
            //Connect to model by passing the username and pwd values.. Validate whether true or false.
            $data   =   array(
                        'user_name'    =>  $_POST['userName'], 
                        'name'   =>  $_POST['Name'], 
                        'contact_no' => $_POST['contactNo']);
                        // 'settings_data' =>  $_POST['settingsData']);
            $documentRes   =   $this->Document_model->add($data);

            if($documentRes != FALSE) {
                if($documentRes > 0) {
                    // Set Session for the logged in user later.
                    $res['data']    =   'success';
                } else {
                    $res['data']   =    'failure';
                }
            } else {
                $res['data']   =    'Client Already exists.';
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForFiltersJSONGeneration(){
        $posts  =   $this->Document_model->getDocuments();
        $writeJSONData  =   $this->fetchDataForSettings($posts);
        $results    =   array();
        $results['data']['success'] =   1;
        $results['data']['writeJSONData']    =   $writeJSONData;
        //echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
    }

    function fetchDataForSettings($posts) {
        $newPosts   =   array();
        $cnt    =   0;

        foreach($posts as $post) {
            $newPosts[$cnt]['SNo']  =   ($cnt + 1);
            $newPosts[$cnt]['Title']   =   $post->title;
            $newPosts[$cnt]['Key']   =   $post->key;
            $newPosts[$cnt]['Description']   =   $post->description;
            // $newPosts[$cnt]['Settings Data']   =   $post->settings_data;
            $newPosts[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$post->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>';
                                            // <a href="javascript:void(0);" id="del-'.$post->client_id.'" class="logAction">
                                            //     <span class="badge badge-danger"> Delete </span>
                                            // </a>';
            $cnt++;
        }
        return $newPosts;
    }
    
    function ajaxResultsForDocumentAction(){
        $res    =   array();
        // echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($_POST);
        // exit();

        if($_POST['reqType'] != '') {
            switch($_POST['reqType']) {
                case 'edit' :
                    $newDocumentsRes   =   array();
                    $formContent    =   '';
                    $title  =   $key    =   $description    =   '';

                    if($_POST['hidRecID'] != 0) {
                        $documentRes  =   $this->Document_model->getDocuments($_POST['hidRecID']);

                        if(!(empty($documentRes))) {
                            $res['data'] = 'success';

                            foreach($documentRes as $s) {
                                $title   =   $s->title;
                                $key   =   $s->key;
                                $description   =   $s->description;
                                //assiign client record data here in an array
                            }
                        }

                        $formContent    .=  ' <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="Title">Title:</label>
                                                        <input required autofocus="" class="form-control inputstl" type="text" name="title" id="title" placeHolder="Enter Title" value="'.$title.'" >
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="Key">Key:</label>
                                                        <input required ="" class="form-control inputstl" type="text" name="key" id="key" placeHolder="Enter Key" value="'.$key.'" >
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="Description">Description:</label>
                                                        <textarea class="form-control inputstl" type="text" name="description" id="description" placeHolder="Enter Description">'.$description.'</textarea>
                                                    </div>
                                                </div>
                                            </div><!-- class="row"-->';
                        $res['data']    =   'success';
                        $res['formContent'] =   $formContent;
                    }

                    break;

                case 'update' :
                    //echo "\r\n <br/> post vals for update : \r\n <br/><pre>"; print_r($_POST);
                    if($_POST['hidRecID'] != '') {
                        // Do the update... 
                        // call the same function which u do for adding new entry with slight modifications.

                        $newClientRes   =   array();

                        if($_POST['hidRecID'] != 0) {
                            $clientRes  =   $this->Clients_model->getClients($_POST['hidRecID']);

                            if(!(empty($clientRes))) {
                                $res['data']    =   'success';

                                foreach($clientRes as $c) {
                                    $newClientRes['client_first_name']   =   $c->client_first_name;
                                    $newClientRes['client_last_name']   =   $c->client_last_name;
                                    $newClientRes['client_email']   =   $c->client_email;
                                    //assiign client record data here in an array
                                }
                            }
                        }

                        $postData   =   array(
                                        //'client_id'    =>  $_POST['hidRecID'],
                                        'client_first_name'    =>  $_POST['clientFirstName'], 
                                        'client_last_name'   =>  $_POST['clientLastName'], 
                                        'client_email' => $_POST['clientEmail']);                     

                        $actualData   =   array_diff($newClientRes,$postData);
                        $newDataForUpdate   =   array('client_id'    =>  $_POST['hidRecID'],);

                        foreach($actualData as $key  => $actData){
                            if(isset($key)){
                                $newDataForUpdate[$key] =   $postData[$key];
                            }
                        }                    

                        // Diffference array.
                        // echo "\r\n <br/> FINAL DIFF ARRAY VALUES : \r\n <br/><pre>"; print_r($actualData);
                        // echo "\r\n <br/>  DIFF ARRAY VALUES : \r\n <br/><pre>"; print_r($newDataForUpdate);
                        
                        $clientRes  =   $this->Clients_model->add($newDataForUpdate);

                        if($clientRes == FALSE) {
                            $res['data']   =    'Client Already exists.';
                        } else{
                            if($clientRes == 1) {
                                $res['data']    =   'success';
                            } else {   
                                    $res['data']    =   'failure';
                            }
                        }
                    }

                    break;
                }
        }
 
        echo json_encode($res);
    }
}