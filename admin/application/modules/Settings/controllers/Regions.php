<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Regions extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Regions_model');
        $this->load->model('Countries_model');
        $this->load->model('States_model');
        $this->load->model('Cities_model');
    }
    
    public function index() {
        $data  =   array();
        $data['pageName'] =   'Regions List';
        $this->load->template('Regions', 'templates/', 'Settings/Regions/', 'regionsList', $data);
    }

    function ajaxResultsForRegionsJSON(){
        $res    =   array();

        if( ($_POST['country_code']) && ($_POST['country_name']) ) {
            //Connect to model by passing the username and pwd values.. Validate whether true or false.
            $data   =   array(
                        'country_code'    =>  $_POST['country_code'], 
                        'country_name'   =>   $_POST['country_name'], );
                        // 'settings_data' =>  $_POST['settingsData']);
            $usersRes   =   $this->Regions_model->add($data);

            if($usersRes != FALSE) {
                if($usersRes > 0) {
                    // Set Session for the logged in user later.
                    $res['data']    =   'success';
                } else {
                    $res['data']   =    'failure';
                }
            } else {
                $res['data']   =    'User Already exists.';
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForStatesJSON(){
        $res    =   array();

        if( ($_POST['state_code']) && ($_POST['state_name']) ) {
            //Connect to model by passing the username and pwd values.. Validate whether true or false.
            $data   =   array(
                        'state_code'    =>  $_POST['state_code'], 
                        'state_name'   =>   $_POST['state_name'], 
                        );
                        
                        // 'settings_data' =>  $_POST['settingsData']);
            $usersRes   =   $this->Regions_model->addStates($data);

            if($usersRes != FALSE) {
                if($usersRes > 0) {
                    // Set Session for the logged in user later.
                    $res['data']    =   'success';
                } else {
                    $res['data']   =    'failure';
                }
            } else {
                $res['data']   =    'User Already exists.';
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForCitiesJSON(){
        $res    =   array();

        if( ($_POST['city_code']) && ($_POST['city_name']) ) {
            //Connect to model by passing the username and pwd values.. Validate whether true or false.
            $data   =   array(
                        'city_code'    =>  $_POST['city_code'], 
                        'city_name'   =>   $_POST['city_name'], 
                        );
                        
                        // 'settings_data' =>  $_POST['settingsData']);
            $usersRes   =   $this->Regions_model->addCities($data);

            if($usersRes != FALSE) {
                if($usersRes > 0) {
                    // Set Session for the logged in user later.
                    $res['data']    =   'success';
                } else {
                    $res['data']   =    'failure';
                }
            } else {
                $res['data']   =    'User Already exists.';
            }
        }

        echo json_encode($res);
    }

    
    function ajaxResultsForCountriesFiltersJSONGeneration(){
        $countries  =   $this->Regions_model->get();
        // echo "\r\n <br/> Result Of Courses :<pre>";print_r($countries);exit();
        $writeJSONData  =   $this->fetchDataForRegions($countries);
        $results    =   array();
        $results['data']['success'] =   1;
        $results['data']['writeJSONData']    =   $writeJSONData;
        // echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
    }

    function ajaxResultsForStatesFiltersJSONGeneration(){
        $countries  =   $this->Regions_model->getStates();
        // echo "\r\n <br/> Result Of Courses :<pre>";print_r($countries);exit();
        $writeJSONData  =   $this->fetchDataForStates($countries);
        $results    =   array();
        $results['data']['success'] =   1;
        $results['data']['writeJSONData']    =   $writeJSONData;
        // echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
 
    }

    function ajaxResultsForCitiesFiltersJSONGeneration(){
        $countries  =   $this->Regions_model->getCities();
        // echo "\r\n <br/> Result Of Courses :<pre>";print_r($countries);exit();
        $writeJSONData  =   $this->fetchDataForCities($countries);
        $results    =   array();
        $results['data']['success'] =   1;
        $results['data']['writeJSONData']    =   $writeJSONData;
        // echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
 
    }

    function fetchDataForRegions($countries) {
        $newUser   =   array();
        $cnt    =   0;

        foreach($countries as $user) {
            $newUser[$cnt]['SNo']  =   ($cnt + 1);
            $newUser[$cnt]['Country Code']  =   $user->country_code;
            $newUser[$cnt]['Country Name']   =   $user->country_name;

            if($user->status    ==  'Active') {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-success">Active</span>';//'<i class="fa fa-check text-success"></i>';
            } else if ($user->status    ==  'InActive') {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-grey">InActive</span>';
            } else {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-warning">None</span>';
            }

            $newUser[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$user->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$user->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }

        return $newUser;
    }

    function fetchDataForStates($countries) {
        $newUser   =   array();
        $cnt    =   0;

        foreach($countries as $user) {
            $newUser[$cnt]['SNo']  =   ($cnt + 1);
            $newUser[$cnt]['State Code']  =   $user->state_code;
            $newUser[$cnt]['State Name']   =   $user->state_name;

            if($user->status    ==  'Active') {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-success">
                Active</span>';//'<i class="fa fa-check text-success"></i>';
            } else if ($user->status    ==  'InActive') {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-grey">InActive</span>';
            } else {
                $newUser[$cnt]['Status']   =   '<i class="fa fa-times text-dark"></i>';
            }

            $newUser[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$user->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$user->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }

        return $newUser;
    }

    function fetchDataForCities($countries) {
        $newUser   =   array();
        $cnt    =   0;

        foreach($countries as $user) {
            $newUser[$cnt]['SNo']  =   ($cnt + 1);
            $newUser[$cnt]['City Code']  =   $user->city_code;
            $newUser[$cnt]['City Name']   =   $user->city_name;

            if($user->status    ==  'Active') {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-success">
                Active</span>';//'<i class="fa fa-check text-success"></i>';
            } else if ($user->status    ==  'InActive') {
                $newUser[$cnt]['Status']   =   '<span class="badge badge-grey">InActive</span>';
            } else {
                $newUser[$cnt]['Status']   =   '<i class="fa fa-times text-dark"></i>';
            }

            $newUser[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$user->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$user->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }

        return $newUser;
    }
    
    function checkFieldExists() {
        $res    =   array();
   
        if( (!(empty($_POST['name']))) && (!(empty($_POST['value'])))) {
            $userRes    =   $this->Regions_model->checkField($_POST['name'], $_POST['value']);

            if($userRes == 1) {
                $res['data']    =   'failure';
            } else {
                $res['data']    =   'success';
            }
        }

        echo json_encode($res);
    }

    function checkFieldExistsStates() {
        $res    =   array();
   
        if( (!(empty($_POST['name']))) && (!(empty($_POST['value'])))) {
            $userRes    =   $this->Regions_model->checkFieldStates($_POST['name'], $_POST['value']);

            if($userRes == 1) {
                $res['data']    =   'failure';
            } else {
                $res['data']    =   'success';
            }
        }

        echo json_encode($res);
    }

    function checkFieldExistsCities() {
        $res    =   array();

        if( (!(empty($_POST['name']))) && (!(empty($_POST['value'])))) {
            $userRes    =   $this->Regions_model->checkFieldCities($_POST['name'], $_POST['value']);

            if($userRes == 1) {
                $res['data']    =   'failure';
            } else {
                $res['data']    =   'success';
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForRegionsAction(){
        $res    =   array();
        $formCont   =   '';

        if($_POST['hidReqType'] != '') {
            // echo "\r\n <br/> hidReqType : ";$_POST['hidReqType'];exit();
            switch($_POST['hidReqType']) {
                case 'formList' :
                    $hidRecID   =  0;
                    $countryCode =   $countryName = $status =   '';

                    if($_POST['hidRecID'] != '') {
                        $hidRecID   =   $_POST['hidRecID'];
                        $courseRes  =   $this->Regions_model->get($hidRecID);

                        if(!(empty($courseRes))) {
                            foreach($courseRes as $c) {
                                //Declare variables atop and assign data from DB to those variables.
                                $countryCode =   $c->country_code;
                                $countryName =   $c->country_name;
                                
                            }
                        }
                    }

                    $formCont   =   '<form id="frmCommon" name="frmCommon" method="post" action="">
                                        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
                                        <input type="hidden" name="hidRecID" id="hidRecID" value="'.$hidRecID.'"/> 
                                            <div class="card">
                                                <div class="card-header"><h4>Countries Add / Edit </h4></div>
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="countryCode">Country Code:<span class="text-red">*</span></label>
                                                                    <input type="text" class="form-control" required name="countryCode" id="countryCode" value="'.$countryCode.'">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="courseName">Country Name:<span class="text-red">*</span></label>
                                                                    <input type="text" class="form-control" required name="countryName" id="countryName" value="'.$countryName.'">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </form>';
                    $res['data']    =   'success';
                    $res['formCont']    =   $formCont;
                    break;

                case 'create' :
                    // echo "\r\n <br/> Post Value: <pre>";print_r($_POST);
                    // echo "\r\n <br/> Image Value: <pre>";print_r($_FILES);exit();
                    // echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($_POST);exit();
                    $data   =   array(
                                'country_code'    =>  $_POST['countryCode'], 
                                'country_name'   =>  $_POST['countryName'],
                                'created_at'    =>  date('Y-m-d h:i:s'),
                                'status'    =>  'Active');
                    // echo "\r\n <br/> Data :";print_r($data);exit();
                    $cRes  =   $this->Regions_model->add($data);

                    if($cRes > 0 ) {
                        $res['data']    =   'success';
                    } else {
                        $res['data']    =   'failure';
                    }

                    break;

                case 'update' :
                    // echo "\r\n <br/> post vals for update : \r\n <br/><pre>"; print_r($_POST);exit();
                    if($_POST['hidRecID'] != '') {
                        $data   =   array(
                            'id'    =>  $_POST['hidRecID'],
                            'country_code'    =>  $_POST['countryCode'], 
                            'country_name'   =>  $_POST['countryName'], 
                            'updated_at'    =>  date('Y-m-d h:i:s'),);
                        $cRes  =   $this->Regions_model->add($data);

                        if($cRes > 0) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;

                case 'dele':
                    if($_POST['hidRecID'] != 0) {
                        $cRes  =   $this->Regions_model->delete($_POST['hidRecID']);

                        if($cRes == 1) {
                            $res['data']    =   'success';        
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForStatesAction(){
        $res    =   array();
        $formCont   =   '';
        if($_POST['hidReqType'] != '') {
            // echo "\r\n <br/> hidReqType : ";$_POST['hidReqType'];exit();
            switch($_POST['hidReqType']) {
                case 'formList' :
                    $hidRecID   =  $countryID   =   0;
                    $stateCode =   $stateName   =   '';

                    if($_POST['hidRecID'] != '') {
                        $hidRecID   =   $_POST['hidRecID'];
                        $sRes  =   $this->Regions_model->getStates($hidRecID);

                        if(!(empty($sRes))) {
                            foreach($sRes as $c) {
                                //Declare variables atop and assign data from DB to those variables.
                                $countryID  =   $c->country_id;
                                $stateCode =   $c->state_code;
                                $stateName =   $c->state_name;
                            }
                        }
                    }

                    $countryRes =   $this->Countries_model->get();
                    $formCont   =   '<form id="frmCommonS" name="frmCommonS" method="post" action="">
                                        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
                                        <input type="hidden" name="hidRecID" id="hidRecID" value="'.$hidRecID.'"/> 
                                        <div class="card">
                                            <div class="card-header"><h4>States Add / Edit </h4></div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="country">Country:<span class="text-red">*</span></label>
                                                                <select data-placeholder="Select Country" class="form-control form-input-styled" data-fouc name="lsCountries" id="lsCountries">
                                                                    <option value="">--Choose One--</option>';
                    if(!(empty($countryRes))) {
                        foreach($countryRes as $co) {
                            if($countryID == $co->id) {
                                $sele   =   'selected="SELECTED"';
                            } else {
                                $sele   =   '';
                            }

                            $formCont   .=  '<option value="'.$co->id.'" ' . $sele.'>' . $co->country_name.'</option>';
                        }
                    }

                    $formCont   .=   '                          </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="stateCode">State Code:<span class="text-red">*</span></label>
                                                                <input type="text" class="form-control" required name="stateCode" id="stateCode" value="'.$stateCode.'">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="stateName">State Name:<span class="text-red">*</span></label>
                                                                <input type="text" class="form-control" required name="stateName" id="stateName" value="'.$stateName.'">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>';
                    $res['data']    =   'success';
                    $res['formCont']    =   $formCont;
                    break;

                case 'create' :
                    //echo "\r\n <br/> Post Value: <pre>";print_r($_POST);
                    // echo "\r\n <br/> Image Value: <pre>";print_r($_FILES);exit();
                    // echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($_POST);exit();
                    $data   =   array(
                                'country_id'    =>  $_POST['lsCountries'],
                                'state_code'    =>  $_POST['stateCode'], 
                                'state_name'   =>  $_POST['stateName'], );
                    // echo "\r\n <br/> Data :";print_r($data);exit();
                    $usersRes  =   $this->Regions_model->addStates($data);

                    if($usersRes > 0 ) {
                        $res['data']    =   'success';
                    } else {
                        $res['data']    =   'failure';
                    }

                    break;

                case 'update' :
                    // echo "\r\n <br/> post vals for update : \r\n <br/><pre>"; print_r($_POST);exit();
                    if($_POST['hidRecID'] != '') {
                        $data   =   array(
                            'id'    =>  $_POST['hidRecID'],
                            'country_id'    =>  $_POST['lsCountries'],
                            'state_code'    =>  $_POST['stateCode'], 
                            'state_name'   =>  $_POST['stateName'], 
                            'status'  =>  $_POST['status']);
                        $usersRes  =   $this->Regions_model->addStates($data);

                        if($usersRes > 0) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }

                    }

                    break;

                case 'dele':
                    if($_POST['hidRecID'] != 0) {
                        $userRes  =   $this->Regions_model->deleteStates($_POST['hidRecID']);

                        if($userRes == 1) {
                            $res['data']    =   'success';        
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForCitiesAction(){
        $res    =   array();
        $formCont   =   '';

        if($_POST['hidReqType'] != '') {
            // echo "\r\n <br/> hidReqType : ";$_POST['hidReqType'];exit();
            switch($_POST['hidReqType']) {
                case 'formList' :
                    $hidRecID   =  $countryID   =   $stateID    =   0;
                    $cityCode =   $cityName =   '';

                    if($_POST['hidRecID'] != '') {
                        $hidRecID   =   $_POST['hidRecID'];
                        $courseRes  =   $this->Regions_model->getCities($hidRecID);

                        if(!(empty($courseRes))) {
                            foreach($courseRes as $c) {
                                //Declare variables atop and assign data from DB to those variables.
                                $stateID    =   $c->state_id;
                                $cityCode =   $c->city_code;
                                $cityName =   $c->city_name;
                            }
                        }
                    }

                    $countryRes =   $this->Countries_model->get();
                    $formCont   =   '<form id="frmCommonC" post="frmCommonC" method="post" action="">
                                        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
                                        <input type="hidden" name="hidRecID" id="hidRecID" value="'.$hidRecID.'"/> 
                                        <div class="card">
                                            <div class="card-header"><h4>Cities Add / Edit </h4></div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="country">Country:<span class="text-red">*</span></label>
                                                                <select data-placeholder="Select Country" class="form-control form-input-styled" data-fouc name="lsCountries" id="lsCountries">
                                                                    <option value="">--Choose One--</option>';
                    if(!(empty($countryRes))) {
                        foreach($countryRes as $co) {
                            if($co->is_default == 1) {
                                $sele   =   'selected="SELECTED"';
                            } else if($countryID == $co->id) {
                                $sele   =   'selected="SELECTED"';
                            } else {
                                $sele   =   '';
                            }

                            $formCont   .=  '<option value="'.$co->id.'" ' . $sele.'>' . $co->country_name.'</option>';
                        }
                    }

                    $formCont   .=   '                          </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label for="country">State:<span class="text-red">*</span></label>
                                                            <select data-placeholder="Select State" class="form-control form-input-styled" data-fouc name="lsStates" id="lsStates">
                                                                <option value="">--Choose One--</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="cityCode">City Code:<span class="text-red">*</span></label>
                                                                <input type="text" class="form-control" required name="cityCode" id="cityCode" value="'.$cityCode.'">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="cityName">City Name:<span class="text-red">*</span></label>
                                                                <input type="text" class="form-control" required name="cityName" id="cityName" value="'.$cityName.'">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>';
                    $res['data']    =   'success';
                    $res['formCont']    =   $formCont;
                    break;

                case 'create' :
                // echo "\r\n <br/> Post Value: <pre>";print_r($_POST);
                // echo "\r\n <br/> Image Value: <pre>";print_r($_FILES);exit();
                    // echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($_POST);exit();
                    $data   =   array(
                                'state_id'  =>  $_POST['lsStates'],
                                'city_code'    =>  $_POST['cityCode'], 
                                'city_name'   =>  $_POST['cityName'], 
                                'created_at'    =>  date('Y-m-d h:i:s'));
                    // echo "\r\n <br/> Data :";print_r($data);exit();
                    $usersRes  =   $this->Regions_model->addCities($data);
                  
                    if($usersRes > 0 ) {
                        $res['data']    =   'success';
                    } else {
                        $res['data']    =   'failure';
                    }

                    break;

                case 'update' :
                    // echo "\r\n <br/> post vals for update : \r\n <br/><pre>"; print_r($_POST);exit();
                    if($_POST['hidRecID'] != '') {
                        $data   =   array(
                            'id'    =>  $_POST['hidRecID'],
                            'state_id'  =>  $_POST['lsStates'],
                            'city_code'    =>  $_POST['cityCode'], 
                            'city_name'   =>  $_POST['cityName'], 
                            'updated_at'    =>  date('Y-m-d h:i:s'));
                        $usersRes  =   $this->Regions_model->addCities($data);
                        
                        if($usersRes > 0) {
                            $res['data']    =   'success';
                        } else {
                            $res['data']    =   'failure';
                        }

                    }

                    break;

                case 'dele':
                    if($_POST['hidRecID'] != 0) {
                        $userRes  =   $this->Regions_model->deleteCities($_POST['hidRecID']);

                        if($userRes == 1) {
                            $res['data']    =   'success';        
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
            }
        }

        echo json_encode($res);
    }
}