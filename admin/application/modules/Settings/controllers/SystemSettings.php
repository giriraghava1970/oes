<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class SystemSettings extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Systemsetting_model');
        $this->load->model('Settingtype_model');
        $this->load->model('Settingsubtype_model');
    }

    function index() {
        $data['pageName'] =   'Settings';
        $data['settingTypes']   =   $this->Settingtype_model->getSettingType();// call the model for fetchiung settingstypes
        $this->load->template('Settings', 'templates/', 'settings/', 'settings', $data);
    
    }

    function ajaxResultsForValidateSettingsJSON(){
        $res    =   array();
        // echo "\r\n <br/> POST VALS : \r\n$o <Br/><pre>"; print_r($_POST);

        if( ($_POST['settingTypesId']) && ($_POST['settingsSubTypesId']) && ($_POST['settingName']) && ($_POST['settingValue'])) {
            //Connect to model by passing the username and pwd values.. Validate whether true or false.
            $data   =   array(
                        'setting_types_id'  =>  $_POST['settingTypesId'],
                        'settings_sub_types_id' =>  $_POST['settingsSubTypesId'],
                        'setting_name'  =>  $_POST['settingName'],
                        'setting_value'  =>  $_POST['settingValue'],
                        'created_at'    =>  $_POST['createdAt'],
                        'created_by'    =>  $_POST['createdBy'],
                        'updated_at'    =>  $_POST['updatedAt']
                    );
            $settingRes   =   $this->Systemsetting_model->add($data);

            if($settingRes != FALSE) {
                if($settingRes > 0) {
                    // Set Session for the logged in user later.
                    $res['data']    =   'success';
                } else {
                    $res['data']   =    'failure';
                }
            } else {
                $res['data']   =    'Already exists.';
            }
        }

        echo json_encode($res);
    }


    function ajaxResultsForSubSettingTypeDropBox() {
        $res    =   array();
        $settingSubTypeId =   array();
        $cnt    =   0;

        if($_POST['settingTypesId'] != '') {
            $settingSubRes   =   $this->Settingsubtype_model->getSubSettingType($_POST['settingTypesId']);

            if(!(empty($settingSubRes))) {
                $res['data']    =   'success';

                foreach($settingSubRes as $s) {
                    $settingSubTypeId[$cnt]['id'] =   $s->id;
                    $settingSubTypeId[$cnt]['setting_sub_type_name'] =   $s->setting_sub_type_name;
                    $cnt++;
                }

                $res['settingSubTypeId']  =   $settingSubTypeId;
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForSettingTypeDropBox(){
        $res    =   array();
        $settingtypeid =   array();
        $cnt    =   0;

        if($_POST['settingTypesId'] != '') {
            $settingRes   =   $this->Settingtype_model->getSettingType($_POST['settingTypesId']);

            if(!(empty($settingRes))) {
                $res['data']    =   'success';

                foreach($settingRes as $s) {
                    $settingtypeid[$cnt]['id'] =   $s->id;
                    $settingtypeid[$cnt]['setting_type_name'] =   $s->setting_type_name;
                    $cnt++;
                }

                $res['settingtypeid']  =   $settingtypeid;
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForFiltersJSONGeneration(){
        if($_POST['hidSettingTypeId'] != 0) {
            $posts  =   $this->Systemsetting_model->getSettings($_POST['hidSettingTypeId']);
            $writeJSONData  =   $this->fetchDataForSettings($posts, $_POST['hidSettingTypeId']);
            $results    =   array();
            $results['data']['success'] =   1;
            $results['data']['writeJSONData']    =   $writeJSONData;
            //echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
            echo json_encode($results['data']);
        }
        
    }

    function fetchDataForSettings($posts, $hidSettingTypeId = 0) {
        $newPosts   =   array();
        $cnt    =   0;

        //echo "\r\n <br/> POST VALS : \r\n <Br/><pre>". count($posts);
        foreach($posts as $post) {

            $settingtypeName =   '';
            $settingsubtypeName  =   '';

            $settingtypeRes  =   $this->Settingtype_model->getSettingtypeByID($post->setting_types_id);

            if(!(empty($settingtypeRes))) {
                foreach($settingtypeRes as $st) {
                    $settingtypeName =   $st->setting_type_name;
                }
            }

            $settingssubtypesRes  =   $this->Settingsubtype_model->getSettingsubtypeByID($post->settings_sub_types_id);

            if(!(empty($settingssubtypesRes))) {
                foreach($settingssubtypesRes as $sst) {
                    $settingsubtypeName =   $sst->setting_sub_type_name;
                }
            }

            $newPosts[$cnt]['SNo']  =   ($cnt + 1);
            $newPosts[$cnt]['Setting Type ID']  =   $settingtypeName;
            $newPosts[$cnt]['Setting Sub Type ID']  =  $settingsubtypeName;
            $newPosts[$cnt]['Setting Name']   =   $post->setting_name;
            $newPosts[$cnt]['Setting Value']   =   $post->setting_value;
            $newPosts[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$post->id.'-'.$hidSettingTypeId.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$post->id.'-'.$hidSettingTypeId.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }
    //  echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($newPosts);
    //  exit();
        return $newPosts;
        
    }
    
    function ajaxResultsForSettingsAction(){
        $res    =   array();
        // echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($_POST);
        // exit();

        if($_POST['reqType'] != '') {
            switch($_POST['reqType']) {

                case 'formList' :
                    $newSettingsRes   =   array();
                    $formContent    =   '';
                    $hidRecID   =  $setting_types_id    =   $settings_sub_types_id   =   0;
                    $setting_name  =   $setting_value   =   '';

                    if($_POST['hidRecID'] != 0) {
                        $hidRecID   =   $_POST['hidRecID'];
                        $settingRes  =   $this->Systemsetting_model->getSettingsByRecordID($hidRecID);//$_POST['hidSettingTypeId']

                        if(!(empty($settingRes))) {
                            //assiign client record data here in an array
                            $res['data'] = 'success';

                            foreach($settingRes as $s) {
                                $setting_types_id   =   $s->setting_types_id;
                                $settings_sub_types_id  =   $s->settings_sub_types_id;
                                $setting_name =   $s->setting_name;
                                $setting_value =   $s->setting_value;
                                $created_at =   $s->created_at;
                                $created_by =   $s->created_by;
                                $updated_at =   $s->updated_at;
                            }
                        }
                    }

                    $formContent    .=  '<form name="formCom" id="formCom" method="POST" action="">
                        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
                        <input type="hidden" name="hidRecID" id="hidRecID" value="'.$hidRecID.'"/>
                            <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                <span class="text-red">*</span>
                                    <label for="Setting Type ID">Setting Type ID:</label>
                                    <select id="settingTypesId" name="settingTypesId" class="form-control" required>
                                        <option value="">Select Setting Type ID</option>';               
                    $settingtypeid =   $this->Settingtype_model->getSettingType();
                    $data['settingtypeid'] =   $settingtypeid;

                    if(!(empty($settingtypeid))) {
                        foreach ($settingtypeid as $stid) {
                            if($setting_types_id == $stid->id) {
                                $sele   =   'selected="SELECTED"';
                            } else {
                                $sele   =   '';
                            }

                            $formContent    .=  '<option value="'.$stid->id.'" '.$sele.'>'.$stid->setting_type_name.'</option>';
                        }
                    }

                    $formContent    .=  '</select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                <span class="text-red">*</span>
                                    <label for="Setting Sub Type ID">Setting Sub Type ID:</label>
                                    <select id="settingsSubTypesId" name="settingsSubTypesId" class="form-control" required>
                                        <option value="">Select Setting Sub Type ID</option>';
                    if($hidRecID != 0) {
                        $settingSubTypeId  =   $this->Settingsubtype_model->getSubSettingType($setting_types_id);
                        $data['settingSubTypeId']   =   $settingSubTypeId;

                        if(!(empty($settingSubTypeId))) {
                            foreach ($settingSubTypeId as $sstid) {
                                if($settings_sub_types_id == $sstid->id) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }

                                $formContent    .=  '<option value="'.$sstid->id.'" '.$sele.'>'.$sstid->setting_sub_type_name.'</option>';
                            }
                        }
                    }

                    $formContent    .= '</select>
                                </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                    <span class="text-red">*</span>
                                        <label for="Name">Setting Name:</label>
                                        <input required autofocus="" type="text" name="settingName" id="settingName" placeHolder="Enter Enter Name" 
                                        value="'.$setting_name.'" class="form-control" >
                                    </div>
                                </div>
                
                                <div class="col-md-4">
                                    <div class="form-group">
                                    <span class="text-red">*</span>
                                        <label for="Name">Setting Value:</label>
                                        <input required type="text" name="settingValue" id="settingValue" placeHolder="Enter Setting Value" 
                                       value="'.$setting_value.'" class="form-control" >
                                    </div>
                                </div>';

                $formContent  .=  
                           '</div><!-- class="row"--->
                            </form>';

                    $res['data']    =   'success';
                    $res['formContent']    =   $formContent;
                    break;

                case 'create' :
                // Do the update... 
                // call the same function which u do for adding new entry with slight modifications.
                $data   =   array(
                            'setting_types_id'   => $_POST['settingTypesId'],
                            'settings_sub_types_id' =>  $_POST['settingsSubTypesId'],
                            'setting_name' => $_POST['settingName'],
                            'setting_value' => $_POST['settingValue'],
                            'created_at'    =>  date('Y-m-d h:i:s'));
                $settingRes  =   $this->Systemsetting_model->add($data);
                if($settingRes == FALSE) {
                    $res['data']   =    'Already exists.';
                } 
                else{
                    if($settingRes > 0) {
                        $res['data']    =   'success';
                    } else {   
                            $res['data']    =   'failure';
                    }
                }

                break;

                case 'update' :
                    //echo "\r\n <br/> post vals for update : \r\n <br/><pre>"; print_r($_POST);
                    if($_POST['hidRecID'] != '') {
                        // Do the update... 
                        // call the same function which u do for adding new entry with slight modifications.
                        $newSettingsRes   =   array();
                        $postData   =   array(
                            'id'    =>  $_POST['hidRecID'],
                            'setting_types_id'   => $_POST['settingTypesId'],
                            'settings_sub_types_id' =>  $_POST['settingsSubTypesId'],
                            'setting_name' => $_POST['settingName'],
                            'setting_value' => $_POST['settingValue'],
                            'updated_at'    =>  date('Y-m-d h:i:s'));
                        $settingRes  =   $this->Systemsetting_model->add($postData);

                        if($settingRes == 1) {                   
                            $res['data']    =   'success';     
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
                

        case 'del':
        $newSettingsRes   =   array();

        if($_POST['hidRecID'] != 0) {
            $settingRes  =   $this->Systemsetting_model->delete($_POST['hidRecID']);

            if($settingRes == 1) {
                $res['data']    =   'success';        
            } else {
                $res['data']    =   'failure';
            }
        }
        break;
}
}
 
        echo json_encode($res);
    }
}