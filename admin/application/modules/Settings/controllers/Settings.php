<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Systemsetting_model');
        $this->load->model('Settingtype_model');
        $this->load->model('Settingsubtype_model');
        $this->load->model('Cities_model');
        $this->load->model('Countries_model'); 
        $this->load->model('institutes_model');
        $this->load->model('States_model'); 
    }

    function System() {
        $data['pageName'] =   'Settings';
        $data['settingTypes']   =   $this->Settingtype_model->getSettingType();// call the model for fetchiung settingstypes
        $this->load->template('Settings', 'templates/', 'settings/', 'settings', $data);
    }

    function Institutes() {
        $data['pageName'] =   'Institute';
        // $data['settingTypes']   =   $this->Settingtype_model->getSettingType();// call the model for fetchiung settingstypes
        $this->load->template('Settings', 'templates/', 'institute/', 'institute', $data);
    }

    function ajaxResultsForValidateInstituteJSON(){
        $res    =   array();
        // echo "\r\n <br/> POST VALS : \r\n$o <Br/><pre>"; print_r($_POST);

        if( ($_POST['instituteName'])) {
            //Connect to model by passing the username and pwd values.. Validate whether true or false.
            $data   =   array(
                        'institute_name'   =>  $_POST['instituteName'],
                        'institute_address_line1'   =>  $_POST['instituteAddressLine1'],
                        'institute_address_line2'   =>  $_POST['instituteAddressLine2'],
                        'pincode'   =>  $_POST['pincode'],
                        'city_id'   =>  $_POST['cityId'],
                        'state_id'  =>  $_POST['stateId'],
                        'country_id'    =>  $_POST['countryId'],
                        'created_at'    =>  $_POST['createdAt'],
                        'created_by'    =>  $_POST['createdBy'],
                        'updated_at'    =>  $_POST['updatedAt'],
                        'status'    =>  $_POST['status']);
            $instituteRes   =   $this->institutes_model->add($data);

            if($instituteRes != FALSE) {
                if($instituteRes > 0) {
                    // Set Session for the logged in user later.
                    $res['data']    =   'success';
                } else {
                    $res['data']   =    'failure';
                }
            } else {
                $res['data']   =    'Institute Already exists.';
            }
        }

        echo json_encode($res);
    }

    function ajaxResultsForValidateSettingsJSON(){
        $res    =   array();
    // echo "\r\n <br/> POST VALS : \r\n$o <Br/><pre>"; print_r($_POST);

        if( ($_POST['settingTypesId']) && ($_POST['settingsSubTypesId']) && ($_POST['settingName']) && ($_POST['settingValue'])) {
            //Connect to model by passing the username and pwd values.. Validate whether true or false.
            $data   =   array(
                        'setting_types_id'  =>  $_POST['settingTypesId'],
                        'settings_sub_types_id' =>  $_POST['settingsSubTypesId'],
                        'setting_name'  =>  $_POST['settingName'],
                        'setting_value'  =>  $_POST['settingValue'],
                        'created_at'    =>  $_POST['createdAt'],
                        'created_by'    =>  $_POST['createdBy'],
                        'updated_at'    =>  $_POST['updatedAt']
                    );
            $settingRes   =   $this->Systemsetting_model->add($data);

            if($settingRes != FALSE) {
                if($settingRes > 0) {
                    // Set Session for the logged in user later.
                    $res['data']    =   'success';
                } else {
                    $res['data']   =    'failure';
                }
            } else {
                $res['data']   =    'Already exists.';
            }
        }

        echo json_encode($res);
    }


    // function ajaxResultsForSubSettingTypeDropBox() {
    //     $res    =   array();
    //     $settingSubTypeId =   array();
    //     $cnt    =   0;

    //     if($_POST['settingTypesId'] != '') {
    //         $settingSubRes   =   $this->Settingsubtype_model->getSubSettingType($_POST['settingTypesId']);

    //         if(!(empty($settingSubRes))) {
    //             $res['data']    =   'success';

    //             foreach($settingSubRes as $s) {
    //                 $settingSubTypeId[$cnt]['id'] =   $s->id;
    //                 $settingSubTypeId[$cnt]['setting_sub_type_name'] =   $s->setting_sub_type_name;
    //                 $cnt++;
    //             }

    //             $res['settingSubTypeId']  =   $settingSubTypeId;
    //         }
    //     }

    //     echo json_encode($res);
    // }

    // function ajaxResultsForSettingTypeDropBox(){
    //     $res    =   array();
    //     $settingtypeid =   array();
    //     $cnt    =   0;

    //     if($_POST['settingTypesId'] != '') {
    //         $settingRes   =   $this->Settingtype_model->getSettingType($_POST['settingTypesId']);

    //         if(!(empty($settingRes))) {
    //             $res['data']    =   'success';

    //             foreach($settingRes as $s) {
    //                 $settingtypeid[$cnt]['id'] =   $s->id;
    //                 $settingtypeid[$cnt]['setting_type_name'] =   $s->setting_type_name;
    //                 $cnt++;
    //             }

    //             $res['settingtypeid']  =   $settingtypeid;
    //         }
    //     }

    //     echo json_encode($res);
    // }

    function ajaxResultsForFiltersJSONGeneration(){
        if($_POST['hidSettingTypeId'] != 0) {
            $posts  =   $this->Systemsetting_model->getSettings($_POST['hidSettingTypeId']);
            $writeJSONData  =   $this->fetchDataForSettings($posts, $_POST['hidSettingTypeId']);
            $results    =   array();
            $results['data']['success'] =   1;
            $results['data']['writeJSONData']    =   $writeJSONData;
            //echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
            echo json_encode($results['data']);
        }
    }

    function fetchDataForSettings($posts, $hidSettingTypeId = 0) {
        $newPosts   =   array();
        $cnt    =   0;

        //echo "\r\n <br/> POST VALS : \r\n <Br/><pre>". count($posts);
            foreach($posts as $post) {
                $settingtypeName =   '';
                $settingsubtypeName  =   '';
    
                $settingtypeRes  =   $this->Settingtype_model->getSettingtypeByID($post->setting_types_id);
    
                if(!(empty($settingtypeRes))) {
                    foreach($settingtypeRes as $st) {
                        $settingtypeName =   $st->setting_type_name;
                    }
                }
    
                $settingssubtypesRes  =   $this->Settingsubtype_model->getSettingsubtypeByID($post->settings_sub_types_id);
    
                if(!(empty($settingssubtypesRes))) {
                    foreach($settingssubtypesRes as $sst) {
                        $settingsubtypeName =   $sst->setting_sub_type_name;
                    }
                }

            $newPosts[$cnt]['SNo']  =   ($cnt + 1);
            $newPosts[$cnt]['Setting Type ID']  =   $settingtypeName;
            $newPosts[$cnt]['Setting Sub Type ID']  =   $settingsubtypeName;
            $newPosts[$cnt]['Setting Name']   =   $post->setting_name;
            $newPosts[$cnt]['Setting Value']   =   $post->setting_value;
            $newPosts[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$post->id.'-'.$hidSettingTypeId.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$post->id.'-'.$hidSettingTypeId.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }
    //  echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($newPosts);
    //  exit();
        return $newPosts;
        
    }
    
    function ajaxResultsForSettingsAction(){
        $res    =   array();
        // echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($_POST);
        // exit();

        if($_POST['reqType'] != '') {
            switch($_POST['reqType']) {

                case 'formList' :
                    $newSettingsRes   =   array();
                    $formContent    =   '';
                    $hidRecID   =  $setting_types_id    =   $settings_sub_types_id   =   0;
                    $setting_name  =   $setting_value   =   '';

                    if($_POST['hidRecID'] != 0) {
                        $hidRecID   =   $_POST['hidRecID'];
                        $settingRes  =   $this->Systemsetting_model->getSettingsByRecordID($hidRecID);//$_POST['hidSettingTypeId']

                        if(!(empty($settingRes))) {
                            //assiign client record data here in an array
                            $res['data'] = 'success';

                            foreach($settingRes as $s) {
                                $setting_types_id   =   $s->setting_types_id;
                                $settings_sub_types_id  =   $s->settings_sub_types_id;
                                $setting_name =   $s->setting_name;
                                $setting_value =   $s->setting_value;
                                $created_at =   $s->created_at;
                                $created_by =   $s->created_by;
                                $updated_at =   $s->updated_at;
                            }
                        }
                    }

                    $formContent    .=  '<form name="formCom" id="formCom" method="POST" action="">
                        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
                        <input type="hidden" name="hidRecID" id="hidRecID" value="'.$hidRecID.'"/>
                            <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                <span class="text-red">*</span>
                                    <label for="Setting Type ID">Setting Type ID:</label>
                                    <select id="settingTypesId" name="settingTypesId" class="form-control" required>
                                        <option value="">Select Setting Type ID</option>';               
                    $settingtypeid =   $this->Settingtype_model->getSettingType();
                    $data['settingtypeid'] =   $settingtypeid;

                    if(!(empty($settingtypeid))) {
                        foreach ($settingtypeid as $stid) {
                            if($setting_types_id == $stid->id) {
                                $sele   =   'selected="SELECTED"';
                            } else {
                                $sele   =   '';
                            }

                            $formContent    .=  '<option value="'.$stid->id.'" '.$sele.'>'.$stid->setting_type_name.'</option>';
                        }
                    }

                    $formContent    .=  '</select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                <span class="text-red">*</span>
                                    <label for="Setting Sub Type ID">Setting Sub Type ID:</label>
                                    <select id="settingsSubTypesId" name="settingsSubTypesId" class="form-control" required>
                                        <option value="">Select Setting Sub Type ID</option>';
                    if($hidRecID != 0) {
                        $settingSubTypeId  =   $this->Settingsubtype_model->getSubSettingType($setting_types_id);
                        $data['settingSubTypeId']   =   $settingSubTypeId;

                        if(!(empty($settingSubTypeId))) {
                            foreach ($settingSubTypeId as $sstid) {
                                if($settings_sub_types_id == $sstid->id) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }

                                $formContent    .=  '<option value="'.$sstid->id.'" '.$sele.'>'.$sstid->setting_sub_type_name.'</option>';
                            }
                        }
                    }

                    $formContent    .= '</select>
                                </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                    <span class="text-red">*</span>
                                        <label for="Name">Setting Name:</label>
                                        <input required autofocus="" type="text" name="settingName" id="settingName" placeHolder="Enter Enter Name" 
                                        value="'.$setting_name.'" class="form-control" >
                                    </div>
                                </div>
                
                                <div class="col-md-4">
                                    <div class="form-group">
                                    <span class="text-red">*</span>
                                        <label for="Name">Setting Value:</label>
                                        <input required type="text" name="settingValue" id="settingValue" placeHolder="Enter Setting Value" 
                                       value="'.$setting_value.'" class="form-control" >
                                    </div>
                                </div>';

                $formContent  .=  
                           '</div><!-- class="row"--->
                            </form>';

                    $res['data']    =   'success';
                    $res['formContent']    =   $formContent;
                    break;

                case 'create' :
                // Do the update... 
                // call the same function which u do for adding new entry with slight modifications.
                $data   =   array(
                            'setting_types_id'   => $_POST['settingTypesId'],
                            'settings_sub_types_id' =>  $_POST['settingsSubTypesId'],
                            'setting_name' => $_POST['settingName'],
                            'setting_value' => $_POST['settingValue'],
                            'created_at'    =>  date('Y-m-d h:i:s'));
                $settingRes  =   $this->Systemsetting_model->add($data);
                if($settingRes == FALSE) {
                    $res['data']   =    'Already exists.';
                } 
                else{
                    if($settingRes > 0) {
                        $res['data']    =   'success';
                    } else {   
                            $res['data']    =   'failure';
                    }
                }

                break;

                case 'update' :
                    //echo "\r\n <br/> post vals for update : \r\n <br/><pre>"; print_r($_POST);
                    if($_POST['hidRecID'] != '') {
                        // Do the update... 
                        // call the same function which u do for adding new entry with slight modifications.
                        $newSettingsRes   =   array();
                        $postData   =   array(
                            'id'    =>  $_POST['hidRecID'],
                            'setting_types_id'   => $_POST['settingTypesId'],
                            'settings_sub_types_id' =>  $_POST['settingsSubTypesId'],
                            'setting_name' => $_POST['settingName'],
                            'setting_value' => $_POST['settingValue'],
                            'updated_at'    =>  date('Y-m-d h:i:s'));
                        $settingRes  =   $this->Systemsetting_model->add($postData);

                        if($settingRes == 1) {                   
                            $res['data']    =   'success';     
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
                

        case 'del':
            $newSettingsRes   =   array();

            if($_POST['hidRecID'] != 0) {
                $settingRes  =   $this->Systemsetting_model->delete($_POST['hidRecID']);

                if($settingRes == 1) {
                    $res['data']    =   'success';        
                } else {
                    $res['data']    =   'failure';
                }
            }

            break;
        }
    } 
        echo json_encode($res);
    }

    function ajaxResultsForInstitutesFiltersJSONGeneration(){
        $posts  =   $this->institutes_model->getInstitute();
        $writeJSONData  =   $this->fetchDataForInstitute($posts);
        $results    =   array();
        $results['data']['success'] =   1;
        $results['data']['writeJSONData']    =   $writeJSONData;
        //echo "\r\n <br/> writejson : \r\n <br/><pre>"; print_r($writeJSONData);
        echo json_encode($results['data']);
    }

    function fetchDataForInstitute($posts) {
        $newPosts   =   array();
        $cnt    =   0;

        foreach($posts as $post) {
            $newPosts[$cnt]['SNo']  =   ($cnt + 1);
            $newPosts[$cnt]['Institute Name']   =   $post->institute_name;
            $newPosts[$cnt]['Institute Address Line1']   =   $post->institute_address_line1;
            $newPosts[$cnt]['Institute Address Line2']   =   $post->institute_address_line2;
            $newPosts[$cnt]['Pincode']   =   $post->pincode;
            $newPosts[$cnt]['City ID']   =   $post->city_id;
            $newPosts[$cnt]['State ID']   =   $post->state_id;
            $newPosts[$cnt]['Country ID']   =   $post->country_id;
            $newPosts[$cnt]['Created At']   =   $post->created_at;
            $newPosts[$cnt]['Created By']   =   $post->created_by;
            $newPosts[$cnt]['Updated At']   =   $post->updated_at;
            $newPosts[$cnt]['Status']   =   $post->status;
            $newPosts[$cnt]['Action']   =   '<a href="javascript:void(0);" id="edit-'.$post->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$post->id.'" class="logAction">
                                                <span class="badge badge-danger"> Delete </span>
                                            </a>';
            $cnt++;
        }
        return $newPosts;
    }
    
    function ajaxResultsForInstituteAction(){
        $res    =   array();
        // echo "\r\n <br/> POST VALS : \r\n <Br/><pre>"; print_r($_POST);
        // exit();

        if($_POST['reqType'] != '') {
            switch($_POST['reqType']) {

                case 'formList' :
                    $newInstituteRes   =   array();
                    $formContent    =   '';
                    $hidRecID   =   $city_id    =   $state_id   =   $country_id =   0;
                    $institute_name  =   $institute_address_line1   =   $institute_address_line2    =   $pincode    =   $status =   '';

                    if($_POST['hidRecID'] != 0) {
                        $hidRecID   =   $_POST['hidRecID'];
                        $instituteRes  =   $this->institutes_model->getInstitute($hidRecID);

                        if(!(empty($instituteRes))) {
                            //assiign client record data here in an array
                            $res['data'] = 'success';

                            foreach($instituteRes as $i) {
                                $institute_name =   $i->institute_name;
                                $institute_address_line1 =   $i->institute_address_line1;
                                $institute_address_line2 =   $i->institute_address_line2;
                                $pincode =   $i->pincode;
                                $city_id =   $i->city_id;
                                $state_id =   $i->state_id;
                                $country_id =   $i->country_id;
                                $created_at =   $i->created_at;
                                $created_by =   $i->created_by;
                                $updated_at =   $i->updated_at;
                                $status =   $i->status;
                            }
                        }
                    }

                    $formContent    .=  '<form name="formCom" id="formCom" method="POST" action="">
                        <input type="hidden" name="hidProjectURL" id="hidProjectURL" value="'.base_url().'"/>
                        <input type="hidden" name="hidRecID" id="hidRecID" value="'.$hidRecID.'"/>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                    <span class="text-red">*</span>
                                        <label for="Name">Institute Name:</label>
                                        <input required autofocus="" type="text" name="instituteName" id="instituteName" placeHolder="Enter Institute Name" 
                                        value="'.$institute_name.'" class="form-control" >
                                    </div>
                                </div>
                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Name">Institute Address Line1:</label>
                                        <textarea name="instituteAddressLine1" id="instituteAddressLine1" placeHolder="Enter Address 1" 
                                       class="form-control" > '.$institute_address_line1.' </textarea>
                                    </div>
                                </div>
                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Name">Institute Address Line2:</label>
                                        <textarea name="instituteAddressLine2" id="instituteAddressLine2" placeHolder="Enter Address 2"
                                          class="form-control">'.$institute_address_line2.'</textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Name">Pincode:</label>
                                        <input type="text" name="pincode" id="pincode" placeHolder="Enter Pincode" 
                                        value="'.$pincode.'" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                    <span class="text-red">*</span>
                                        <label for="Name">City ID:</label>
                                        <select id="cityId" name="cityId" class="form-control" required>
                                            <option value="">Select City</option>';

                    if($hidRecID != 0) {
                        $cities =   $this->Cities_model->getCityByState($state_id);
                        $data['cities'] =   $cities;

                        if(!(empty($cities))) {
                            foreach ($cities as $cid) {
                                if($city_id == $cid->id) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }

                                $formContent    .=  '<option value="'.$cid->id.'"'.$sele.'">'.$cid->city_name.'</option>';
                            }
                        }
                    }

                    $formContent    .=  '</select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                    <span class="text-red">*</span>
                                        <label for="Name">State ID:</label>
                                        <select id="stateId" name="stateId" class="form-control" required>
                                            <option value="">Select State</option>';
                    if($hidRecID != 0) {
                        $states  =   $this->States_model->getStatesByCountry($country_id);//$state_id
                        $data['states']   =   $states;

                        if(!(empty($states))) {
                            foreach ($states as $sid) {
                                if($state_id == $sid->id) {
                                    $sele   =   'selected="SELECTED"';
                                } else {
                                    $sele   =   '';
                                }

                                $formContent    .=  '<option value="'.$sid->id.'"'.$sele.'">'.$sid->state_name.'</option>';
                            }
                        }
                    }

                    $formContent    .= '</select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                    <span class="text-red">*</span>
                                        <label for="Name">Country ID:</label>
                                        <select id="countryId" name="countryId" class="form-control" required>
                                            <option value="">Select Country</option>';

                    $countries  =   $this->Countries_model->get();
                    $data['countries']   =   $countries;

                    if(!(empty($countries))) {
                        foreach ($countries as $countryid) {
                            if($country_id == $countryid->id) {    
                                $sele   =   'selected="SELECTED"';
                            } else if($countryid->is_default == 1) {
                                $sele   =   'selected="SELECTED"';
                            } else {    
                                $sele   =   '';
                            }

                            $formContent    .=  '<option value="'.$countryid->id.'"'.$sele.'">'.$countryid->country_name.'</option>';
                        }
                    }
                        
                    $formContent    .= '</select>
                                    </div>
                                </div>';

                    if($hidRecID != 0) {            
                        $formContent    .= '<div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="Name">Status:</label>
                                                    <select id="status" name="status" class="form-control">
                                                        <option value="'.$status.'">Select Status</option>';

                        if($status == 'Active') {
                            $formContent    .=  '<option value="Active" SELECTED>Active</option>
                                                <option value="InActive">InActive</option>
                                                <option value="None">NONE</option>';
                        } else if($status == 'InActive') {
                            $formContent    .=  '<option value="Active">Active</option>
                                                <option value="InActive" SELECTED>InActive</option>
                                                <option value="None">NONE</option>';
                        } else if($status == 'NONE') {
                            $formContent    .=  '<option value="Active">Active</option>
                                                <option value="InActive">InActive</option>
                                                <option value="None" SELECTED>NONE</option>';
                        } else {
                            $formContent    .=  '<option value="Active">Active</option>
                                                <option value="InActive">InActive</option>
                                                <option value="None">NONE</option>';
                        }

                        $formContent  .=  '</select>
                                        </div>';
                    }

                    $formContent  .=  '</div>
                            </div><!-- class="row"--->
                            </form>';
                    $res['data']    =   'success';
                    $res['formContent']    =   $formContent;
                    break;

                case 'create' :
                // Do the update... 
                // call the same function which u do for adding new entry with slight modifications.
                    $data   =   array(
                        'institute_name'   =>  $_POST['instituteName'],
                        'institute_address_line1'   =>  $_POST['instituteAddressLine1'],
                        'institute_address_line2'   =>  $_POST['instituteAddressLine2'],
                        'pincode'   =>  $_POST['pincode'],
                        'city_id'   =>  $_POST['cityId'],
                        'state_id'  =>  $_POST['stateId'],
                        'country_id'    =>  $_POST['countryId'],
                        'created_at'    =>  date('Y-m-d h:i:s'));
                        // Need to include created_by field after Session workflow is implemented.
                       
                    $instituteRes  =   $this->institutes_model->add($data);

                    if($instituteRes == FALSE) {
                        $res['data']   =    'Already exists.';
                    } else{
                        if($instituteRes > 0) {
                            $res['data']    =   'success';
                        } else {   
                                $res['data']    =   'failure';
                        }
                    }

                 case 'update' :
                    // echo "\r\n <br/> post vals for update : \r\n <br/><pre>"; print_r($_POST);
                    if($_POST['hidRecID'] != '') {
                        // Do the update... 
                        // call the same function which u do for adding new entry with slight modifications.
                        $newInstituteRes   =   array();
                        $postData   =   array(
                                        'id'    =>  $_POST['hidRecID'],
                                        'institute_name'   =>  $_POST['instituteName'],
                                        'institute_address_line1'   =>  $_POST['instituteAddressLine1'],
                                        'institute_address_line2'   =>  $_POST['instituteAddressLine2'],
                                        'pincode'   =>  $_POST['pincode'],
                                        'city_id'   =>  $_POST['cityId'],
                                        'state_id'  =>  $_POST['stateId'],
                                        'country_id'    =>  $_POST['countryId'],
                                        'updated_at'    =>  date('Y-m-d h:i:s'));
                        $instituteRes  =   $this->institutes_model->add($postData);

                        if($instituteRes == 1) {                   
                            $res['data']    =   'success';     
                        } else {        
                            $res['data']    =   'failure';
                        }
                    }

                    break;
                

                case 'del':
                    $newInstituteRes   =   array();

                    if($_POST['hidRecID'] != 0) {
                        $instituteRes  =   $this->institutes_model->delete($_POST['hidRecID']);

                        if($instituteRes == 1) {
                            $res['data']    =   'success';        
                        } else {
                            $res['data']    =   'failure';
                        }
                    }

                    break;
                }   
            }
 
        echo json_encode($res);
    }
}
}