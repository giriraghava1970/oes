<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Generalfunctions {
    function __construct() {
        $this->CI	=	& get_instance();
        $this->CI->load->model('Menumodules_model');
        $this->CI->load->model('Submodulemenus_model');
        $this->CI->load->library('user_agent');
        $this->CI->load->model('Userlog_model','',TRUE);
    }

    function fetchStatusDrop() {
        $statRes    =   $this->CI->QuestionTypeMaster_model->fetchStatus();
        return $statRes;
    }
    
    public function fetchMenus() {
        $menuDtls   =   '';
        $modRes =   $this->CI->Menumodules_model->fetchModules();
        $menuDtls   =   '<ul class="nav nav-sidebar" data-nav-type="accordion">
    						<!-- Main -->
    						<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
					        <li class="nav-item">
    							<a href="'.base_url().'Dashboard" class="nav-link <?php echo $activMenu;?>">
    								<i class="icon-home2"></i>
    								<span data-i18n="nav_inline.nav.Dashboard" data-fouc> Dashboard</span>
    							</a>
    						</li>';
        if(!(empty($modRes))) {
            $res['data']    =   'success';
            
            foreach ($modRes as $mod) {
                $liClass	=	'nav-item';
                $navLinkActive	=	'';
                $ulShow =   '';//style="display:none;"';
                $menuDtls   .=  '<li class="'.$liClass.'">
									<a href="#" class="nav-link"><i class="'.$mod->icoClass.'"></i> <span>'.$mod->name.'</span></a>';
                $subModMenusRes =   $this->CI->Submodulemenus_model->fetchSubModuleMenus($mod->id);
                //echo "\r\n <br/> subModMenusRes : \r\n <br/><pre>"; print_r($subModMenusRes);echo '</pre>';
                if(!(empty($subModMenusRes))) {
                    $menuDtls	.=	'<ul id="'.$mod->name.'" class="nav nav-group-sub" data-submenu-title="'.$mod->name.'">';
                    
                    foreach($subModMenusRes as $subModMenu) {
                        $liClass2	=	'';
                        $navLinkActive2	=	'';
                        $menuDtls	.=	'<li class="'.$liClass2.'">
											<a href="'.base_url().$subModMenu->menuLink.'" class="nav-link" '.$navLinkActive2.'><i class="'.$subModMenu->icoClass.'"></i><span>'.$subModMenu->name.'</a>
										</li>';
                    }
                }
                
                $menuDtls	.=	'     </ul><!--closure of submodule ul -->';
                $menuDtls   .=  '</li>';
            }
            
            $menuDtls   .=  '</ul>';
        }
        
        return $menuDtls;
    }

    public function field_enums($table = '', $field = '') {
        $enums  =   array();

        if ($table == '' || $field == '') return $enums;
        $CI     =   & get_instance();
        preg_match_all("/'(.*?)'/", $CI->db->query("SHOW COLUMNS FROM {$table} LIKE '{$field}'")->row()->Type, $matches);
        $cnt    =   0;

        foreach ($matches[1] as $key => $value) {
            $enums[$value]  =   $value;
            $cnt++;
        }

        return $enums;
    }

    function getUserRole() {
        $admin  =   $this->CI->session->userdata('admin');
        $roles  =   $admin['roles'];

        if ($admin) {
            $role_key   =   key($roles);
            return json_encode(array('id' => $roles[$role_key], 'name' => $role_key));
        }
    }

    public function setUserLog($username, $role) {
        if ($this->CI->agent->is_browser()) {
            $agent  =   $this->CI->agent->browser() . ' ' . $this->CI->agent->version();
        } elseif ($this->CI->agent->is_robot()) {
            $agent  =   $this->CI->agent->robot();
        } elseif ($this->CI->agent->is_mobile()) {
            $agent  =   $this->CI->agent->mobile();
        } else {
            $agent  =   'Unidentified User Agent';
        }

        $data   =   array(
            'user'  =>  $username,
            'role'  =>  $role,
            'ipaddress' =>  $this->CI->input->ip_address(),
            'user_agent'    =>  $agent . ", " . $this->CI->agent->platform(), );
        $this->CI->Userlog_model->add($data);
    }
}?>