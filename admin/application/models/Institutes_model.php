<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Institutes_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getInstitute($id=null){
        $this->db->select('*')->from('institute_details');
        if($id != null){
            $this->db->where('id',$id);
        }
        //$this->db->order_by('client_first_name','asc');
        $this->db->order_by('id','ASC');
        $query  =   $this->db->get();
         return $query->result();
    }

    public function add($data) {
        if(isset($data['institute_name'])){
            $resultForIname =   $this->checkInstitute('institute_name', $data['institute_name']);
        } else {
            $resultForIname = 0;
        }
        if ($resultForIname == 1){
        if(isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('institute_details', $data);
                    return 1;
                } else {
                    $this->db->insert('institute_details', $data);
                    return $this->db->insert_id();
                } 
            }else {
                if  ($resultForIname == 0) {
                    if (isset($data['id'])) {
                        $this->db->where('id', $data['id']);
                        $this->db->update('institute_details', $data);
                        return 1;
                    } else {
                        $this->db->insert('institute_details', $data);
                        return $this->db->insert_id();
                    }
                } else {
                    return FALSE;
                }
            }
    }
    
    public function checkInstitute($fieldName = '', $fieldVal = '') {
        if ( ($fieldName != '' ) && ($fieldVal != '') ) {
            $this->db->select('*')->from('institute_details');
            $this->db->where($fieldName, $fieldVal);
            $query  =   $this->db->get();

            if($query->num_rows() > 0) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    public function delete($id = null) {
        if($id != null) {
            $this->db->where('id', $id);
            $this->db->delete('institute_details');
            return 1;
        }
    }
    }