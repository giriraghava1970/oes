<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {
	public function checkLogin($data) {
	    $record    =   $this->getByEmail($data['email']);

	    if ($record) {
	        $pass_verify   =   $this->enc_lib->passHashDyc($data['password'], $record->userpassword);

	        if ($pass_verify) {
	            $roles =   $this->Userroles_model->getUserRoles($record->id);
	            $record->roles =   array($roles[0]->user_role_name => $roles[0]->id);
	            return $record;
	        }
	    }

	    return false;
	}

	public function getByEmail($email) {
	    $this->db->select('*');
	    $this->db->from('users');
	    $this->db->where('username', $email);
	    $query  =   $this->db->get();

	    if ($query->num_rows() == 1) {
	        return $query->row();
	    } else {
	        return FALSE;
	    }
	}
}

/* End of file accounts.php */
/* Location: ./application/models/accounts.php */