<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CourseDetails_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get($id=null){ 
        $this->db->select('*')->from('course_details');

        if($id != null){
            $this->db->where('id',$id);
        }

        $this->db->order_by('id', 'DESC');
        $query  =   $this->db->get();
        return $query->result(); 
    }

    public function getClassesByCourseID($courseID = null) {
        if($courseID != null) {
            $this->db->select('*')->from('course_details');
            $this->db->where('course_id', $courseID);
            $query  =   $this->db->get();
            return $query->result();
        }
    }

    public function add($data) {
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('course_details', $data);
            return 1;
        } else {
            $this->db->insert('course_details', $data);
            return $this->db->insert_id();
        }
    }

    public function delete($courseID = null) {
        if($courseID != null) {
            $this->db->where('course_id', $courseID);
            $this->db->delete('course_details');
            return 1;
        }
    }
}