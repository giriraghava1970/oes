<?php   if (!defined('BASEPATH'))
            exit('No direct script access allowed');

class Userroles_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getUserRoles($userID) {
        echo "\r\n <br/> $userID : " .$userID;
        $this->db->select('*');
        $this->db->from('user_roles');
        //$this->db->join('roles', 'roles.id=user_roles.role_id', 'inner');
        $this->db->where('id', $userID);
        $query  =   $this->db->get();
        return $query->result(); 
    }
}