<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Modules_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getModulesBySubjectID($subjectID = null) {
        if($subjectID != null) {
            $this->db->select('*')->from('modules');
            $this->db->where('subject_id', $subjectID);
            $this->db->order_by('id', 'ASC');
            $query  =   $this->db->get();
            return $query->result();
        }
    }

    public function get($moduleID = null){
        $this->db->select('*')->from('modules');

        if($moduleID != null){
            $this->db->where('id',$moduleID);
        }

        $this->db->order_by('id', 'ASC');
        $query  =   $this->db->get();
        return $query->result();
    }

    public function getList($dataForFilter = array()){
        $this->db->select('modules.*, subjects.subject_name as subjectName')->from('modules');
        $this->db->join('subjects', 'modules.subject_id = subjects.id');

        if(!(empty($dataForFilter))) {
            if($dataForFilter['subjectID'] != null) {
                $this->db->where('subject_id', $dataForFilter['subjectID']);
            }

            if($dataForFilter['moduleName'] != null) {
                $this->db->where('module_name LIKE "%'.$dataForFilter['moduleName'].'%"');
            }
        }

        $this->db->order_by('id', 'ASC');
        $query  =   $this->db->get();
        return $query->result(); 
    }

    public function add($data) {
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('modules', $data);
            return 1;
        } else {
            $this->db->insert('modules', $data);
            return $this->db->insert_id();
        }
    }

    public function checkExists($fieldName, $fieldVal) {
        $this->db->select('*')->from('modules');
        $this->db->where($fieldName, $fieldVal);
        $query  =   $this->db->get();
        return $query->num_rows();
    }

    public function delete($moduleID = null) {
        if($moduleID != null) {
            $this->db->where('id', $moduleID);
            $this->db->delete('modules');
            return 1;
        }
    }
}