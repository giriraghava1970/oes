<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Courses_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get($id=null){ 
        $this->db->select('*')->from('courses');

        if($id != null){
            $this->db->where('id',$id);
        }

        $this->db->order_by('id', 'DESC');
        $query  =   $this->db->get();
        return $query->result(); 
    }

    public function add($data) {
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('courses', $data);
            return 1;
        } else {
            $this->db->insert('courses', $data);
            return $this->db->insert_id();
        }
    }

    public function checkExists($fieldName, $fieldVal) {
        $this->db->select('*')->from('courses');
        $this->db->where($fieldName, $fieldVal);
        $query  =   $this->db->get();
        return $query->num_rows();
    }

    public function delete($id = null) {
        if($id != null) {
            $this->db->where('id', $id);
            $this->db->delete('courses');
            return 1;
        }
    }
}