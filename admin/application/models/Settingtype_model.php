<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Settingtype_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSettingType($id = null){
        $this->db->select('*')->from('settings_types_mst');
        if($id != null) {
            $this->db->where('id', $id);
        }
        $query  =   $this->db->get();
        return $query->result();
    }

    public function getSettingtypeByID($settingtypeID = null) {
        if($settingtypeID != null) {
            $this->db->select('*')->from('settings_types_mst');
            $this->db->where('id', $settingtypeID);
            $query  =   $this->db->get();
            // echo "\r\n <Br/> getStateByID SQL : \r\n <br/><pre>"; print_r($this->db->last_query());
            return $query->result();
        }
    }

}