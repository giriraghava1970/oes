<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Document_model extends CI_Model {
    public function __construct() {
        parent::__construct();

    }

    public function get($id=null){
        $this->db->select('*')->from('doc_type_mst');
        if($id != null){
            $this->db->where('id',$id);
        }
        $this->db->order_by('id', 'DESC');
        $query  =   $this->db->get();
        // echo "Last Query:<pre><br/>";print_r($this->db->last_query());
        return $query->result(); 
    }

    public function add($data) {
            if (isset($data['id'])) {
                $this->db->where('id', $data['id']);
                $this->db->update('doc_type_mst', $data);
                return 1;
            } else {
                $this->db->insert('doc_type_mst', $data);
                return $this->db->insert_id();
            }
    }


    public function checkField($fieldName,$fieldVal) {
        $this->db->select('*')->from('doc_type_mst');
        $this->db->where($fieldName, $fieldVal);
        $query  =   $this->db->get();
        return $query->num_rows();
    }

    public function delete($id = null) {
        if($id != null) {
            $this->db->where('id', $id);
            $this->db->delete('doc_type_mst');
            return 1;
        }
    }
    public function getStatus()
    {
        $this->db->select('status')->from('doc_type_mst');
        $query  =   $this->db->get();
        return $query->result();
    }

    // public function getCourses($id  =   null)
    // {
    //     if($id  !=  null)
    //     {
    //         $this->db->select('*')->from('courses');
    //         $this->db->where('id', $id);
    //         $query  =   $this->db->get();
    //         return $query->row()->course_name;
    //     }
    //     else
    //     {
    //         $this->db->select('*')->from('courses');
    //         $query  =   $this->db->get();
    //         return $query->result();
    //     }
    // }
}