<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Batches_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getBatchByCourseID($courseID = null) {
        if($courseID != null) {
            $this->db->select('*')->from('batches');
            $this->db->where('course_id', $courseID);
            $query  =   $this->db->get();
            return $query->result();
        }
    }

    public function getBatchByID($batchID = null) {
        if($batchID != null) {
            $this->db->select('*')->from('batches');
            $this->db->where('id', $batchID);
            $query  =   $this->db->get();
            return $query->result();
        }
    }

    public function get($id=null){
        $this->db->select('*')->from('batches');
        if($id != null){
            $this->db->where('id',$id);
        }
        $this->db->order_by('id', 'DESC');
        $query  =   $this->db->get();
        return $query->result(); 
    }

    public function add($data) {
            if (isset($data['id'])) {
                $this->db->where('id', $data['id']);
                $this->db->update('batches', $data);
                return 1;
            } else {
                $this->db->insert('batches', $data);
                return $this->db->insert_id();
            }
    }

    public function checkField($fieldName,$fieldVal) {
        $this->db->select('*')->from('batches');
        $this->db->where($fieldName, $fieldVal);
        $query  =   $this->db->get();
        return $query->num_rows();
    }

    public function delete($id = null) {
        if($id != null) {
            $this->db->where('id', $id);
            $this->db->delete('batches');
            return 1;
        }
    }
    
    public function getStatus()
    {
        $this->db->select('status')->from('batches');
        $query  =   $this->db->get();
        return $query->result();
    }

    public function getCourses($id  =   null)
    {
        if($id  !=  null)
        {
            $this->db->select('*')->from('courses');
            $this->db->where('id', $id);
            $query  =   $this->db->get();
            return $query->row()->course_name;
        }
        else
        {
            $this->db->select('*')->from('courses');
            $query  =   $this->db->get();
            return $query->result();
        }
    }
}