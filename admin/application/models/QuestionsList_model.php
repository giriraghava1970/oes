<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class QuestionsList_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get($id=null){
        $this->db->select('*')->from('questions');
        if($id != null){
            $this->db->where('id', $id);
        }

        $query  =   $this->db->get(); 
        return $query->result();
    }

   

     public function add($data) {
            if (isset($data['id'])) {
                $this->db->where('id', $data['id']);
                $this->db->update('questions', $data);
                return 1;
            } else {
                $this->db->insert('questions', $data);
                return $this->db->insert_id();
            }
    }
    

    public function checkSettings($fieldName = '', $fieldVal = '') {
        if ( ($fieldName != '' ) && ($fieldVal != '') ) {
            $this->db->select('*')->from('questions');
            $this->db->where($fieldName, $fieldVal);
            $query  =   $this->db->get();

            if($query->num_rows() > 0) {
                return 1;
            } else {
                return 0;
            }
        }
    }
   
    public function delete($id = null) {
        if($id != null) {
            $this->db->where('id', $id);
            $this->db->delete('questions');
            return 1;
        }
    }
    }


