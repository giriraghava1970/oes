<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    Class Userprivileges_model extends CI_Model {
		//public function __construct()
		//{
		//$this->load->database();
		//}

		public function getTotal() {
			return $this->db->get('user_modules')->result_array();
		}

		public function UserPriviliges() { 
			$this->db->select('*');
			$this->db->from('user_modules');
			//$this->db->where('idROLE',$idUser);
			$query	 =	$this->db->get();
			$result=$query->result_array();

			if($query->num_rows() > 0 ) {
				foreach($result as $row) {
					$record['idPRIVILEGES']=$row['idPRIVILEGES'];
					$record['idMODULES']=$row['idMODULES'];
					$record['Create']=$row['Create'];
					$record['Update']=$row['Update'];
					$record['View']=$row['View'];
					$record['Delete']=$row['Delete'];
					$record['Permission']=$row['Permission'];
				}
				return $record;
			} else
				return false;
		}
	}?>