<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class States_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function add($data) {

    }

    public function getStatesByCountry($countryID = null) {
        // echo "\r\n <br/> ountry id : " . $countryID;
        if($countryID != null) {
            $this->db->select('*')->from('states_mst');
            $this->db->where('country_id', $countryID);
            $this->db->order_by('state_name', 'ASC');
            $query  =   $this->db->get();
            // echo "\r\n <Br/> getStatesByCount SQL : \r\n <br/><pre>"; print_r($this->db->last_query());
            return $query->result();
        }
    }

    public function getStateByID($stateID = null) {
        if($stateID != null) {
            $this->db->select('*')->from('states_mst');
            $this->db->where('id', $stateID);
            $query  =   $this->db->get();
            // echo "\r\n <Br/> getStateByID SQL : \r\n <br/><pre>"; print_r($this->db->last_query());
            return $query->result();
        }
    }


    public function get($id = null) {
        $this->db->select('*')->from('countries_mst');

        if($id != null) {
            $this->db->where('id', $id);
        }
        $this->db->order_by('id', 'DESC');
        $query  =   $this->db->get();
        return $query->result();
    }
}