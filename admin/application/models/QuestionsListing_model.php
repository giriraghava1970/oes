<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class QuestionsListing_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get($id=null){
        $this->db->select('*')->from('questions');
        if($id != null){
            $this->db->where('id',$id);
        }
        $this->db->order_by('id', 'DESC');
        $query  =   $this->db->get(); 
        return $query->result(); 
    }
    public function getSubject($id = null)
    {
        $this->db->select('subject_name')->from('subjects');
        if($id != null)
        {
        $this->db->where('id',$id);
        $query  =   $this->db->get();
        return $query->row()->subject_name;
    }
    else{
        $query  =   $this->db->get();
        return $query->result();
   	 	}
	}
   
     public function getTopic($id = null)
    
    {
        $this->db->select('topic_name')->from('topics');
        if($id != null)
        {
        $this->db->where('id',$id);
        $query  =   $this->db->get();
        return $query->row()->topic_name;
    }
    else{
        $query  =   $this->db->get();
        return $query->result();
   	 	}
	}

	public function getQuestionType($id = null)
    {
        $this->db->select('question_type')->from('question_types_mst');
        if($id != null)
        {
        $this->db->where('id',$id);
        $query  =   $this->db->get();
        return $query->row()->question_type;
    }
    else{
        $query  =   $this->db->get();
        return $query->result();
    }

}

    