<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Settings_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSettings($id=null){
        $this->db->select('*')->from('settings');
        if($id != null){
            $this->db->where('id',$id);
        }
        //$this->db->order_by('client_first_name','asc');
        $this->db->order_by('id','ASC');
        $query  =   $this->db->get();
         return $query->result();
    }

    public function add($data) {
        // print_r($data);
        if(isset($data['title'])){
            $resultForTitle =   $this->checkSettings('title', $data['title']);
        } else {
            $resultForTitle = 0;
        }
        if(isset($data['key'])){
            $resultForKey =   $this->checkSettings('key', $data['key']);
        } else {
            $resultForKey = 0;
        }
        if(isset($data['description'])){
            $resultForDescription =   $this->checkSettings('description', $data['description']);
        } else {
            $resultForDescription = 0;
        }

        //echo "\r\n <br/> resultFor Nam : " . $resultForFName . " email : " . $resultForEmail;
        if ( ($resultForTitle == 1) && ($resultForKey == 1) && ($resultForDescription == 1)){
            if (isset($data['id'])) {
                $this->db->where('id', $data['id']);
                $this->db->update('settings', $data);
                return 1;
            } else {
                $this->db->insert('settings', $data);
                return $this->db->insert_id();
            }
        } else {
            if ( ($resultForTitle == 0) && ($resultForKey == 0) && ($resultForDescription == 0)){
                if (isset($data['id'])) {
                    $this->db->where('id', $data['id']);
                    $this->db->update('settings', $data);
                    return 1;
                } else {
                    $this->db->insert('settings', $data);
                    return $this->db->insert_id();
                }
            } else {
                return FALSE;
            }
        }
    }

    public function checkSettings($fieldName = '', $fieldVal = '') {
        if ( ($fieldName != '' ) && ($fieldVal != '') ) {
            $this->db->select('*')->from('settings');
            $this->db->where($fieldName, $fieldVal);
            $query  =   $this->db->get();

            if($query->num_rows() > 0) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    // public function delete($id = null) {
    //     if($id != null) {
    //         $this->db->where('client_id', $id);
    //         $this->db->delete('clients');
    //         return 1;
    //     }
    // }
}