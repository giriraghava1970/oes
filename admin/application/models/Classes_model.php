<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Classes_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getClassesByIDs($classIDs = null) {
        if($classIDs != null){
            $this->db->select('*')->from('classes');
            $this->db->where('id IN ('.$classIDs.')');
            $this->db->order_by('id', 'ASC');
            $query  =   $this->db->get();
            return $query->result();
        }
    }

    public function get($classID = null){
        $this->db->select('*')->from('classes');
        
        if($classID != null){
            $this->db->where('id',$classID);
        }

        $this->db->order_by('id', 'ASC');
        $query  =   $this->db->get();
        return $query->result();
    }

    public function getList($dataForFilter = array()){
        $this->db->select('*')->from('classes');

        if(!(empty($dataForFilter))) {
            if($dataForFilter['className'] != null) {
                $this->db->where('class_name LIKE "%'.$dataForFilter['className'].'%"');
            }
        }

        $this->db->order_by('id', 'DESC');
        $query  =   $this->db->get();
        //echo "\r\n <br/> last qry in classes getList : \r\n <br/><pre>"; print_r($this->db->last_query());
        return $query->result(); 
    }

    public function add($data) {
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('classes', $data);
            return 1;
        } else {
            $this->db->insert('classes', $data);
            return $this->db->insert_id();
        }
    }

    public function checkExists($fieldName, $fieldVal) {
        $this->db->select('*')->from('classes');
        $this->db->where($fieldName, $fieldVal);
        $query  =   $this->db->get();
        return $query->num_rows();
    }

    public function delete($classID = null) {
        if($classID != null) {
            $this->db->where('id', $classID);
            $this->db->delete('classes');
            return 1;
        }
    }
}