<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Submodulemenus_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function fetchSubModuleMenus($parentModuleID=null){
        $this->db->select('*')->from('submodulemenus');

        if($parentModuleID != null){
            $this->db->where('parentModuleID',$parentModuleID);
        }

        $this->db->where('status', 1);
        $this->db->order_by('sorting_order', 'asc');
        $query  =   $this->db->get();
        return $query->result();
    }
}