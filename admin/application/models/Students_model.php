<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Students_model extends CI_Model {
    public function __construct() {
        parent::__construct();

    }

    public function get($id = null){
        $this->db->select('*')->from('students');

        if($id != null){
            $this->db->where('id',$id);
        }

        $this->db->order_by('id', 'DESC');
        $query  =   $this->db->get();
        return $query->result(); 
    }

    public function add($data) {
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('students', $data);
            return 1;
        } else {
            $this->db->insert('students', $data);
            return $this->db->insert_id();
        }
    }

    public function checkField($fieldName,$fieldVal) {
        $this->db->select('*')->from('students');
        $this->db->where($fieldName, $fieldVal);
        $query  =   $this->db->get();
        return $query->num_rows();
    }

    public function delete($id = null) {
        if($id != null) {
            $this->db->where('id', $id);
            $this->db->delete('students');
            return 1;
        }
    }

    public function getStatus() {
        $this->db->select('status')->from('students');
        $query  =   $this->db->get();
        return $query->result();
    }

    public function getEmail($id  =   null){
        if($id  !=  null){
            $this->db->select('*')->from('student_contacts');
            $this->db->where('student_id', $id);
            $query  =   $this->db->get();
            // echo "<pre>";print_r($this->db->last_query());exit();
            return $query->row()->email;
        } else {
            $this->db->select('*')->from('student_contacts');
            $query  =   $this->db->get();
            return $query->result();
        }
    }

    public function image_update( $id, $img_name) {
        $this->db->set('photo', $img_name);
        $this->db->where('id', $id);
        $this->db->update('students');
        return 1;
    }

    public function addContact($data)
    {
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('student_contacts', $data);
            return 1;
        } else {
            $this->db->insert('student_contacts', $data);
            return $this->db->insert_id();
        }
    }
}