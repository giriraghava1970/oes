<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Questiontypemaster_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getList($id = null){
        $this->db->select('*')->from('question_types_master');
        $this->db->order_by('id', 'ASC');
        $query  =   $this->db->get();
        return $query->result();
    }

    public function get($id = null){
        $this->db->select('*')->from('question_types_master');

        if($id != null){
            $this->db->where('id',$id);
        }

        $this->db->order_by('id', 'ASC');
        $query  =   $this->db->get(); 
        return $query->result(); 
    }

    public function add($data) {
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('question_types_master', $data);
            return 1;
        } else {
            $this->db->insert('question_types_master', $data);
            return $this->db->insert_id();
        }
    }

    public function checkExists($fieldName, $fieldVal) {
        $this->db->select('*')->from('question_types_master');
        $this->db->where($fieldName, $fieldVal);
        $query  =   $this->db->get();
        return $query->num_rows();
    }

    public function delete($id = null) {
        if($id != null) {
            $this->db->where('id', $id);
            $this->db->delete('question_types_master');
            return 1;
        }
    }
}