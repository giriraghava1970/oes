<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Regions_model extends CI_Model {
    public function __construct() {
        parent::__construct();

    }

    //////COUNTRIES//////

    public function get($id=null){ 
        $this->db->select('*')->from('countries_mst');
        if($id != null){
            $this->db->where('id',$id);
        }
        $this->db->order_by('id', 'DESC');
        $query  =   $this->db->get();
        return $query->result(); 
    }

    public function add($data) {
            if (isset($data['id'])) {
                $this->db->where('id', $data['id']);
                $this->db->update('countries_mst', $data);
                return 1;
            } else {
                $this->db->insert('countries_mst', $data);
                return $this->db->insert_id();
            }
    }

    public function checkField($fieldName,$fieldVal) {
        $this->db->select('*')->from('countries_mst');
        $this->db->where($fieldName, $fieldVal);
        $query  =   $this->db->get();
        return $query->num_rows();
    }

    public function delete($id = null) {
        if($id != null) {
            $this->db->where('id', $id);
            $this->db->delete('countries_mst');
            return 1;
        }
    }

    public function getStatus()
    {
        $this->db->select('status')->from('countries_mst');
        $query  =   $this->db->get();
        return $query->result();
    }


    //////STATES/////

    public function getStates($id=null){ 
        $this->db->select('*')->from('states_mst');
        if($id != null){
            $this->db->where('id',$id);
        }
        $this->db->order_by('id', 'DESC');
        $query  =   $this->db->get();
        return $query->result(); 
    }

    public function addStates($data) {
            if (isset($data['id'])) {
                $this->db->where('id', $data['id']);
                $this->db->update('states_mst', $data);
                return 1;
            } else {
                $this->db->insert('states_mst', $data);
                return $this->db->insert_id();
            }
    }

    public function checkFieldStates($fieldName,$fieldVal) {
        $this->db->select('*')->from('states_mst');
        $this->db->where($fieldName, $fieldVal);
        $query  =   $this->db->getStates();
        return $query->num_rows();
    }

    public function deleteStates($id = null) {
        if($id != null) {
            $this->db->where('id', $id);
            $this->db->delete('states_mst');
            return 1;
        }
    }

    public function getStatusStates()
    {
        $this->db->select('status')->from('states_mst');
        $query  =   $this->db->get();
        return $query->result();
    }



///// CITIES /////


public function getCities($id=null){ 
        $this->db->select('*')->from('cities_mst');
        if($id != null){
            $this->db->where('id',$id);
        }
        $this->db->order_by('id', 'DESC');
        $query  =   $this->db->get();
        return $query->result(); 
    }

    public function addCities($data) {
            if (isset($data['id'])) {
                $this->db->where('id', $data['id']);
                $this->db->update('cities_mst', $data);
                return 1;
            } else {
                $this->db->insert('cities_mst', $data);
                return $this->db->insert_id();
            }
    }

    public function checkFieldCities($fieldName,$fieldVal) {
        $this->db->select('*')->from('cities_mst');
        $this->db->where($fieldName, $fieldVal);
        $query  =   $this->db->get();
        return $query->num_rows();
    }

    public function deleteCities($id = null) {
        if($id != null) {
            $this->db->where('id', $id);
            $this->db->delete('cities_mst');
            return 1;
        }
    }

    public function getStatusCities()
    {
        $this->db->select('status')->from('cities_mst');
        $query  =   $this->db->get();
        return $query->result();
    }
}


