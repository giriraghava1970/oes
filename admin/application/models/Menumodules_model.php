<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menumodules_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function fetchModules($id = null){
        $this->db->select('*')->from('menu_modules');

        if($id != null){
            $this->db->where('id',$id);
        }

        $this->db->where('status', 1);
        $this->db->order_by('sorting_order', 'asc');
        $query  =   $this->db->get();
        return $query->result();
    }
}