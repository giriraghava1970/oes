<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Settingsubtype_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSubSettingType($settingTypesId = null) {
        if($settingTypesId != null) {
            $this->db->select('*')->from('settings_sub_types_mst');
            $this->db->where('setting_type_id', $settingTypesId);
            $query  =   $this->db->get();
            // echo "\r\n <Br/> getStatesByCount SQL : \r\n <br/><pre>"; print_r($this->db->last_query());
            return $query->result();
        }
    }

    public function get($id = null) {
        $this->db->select('*')->from('settings_types_mst');

        if($id != null) {
            $this->db->where('id', $id);
        }

        $query  =   $this->db->get();
        return $query->result();
    }

    public function getSettingsubtypeByID($settingsubtypeID = null) {
        if($settingsubtypeID != null) {
            $this->db->select('*')->from('settings_sub_types_mst');
            $this->db->where('id', $settingsubtypeID);
            $query  =   $this->db->get();
            // echo "\r\n <Br/> getStateByID SQL : \r\n <br/><pre>"; print_r($this->db->last_query());
            return $query->result();
        }
    }

}
