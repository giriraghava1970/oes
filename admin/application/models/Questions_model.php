<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Questions_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get($questionID = null){
        $this->db->select('*')->from('questions');

        if($questionID != null){
            $this->db->where('id', $questionID);
        }

        $this->db->order_by('id', 'ASC');
        $query  =   $this->db->get();
        return $query->result();
    }

    public function getTotalQuestions($questionID = null) {
        if($questionID != null) {
            $this->db->select('count(*) as totalQuestions')->from('question_details');
            $this->db->where('question_id', $questionID);
            $query  =   $this->db->get();
            return $query->result();
        }
    }

    public function getList($dataForFilter = array()){
        $this->db->select('questions.*, question_details.*, chapters.id as chapterID, chapters.chapter_name as chapterName, 
                           subjects.id as subjectID, subjects.subject_name as subjectName,
                           topics.id as topicID, topics.topic_name as topicName')->from('questions');
        $this->db->join('question_details', 'question_details.question_id= questions.id');
        $this->db->join('chapters', 'chapters.id = questions.chapter_id');
        $this->db->join('subjects', 'subjects.id = questions.subject_id');
        $this->db->join('topics', 'topics.id = questions.topic_id');

        if(!(empty($dataForFilter))) {
            if ( ($dataForFilter['subjectID'] != null) && ($dataForFilter['chapterID'] != null) ) {
                $this->db->where('topics.subject_id', $dataForFilter['subjectID']);
                $this->db->where('topics.module_id', $dataForFilter['chapterID']);
            }

            if($dataForFilter['questionName'] != null) {
                $this->db->where('question_details.question_details_text LIKE "%' . $dataForFilter['questionName'].'%"');
            }
        }

        $this->db->order_by('questions.id', 'ASC');
        $query  =   $this->db->get();
        return $query->result(); 
    }

    public function getQuestionOptions($questionID = null) {
        if($questionID != null) {
            $this->db->select()->from('questionoptions_details');
            $this->db->where('question_id', $questionID);
            $query  =   $this->db->get();
            return $query->result();
        }
    }

    public function add($data) {
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('questions', $data);
            return 1;
        } else {
            $this->db->insert('questions', $data);
            return $this->db->insert_id();
        }
    }

    public function addQuestionDetails($data) {
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('question_details', $data);
            return 1;
        } else {
            $this->db->insert('question_details', $data);
            return $this->db->insert_id();
        }
    }

    public function addQuestionOptions($data) {
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('questionoptions_details', $data);
            return 1;
        } else {
            $this->db->insert('questionoptions_details', $data);
            return $this->db->insert_id();
        }
    }

    public function checkExists($fieldName = null, $chapterID = null, $fieldVal = null) {
        if ( ($fieldName != null) && ($chapterID != null) && ($fieldVal != null) ) {
            $this->db->select('*')->from('questions');
            $this->db->where('module_id', $chapterID);
            $this->db->where($fieldName, $fieldVal);
            $query  =   $this->db->get();
            return $query->num_rows();
        }
    }

    public function delete($questionID = null) {
        if($questionID != null) {
            $this->db->where('id', $questionID);
            $this->db->delete('questions');
            return 1;
        }
    }
}