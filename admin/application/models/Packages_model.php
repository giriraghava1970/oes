<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Packages_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getPackages($id=null){
        /*$this->db->select('packages.*, courses.course_name, batches.batch_name, 
                        institute_details.institute_name')->from('packages');
        $this->db->join('courses', 'packages.course_id = course_id');
        $this->db->join('batches', 'packages.batch_id = batch_id');
        $this->db->join('institute_details', 'packages.institute_id = institute_id');
        $this->db->distinct();*/
        $this->db->select('*')->from('packages');

        if($id != null){
            $this->db->where('id', $id);
        }

        $this->db->order_by('id', 'DESC'); //Default to Descending 
        $query  =   $this->db->get(); 
        return $query->result();
    }

    public function add($data) {
        if(isset($data['package_name'])){
            $resultForPname =   $this->checkSettings('package_name', $data['package_name']);
        } else {
            $resultForPname = 0;
        }

        if ($resultForPname == 1){
            // echo "\r\n <br/> data : \r\n <br/><pre>"; print_r($data);
            // exit();
            if(isset($data['id'])) {
                $this->db->where('id', $data['id']);
                $this->db->update('packages', $data);
                        return 1;
            } else {
                $this->db->insert('packages', $data);
                return $this->db->insert_id();
            } 
        }else {
            if  ($resultForPname == 0) {
                if (isset($data['id'])) {
                    $this->db->where('id', $data['id']);
                    $this->db->update('packages', $data);
                    return 1;
                } else {
                    $this->db->insert('packages', $data);
                    return $this->db->insert_id();
                }
            } else {
                return FALSE;
            }
        }
        
    }

    public function checkSettings($fieldName = '', $fieldVal = '') {
        if ( ($fieldName != '' ) && ($fieldVal != '') ) {
            $this->db->select('*')->from('packages');
            $this->db->where($fieldName, $fieldVal);
            $query  =   $this->db->get();

            if($query->num_rows() > 0) {
                return 1;
            } else {
                return 0;
            }
        }
    }
   
    public function delete($id = null) {
        if($id != null) {
            $this->db->where('id', $id);
            $this->db->delete('packages');
            return 1;
        }
    }

    // public function image_update( $id, $img_name) {
    //     $this->db->set('package_image_link', $img_name);
    //     $this->db->where('id', $id);
    //     $this->db->update('packages');
    //     return 1;
    // }

    // public function image_delete($id, $img_name) {
    //     $file = "./uploads/packages/" . $img_name;
    //     unlink($file);
    //     $this->db->where('id', $id);
    //     $this->db->delete('packages');
    //     return 1;
    // }



    }


    