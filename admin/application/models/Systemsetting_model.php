<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Systemsetting_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSettingsByRecordID($id=null){
        $this->db->select('*')->from('system_settings_mst');
        if($id != null){
            $this->db->where('id', $id);
        }
        $this->db->order_by('id','ASC');
        $query  =   $this->db->get(); 
        return $query->result();
    }

    public function getSettings($id=null){
        $this->db->select('*')->from('system_settings_mst');
        if($id != null){
            $this->db->where('`system_settings_mst`.`setting_types_id`',$id);
        }
        $query  =   $this->db->get(); 
        return $query->result();
    }

    public function add($data) {
        if(isset($data['setting_name'])){
            $resultForSname =   $this->checkSettings('setting_name', $data['setting_name']);
        } else {
            $resultForSname = 0;
        }

        if ($resultForSname == 1){
            // echo "\r\n <br/> data : \r\n <br/><pre>"; print_r($data);
            // exit();
            if(isset($data['id'])) {
                $this->db->where('id', $data['id']);
                $this->db->update('system_settings_mst', $data);
                        return 1;
            } else {
                $this->db->insert('system_settings_mst', $data);
                return $this->db->insert_id();
            } 
        }else {
            if  ($resultForSname == 0) {
                if (isset($data['id'])) {
                    $this->db->where('id', $data['id']);
                    $this->db->update('system_settings_mst', $data);
                    return 1;
                } else {
                    $this->db->insert('system_settings_mst', $data);
                    return $this->db->insert_id();
                }
            } else {
                return FALSE;
            }
        }
        
    }

    public function checkSettings($fieldName = '', $fieldVal = '') {
        if ( ($fieldName != '' ) && ($fieldVal != '') ) {
            $this->db->select('*')->from('system_settings_mst');
            $this->db->where($fieldName, $fieldVal);
            $query  =   $this->db->get();

            if($query->num_rows() > 0) {
                return 1;
            } else {
                return 0;
            }
        }
    }
   
    public function delete($id = null) {
        if($id != null) {
            $this->db->where('id', $id);
            $this->db->delete('system_settings_mst');
            return 1;
        }
    }
    }


    