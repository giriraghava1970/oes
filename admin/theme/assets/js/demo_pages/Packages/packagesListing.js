var light_8 = $('#spinner-light-8').closest('.card');
(function () {
    "use strict";
    preLoadData();
    packageTypeData();
    packageTypeAdd();
    packageListing();
    doLoadFormData();
    doCancel();
    doDTableActions();
})(jQuery);

function preLoadData() {
    $(light_8).block({
        message: '<i class="icon-spinner11 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });

    //LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
    var myParams = 'limitRowsFirstTime=100';
    var newURLLink = $('#hidProjectURL').val() + 'Packages/ajaxResultsForFiltersPackageJSONGeneration';

    $.ajax({
        type: 'POST',
        url: newURLLink,
        data: myParams,
        dataType: 'json',
    })
        .done(function (data) {
            // Handles successful responses only
            console.log('Handles successful responses only' + data);
            ajaxDonePageLoad(data);
        })
        .fail(function (reason) {
            // Handles errors only
            console.log('Handles errors only ' + reason);
        })
        .always(function (data, textStatus, response) { })
        .then(function (data, textStatus, response) { });
}

function doLoadFormData() {
    $('#courseId').on('change', (function () {
        if ($(this).val() != '') {
            var myParams = 'limitRowsFirstTime=100&courseID=' + $('#courseId').val();
            var newURLLink = $('#hidProjectURL').val() + 'Common/ajaxResultsForBatchDropBox';

            $.ajax({
                type: 'POST',
                url: newURLLink,
                data: myParams,
                dataType: 'json',
            })
                .done(function (data) {
                    // Handles successful responses only
                    console.log('Handles successful responses only' + data);
                    if (data.data == 'success') {
                        $('#batchId').empty();
                        var list = $('#batchId');
                        list.append('<option value="">--Select Batch -- </option>');

                        $.each(data.batches, function (index, value) {
                            list.append('<option value="' + value.id + '">' + value.batch_name + '</option>');
                        });
                    }
                })
                .fail(function (reason) {
                    // Handles errors only
                    console.log('Handles errors only ' + reason);
                })
                .always(function (data, textStatus, response) { })
                .then(function (data, textStatus, response) { });
        }
    }))

    //     $('#batchId').on('change', (function () {
    //         if ($(this).val() != '') {
    //             var myParams = 'limitRowsFirstTime=100&batchId=' + $(this).val();
    //             var newURLLink = $('#hidProjectURL').val() + 'Packages/ajaxResultsForCourseDropBox';

    //             $.ajax({
    //                 type: 'POST',
    //                 url: newURLLink,
    //                 data: myParams,
    //                 dataType: 'json',
    //             })
    //                 .done(function (data) {
    //                     // Handles successful responses only
    //                     console.log('Handles successful responses only' + data);
    //                     if (data.data == 'success') {
    //                         $('#courseId').empty();
    //                         var list = $('#courseId');
    //                         list.append('<option value="">-- Select Course -- </option>');

    //                         $.each(data.courses, function (index, value) {
    //                             list.append('<option value="' + value.id + value.course_name + '">' + value.course_name + '</option>');
    //                         });
    //                     }
    //                 })
    //                 .fail(function (reason) {
    //                     // Handles errors only
    //                     console.log('Handles errors only ' + reason);
    //                 })
    //                 .always(function (data, textStatus, response) { })
    //                 .then(function (data, textStatus, response) { });
    //         }
    //     }));
}


function packageTypeData() {
    $('#packageName').on('blur', (function () {
        if ($(this).val() == '') {
            // alert('User Name should not be blank');
            // $('#txtUserName').attr('autofocus', true);
            return false;
        } else {
            $('#packageName').focus();
        }
    }));

    $('#packageDesc').on('blur', (function () {
        if ($(this).val() == '') {
            return false;
        } else {
            $('#packageDesc').focus();
        }
    }));
}

function doCancel() {
    $('#btnCancel').on('click', (function (e) {
        $('.form-control').each(function () {
            $(this).val('');
        });
    }));
}

// function doClear() {
//     $('.form-control').each(function () {
//         $(this).val('');
//     });
// }

function packageListing() {
    $('#btnBack').on('click', (function (e) {
        $('#showPackageForm').hide();
        $('#showPackageListing').show();
    }))
}

function packageTypeAdd() {
    $('#btnSave').on('click', (function (e) {
        console.log('btn Value : ' + $(this).html());
        e.preventDefault();
        //  alert('btnSave val : ' + $(this).html());
        var myForm = $('#formCom');
        if (myForm.valid()) {
            var myReqType = '';

            if ($(this).html().trim() == 'Save') {
                myReqType = 'create';
            } else if ($(this).html().trim() == 'Update') {
                myReqType = 'update';
            }

            if (($('#courseId').val() != '') && ($('#batchId').val() != '') && ($('#instituteId').val() != '')
                && ($('#packageName').val() != '') && ($('#packageDesc').val() != '') && ($('#startDate').val() != '')
                && ($('#endDate').val() != '') && ($('#packageCost').val() != '') && ($('#packageMrp').val() != '')
                && ($('#status').val() != '')) {
                var newURLLink = $('#hidProjectURL').val() + 'Packages/ajaxResultsForPackagesAction';
                console.log('newURLLINK : ' + myReqType);

                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: $('#formCom').serialize() + '&reqType=' + myReqType,
                    success: function (data) {
                        if (data.data == 'success') {
                            // initialText = btn.data('initial-text');
                            // btn.html(initialText).removeClass('disabled');
                            // swal({
                            //     title: 'Information',
                            //     text: 'Record inserted successfully.',
                            //     type: 'info'
                            // });
                            // doClear();
                            preLoadData();
                            // $("#formCom").modal('hide');

                        } else {
                            // swal({
                            //     title: 'Warning',
                            //     text: 'Error in creating new record.',
                            //     type: 'warning'
                            // });
                            // alert(data.data);
                        }
                    },
                    failure: function (data) { }
                });
            }
        }
    }));
}

function ajaxDonePageLoad(data, myProp) {
    //common For all the ajax Done process.. [ Search Filter, All, Default, Delete action etc...]

    if (data.success == 1) { //Success
        var x = new Date();
        curDate = x.toUTCString();// changing the display to UTC string
        console.log(' success date : ' + curDate);
        //$(light_8).unblock();
        console.log('data write : ' + data.writeJSONData);
        //$('#spinner-light-8').hide();
        //Steps for filling up the contents from the json response.
        $('#showPackageListing').show();
        $('#showPackageForm').hide();
        //STEP 2 : Data Table
        var table = $('#packageTable').DataTable({ //.datatable-button-html5-basic
            destroy: true,
            //processing	:	true,
            //serverSide	:	true,
            //deferLoading	:	57,
            aaData: data.writeJSONData,
            selectAllPages: false,
            columns: [
                { "data": "SNo" },
                { "data": "Package" },
                // { "data": "Package Description" },
                { "data": "Start Date" },
                { "data": "End Date" },
                { "data": "Course / Batch" },
                // { "data": "Batch" },
                { "data": "Publish Online" },
                { "data": "Free Package" },
                { "data": "Package Cost" },
                { "data": "Package MRP" },
                { "data": "Institute" },
                { "data": "Package Image" },
                { "data": "Status" },
                { "data": "Action" },
            ],
            columnDefs: [
                { targets: 'no-sort', orderable: false }
            ],

            buttons: {
                dom: {
                    button: {
                        classname: 'btn btn-dark'
                    }
                },
                buttons: [{
                    text: 'Create Package',
                    className: 'btn bg-primary',
                    action: function () {
                        $('#showPackageForm').show();
                        $('#showPackageListing').hide();
                    }
                },
                {
                    extend: 'copyHtml5',
                    className: 'btn bg-grey',
                    title: $('#hidAppTitle').val() + ' - Academics Listing',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'pdfHtml5',
                    className: 'btn bg-grey',
                    title: $('#hidAppTitle').val() + ' - Academics Listing',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'csvHtml5',
                    className: 'btn bg-grey',
                    title: $('#hidAppTitle').val() + ' - Academics Listing',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'excelHtml5',
                    className: 'btn bg-grey',
                    title: $('#hidAppTitle').val() + ' - Academics Listing',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                ]
            },
            select: 'single',
            style: 'os',
            order: [[0, 'asc']],
            bFilter: false,
            bDestroy: true,
        });

        //STEP 2 : Data Table
        // $('#packageTable').hide();
        $('#packageTable select').addClass('form-control form-input-styled');
        var scrollableDiv = $("div#packageTable .dataTables_scrollBody");
        var divTableRows = $(scrollableDiv).find("table tbody tr");

        if (divTableRows.length < 5) {
            $(scrollableDiv).css("overflow", ""); //For controlling the Actions popup..
        }

        $('div .dt-buttons').css('float', 'left');
    }
}

//Edit and Delete
function doDTableActions() {
    $('#packageTable').on('click', '.logAction', (function () {
        var myAttr = $(this).attr('id').split('-');
        $('#hidRecID').val(myAttr[1]);
        var myReqType = '';

        switch (myAttr[0]) {


            case 'edit':
                var myParams = 'reqType=edit&hidRecID=' + myAttr[1];
                var newURLLink = $('#hidProjectURL').val() + 'Packages/ajaxResultsForPackagesAction';
                console.log('newURLLINK : ' + myReqType);
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: myParams,
                    success: function (data) {

                        if (data.data == 'success') {
                            $.each(data.packageRecord, (function (index, value) {
                                console.log(' index : ' + index + ' value : ' + value);
                                if (index == 'course_id') {
                                    $('#courseId').val(value);
                                }
                                if (index == 'batch_id') {
                                    $('#batchId').val(value);
                                }
                                if (index == 'institute_id') {
                                    $('#instituteId').val(value);
                                }
                                if (index == 'package_name') {
                                    $('#packageName').val(value);
                                }
                                if (index == 'package_description') {
                                    $('#packageDesc').val(value);
                                }
                                if (index == 'package_start_date') {
                                    $('#startDate').val(value);
                                }
                                if (index == 'package_end_date') {
                                    $('#endDate').val(value);
                                }
                                if (index == 'package_is_free') {
                                    $('#freePackage').val(value);
                                }
                                if (index == 'package_cost') {
                                    $('#packageCost').val(value);
                                }
                                if (index == 'package_mrp') {
                                    $('#packageMrp').val(value);
                                }
                                if (index == 'package_image_link') {
                                    $('#photo').val(value);
                                }
                                if (index == 'status') {
                                    $('#status').val(value);
                                }
                                if (index == 'created_at') {
                                    $('#createdAt').val(value);
                                }
                                if (index == 'created_by') {
                                    $('#createdBy').val(value);
                                }
                                if (index == 'updated_at') {
                                    $('#updatedAt').val(value);
                                }
                                if (index == 'status') {
                                    $('#status').val(value);
                                }
                            }));
                            //doClear();
                            $('#btnSave').html('Update');
                            $('#showPackageForm').show();
                            $('#showPackageListing').hide();
                        }
                    },
                    error: function (data) { },
                });
                break;


            case 'dele':
                //Get confirmation b4 deleting..
                //alert('Thanks for confirming');
                var myParams = 'reqType=dele&hidRecID=' + myAttr[1];
                var newURLLink = $('#hidProjectURL').val() + 'Packages/ajaxResultsForPackagesAction';
                console.log('newURLLINK : ' + newURLLink + myParams);
                // return;
                swal({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover the deleted record!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    showLoaderOnConfirm: true,
                    preConfirm: function (email) {
                        return new Promise(function (resolve) {
                            $.ajax({
                                type: 'POST',
                                url: newURLLink,
                                dataType: 'json',
                                data: myParams,
                            })
                                .done(function (data) {
                                    // Handles successful responses only
                                    if (data.data == 'success') {
                                        swal({
                                            title: 'Information',
                                            text: 'Record deleted successfully.',
                                            type: 'info'
                                        });

                                        // doClear();
                                        preLoadData();
                                    } else {
                                        alert('error in deleting');
                                    }
                                })
                                .fail(function (reason) {
                                    // Handles errors only
                                    console.log('Handles errors only ' + reason);
                                })
                                .always(function (data, textStatus, response) { })
                                .then(function (data, textStatus, response) { });
                        });
                    },
                });
                break;
        }
    }
    ))
}

// function doOpenSwal(myAction, formCont) {

//     swal({
//         html: formCont,
//         showCancelButton: true,
//         width: '60%',
//         confirmButtonText: myAction,

//         preConfirm: function (email) {
//             return new Promise(function (resolve) {
//                 var myParams = '&reqType=' + myAction;
//                 var newURLLink = $('#hidProjectURL').val() + 'Packages/ajaxResultsForPackagesAction';
//                 $.ajax({
//                     type: 'POST',
//                     url: newURLLink,
//                     dataType: 'json',
//                     data: $('#formCom').serialize() + myParams,
//                 })
//                     .done(function (data) {
//                         // Handles successful responses only
//                         if (data.data == 'success') {
//                             // swal({
//                             //     title: 'Information',
//                             //     text: 'Record Updated Successfully!',
//                             //     type: 'success'
//                             //    });
//                             resolve();
//                             preLoadData();
//                         } else {
//                              $('.btn-primary').prop('disabled', false);
                    // $('.swal2-cancel').prop('disabled', false);
                    // $('.btn-primary').removeAttr('style');
                    // $('.swal2-actions').removeClass('swal2-loading');
                    // $('#instituteName').focus();
//                         }
//                     })
//             })
//         }

//     })
// }