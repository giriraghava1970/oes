var light_8 = $('#spinner-light-8').closest('.card');
(function () {
    "use strict";
    preLoadData();
    btnActions();
    doDTableActions();
})(jQuery);

function preLoadData() {
	$('#showCoursesForm').hide();
    $('#showCoursesListing').show();

    $(light_8).block({
        message: '<i class="icon-spinner11 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });

    //LOADING THE DEFAULT 100 RECORDS FOR Academics FOR THE FIRST TIME WITHOUT ANY FILTERS
    var myParams = 'limitRowsFirstTime=100';
    var newURLLink = $('#hidProjectURL').val() + 'Academics/Courses/ajaxResultsForFiltersJSONGeneration';

    $.ajax({
        type: 'POST',
        url: newURLLink,
        data: myParams,
        dataType: 'json',
    })
        .done(function (data) {
            // Handles successful responses only
            console.log('Handles successful responses only' + data);
            ajaxDonePageLoad(data);
        })
        .fail(function (reason) {
            // Handles errors only
            console.log('Handles errors only ' + reason);
        })
        .always(function (data, textStatus, response) { })
        .then(function (data, textStatus, response) { });

    $('#showBackToListing').on('click', (function(){
    	$('#showCoursesForm').hide();
        $('#showCoursesListing').show();
    }));

    $('#lsSubjects').on('change', (function(){
		if ($(this).val() != '') {
			var newURLLink	=	$('#hidProjectURL').val()+'Common/ajaxResultsForChaptersDropBox';
	        var myParams	=	'reqType=fetchChaptersBySubjectForCourseForm&subjectID='+ $(this).val();
			console.log('newURL : ' + newURLLink);
			$.ajax({
				type	:	'POST',
				url	:	newURLLink,
				dataType	:	'json',
				data	:	myParams,
			})
				.done(function (data) {
					if(data.data == 'success') {
						$("#lsModules").empty(); //Emptying the modules every time when the Subject selection is altered.
						var list	=	$("#lsModules");
						list.append('<option value="">--Choose One -- </option>');

						$.each(data.modulesDtls, function (index, value) {
							console.log('module id : ' + value.id + ' mod name : ' + value.moduleName);
							list.append('<option value="' + value.id+'">' + value.moduleName+ '</option>');
						});
					}
                })
                .fail(function (reason) {
                    // Handles errors only
                    console.log('Handles errors only ' + reason);
                })
                .always(function (data, textStatus, response) { })
                .then(function (data, textStatus, response) { });
		} else {
			$('#lsModules')
			    .find('option')
			    .remove()
			    .end()
			    .append('<option value="">--Choose One--</option>')
			    .val('');
		}
	}));
}

function doClear() {
    $('.form-control').each(function () {
        $(this).val('');
    });
}

function btnActions() {
    $('#btnReset').on('click', (function (e) {
        $('.form-control').each(function () {
            $(this).val('');
        });
    }));

    $('#confirm_pass').on('blur', function () {
        if ($('#pass').val() == $('#confirm_pass').val()) {
            $('#btnSave').attr("disabled", false);
            $('#message').hide();
        } else {
            $('#message').show();
            $("#btnSave").attr("disabled", true);
            $('#message').html('Not Matching').css('color', 'red');
        }
    });

    $('#btnSave').on('click', (function (e) {
        console.log('btn Value : ' + $(this).html());
        e.preventDefault();
        // if ($('#pass').val() == $('#confirm_pass').val()) {
        //     $('#message').hide();
        // } else {
        //     $('#message').show();

        //   $('#message').html('Not Matching').css('color', 'red');
        // }
        // alert('btnSave val : ' + $(this).html());
        var myForm = $('#frmUsers');
        if (myForm.valid()) {
            var myReqType = '';

            if ($(this).html().trim() == 'Save') {
                myReqType = 'create';
            } else if ($(this).html().trim() == 'Update') {
                myReqType = 'update';
            }
            $('#hidReqType').val(myReqType);
            var userForm = $('#frmUsers')[0];
            var form = new FormData(userForm);
            var newURLLink = $('#hidProjectURL').val() + 'Academics/Courses/ajaxResultsForCoursesAction';
            // console.log('newURLLINK : ' + newURLLink);    +'&reqType=' + myReqType

            $.ajax({
                type: 'POST',
                url: newURLLink,
                dataType: 'json',
                processData: false,
                contentType: false,
                data: form,
                success: function (data) {
                    if (data.data == 'success') {
                        if ($('#btnSave').val() == 'Save') {
                            swal({
                                title: 'Information',
                                text: 'Record Inserted successfully.',
                                type: 'success'
                            });
                        } else {
                            swal({
                                title: 'Information',
                                text: 'Record Updated successfully.',
                                type: 'success'
                            });
                        }
                        doClear();
                        $('#btnSave').html('Save');
                        preLoadData();
                    } else {
                        swal({
                            title: 'Warning',
                            text: 'Error in creating new record.',
                            type: 'warning'
                        });
                    }
                },
                failure: function (data) { }
            });
        }
    }));
}

function ajaxDonePageLoad(data, myProp) {
    //common For all the ajax Done process.. [ Search Filter, All, Default, Delete action etc...]

    if (data.success == 1) { //Success
        var x = new Date();
        curDate = x.toUTCString();// changing the display to UTC string
        console.log(' success date : ' + curDate);
        //$(light_8).unblock();
        $('#coursesTable').show();
        //$('#spinner-light-8').hide();
        //Steps for filling up the contents from the json response.
        //STEP 2 : Data Table
        var table = $('#coursesTable').DataTable({ //.datatable-button-html5-basic
            destroy: true,
            //processing	:	true,
            //serverSide	:	true,
            //deferLoading	:	57,
            aaData: data.writeJSONData,
            selectAllPages: false,
            columns: [
                { "data": "SNo" },
                { "data": "Course Code" },
                { "data": "Course Name" },
                { "data": "Course Description" },
                { "data": "Status" },
                { "data": "Action" },
            ],
            columnDefs: [
                { targets: 'no-sort', orderable: false }
            ],
            buttons: {
                dom: {
                    button: {
                        classname: 'btn btn-dark'
                    }
                },
                buttons: [{
                    text: 'Create Course',
                    className: 'btn bg-primary',
                    action: function () {
                        $('#showCoursesForm').show();
                        $('#showCoursesListing').hide();
                    }
                },
                {
                    extend: 'copyHtml5',
                    className: 'btn bg-grey',
                    title: $('#hidAppTitle').val() + ' - Academics Listing',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'pdfHtml5',
                    className: 'btn bg-grey',
                    title: $('#hidAppTitle').val() + ' - Academics Listing',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'csvHtml5',
                    className: 'btn bg-grey',
                    title: $('#hidAppTitle').val() + ' - Academics Listing',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'excelHtml5',
                    className: 'btn bg-grey',
                    title: $('#hidAppTitle').val() + ' - Academics Listing',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                ]
            },
            select: 'single',
            style: 'os',
            order: [[0, 'asc']],
            bFilter: false,
            bDestroy: true,
        });
        //STEP 2 : Data Table
        $('#coursesTable_length').hide();
        $('#coursesTable_length select').addClass('form-control form-input-styled');
        var scrollableDiv = $("div#coursesTable_wrapper .dataTables_scrollBody");
        var divTableRows = $(scrollableDiv).find("table tbody tr");

        if (divTableRows.length < 5) {
            $(scrollableDiv).css("overflow", ""); //For controlling the Actions popup..
        }

        $('div .dt-buttons').css('float', 'left');
    }
}

//Edit and Delete
function doDTableActions() {
    $('#coursesTable').on('click', '.logAction', (function () {
        var myAttr = $(this).attr('id').split('-');
        $('#hidRecID').val(myAttr[1]);

        switch (myAttr[0]) {
            case 'edit':
                var myParams = 'hidReqType=formList&hidRecID=' + myAttr[1];
                var newURLLink = $('#hidProjectURL').val() + 'Academics/Courses/ajaxResultsForCoursesAction';
                console.log('newURLLINK : ' + newURLLink);
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: myParams,
                })
                    .done(function (data) {
                        //doClear();
                        if (data.data == 'success') {
                            doOpenSwal('update', data.formCont);
                        }
                    })
                    .fail(function (reason) {
                        // Handles errors only
                        console.log('Handles errors only ' + reason);
                    })
                    .always(function (data, textStatus, response) { })
                    .then(function (data, textStatus, response) { });

                break;

            case 'del':
                //Get confirmation b4 deleting..
                //alert('Thanks for confirming');
                var myParams = 'hidReqType=dele&hidRecID=' + myAttr[1];
                var newURLLink = $('#hidProjectURL').val() + 'Academics/Courses/ajaxResultsForCoursesAction';
                console.log('newURLLINK : ' + newURLLink + myParams);
                // return;
                swal({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover the deleted record!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    showLoaderOnConfirm: true,
                    preConfirm: function (email) {
                        return new Promise(function (resolve) {
                            $.ajax({
                                type: 'POST',
                                url: newURLLink,
                                dataType: 'json',
                                data: myParams,
                            })
                                .done(function (data) {
                                    // Handles successful responses only
                                    if (data.data == 'success') {
                                        resolve();
                                        preLoadData();
                                    } else {
                                        alert('error in Deleting');
                                    }
                                })
                                .fail(function (reason) {
                                    // Handles errors only
                                    console.log('Handles errors only ' + reason);
                                })
                                .always(function (data, textStatus, response) { })
                                .then(function (data, textStatus, response) { });
                        });
                    },
                });

                break;
        }
    }
    ))
}

function doOpenSwal(myAction, formCont) {
    swal({
        html: formCont,
        showCancelButton: true,
        width: '60%',
        confirmButtonText: myAction,
        onOpen : function() {
        	$('.form-input-styled').uniform({
	            fileButtonClass: 'action btn bg-blue'
	        });
        },
        preConfirm: function (email) {
            return new Promise(function (resolve) {
                var myParams = '&hidReqType=' + myAction;
                var newURLLink = $('#hidProjectURL').val() + 'Academics/Courses/ajaxResultsForCoursesAction';
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: $('#frmCommon').serialize() + myParams,
                })
                    .done(function (data) {
                        // Handles successful responses only
                        if (data.data == 'success') {
                            // swal({
                            //     title: 'Information',
                            //     text: 'Record Updated Successfully!',
                            //     type: 'success'
                            //    });
                            resolve();
                            preLoadData();
                        } else {
                            alert('error in Updating');
                        }
                    })
            })
        }

    })
}