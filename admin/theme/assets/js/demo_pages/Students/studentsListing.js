var light_8 = $('#spinner-light-8').closest('.card');
(function () {
    "use strict";
    preLoadData();
    btnActions();
    doDTableActions();
})(jQuery);

function preLoadData() {
    $(light_8).block({
        message: '<i class="icon-spinner11 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });

    //LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
    var myParams = 'limitRowsFirstTime=100';
    var newURLLink = $('#hidProjectURL').val() + 'Students/ajaxResultsForFiltersJSONGeneration';

    $.ajax({
        type: 'POST',
        url: newURLLink,
        data: myParams,
        dataType: 'json',
    })
        .done(function (data) {
            // Handles successful responses only
            console.log('Handles successful responses only' + data);
            ajaxDonePageLoad(data);
        })
        .fail(function (reason) {
            // Handles errors only
            console.log('Handles errors only ' + reason);
        })
        .always(function (data, textStatus, response) { })
        .then(function (data, textStatus, response) { });

    var newURLLink = $('#hidProject').val() + 'Students/checkFieldExists';

    $('.checkField').on('blur', (function () {
        console.log('checkFiled ID : ' + $(this).attr('id'));
        var myAttrID = $(this).attr('id');
        if ($(this).val() != '') {

            // ajax controller 
            $.ajax({
                type: 'POST',
                url: newURLLink,
                data: { 'name': myAttrID, 'value': $(this).val() },
                dataType: 'json',
                success: function (data) {
                    var myErrorAttrID = myAttrID + '_error';
                    console.log('myErrorAttrID' + myErrorAttrID);
                    if (data.data == 'success') {
                        $('#' + myErrorAttrID).html('');
                        $('#' + myErrorAttrID).hide();
                    } else {
                        var myErrorMsg = '';

                        if (myAttrID == 'username') {
                            myErrorMsg = 'User Name';
                        } else if (myAttrID == 'email') {
                            myErrorMsg = 'Email';
                        }
                        $('#' + myErrorAttrID).show();
                        $('#' + myErrorAttrID).html(myErrorMsg + ' Already Exists');
                    }
                },
                failure: function (data) { }
            })
        }
    }));

}

function doClear() {
    $('.form-control').each(function () {
        $(this).val('');
    });
}

function btnActions() {
    $('#btnReset').on('click', (function (e) {
        $('.form-control').each(function () {
            $(this).val('');
        });
    }));

    $('#confirm_pass').on('blur', function () {
        if ($('#pass').val() == $('#confirm_pass').val()) {
            $('#btnSave').attr("disabled", false);
            $('#message').hide();
        } else {
            $('#message').show();
            $("#btnSave").attr("disabled", true);
            $('#message').html('Not Matching').css('color', 'red');
        }
    });

    $('#btnSave').on('click', (function (e) {
        console.log('btn Value : ' + $(this).html());
        e.preventDefault();
        // if ($('#pass').val() == $('#confirm_pass').val()) {
        //     $('#message').hide();
        // } else {
        //     $('#message').show();

        //   $('#message').html('Not Matching').css('color', 'red');
        // }
        // alert('btnSave val : ' + $(this).html());
        var myForm = $('#frmStudents');
        if (myForm.valid()) {
            var myReqType = '';

            if ($(this).html().trim() == 'Save') {
                myReqType = 'create';
            } else if ($(this).html().trim() == 'Update') {
                myReqType = 'update';
            }
            $('#hidReqType').val(myReqType);
            var userForm = $('#frmStudents')[0];
            var form = new FormData(userForm);
            var newURLLink = $('#hidProjectURL').val() + 'Students/ajaxResultsForStudentsAction';
            // console.log('newURLLINK : ' + newURLLink);    +'&reqType=' + myReqType

            $.ajax({
                type: 'POST',
                url: newURLLink,
                dataType: 'json',
                processData: false,
                contentType: false,
                data: form,
                success: function (data) {
                    if (data.data == 'success') {
                        if ($('#btnSave').val() == 'Save') {
                            swal({
                                title: 'Information',
                                text: 'Record Inserted successfully.',
                                type: 'success'
                            });
                        } else {
                            swal({
                                title: 'Information',
                                text: 'Record Updated successfully.',
                                type: 'success'
                            });
                        }
                        doClear();
                        $('#btnSave').html('Save');
                        preLoadData();
                    } else {
                        swal({
                            title: 'Warning',
                            text: 'Error in creating new record.',
                            type: 'warning'
                        });
                    }
                },
                failure: function (data) { }
            });
        }
    }));
}

function ajaxDonePageLoad(data, myProp) {
    //common For all the ajax Done process.. [ Search Filter, All, Default, Delete action etc...]

    if (data.success == 1) { //Success
        var x = new Date();
        curDate = x.toUTCString();// changing the display to UTC string
        console.log(' success date : ' + curDate);
        //$(light_8).unblock();
        $('#studentsTable').show();
        //$('#spinner-light-8').hide();
        //Steps for filling up the contents from the json response.
        //STEP 2 : Data Table
        var table = $('#studentsTable').DataTable({ //.datatable-button-html5-basic
            destroy: true,
            //processing	:	true,
            //serverSide	:	true,
            //deferLoading	:	57,
            aaData: data.writeJSONData,
            selectAllPages: false,
            columns: [
                { "data": "ID" },
                { "data": "SNo" },
                { "data": "Student Name" },
                //{ "data": "E-mail" },
                { "data": "Course" },
                { "data": "Batch" },
                { "data": "Institute" },
                { "data": "Register Date" },
                { "data": "Status" },
            ],
            'columnDefs': [
                {
                    'targets': 0,
                    'render': function (data, type, row, meta) {
                        if (type === 'display') { }
                        return data;
                    },
                    'checkboxes': {
                        'selectRow': true,
                        'selectAllRender': '<div class="checkbox"><input type="checkbox" name="myTableSelectAll" value="All" class="dt-checkboxes"><label></label></div>',
                        'selectCallback': function (nodes, selected) {
                            // alert('select all called.. for');
                        },
                        'stateSave': false,
                    }
                },
                { targets: 'no-sort', orderable: false }
            ],
            buttons: {
                dom: {
                    button: {
                        className: 'btn btn-dark'
                    }
                },
                buttons: [
                    {
                        text: 'Create Student',
                        className: 'btn bg-primary',
                        action: function () {
                            $('#frmStudents').attr('action', 'Students/Create');
                            $('#frmStudents').submit();
                        }
                    },
                    {
                        text: 'Cancel Selected',
                        className: 'btn bg-warning',
                        action: function () {
                            table.rows({ selected: true }).deselect();//page: 'current',
                            var count = table.rows({ page: 'current', selected: true }).count();

                            if (count == 0) {
                                $('.dt-checkboxes').removeClass("selected");
                            }
                        }
                    },
                    {
                        text: 'Delete Selected',
                        className: 'btn bg-danger',
                        attr: {
                            id: 'sweet_warning',
                        },
                        enabled: true,
                        action: function () {
                            if (table.rows('.selected', { page: 'current' }).data().length > 0) {
                                swal({
                                    title: 'Are you sure?',
                                    text: 'You will not be able to recover the selected students!',
                                    type: 'warning',
                                    showCancelButton: true,
                                    confirmButtonText: 'Yes, delete it!',
                                    showLoaderOnConfirm: true,
                                    preConfirm: function (email) {
                                        return new Promise(function (resolve) {
                                            $('.swal2-confirm').html('Deleting...');
                                            var myFinalRecs = [];

                                            for (var i = 0; i < table.rows('.selected', { page: 'current' }).data().length; i++) {
                                                var myData = table.rows('.selected', { page: 'current' }).data()[i].ID.split('<div class="checkbox"><input class="dt-checkboxes" type="checkbox" name="id[]" value="');
                                                console.log(' myData : ' + myData[0] + ' 1 : ' + myData[1]);
                                                var myDat = myData[1].split('"');
                                                console.log(' myDat : ' + myDat[0] + ' 1 : ' + myDat[1]);
                                                myFinalRecs.push(myDat[0]);
                                            };

                                            if (myFinalRecs.length != 0) {
                                                $('#hidDelIds').val(myFinalRecs);
                                                console.log('Hid Del : ' + $('#hidDelIds').val());
                                                //doDeleteRecs($('#hidDelIds').val());
                                                $('#hidSearchFlag').val(2);

                                                if ($('#hidDelIds').val() != '') {
                                                    var myParams = 'hidDelIds=' + $('#hidDelIds').val() + '&hidSearchFlag=2&hidSearchConcat=' + $('#hidSearchConcat').val();
                                                    console.log('myParams For Deleting : ' + myParams);
                                                    var newURLLink = 'index.php?r=Students/ajaxResultsForDeleteAction';

                                                    $.ajax({
                                                        type: 'POST',
                                                        url: newURLLink,
                                                        data: myParams,
                                                        dataType: 'json',
                                                    })
                                                        .done(function (data) {
                                                            // Handles successful responses only
                                                            console.log('Handles successful deleted responses only' + data);
                                                            //calling the preloadconfirm ...
                                                            resolve();
                                                            commonDataListing(data);
                                                        })
                                                        .fail(function (reason) {
                                                            // Handles errors only
                                                            console.log('Handles deleted errors only ' + reason);
                                                        })
                                                        .always(function (data, textStatus, response) { })
                                                        .then(function (data, textStatus, response) { });
                                                }
                                            }
                                        });
                                    },
                                    allowOutsideClick: false
                                });
                            }
                        }
                    },
                    {
                        extend: 'copyHtml5',
                        className: 'btn bg-grey',
                        title: $('#hidAppTitle').val() + ' - Students Listing',
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        className: 'btn bg-grey',
                        title: $('#hidAppTitle').val() + ' - Students Listing',
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    },
                    {
                        extend: 'csvHtml5',
                        className: 'btn bg-grey',
                        title: $('#hidAppTitle').val() + ' - Students Listing',
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        className: 'btn bg-grey',
                        title: $('#hidAppTitle').val() + ' - Students Listing',
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    },
                ]
            },
            select: 'multi',
            style: 'os',
            order: [[1, 'asc']],
            bFilter: false,
            scrollX: true,
        });
        //STEP 2 : Data Table
        $('#studentsTable_length').hide();
        $('#studentsTable_length select').addClass('form-control form-input-styled');
        var scrollableDiv = $("div#studentsTable_wrapper .dataTables_scrollBody");
        var divTableRows = $(scrollableDiv).find("table tbody tr");

        if (divTableRows.length < 5) {
            $(scrollableDiv).css("overflow", ""); //For controlling the Actions popup..
        }

        $('div .dt-buttons').css('float', 'left');
    }
}

//Edit and Delete
function doDTableActions() {
    $('#studentsTable').on('click', '.logAction', (function () {
        var myAttr = $(this).attr('id').split('-');
        $('#hidRecID').val(myAttr[1]);

        switch (myAttr[0]) {
            case 'edit':
                var myParams = 'hidReqType=formList&hidRecID=' + myAttr[1];
                var newURLLink = $('#hidProjectURL').val() + 'Students/ajaxResultsForStudentsAction';
                console.log('newURLLINK : ' + newURLLink);
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: myParams,
                    success: function (data) {
                        //doClear();
                        if (data.data == 'success') {
                            doOpenSwal('update', data.formCont);
                        }
                    },
                    error: function (data) { },
                });

                break;

            case 'del':
                //Get confirmation b4 deleting..
                //alert('Thanks for confirming');
                var myParams = 'hidReqType=dele&hidRecID=' + myAttr[1];
                var newURLLink = $('#hidProjectURL').val() + 'Students/ajaxResultsForStudentsAction';
                console.log('newURLLINK : ' + newURLLink + myParams);
                // return;
                swal({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover the deleted record!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    showLoaderOnConfirm: true,
                    preConfirm: function (email) {
                        return new Promise(function (resolve) {
                            $.ajax({
                                type: 'POST',
                                url: newURLLink,
                                dataType: 'json',
                                data: myParams,
                            })
                                .done(function (data) {
                                    // Handles successful responses only
                                    if (data.data == 'success') {
                                        resolve();
                                        preLoadData();
                                    } else {
                                        alert('error in Deleting');
                                    }
                                })
                                .fail(function (reason) {
                                    // Handles errors only
                                    console.log('Handles errors only ' + reason);
                                })
                                .always(function (data, textStatus, response) { })
                                .then(function (data, textStatus, response) { });
                        });
                    },
                });
                break;
        }
    }
    ))
}

function doOpenSwal(myAction, formCont) {

    swal({
        html: formCont,
        showCancelButton: true,
        width: '60%',
        confirmButtonText: myAction,

        preConfirm: function (email) {
            return new Promise(function (resolve) {
                var myParams = '&hidReqType=' + myAction;
                var newURLLink = $('#hidProjectURL').val() + 'Students/ajaxResultsForStudentsAction';
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: $('#frmCommon').serialize() + myParams,
                })
                    .done(function (data) {
                        // Handles successful responses only
                        if (data.data == 'success') {
                            // swal({
                            //     title: 'Information',
                            //     text: 'Record Updated Successfully!',
                            //     type: 'success'
                            //    });
                            resolve();
                            preLoadData();
                        } else {
                            alert('error in Updating');
                        }
                    })
            })
        }

    })
}