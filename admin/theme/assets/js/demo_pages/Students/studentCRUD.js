	/* Working section */
	$(window).on('load', (function() {
		$('.daterange-single').daterangepicker({ 
            singleDatePicker: true
		});

		if ($('#lsCountries').val() != '') { 
			doLoadStates($('#lsCountries').val());
		}

		$('#lsCountries').on('change', (function(e){
			e.preventDefault();
			if($(this).val() != '') {
				doLoadStates($(this).val());
			}
		}));
	}));

function doLoadCities() {
    //Load Cities
    $('#lsStates').on('change', (function () {
        if ($(this).val() != '') {
            var myParams = 'limitRowsFirstTime=100&stateId=' + $(this).val();
            var newURLLink = $('#hidProjectURL').val() + 'Students/ajaxResultsForcityDropBox';

            $.ajax({
                type: 'POST',
                url: newURLLink,
                data: myParams,
                dataType: 'json',
            })
                .done(function (data) {
                    // Handles successful responses only
                    console.log('Handles successful responses only' + data);
                    if (data.data == 'success') {
                        $('#lsCities').empty();
                        var list = $('#lsCities');
                        list.append('<option value="">-- Select City -- </option>');

                        $.each(data.cities, function (index, value) {
                            list.append('<option value="' + value.id + '">' + value.city_name + '</option>');
                        });
                    }
                })
                .fail(function (reason) {
                    // Handles errors only
                    console.log('Handles errors only ' + reason);
                })
                .always(function (data, textStatus, response) { })
                .then(function (data, textStatus, response) { });
        }
    }));
    //Load Cities
}

function doLoadStates(countryID) {
    var myParams = 'limitRowsFirstTime=100&countryID=' + countryID;
    var newURLLink = $('#hidProjectURL').val() + 'Students/ajaxResultsForStatesDropBox';

    $.ajax({
        type: 'POST',
        url: newURLLink,
        data: myParams,
        dataType: 'json',
    })
        .done(function (data) {
            // Handles successful responses only
            console.log('Handles successful responses only' + data);
            if (data.data == 'success') {
                $('#lsStates').empty();
                var list = $('#lsStates');
                list.append('<option value="">--Select State -- </option>');

                $.each(data.states, function (index, value) {
                    list.append('<option value="' + value.id + '">' + value.state_name + '</option>');
                });
                doLoadCities();
            }
        })
        .fail(function (reason) {
            // Handles errors only
            console.log('Handles errors only ' + reason);
        })
        .always(function (data, textStatus, response) { })
        .then(function (data, textStatus, response) { });
}

function instituteData() {
    $('#lsCities').on('blur', (function () {
        if ($(this).val() == '') {
            return false;
        } else {
            $('#lsCities').focus();
        }
    }));
}