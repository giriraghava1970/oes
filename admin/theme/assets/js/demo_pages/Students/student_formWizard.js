/* ------------------------------------------------------------------------------
 *
 *  # Steps wizard
 *
 *  Demo JS code for form_wizard.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------
var formSubmittedAlready	=	false;
var studentUpdateWizard	=	$('#spinner-light-studentUpdateWizard').closest('.card');

var FormWizard = function() {
    //
    // Setup module components
    //

    // Wizard
    var _componentWizard = function() {
        if (!$().steps) {
            console.warn('Warning - steps.min.js is not loaded.');
            return;
        }

        // Wizard with validation
        // Stop function if validation is missing

        if (!$().validate) {
            console.warn('Warning - validate.min.js is not loaded.');
            return;
        }

        // Show form
        var form	=	$('.steps-validation').show();
        var light_8	=	$('#spinner-light-8').closest('.card');
        // Initialize wizard
        var count = 0;
        $('.steps-validation').steps({
            headerTag: 'h6',
            bodyTag: 'fieldset',
            titleTemplate: '<span class="number">#index#</span> #title#',
            labels: {
                previous: '<i class="icon-arrow-left13 mr-2" /> Previous',
                next: 'Next <i class="icon-arrow-right14 ml-2" />',
                finish: 'Submit form <i class="icon-arrow-right14 ml-2" />'
            },
            transitionEffect: 'fade',
            autoFocus: true,
            saveState: true,
            onStepChanging: function (event, currentIndex, newIndex) {
                // Allways allow previous action even if the current form is not valid!
                if (currentIndex > newIndex) {
                    return true;
                }

                // Needed in some cases if the user went back (clean up)
                if (currentIndex < newIndex) {
                     // To remove error styles
                    form.find('.body:eq(' + newIndex + ') label.error').remove();
                    form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
                }

                form.validate().settings.ignore	=	':disabled,:hidden';
                return form.valid();
            },
            onFinishing: function (event, currentIndex) {
                //alert('on Finishing! GIRISH HERE ? ');
                form.validate().settings.ignore	=	':disabled';
                return form.valid();
            },
            onFinished: function (event, currentIndex) {
                event.preventDefault();
                count++;

                if(count == 1) {
                    // alert('else part check now the mandatory docs are uploaded ?');
                    $('#studentForm').submit();
                    $( this ).off( event );
                    $('#btn-primary').prop('disabled', true);
                }
            }
        });

        // Initialize validation
        $('.steps-validation').validate({
            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
            errorClass: 'validation-invalid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },

            // Different components require proper error label placement
            errorPlacement: function(error, element) {
                // Unstyled checkboxes, radios
                if (element.parents().hasClass('form-check')) {
                    error.appendTo( element.parents('.form-check').parent() );
                } else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {// Input with icons and Select2
                    error.appendTo( element.parent() );
                } else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {// Input group, styled file input
                    error.appendTo( element.parent().parent() );
                } else {// Other elements
                    error.insertAfter(element);
                }
            },
            rules: {
                email: {
                    email: true
                },
                image: {
                    required: true,
                    extension: "jpg,jpeg",
                    filesize: 1,
                }
            }
        });
    };

    // Uniform
    var _componentUniform	=	function() {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-blue'
        });
    };

    // Select2 select
    var _componentSelect2 = function() {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Initialize
        var $select	=	$('.form-control-select2').select2({
            minimumResultsForSearch: Infinity,
            width: '100%'
        });

        // Trigger value change when selection is made
        $select.on('change', function() {
            $(this).trigger('blur');
        });
    };

    //
    // Return objects assigned to module
    //

    return {
        init: function() {
           _componentWizard();
          _componentUniform();
          // _componentSelect2();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    FormWizard.init();
});
