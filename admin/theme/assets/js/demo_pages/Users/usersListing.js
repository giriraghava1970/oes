var light_8 = $('#spinner-light-8').closest('.card');
(function () {
    "use strict";
    preLoadData();
    btnActions();
    doDTableActions();
})(jQuery);

function preLoadData() {
    $(light_8).block({
        message: '<i class="icon-spinner11 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });

    //LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
    var myParams = 'limitRowsFirstTime=100';
    var newURLLink = $('#hidProjectURL').val() + 'Users/ajaxResultsForFiltersJSONGeneration';

    $.ajax({
        type: 'POST',
        url: newURLLink,
        data: myParams,
        dataType: 'json',
    })
        .done(function (data) {
            // Handles successful responses only
            console.log('Handles successful responses only' + data);
            ajaxDonePageLoad(data);
        })
        .fail(function (reason) {
            // Handles errors only
            console.log('Handles errors only ' + reason);
        })
        .always(function (data, textStatus, response) { })
        .then(function (data, textStatus, response) { });

        var newURLLink  =   $('#hidProject').val() + 'Users/checkFieldExists';

        $('.checkField').on('blur', (function () {
            console.log('checkFiled ID : ' + $(this).attr('id'));
            var myAttrID    =   $(this).attr('id');
            if ($(this).val() != '') {

                // ajax controller 
                $.ajax({
                    type: 'POST',
                    url: newURLLink ,
                    data: {'name':myAttrID, 'value':$(this).val()},
                    dataType: 'json',
                    success: function (data) {
                        var myErrorAttrID    =   myAttrID + '_error';
                        console.log('myErrorAttrID' + myErrorAttrID);
                        if (data.data == 'success') {
                            $('#'+myErrorAttrID).html('');
                            $('#'+myErrorAttrID).hide();
                        } else {
                            var myErrorMsg  =   '';

                            if(myAttrID == 'username') {
                                myErrorMsg  =   'User Name';
                            } else if(myAttrID == 'email') {
                                myErrorMsg  =   'Email';
                            }
                            $('#'+myErrorAttrID).show();
                            $('#'+myErrorAttrID).html(myErrorMsg + ' Already Exists');
                        }
                    },
                    failure: function (data) { }
                })    
            }
        }));
    
}

function doClear() {
    $('.form-control').each(function(){
        $(this).val('');
    });
}

function btnActions() {
    $('#btnReset').on('click', (function (e) {
        $('.form-control').each(function(){
            $(this).val('');
        });
    }));

    $('#confirm_pass').on('blur', function () {
        if ($('#pass').val() == $('#confirm_pass').val()) {
            $('#btnSave').attr("disabled", false);
            $('#message').hide();
        } else {
            $('#message').show();
            $("#btnSave").attr("disabled", true);
          $('#message').html('Not Matching').css('color', 'red');
        }
      });

        $('#btnSave').on('click', (function (e) {
        console.log('btn Value : ' + $(this).html());
        e.preventDefault();
        // if ($('#pass').val() == $('#confirm_pass').val()) {
        //     $('#message').hide();
        // } else {
        //     $('#message').show();
           
        //   $('#message').html('Not Matching').css('color', 'red');
        // }
        // alert('btnSave val : ' + $(this).html());
        var myForm  =   $('#frmUsers');
        if(myForm.valid()) {
            var myReqType = '';

            if ($(this).html().trim() == 'Save') {
                myReqType = 'create';
            } else if ($(this).html().trim() == 'Update') {
                myReqType = 'update';
            }
            $('#hidReqType').val(myReqType);
            var userForm  =   $('#frmUsers')[0];
            var form  =   new FormData(userForm);
            var newURLLink = $('#hidProjectURL').val() + 'Users/ajaxResultsForUsersAction';
            // console.log('newURLLINK : ' + newURLLink);    +'&reqType=' + myReqType

            $.ajax({
                type: 'POST',
                url: newURLLink,
                dataType: 'json',
                processData: false,
                contentType: false,
                data: form,
                success: function (data) {
                    if (data.data == 'success') {
                        if($('#btnSave').val() == 'Save'){
                            swal({
                                title: 'Information',
                                text: 'Record Inserted successfully.',
                                type: 'success'
                            });
                        }else{
                            swal({
                                title: 'Information',
                                text: 'Record Updated successfully.',
                                type: 'success'
                            });
                        }
                        doClear();
                        $('#btnSave').html('Save');
                        preLoadData();
                    } else {
                        swal({
                            title: 'Warning',
                            text: 'Error in creating new record.',
                            type: 'warning'
                        });
                    }
                },
                failure: function (data) { }
            });
        }
    }));
}

function ajaxDonePageLoad(data, myProp) {
    //common For all the ajax Done process.. [ Search Filter, All, Default, Delete action etc...]

    if (data.success == 1) { //Success
        var x = new Date();
        curDate = x.toUTCString();// changing the display to UTC string
        console.log(' success date : ' + curDate);
        //$(light_8).unblock();
        $('#usersTable').show();
        //$('#spinner-light-8').hide();
        //Steps for filling up the contents from the json response.
        //STEP 2 : Data Table
        var table = $('#usersTable').DataTable({ //.datatable-button-html5-basic
            destroy: true,
            //processing	:	true,
            //serverSide	:	true,
            //deferLoading	:	57,
            aaData: data.writeJSONData,
            selectAllPages: false,
            columns: [
                { "data" : "SNo"},
                { "data": "Name" },
                { "data": "Email" },
                { "data": "Image" },
                { "data": "Role" },
                { "data": "Status" },
                { "data": "Action" },
            ],
            columnDefs: [
                { targets: 'no-sort', orderable: false }
            ],
            select: 'single',
            style: 'os',
            order: [[0, 'asc']],
            bFilter: false,
            bDestroy: true,
        });
        //STEP 2 : Data Table
        $('#usersTable_length').hide();
        $('#usersTable_length select').addClass('form-control form-input-styled');
        var scrollableDiv = $("div#usersTable_wrapper .dataTables_scrollBody");
        var divTableRows = $(scrollableDiv).find("table tbody tr");

        if (divTableRows.length < 5) {
            $(scrollableDiv).css("overflow", ""); //For controlling the Actions popup..
        }

        $('div .dt-buttons').css('float', 'left');
    }
}

//Edit and Delete
function doDTableActions() {
    $('#usersTable').on('click', '.logAction', (function () {
        var myAttr = $(this).attr('id').split('-');
        $('#hidRecID').val(myAttr[1]);

        switch (myAttr[0]) {
            case 'edit':
                var myParams = 'hidReqType=edit&hidRecID=' + $('#hidRecID').val();
                var newURLLink = $('#hidProjectURL').val() + 'Users/ajaxResultsForUsersAction';
                console.log('newURLLINK : ' + newURLLink);
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: myParams,
                    success: function (data) {
                        //doClear();
                        if (data.data == 'success') {
                            $('#btnSave').html('Update');
                            //alert('data client ' + data.userRecord.client_first_name);
                            $.each(data.userRecord, (function (index, value) {
                                console.log(' index : ' + index + ' value : ' + value);
                                if (index == 'name') {
                                    $('#name').val(value);
                                }
                                if (index == 'username') {
                                    $('#username').val(value);
                                }
                                if (index == 'email') {
                                    $('#email').val(value);
                                }
                                if (index == 'password') {
                                    $('#password').val(value);
                                }
                                if (index == 'role_id') {
                                    $('#role_id').val(value);
                                }
                                if (index == 'phone') {
                                    $('#phone').val(value);
                                }
                            }));
                            //clientAdd();
                        }
                    },
                    error: function (data) { },
                });

                break;

            case 'del':
                //Get confirmation b4 deleting..
                    //alert('Thanks for confirming');
                    var myParams = 'hidReqType=dele&hidRecID=' + myAttr[1];
                    var newURLLink = $('#hidProjectURL').val() + 'Users/ajaxResultsForUsersAction';
                    console.log('newURLLINK : ' + newURLLink + myParams);
                    // return;
                   swal ({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover the deleted record!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    showLoaderOnConfirm: true,
                    preConfirm: function (email) {
                        return new Promise(function (resolve) {
                    $.ajax({
                        type: 'POST',
                        url: newURLLink,
                        dataType: 'json',
                        data: myParams,
                    })
                        .done(function (data) {
                            // Handles successful responses only
                            if (data.data == 'success') {
                               swal({
                                title: 'Information',
                                text: 'Record Deleted Successfully!',
                                type: 'success'
                               });
                                preLoadData();
                            } else {
                                alert('error in Deleting');
                            }
                        })
                        .fail(function (reason) {
                            // Handles errors only
                            console.log('Handles errors only ' + reason);
                        })
                        .always(function (data, textStatus, response) { })
                        .then(function (data, textStatus, response) { });
                    });
        },
    });
break;
        }
    }
    ))
}