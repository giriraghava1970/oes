/* ------------------------------------------------------------------------------
 *
 *  # jQuery UI forms
 *
 *  Demo JS code for jqueryui_forms.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var JqueryUiForms = function() {
    //
    // Setup module components
    //

    // Datepicker
    var _componentUiDatepicker = function() {
        if (!$().datepicker) {
            console.warn('Warning - jQuery UI components are not loaded.');
            return;
        }

        // Default functionality
        $('.datepicker').datepicker({
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        });

        // Dates in other months
        $('.datepicker-dates').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        });

        // Button bar
        $('.datepicker-button-bar').datepicker({
            showButtonPanel: true,
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        });

        // Month and year menu
        $('.datepicker-menus').datepicker({
            changeMonth: true,
            changeYear: true,
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        });

        // Multiple months
        $('.datepicker-multiple').datepicker({
            numberOfMonths: 3,
            showButtonPanel: true,
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        });

        // Icon trigger
        $('.datepicker-icon').datepicker({
            showOn: 'button',
            buttonImage: 'theme/assets/images/ui/datepicker_trigger.png',
            buttonImageOnly: true,
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        });

        // Populate alternate field
        $('.datepicker-alternate').datepicker({
            altField: '#alternate',
            altFormat: 'DD, d MM, yy',
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        });

        // Restrict date range
        $('.datepicker-restrict').datepicker({ 
            minDate: -20,
            maxDate: '+1M +10D',
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        });

        // Show week number
        $('.datepicker-weeks').datepicker({
            showWeek: true,
            firstDay: 1,
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        });


        //
        // Date range
        //

        // From
        $('#range-from').datepicker({
            defaultDate: '+1w',
            numberOfMonths: 3,
            onClose: function( selectedDate ) {
                $( '#range-to' ).datepicker( 'option', 'minDate', selectedDate );
            },
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        });

        // To
        $('#range-to').datepicker({
            defaultDate: '+1w',
            numberOfMonths: 3,
            onClose: function( selectedDate ) {
                $( '#range-from' ).datepicker( 'option', 'maxDate', selectedDate );
            },
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        });


        //
        // Format date
        //

        // Initialize
        $('.datepicker-format').datepicker({
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        });

        // Format date on change
        $('#format').on('change', function() {
            $('.datepicker-format').datepicker('option', 'dateFormat', $(this).val());
        });

        //
        // Format date
        //

        // Initialize
        $('.datepicker-animation').datepicker({
            isRTL: $('html').attr('dir') == 'rtl' ? true : false
        });

        // Animate picker on change
        $('#anim').on('change', function() {
            $('.datepicker-animation').datepicker('option', 'showAnim', $(this).val());
        });
    };

    // Autocomplete
    var _componentUiAutocomplete = function() {
        if (!$().autocomplete) {
            console.warn('Warning - jQuery UI components are not loaded.');
            return;
        }

        // Split values
        function split(val) {
            return val.split(/,\s*/);
        }

        //
        // Remote data
        
        /*else {
			alert('girish here ? ');
			$(this).parent().removeClass('ui-autocomplete-processing');
			$(this).val('');
    		$(this).append('<label id="'+$(this).attr('id')+'-error" class="validation-invalid-label" for="'+$(this).attr('id')+'">No Records Found.</label>');
		}*/
        // Remote data
        $('#student_name').autocomplete({
            minLength: 2,
            //source: 'theme/assets/demo_data/jquery_ui/autocomplete.php',
            source	:	'index.php/?r=settings/site/autocomplete/'+$('#student_name').val(),
            select: function (event, ui) {
	            $("#student_id").val(ui.item.id); // save selected id to hidden input	

	            if ($('#student_id').val() != '') {
	        		//call ajax and fill..
	            	doAjaxCall('student', $('#student_id'));
	        	}
            },
            response: function(event, ui) {
                if (!ui.content.length) {
                    var noResult = { value:"",label:"No results found" };
                    ui.content.push(noResult);
                    $(this).parent().removeClass('ui-autocomplete-processing');
                }
            },

            search: function() {
                $(this).parent().addClass('ui-autocomplete-processing');
            },
            open: function() {
            	$(this).parent().removeClass('ui-autocomplete-processing');
            }
        });

        $('#parent_name').autocomplete({
            minLength: 2,
            //source: 'theme/assets/demo_data/jquery_ui/autocomplete.php',
            source	:	'index.php/?r=settings/site/parentautocomplete/'+$('#parent_name').val(),
            select: function (event, ui) {
                $("#guardian_id").val(ui.item.id); // save selected id to hidden input

                if ($('#guardian_id').val() != '') {
                	doAjaxCall('guardian', $('#guardian_id'));
        		}
            },
            response: function(event, ui) {
                if (!ui.content.length) {
                    var noResult = { value:"",label:"No results found" };
                    ui.content.push(noResult);
                    $(this).parent().removeClass('ui-autocomplete-processing');
                }
            },
            search: function() {
                $(this).parent().addClass('ui-autocomplete-processing');
            },
            open: function() {
            	$(this).parent().removeClass('ui-autocomplete-processing');
            }
        });

        $('#parent_email').autocomplete({
            minLength: 2,
            //source: 'theme/assets/demo_data/jquery_ui/autocomplete.php',
            source	:	'index.php/?r=settings/site/parentemailcomplete/'+$('#parent_email').val(),
            select: function (event, ui) {
            	$("#guardian_id").val(ui.item.id); // save selected id to hidden input

            	if ($('#guardian_id').val() != '') {
            		doAjaxCall('guardian', $('#guardian_id'));
        		}
            },
            response: function(event, ui) {
                if (!ui.content.length) {
                    var noResult = { value:"",label:"No results found" };
                    ui.content.push(noResult);
                    $(this).parent().removeClass('ui-autocomplete-processing');
                }
            },
            search: function() {
                $(this).parent().addClass('ui-autocomplete-processing');
            },
            open: function() {
            	$(this).parent().removeClass('ui-autocomplete-processing');
            }
        });

        //Check for existing email (if any) when student's email id is entered completely..
        $('#txtEmail').on('blur', (function(){
        	if($(this).val() != '') {
        		var myMailExists	=	doCheckEmailExists($(this));
        	}
        }));

        //Check for existing email (if any) for parent..
        $('#txtParentEmail').on('blur', (function(){
        	if($(this).val() != '') {
        		var myMailExists	=	doCheckParentEmailExists($(this));
        	}
        }));
        //
        // Combo box
        //
        // Configure custom widget
        $.widget('custom.combobox', {
            _create: function() {
                this.wrapper = $('<div>').addClass('custom-combobox input-group').insertAfter(this.element);
                this.element.hide();
                this._createAutocomplete();
                this._createShowAllButton();
            },
            _createAutocomplete: function() {
                var selected	=	this.element.children(':selected'),
                value	=	selected.val() ? selected.text() : '';

                var input	=	this.input = $('<input>')
                    .appendTo(this.wrapper)
                    .val(value)
                    .attr('title', '')
                    .attr('placeholder', 'Search')
                    .addClass('form-control')
                    .autocomplete({
                        delay: 0,
                        minLength: 0,
                        source: $.proxy(this, '_source')
                    });

                this._on(this.input, {
                    autocompleteselect: function( event, ui ) {
                        ui.item.option.selected = true;
                        this._trigger('select', event, {
                            item: ui.item.option
                        });
                    },

                    autocompletechange: '_removeIfInvalid'
                });
            },
            _createShowAllButton: function() {
                var input	=	this.input,
                    wasOpen	=	false;

                // Append fonr control icon
                this.wrapper.append('<div class="form-control-feedback"><i class="icon-search4 font-size-base"></i></div>');

                // Add input group button
                var wrapper2 = $('<span>').attr('class', 'input-group-append').appendTo(this.wrapper);
                // Link
                $( '<a>' )
                    .attr( 'tabIndex', -1 )
                    .appendTo( wrapper2 )
                    .button({
                        icons: {
                            primary: 'icon-arrow-down12'
                        },
                        text: false
                    })
                    .removeClass('')
                    .on('mousedown', function() {
                        wasOpen = input.autocomplete('widget').is(':visible');
                    })
                    .on('click', function() {
                        input.focus();

                        // Close if already visible
                        if (wasOpen) {
                            return;
                        }

                        // Pass empty string as value to search for, displaying all results
                        input.autocomplete('search', '');
                    });
            },
            _source: function( request, response ) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), 'i');
                response( this.element.children('option').map(function() {
                    var text = $(this).text();
                    if (this.value && (!request.term || matcher.test(text)))
                        return {
                            label: text,
                            value: text,
                            option: this
                        };
                    })
                );
            },
            _removeIfInvalid: function( event, ui ) {

                // Selected an item, nothing to do
                if (ui.item) {
                    return;
                }

                // Search for a match (case-insensitive)
                var value = this.input.val(),
                    valueLowerCase = value.toLowerCase(),
                    valid = false;

                this.element.children('option').each(function() {
                    if ($(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        return false;
                    }
                });

                // Found a match, nothing to do
                if (valid) {
                    return;
                }

                // Remove invalid value
                this.input.val('').attr('title', value + ' didn\'t match any item');

                this.element.val('');
                this._delay(function() {
                    this.input.tooltip('close').attr('title', '');
                }, 2500);
                this.input.autocomplete('instance').term = '';
            },
            _destroy: function() {
                this.wrapper.remove();
                this.element.show();
            }
        });

        // Initialize
        $('#ac-combo').combobox();
    };

    // Selectmenu
    var _componentUiSelectmenu = function() {
        if (!$().selectmenu) {
            console.warn('Warning - jQuery UI components are not loaded.');
            return;
        }

        // Basic select
        $('.select-basic').selectmenu({
            width: '100%'
        });

        // Optgroups example
        $('#select-optgroups').selectmenu({
            width: '100%'
        });

        // Default width
        $('#select-width').selectmenu({
            width: 200
        });

        // Disabled select
        $('#select-disabled').selectmenu({
            width: '100%',
            disabled: true
        });
    };

    // Spinner
    var _componentUiSpinner = function() {
        if (!$().spinner) {
            console.warn('Warning - jQuery UI components are not loaded.');
            return;
        }

        // Basic spinner
        $('#spinner-basic').spinner();

        // Disabled spinner
        $('#spinner-disabled').spinner({
            disabled: true
        });

        // Spinner min/max limits
        $('#spinner-limits').spinner({
            min: 90,
            max: 110,
            start: 100
        });

        // Spinner step
        $('#spinner-step').spinner({
            step: 20,
            start: 500
        });

        // Spinner overflow
        $('#spinner-overflow').spinner({
            spin: function(event, ui) {
                if (ui.value > 10) {
                    $(this).spinner('value', -10);
                    return false;
                }
                else if (ui.value < -10) {
                    $(this).spinner('value', 10);
                    return false;
                }
            }
        });

        //
        // Time spinner
        //
        // Configure custom widget
        $.widget('ui.timespinner', $.ui.spinner, {
            options: {
                step: 60 * 1000, // seconds
                page: 60 // hours
            },
            _parse: function(value) {
                if (typeof value === 'string') {

                    // Already a timestamp
                    if (Number(value) == value) {
                        return Number(value);
                    }
                    return +Globalize.parseDate(value);
                }
                return value;
            },
            _format: function(value) {
                return Globalize.format(new Date(value), 't');
            }
        });

        // Culture
        $('#spinner-time-culture').on('selectmenuchange', function() {
            var current = $('#spinner-time').timespinner('value');
            Globalize.culture($(this).val());
            $('#spinner-time').timespinner('value', current);
        });

        // Initialize
        $('#spinner-time').timespinner();
    };

    //
    // Return objects assigned to module
    //
    return {
        init: function() {
            _componentUiDatepicker();
            _componentUiAutocomplete();
            _componentUiSelectmenu();
            _componentUiSpinner();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    JqueryUiForms.init();
});


function doAjaxCall(forVal, idVal) {
	if(forVal != '') {
		var myParams	=	'for='+forVal+'&idVal='+idVal.val();
    	var newURLLink	=	'index.php?r=students/students/ajaxResultsForExistingParentJSONGeneration';
    	console.log('myParams For Second Runner : ' + myParams);
        $.ajax( {
        	type	:	'POST',
            url : newURLLink,
            data : myParams,
            dataType	: 'json',
        } )
        .done( function( data ) {
            // Handles successful responses only
        	//alert('data succ : ' + data.success);
        	if(data.success == 1) {
        		$('#txtParentFName').val(data.data.first_name);
        		$('#txtParentLName').val(data.data.last_name);

        		if(data.data.relation != '') {
        			$('#lsBloodRelation').val(data.data.relation).change();
        			$('#lsBloodRelation').attr('selected', "SELECTED");
        		}

        		$('#txtDOB').val(data.data.dob);
        		$('#txtParentEducation').val(data.data.education);
        		$('#txtParentOccupation').val(data.data.occupation);
        		$('#txtParentIncome').val(data.data.income);
        		$('#txtParentEmail').val(data.data.email);

        		$('#txtParentMobile').val(data.data.mobile_phone);
        		$('#txtParentOfficePhone').val(data.data.office_phone1);
        		$('#txtParentOfficePhone2').val(data.data.office_phone2);
        		$('#txtParentOfficeAddress1').val(data.data.office_address_line1);
        		$('#txtParentOfficeAddress2').val(data.data.office_address_line2);

        		$('#txtParentCity').val(data.data.city);
        		$('#txtParentState').val(data.data.state);

        		if(data.data.country_id != '') {
        			$('#lsParentCountries').val(data.data.country_id).change();
        			$('#lsParentCountries').attr('selected', "SELECTED");
        		}
        	}
        } )
        .fail( function( reason ) {
            // Handles errors only
        	console.log('Handles 2nd errors only ' + reason);
        } )
	}
}

function doCheckEmailExists(id){
	if (id.val() != '') {
		var myParams	=	'&emailExists='+id.val() /*+'&hidRecId='+$('#hidRecId').val();*/
    	var newURLLink	=	'index.php?r=students/students/ajaxResultsForExistingEmailJSONGeneration';
    	console.log('myParams For Email Existing: ' + myParams);
        $.ajax( {
        	type	:	'POST',
            url : newURLLink,
            data : myParams,
            dataType	: 'json',
        } )
        .done( function( data ) {
        	if(data == 1) {
        		//alert('email already exists..');
        		swal({
                    title: 'Oops...',
                    text: 'This email already exists..!',
                    type: 'error'
                }).then(function() {
                    swal.close();
                    id.focus();
                    return;
                });
        	}
        } )
        .fail( function( reason ) {
            // Handles errors only
        } )
	}
}

function doCheckParentEmailExists(id){
	if ( (id.val() != '') &&($('#hidRecId').val() == '') ) {
		var myParams	=	'&emailExists='+id.val()+'&hidRecId='+$('#hidRecId').val();
    	var newURLLink	=	'index.php?r=students/students/ajaxResultsForExistingEmailJSONGeneration';
    	console.log('myParams For Email Existing: ' + myParams);

    	$.ajax( {
        	type	:	'POST',
            url : newURLLink,
            data : myParams,
            dataType	: 'json',
        } )
        .done( function( data ) {
        	if(data == 1) {
        		//alert('email already exists..');
        		swal({
                    title: 'Oops...',
                    text: 'This email already exists..!',
                    type: 'error'
                }).then(function() {
                    swal.close();
                    id.focus();
                    return;
                });
        	}
        } )
        .fail( function( reason ) {
            // Handles errors only
        } )
	}
}