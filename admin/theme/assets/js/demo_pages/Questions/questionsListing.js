	var mySearchArr	=	[];

	///alert('SEARCH CONCAT POST LOAD ?'  + $('#hidSearchConcat').val());
	console.log('hidSearch ? '  + $('#hidSearchConcat').val());
	if ( ($('#hidSearchConcat').val() != '') && ($('#hidSearchConcat').val() != undefined) ) {
		var myJSONVal	=	$.parseJSON($('#hidSearchConcat').val());
		mySearchArr	=	myJSONVal;
	}

	var x	=	new Date()
	var ajaxStartTime	=	x.toUTCString();
	var light_8	=	$('#spinner-light-8').closest('.card');

	( function() {
	    "use strict";
	    doQuestionsPreLoadData();
	    //doFilterBtnActions();
	    doNavTabActions();
	    doDTableActions();
	} )(jQuery);

	function doClearSearchFilters(attrID) {
		//alert('attrID: ' + attrID);
		$('#lsSubjectsForFilter').val('').change();
		$('#lsModulesForFilter')
	    .find('option')
	    .remove()
	    .end()
	    .append('<option value="">--Choose One--</option>')
	    .val('');

		$('#lsChaptersForFilter')
	    .find('option')
	    .remove()
	    .end()
	    .append('<option value="">--Choose One--</option>')
	    .val('');
		$('#topicName').val('');
	}

	function doNavTabActions() {
		$('.navbar-nav-link').on('click', (function(e){
			e.preventDefault();
			doClearSearchFilters($(this).attr('id'));
		}));
	}

	function doQuestionsPreLoadData() {
		 $(light_8).block({
	        message: '<i class="icon-spinner11 spinner"></i>',
	        overlayCSS: {
	            backgroundColor: '#fff',
	            opacity: 0.8,
	            cursor: 'wait'
	        },
	        css: {
	            border: 0,
	            padding: 0,
	            backgroundColor: 'none'
	        }
	    });
		//LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
		var myParams	=	'hidSearchFlag=0&hidSearchConcat=&limitRowsFirstTime=100&hidSaveFilterRecID='+$('#hidSaveFilterRecID').val();
		var newURLLink	=	$('#hidProjectURL').val()+'Questions/ajaxResultsForFiltersJSONGeneration';

		$.ajax( {
	    	type	:	'POST',
	        url : newURLLink,
	        data : myParams,
	        dataType	: 'json',
	    } )
	        .done( function( data ) {
	            // Handles successful responses only
	        	console.log('Handles successful responses only' + data);
	        	commonDataListing(data, null);
	        	$('#activeFilters .remBtn').on('click', (function() {
	    			doRemActions($('.btn-loading'), $(this).attr('id'));
	    		}));
	        } )
	        .fail( function( reason ) {
	            // Handles errors only
	        	console.log('Handles errors only ' + reason);
	        } )
	        .always( function( data, textStatus, response ) {} )
	        .then( function( data, textStatus, response ) {} );

		$('.btnRefineSearch').on('click', (function(){
			$('#searchByKeyWord').val('');
			$('#showChatpersListing').hide();
			$('#showTopicsListing').hide();
			$('#showSubTopicsListing').hide();
		}))

		$('.populateListing').on('click', (function(){
			var myAttr	=	$(this).attr('id').split('-');

			switch(myAttr[0]) {
				case 'subjects' :
					if(myAttr[1] != 0) {
						var newURLLink	=	$('#hidProjectURL').val()+'Common/ajaxResultsForChaptersDropBox';
				        var myParams	=	'reqType=fetchChaptersBySubject&subjectID='+ myAttr[1];
						console.log('newURL : ' + newURLLink);
						$.ajax({
							type	:	'POST',
							url	:	newURLLink,
							dataType	:	'json',
							data	:	myParams,
						})
		    				.done(function (data) {
		    					if(data.data == 'success') {
		    						var myHtml	=	'<a id="chapters-"'+myAttr[1]+'-0"+ href="#" class="dropdown-item populateListing">--Choose One--</a>';

									$.each(data.chapterDtls, function (index, value) {
										myHtml	+=	'<a id="chapters-'+myAttr[1]+'-'+value.id+'" href="#" class="dropdown-item populateListing"><i class="icon-question7"></i> '+ value.chapterName+'</a>';
									});
									$('#fillChapters').html(myHtml);
									$('#showChatpersListing').show();
								}

		    					doPopulateSubListings();
		                    })
		                    .fail(function (reason) {
		                        // Handles errors only
		                        console.log('Handles errors only ' + reason);
		                    })
		                    .always(function (data, textStatus, response) { })
		                    .then(function (data, textStatus, response) { });
					} else {
						//empty the ul for chapters.
						$('#showChatpersListing').hide();
					}

					break;
			}
		}));

		$('.btn-loading').on('click', (function(e) {
			e.preventDefault();
	    	var myBtnID	=	$(this).attr('id');

	    	if(myBtnID == 'btnQuestion') {
	    		$('#frmQuestions').attr('action', $('#hidProjectURL').val()+'Questions/Create');
                $('#frmQuestions').submit();
	    	}
		}));
	}

	function doPopulateSubListings() {
		$('.populateListing').on('click', (function(){
			var myAttr	=	$(this).attr('id').split('-');

			switch(myAttr[0]) {
				case 'chapters':
					if ( (myAttr[1] != '') && (myAttr[2] != '') ) {
						var newURLLink	=	$('#hidProjectURL').val()+'Common/ajaxResultsForTopicsDropBox';
				        var myParams	=	'reqType=fetchTopicsBySubject&subjectID='+myAttr[1]+'&chapterID='+ myAttr[2];
						console.log('newURL : ' + newURLLink);

						$.ajax({
							type	:	'POST',
							url	:	newURLLink,
							dataType	:	'json',
							data	:	myParams,
						})
		    				.done(function (data) {
		    					if(data.data == 'success') {
		    						var myHtml	=	'<a id="topics-"'+myAttr[1]+'-'+myAttr[2]+'-0"+ href="#" class="dropdown-item populateListing">--Choose One--</a>';
		    						console.log('topic dtls : ' + data.topicDtls);
									$.each(data.topicDtls, function (index, value) {
										console.log('topic id : ' + value.id + ' topic name : ' + value.topicName);
										myHtml	+=	'<a id="topics-'+myAttr[1]+'-'+myAttr[2]+'-'+value.id+'" href="#" class="dropdown-item populateListing"><i class="icon-question7"></i> '+ value.topicName+'</a>';
									});

									$('#fillTopics').html(myHtml);
									$('#showTopicsListing').show();
								}

		    					doPopulateSubListings();
		                    })
		                    .fail(function (reason) {
		                        // Handles errors only
		                        console.log('Handles errors only ' + reason);
		                    })
		                    .always(function (data, textStatus, response) { })
		                    .then(function (data, textStatus, response) { });
					} else {
						//empty the ul for chapters.
						$('#showTopicsListing').hide();
					}

					break;

				case 'topics':
					if ( (myAttr[1] != '') && (myAttr[2] != '') && (myAttr[3] != '') ) {
						var newURLLink	=	$('#hidProjectURL').val()+'Common/ajaxResultsForSubTopicsDropBox';
				        var myParams	=	'reqType=fetchTopicsBySubject&subjectID='+myAttr[1]+'&chapterID='+ myAttr[2]+'&topicID='+ myAttr[3];
						console.log('newURL : ' + newURLLink);

						$.ajax({
							type	:	'POST',
							url	:	newURLLink,
							dataType	:	'json',
							data	:	myParams,
						})
		    				.done(function (data) {
		    					if(data.data == 'success') {
		    						var myHtml	=	'<a id="topics-"'+myAttr[1]+myAttr[2]+'-0"+ href="#" class="dropdown-item populateListing">--Choose One--</a>';
		    						console.log('topic dtls : ' + data.topicDtls);
									$.each(data.topicDtls, function (index, value) {
										console.log('topic id : ' + value.id + ' topic name : ' + value.topicName);
										myHtml	+=	'<a id="topics-'+myAttr[1]+'-'+myAttr[2]+'-'+value.id+'" href="#" class="dropdown-item populateListing"><i class="icon-question7"></i> '+ value.topicName+'</a>';
									});

									$('#fillSubTopics').html(myHtml);
									$('#showSubTopicsListing').show();
								}
		                    })
		                    .fail(function (reason) {
		                        // Handles errors only
		                        console.log('Handles errors only ' + reason);
		                    })
		                    .always(function (data, textStatus, response) { })
		                    .then(function (data, textStatus, response) { });
					break;
				} else {
					$('#showSubTopicsListing').hide();
				}
			}
		}));
	}

	function doFilterBtnActions() {
		//Check dtable existance
	    $('.btn-loading').on('click', function (e) {
	    	e.preventDefault();
	    	var myBtnID	=	$(this).attr('id');

	    	if(myBtnID == 'Search') {
		        var btn	=	$(this),
		        initialText	=	btn.data('initial-text'),
		        loadingText	=	btn.data('loading-text');
	
		        if(validateFilters(btn)) {
					btn.html(loadingText).addClass('disabled');
					console.log('Ajax Start Time : ' + ajaxStartTime);
					doGenerateSearchFilters(btn);
	
					if (!(mySearchArr)){
						//do nothing..
					} else {
						//alert('mySearchArr : ' + mySearchArr + ' len : ' + mySearchArr.length);
						 $(light_8).block({
							message: '<i class="icon-spinner11 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'none'
							}
						});
						console.log('hidSearchConcat after generating : ' + $('#hidSearchConcat').val());
						var myParams	=	'hidSearchFlag=1&hidSearchConcat='+$('#hidSearchConcat').val();
						var newURLLink	=	$('#hidProjectURL').val()+'Questions/ajaxResultsForFiltersJSONGeneration';
						console.log('myParams For Second Runner : ' + myParams);
						$.ajax( {
							type	:	'POST',
							url : newURLLink,
							data : myParams,
							dataType	: 'json',
						} )
						.done( function( data ) {
							// Handles successful responses only
							console.log('Handles successful 2nd responses only' + data);
							commonDataListing(data, btn);
							$('#activeFilters .remBtn').on('click', (function() {
								doRemActions(btn, $(this).attr('id'));
							}));
						} )
						.fail( function( reason ) {
							// Handles errors only
							console.log('Handles errors only ' + reason);
						} )
						.always( function( data, textStatus, response ) {} )
						.then( function( data, textStatus, response ) {} );
					}
				} else {
					//do nothing..
				}
	    	} else {
	    		//reset
	    		doClearSearchFilters();
	    	}
	    });
	}

	function commonDataListing(data, myProp) {
		//common For all the ajax Done process.. [ Search Filter, All, Default, Delete action etc...]

		if(data.success == 1) { //Success
			//alert('data succ ? ' + data.success);
			var x	=	new Date();
			curDate	=	x.toUTCString();// changing the display to UTC string
			console.log(' success date : ' + curDate);
			$(light_8).unblock();
	    	$('#questionsTable').show();
	    	$('#spinner-light-8').hide();
	    	//Steps for filling up the contents from the json response.
			if(myProp != null) {
				initialText	=	myProp.data('initial-text');
				myProp.html(initialText).removeClass('disabled');
			}

			//STEP 1 : Active Filters
			if(data.activeFilters != null) {
				$('#activeFilters').html(data.activeFilters);
			}
	    	//STEP 1 : Active Filters

			//STEP 2 : Data Table
	        var table	=	$('#questionsTable').DataTable({ //.datatable-button-html5-basic
	    		destroy :	true,
	    		aaData	:	data.writeJSONData,
	    		selectAllPages	:	false,
	    		columns: [
	    			{ "data": "SNo" },
	                { "data": "Subject" },
	                //{ "data": "Module" },
	                { "data": "Chapter" },
	                { "data": "Topic" },
	                { "data": "Total Questions" },
	                { "data": "Status" },
	                { "data": "Action" },
	            ],
				'columnDefs': [
			         { targets: 'no-sort', orderable: false }
			      ],
	    		buttons: {
	    	        dom: {
	                    button: {
	                        className: 'btn btn-light'
	                    }
	                },
	                buttons: [
	                	{
	                        text: 'Create Question',
	                        className: 'btn bg-primary',
	                        action: function () {
	                            $('#frmFilters').attr('action', $('#hidProjectURL').val()+'Questions/Create');
	                            $('#frmFilters').submit();
	                        }
	                    },
	                    {
	                        extend: 'copyHtml5',
	                        className: 'btn bg-grey',
	                        title: $('#hidAppTitle').val() + ' - Classes Listing',
	                        exportOptions: {
	                            columns: "thead th:not(.noExport)"
	                        }
	                    },
	                	{
	                        extend: 'pdfHtml5',
	                        className: 'btn bg-grey',
	                        title: $('#hidAppTitle').val() + ' - Classes Listing',
	                        exportOptions: {
	                        	columns: "thead th:not(.noExport)"
	                        }
	                    },
	                    {
	                        extend: 'csvHtml5',
	                        className: 'btn bg-grey',
	                        title: $('#hidAppTitle').val() + ' - Classes Listing',
	                        exportOptions: {
	                        	columns: "thead th:not(.noExport)"
	                        }
	                    },
	                    {
	                        extend: 'excelHtml5',
	                        className: 'btn bg-grey',
	                        title: $('#hidAppTitle').val() + ' - Classes Listing',
	                        exportOptions: {
	                        	columns: "thead th:not(.noExport)"
	                        }
	                    },
	            ]},
	            select	:	'single',
	            style: 'os',
	            order	: [[0, 'DESC']],
	            bFilter	: false,
	            //bDestroy	:	true,
		    });

	        //STEP 2 : Data Table
	        $("div#questionsTable_wrapper").find($(".dt-buttons")).css("float", "left");
	        $("div#questionsTable_wrapper").find($(".datatable-scroll-wrap")).css("height", "300px");
	        $("div#questionsTable_wrapper").find('select').addClass('form-control form-input-styled');
		}
	}

	function validateFilters(myProp) {
		var isOpenedTabValid	=	$(".tab-pane:visible :input").valid();
		return isOpenedTabValid;
	}
	
	function doGenerateSearchFilters(myProp) {
		$('#hidSearchFlag').val(1);

		//Subject Code Search
		if(myProp.attr('id') =='btnSubject') {
			var isOpenedTabValid	=	$(".tab-pane:visible :input").valid();

			if(isOpenedTabValid == false) {
				initialText	=	myProp.data('initial-text');
				myProp.html(initialText).removeClass('disabled');
			} else {
				mySearchArr	=	removeAction(mySearchArr, 'subject');
				mySearchArr	=	removeAction(mySearchArr, 'module');
				//Add new / latest search
				mySearchArr.push({subject	:	$('#lsSubjectsForFilter').val().trim()});
				mySearchArr.push({module	:	$('#lsModulesForFilter').val().trim()});
			}
		}

		//Name Search
		if(myProp.attr('id') =='btnName') {
			var isOpenedTabValid	=	$(".tab-pane:visible :input").valid();

			if(isOpenedTabValid == false) {
				initialText	=	myProp.data('initial-text');
				myProp.html(initialText).removeClass('disabled');
			} else {
				mySearchArr	=	removeAction(mySearchArr, 'chapterName');
				//Add new / latest search
				mySearchArr.push({chapterName	:	$('#chapterName').val().trim()});
			}
		}

		console.log('final mySearchArr : ' + mySearchArr);

		if (mySearchArr != null) {
			$('#hidSearchConcat').val(JSON.stringify(mySearchArr));
		}
	}

	function removeAction(arrOriginal, elementToRemove){
		var myNewSearchArr	=	[];

		if(arrOriginal != null) {
			$.each(arrOriginal, function(key, value){
				$.each(value, function(key, value1){
					if(elementToRemove != key) {
						//return arrOriginal.filter(function(el){return el !== elementToRemove});
						myNewSearchArr.push(value);
					}
				});
			});
		}

		return myNewSearchArr;
	}

	function doRemActions(myId, myRemId) {
		$('#hidSearchFlag').val(2);

		switch(myRemId) {
			// REM ACTIONS
			case 'btnRemSubject' :
				mySearchArr	=	removeAction(mySearchArr, 'subject');
				mySearchArr	=	removeAction(mySearchArr, 'module');
				$('#lsSubjectsForFilter').val('').change();
				$('#lsModulesForFilter')
			    .find('option')
			    .remove()
			    .end()
			    .append('<option value="">--Choose One--</option>')
			    .val('');
				break;

			case 'btnRemName' :
				mySearchArr	=	removeAction(mySearchArr, 'chapterName');
				$('#moduleName').val('');
				break;
		}

		if ( (mySearchArr != null) && (mySearchArr.length > 0) ) {
			$('#hidSearchConcat').val(JSON.stringify(mySearchArr));

			$(light_8).block({
		        message: '<i class="icon-spinner11 spinner"></i>',
		        overlayCSS: {
		            backgroundColor: '#fff',
		            opacity: 0.8,
		            cursor: 'wait'
		        },
		        css: {
		            border: 0,
		            padding: 0,
		            backgroundColor: 'none'
		        }
		    });
	    	var myParams	=	'hidSearchFlag=2&hidSearchConcat='+$('#hidSearchConcat').val()+'&limitRowsFirstTime=100';
	    	var newURLLink	=	$('#hidProjectURL').val()+'Questions/ajaxResultsForFiltersJSONGeneration';
	    	console.log('myParams For Third Runner : ' + myParams);

	    	$.ajax( {
	        	type	:	'POST',
	            url : newURLLink,
	            data : myParams,
	            dataType	: 'json',
	        } )
	        .done( function( data ) {
	            // Handles successful responses only
	        	console.log('Handles successful 2nd responses only' + data);
	        	commonDataListing(data, myId);
	        	$('#activeFilters .remBtn').on('click', (function() {
					doRemActions(myId, $(this).attr('id'));
				}));
	        } )
	        .fail( function( reason ) {
	            // Handles errors only
	        	console.log('Handles 2nd errors only ' + reason);
	        } )
	        .always( function( data, textStatus, response ) {} )
	        .then( function( data, textStatus, response ) {});
		} else {
			 $(light_8).block({
		        message: '<i class="icon-spinner11 spinner"></i>',
		        overlayCSS: {
		            backgroundColor: '#fff',
		            opacity: 0.8,
		            cursor: 'wait'
		        },
		        css: {
		            border: 0,
		            padding: 0,
		            backgroundColor: 'none'
		        }
		    });
			//LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
			var myParams	=	'hidSearchFlag=1&hidSearchConcat=&limitRowsFirstTime=100';
			var newURLLink	=	$('#hidProjectURL').val()+'Questions/ajaxResultsForFiltersJSONGeneration';

		    $.ajax( {
		    	type	:	'POST',
		        url : newURLLink,
		        data : myParams,
		        dataType	: 'json',
		    } )
		        .done( function( data ) {
		            // Handles successful responses only
		        	console.log('Handles successful empty filters final responses only' + data);
		        	commonDataListing(data);
		        } )
		        .fail( function( reason ) {
		            // Handles errors only
		        	console.log('Handles empty filters final errors only ' + reason);
		        } )
		        .always( function( data, textStatus, response ) {} )
		        .then( function( data, textStatus, response ) {} );
		}
	}

	//Edit and Delete
	function doDTableActions() {
	    $('#questionsTable').on('click', '.logAction', (function () {
	        var myAttr = $(this).attr('id').split('-');
	        $('#hidRecID').val(myAttr[1]);

	        switch (myAttr[0]) {
	        	case 'view':
	        		$('#frmQuestions').attr('action', $('#hidProjectURL').val()+'Questions/View/'+ myAttr[1]);
	        		$('#frmQuestions').submit();
	        		break;
	            case 'edit':
	                var myParams	=	'hidReqType=formList&hidRecID=' + myAttr[1];
	                var newURLLink	=	$('#hidProjectURL').val() + 'Questions/ajaxResultsForQuestionsAction';
	                console.log('newURLLINK : ' + newURLLink);

	                $.ajax({
	                    type: 'POST',
	                    url: newURLLink,
	                    dataType: 'json',
	                    data: myParams,
	                })
	                    .done (function (data) {
	                        //doClear();
	                        if (data.data == 'success') {
	                            doOpenSwal('update', data.formCont);
	                        }
	                    })
	                    .fail( function( reason ) {
	    		            // Handles errors only
	    		        	console.log('Handles empty filters final errors only ' + reason);
	    		        } )
	    		        .always( function( data, textStatus, response ) {} )
	    		        .then( function( data, textStatus, response ) {} );

	                break;

	            case 'del':
	                //Get confirmation b4 deleting..
	                //alert('Thanks for confirming');
	                var myParams	=	'hidReqType=dele&hidRecID=' + myAttr[1];
	                var newURLLink	=	$('#hidProjectURL').val() + 'Questions/ajaxResultsForQuestionsAction';
	                console.log('newURLLINK : ' + newURLLink + myParams);
	                // return;
	                swal({
	                    title: 'Are you sure?',
	                    text: 'You will not be able to recover the deleted record!',
	                    type: 'warning',
	                    showCancelButton: true,
	                    confirmButtonText: 'Yes, delete it!',
	                    showLoaderOnConfirm: true,
	                    preConfirm: function (email) {
	                        return new Promise(function (resolve) {
	                            $.ajax({
	                                type: 'POST',
	                                url: newURLLink,
	                                dataType: 'json',
	                                data: myParams,
	                            })
	                                .done(function (data) {
	                                    // Handles successful responses only
	                                    if (data.data == 'success') {
	                                        resolve();
	                                        preLoadData();
	                                    } else {
	                                        alert('error in Deleting');
	                                    }
	                                })
	                                .fail(function (reason) {
	                                    // Handles errors only
	                                    console.log('Handles errors only ' + reason);
	                                })
	                                .always(function (data, textStatus, response) { })
	                                .then(function (data, textStatus, response) { });
	                        });
	                    },
	                });

	                break;
	        }
	    }
	    ))
	}

	function doOpenSwal(myAction, formCont) {
	    swal({
	        html: formCont,
	        showCancelButton: true,
	        confirmButtonText: myAction.toUpperCase(),
	        onOpen : function() {
	        	$('.form-input-styled').uniform({
		            fileButtonClass: 'action btn bg-blue'
		        });

	        	$('#lsSubjects').on('change', (function(){
	    			if ($(this).val() != '') {
	    				var newURLLink	=	$('#hidProjectURL').val()+'Common/ajaxResultsForModulesDropBox';
	    		        var myParams	=	'reqType=fetchModulesBySubject&subjectID='+ $(this).val();
	    				console.log('newURL : ' + newURLLink);
	    				$.ajax({
	    					type	:	'POST',
	    					url	:	newURLLink,
	    					dataType	:	'json',
	    					data	:	myParams,
	    				})
		    				.done(function (data) {
		    					if(data.data == 'success') {
	    							$("#lsModules").empty(); //Emptying the modules every time when the Subject selection is altered.
	    							var list	=	$("#lsModules");
	    							list.append('<option value="">--Choose One -- </option>');

	    							$.each(data.modulesDtls, function (index, value) {
	    								console.log('module id : ' + value.id + ' mod name : ' + value.moduleName);
	    								list.append('<option value="' + value.id+'">' + value.moduleName+ '</option>');
	    							});
	    						}
		                    })
		                    .fail(function (reason) {
		                        // Handles errors only
		                        console.log('Handles errors only ' + reason);
		                    })
		                    .always(function (data, textStatus, response) { })
		                    .then(function (data, textStatus, response) { });
	    			} else {
	    				$('#lsModules')
	    				    .find('option')
	    				    .remove()
	    				    .end()
	    				    .append('<option value="">--Choose One--</option>')
	    				    .val('');
	    			}
	    		}));

	        	$('#lsModules').on('change', (function(){
	    			if ($(this).val() != '') {
	    				var newURLLink	=	$('#hidProjectURL').val()+'Common/ajaxResultsForChaptersDropBox';
	    		        var myParams	=	'reqType=fetchModulesBySubject&subjectID='+ $('#lsSubjects').val()+'&moduleID='+ $(this).val();
	    				console.log('newURL : ' + newURLLink);

	    				$.ajax({
	    					type	:	'POST',
	    					url	:	newURLLink,
	    					dataType	:	'json',
	    					data	:	myParams,
	    				})
		    				.done(function (data) {
		    					if(data.data == 'success') {
	    							$("#lsChapters").empty(); //Emptying the Chapters every time when the Subject and Modules selection are altered.
	    							var list	=	$("#lsChapters");
	    							list.append('<option value="">--Choose One -- </option>');

	    							$.each(data.chapterDtls, function (index, value) {
	    								console.log('chapter id : ' + value.id + ' chapter name : ' + value.chapterName);
	    								list.append('<option value="' + value.id+'">' + value.chapterName+ '</option>');
	    							});
	    						}
		                    })
		                    .fail(function (reason) {
		                        // Handles errors only
		                        console.log('Handles errors only ' + reason);
		                    })
		                    .always(function (data, textStatus, response) { })
		                    .then(function (data, textStatus, response) { });
	    			} else {
	    				$('#lsChapters')
	    				    .find('option')
	    				    .remove()
	    				    .end()
	    				    .append('<option value="">--Choose One--</option>')
	    				    .val('');
	    			}
	    		}));
	        },
	        preConfirm: function (email) {
	            return new Promise(function (resolve) {
	                var myParams = '&hidReqType=' + myAction;
	                var newURLLink = $('#hidProjectURL').val() + 'Questions/ajaxResultsForQuestionsAction';
	                $.ajax({
	                    type: 'POST',
	                    url: newURLLink,
	                    dataType: 'json',
	                    data: $('#frmCommon').serialize() + myParams,
	                })
	                    .done(function (data) {
	                        // Handles successful responses only
	                        if (data.data == 'success') {
	                            resolve();
	                            preLoadData();
	                        } else {
	                            alert('error in Updating');
	                        }
	                    })
	            })
	        }
	    })
	}