	var mySearchArr	=	[];

	///alert('SEARCH CONCAT POST LOAD ?'  + $('#hidSearchConcat').val());
	console.log('hidSearch ? '  + $('#hidSearchConcat').val());
	if ( ($('#hidSearchConcat').val() != '') && ($('#hidSearchConcat').val() != undefined) ) {
		var myJSONVal	=	$.parseJSON($('#hidSearchConcat').val());
		mySearchArr	=	myJSONVal;
	}

	var x	=	new Date()
	var ajaxStartTime	=	x.toUTCString();
	var light_8	=	$('#spinner-light-8').closest('.card');

	( function() {
	    "use strict";
	    doQuestionsUploadPreLoadData();
	    doDTableActions();
	} )(jQuery);

	function doQuestionsUploadPreLoadData() {
		$('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-blue'
        });

		 $(light_8).block({
	        message: '<i class="icon-spinner11 spinner"></i>',
	        overlayCSS: {
	            backgroundColor: '#fff',
	            opacity: 0.8,
	            cursor: 'wait'
	        },
	        css: {
	            border: 0,
	            padding: 0,
	            backgroundColor: 'none'
	        }
	    });
		//LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
		var myParams	=	'hidSearchFlag=0&hidSearchConcat=&limitRowsFirstTime=100&hidSaveFilterRecID='+$('#hidSaveFilterRecID').val();
		var newURLLink	=	$('#hidProjectURL').val()+'Questions/ajaxResultsForQuestionUploadsFiltersJSONGeneration';

		$.ajax( {
	    	type	:	'POST',
	        url : newURLLink,
	        data : myParams,
	        dataType	: 'json',
	    } )
	        .done( function( data ) {
	            // Handles successful responses only
	        	console.log('Handles successful responses only' + data);
	        	commonDataListing(data, null);
	        	$('#activeFilters .remBtn').on('click', (function() {
	    			doRemActions($('.btn-loading'), $(this).attr('id'));
	    		}));
	        } )
	        .fail( function( reason ) {
	            // Handles errors only
	        	console.log('Handles errors only ' + reason);
	        } )
	        .always( function( data, textStatus, response ) {} )
	        .then( function( data, textStatus, response ) {} );

		$('#lsSubjects').on('change', (function(){
			if ($(this).val() != '') {
				var newURLLink	=	$('#hidProjectURL').val()+'Common/ajaxResultsForChaptersDropBox';
		        var myParams	=	'reqType=fetchChaptersBySubject&subjectID='+ $(this).val();
				console.log('newURL : ' + newURLLink);

				$.ajax({
					type	:	'POST',
					url	:	newURLLink,
					dataType	:	'json',
					data	:	myParams,
				})
    				.done(function (data) {
    					if(data.data == 'success') {
							$("#lsChapters").empty(); //Emptying the Chapters every time when the Subject and Modules selection are altered.
							var list	=	$("#lsChapters");
							list.append('<option value="">--Choose One -- </option>');

							$.each(data.chapterDtls, function (index, value) {
								console.log('chapter id : ' + value.id + ' chapter name : ' + value.chapterName);
								list.append('<option value="' + value.id+'">' + value.chapterName+ '</option>');
							});
						}
                    })
                    .fail(function (reason) {
                        // Handles errors only
                        console.log('Handles errors only ' + reason);
                    })
                    .always(function (data, textStatus, response) { })
                    .then(function (data, textStatus, response) { });
			} else {
				$('#lsChapters')
				    .find('option')
				    .remove()
				    .end()
				    .append('<option value="">--Choose One--</option>')
				    .val('');
			}
		}));

		$('#lsChapters').on('change', (function(){
			if ($(this).val() != '') {
				var newURLLink	=	$('#hidProjectURL').val()+'Common/ajaxResultsForTopicsDropBox';
		        var myParams	=	'reqType=fetchModulesBySubject&subjectID='+ $('#lsSubjects').val();
		        myParams	+=	'&chapterID='+$(this).val();
				console.log('newURL : ' + newURLLink);

				$.ajax({
					type	:	'POST',
					url	:	newURLLink,
					dataType	:	'json',
					data	:	myParams,
				})
    				.done(function (data) {
    					if(data.data == 'success') {
							$("#lsTopics").empty(); //Emptying the Chapters every time when the Subject and Modules selection are altered.
							var list	=	$("#lsTopics");
							list.append('<option value="">--Choose One -- </option>');

							$.each(data.topicDtls, function (index, value) {
								console.log('topic id : ' + value.id + ' topic name : ' + value.topicName);
								list.append('<option value="' + value.id+'">' + value.topicName+ '</option>');
							});
						}
                    })
                    .fail(function (reason) {
                        // Handles errors only
                        console.log('Handles errors only ' + reason);
                    })
                    .always(function (data, textStatus, response) { })
                    .then(function (data, textStatus, response) { });
			} else {
				$('#lsTopics')
				    .find('option')
				    .remove()
				    .end()
				    .append('<option value="">--Choose One--</option>')
				    .val('');
			}
		}));
	}

	function commonDataListing(data, myProp) {
		//common For all the ajax Done process.. [ Search Filter, All, Default, Delete action etc...]

		if(data.success == 1) { //Success
			//alert('data succ ? ' + data.success);
			var x	=	new Date();
			curDate	=	x.toUTCString();// changing the display to UTC string
			console.log(' success date : ' + curDate);
			$(light_8).unblock();
	    	$('#questionsTable').show();
	    	$('#spinner-light-8').hide();
	    	//Steps for filling up the contents from the json response.
			if(myProp != null) {
				initialText	=	myProp.data('initial-text');
				myProp.html(initialText).removeClass('disabled');
			}

			//STEP 1 : Active Filters
			if(data.activeFilters != null) {
				$('#activeFilters').html(data.activeFilters);
			}
	    	//STEP 1 : Active Filters

			//STEP 2 : Data Table
	        var table	=	$('#questionsTable').DataTable({ //.datatable-button-html5-basic
	    		destroy :	true,
	    		aaData	:	data.writeJSONData,
	    		selectAllPages	:	false,
	    		columns: [
	    			{ "data": "SNo" },
	                { "data": "Subject" },
	                { "data": "Module" },
	                { "data": "Chapter" },
	                { "data": "Topic" },
	                { "data": "Question" },
	                { "data": "Status" },
	                { "data": "Action" },
	            ],
				'columnDefs': [
			         { targets: 'no-sort', orderable: false }
			      ],
	    		buttons: {
	    	        dom: {
	                    button: {
	                        className: 'btn btn-light'
	                    }
	                },
	                buttons: [
	                	{
	                        text: 'Create Question',
	                        className: 'btn bg-primary',
	                        action: function () {
	                        	// TBD
	                        }
	                    },
	                    {
	                        extend: 'copyHtml5',
	                        className: 'btn bg-grey',
	                        title: $('#hidAppTitle').val() + ' - Classes Listing',
	                        exportOptions: {
	                            columns: "thead th:not(.noExport)"
	                        }
	                    },
	                	{
	                        extend: 'pdfHtml5',
	                        className: 'btn bg-grey',
	                        title: $('#hidAppTitle').val() + ' - Classes Listing',
	                        exportOptions: {
	                        	columns: "thead th:not(.noExport)"
	                        }
	                    },
	                    {
	                        extend: 'csvHtml5',
	                        className: 'btn bg-grey',
	                        title: $('#hidAppTitle').val() + ' - Classes Listing',
	                        exportOptions: {
	                        	columns: "thead th:not(.noExport)"
	                        }
	                    },
	                    {
	                        extend: 'excelHtml5',
	                        className: 'btn bg-grey',
	                        title: $('#hidAppTitle').val() + ' - Classes Listing',
	                        exportOptions: {
	                        	columns: "thead th:not(.noExport)"
	                        }
	                    },
	            ]},
	            select	:	'single',
	            style: 'os',
	            order	: [[0, 'DESC']],
	            bFilter	: false,
	            //bDestroy	:	true,
		    });

	        //STEP 2 : Data Table
	        $("div#questionsTable_wrapper").find($(".dt-buttons")).css("float", "left");
	        $("div#questionsTable_wrapper").find($(".datatable-scroll-wrap")).css("height", "300px");
	        $("div#questionsTable_wrapper").find('select').addClass('form-control form-input-styled');
		}
	}

	//Edit and Delete
	function doDTableActions() {
	    $('#questionsTable').on('click', '.logAction', (function () {
	        var myAttr = $(this).attr('id').split('-');
	        $('#hidRecID').val(myAttr[1]);

	        switch (myAttr[0]) {
	            case 'edit':
	                var myParams	=	'hidReqType=formList&hidRecID=' + myAttr[1];
	                var newURLLink	=	$('#hidProjectURL').val() + 'Questions/ajaxResultsForQuestionsAction';
	                console.log('newURLLINK : ' + newURLLink);

	                $.ajax({
	                    type: 'POST',
	                    url: newURLLink,
	                    dataType: 'json',
	                    data: myParams,
	                })
	                    .done (function (data) {
	                        //doClear();
	                        if (data.data == 'success') {
	                            //doOpenSwal('update', data.formCont);
	                        }
	                    })
	                    .fail( function( reason ) {
	    		            // Handles errors only
	    		        	console.log('Handles empty filters final errors only ' + reason);
	    		        } )
	    		        .always( function( data, textStatus, response ) {} )
	    		        .then( function( data, textStatus, response ) {} );

	                break;

	            case 'del':
	                //Get confirmation b4 deleting..
	                //alert('Thanks for confirming');
	                var myParams	=	'hidReqType=dele&hidRecID=' + myAttr[1];
	                var newURLLink	=	$('#hidProjectURL').val() + 'Questions/ajaxResultsForQuestionsAction';
	                console.log('newURLLINK : ' + newURLLink + myParams);
	                // return;
	                swal({
	                    title: 'Are you sure?',
	                    text: 'You will not be able to recover the deleted record!',
	                    type: 'warning',
	                    showCancelButton: true,
	                    confirmButtonText: 'Yes, delete it!',
	                    showLoaderOnConfirm: true,
	                    preConfirm: function (email) {
	                        return new Promise(function (resolve) {
	                            $.ajax({
	                                type: 'POST',
	                                url: newURLLink,
	                                dataType: 'json',
	                                data: myParams,
	                            })
	                                .done(function (data) {
	                                    // Handles successful responses only
	                                    if (data.data == 'success') {
	                                        resolve();
	                                        preLoadData();
	                                    } else {
	                                        alert('error in Deleting');
	                                    }
	                                })
	                                .fail(function (reason) {
	                                    // Handles errors only
	                                    console.log('Handles errors only ' + reason);
	                                })
	                                .always(function (data, textStatus, response) { })
	                                .then(function (data, textStatus, response) { });
	                        });
	                    },
	                });

	                break;
	        }
	    }
	    ))
	}