var light_8 = $('#spinner-light-8').closest('.card');
(function () {
    "use strict";
    preLoadData();
    btnActions();
    doDTableActions();
    

    preLoadDataStates();
    btnActionsStates();
    doDTableActionsStates();
    
    
    preLoadDataCities();
    btnActionsCities();
    doDTableActionsCities();
    
    
})(jQuery);

/////COUNTRIES/////

function preLoadData() {
    $(light_8).block({
        message: '<i class="icon-spinner11 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });

    //LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
    var myParams = 'limitRowsFirstTime=100';
    var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForCountriesFiltersJSONGeneration' ;

    $.ajax({
        type: 'POST',
        url: newURLLink,
        data: myParams,
        dataType: 'json',
    })
        .done(function (data) {
            // Handles successful responses only
            console.log('Handles successful responses only' + data);
            ajaxDonePageLoad(data);
            // ajaxDonePageLoadStates(data);
        })
        .fail(function (reason) {
            // Handles errors only
            console.log('Handles errors only ' + reason);
        })
        .always(function (data, textStatus, response) { })
        .then(function (data, textStatus, response) { });
}


function doClear() {
    $('.form-control').each(function(){
        $(this).val('');
    });
}

function btnActions() {
    $('#btnReset').on('click', (function (e) {
        $('.form-control').each(function(){
            $(this).val('');
        });
    }));

    $('#confirm_pass').on('blur', function () {
        if ($('#pass').val() == $('#confirm_pass').val()) {
            $('#btnSave').attr("disabled", false);
            $('#message').hide();
        } else {
            $('#message').show();
            $("#btnSave").attr("disabled", true);
          $('#message').html('Not Matching').css('color', 'red');
        }
      });

        $('#btnSave').on('click', (function (e) {
        console.log('btn Value : ' + $(this).html());
        e.preventDefault();
        // if ($('#pass').val() == $('#confirm_pass').val()) {
        //     $('#message').hide();
        // } else {
        //     $('#message').show();
           
        //   $('#message').html('Not Matching').css('color', 'red');
        // }
        // alert('btnSave val : ' + $(this).html());
        var myForm  =   $('#frmUsers');
        if(myForm.valid()) {
            var myReqType = '';

            if ($(this).html().trim() == 'Save') {
                myReqType = 'create';
            } else if ($(this).html().trim() == 'Update') {
                myReqType = 'update';
            }
            $('#hidReqType').val(myReqType);
            var userForm  =   $('#frmUsers')[0];
            var form  =   new FormData(userForm);
            var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForRegionsAction';
            // console.log('newURLLINK : ' + newURLLink);    +'&reqType=' + myReqType

            $.ajax({
                type: 'POST',
                url: newURLLink,
                dataType: 'json',
                processData: false,
                contentType: false,
                data: form,
                success: function (data) {
                    if (data.data == 'success') {
                        if($('#btnSave').val() == 'Save'){
                            swal({
                                title: 'Information',
                                text: 'Record Inserted successfully.',
                                type: 'success'
                            });
                        }else{
                            swal({
                                title: 'Information',
                                text: 'Record Updated successfully.',
                                type: 'success'
                            });
                        }
                        doClear();
                        $('#btnSave').html('Save');
                        preLoadData();
                    } else {
                        swal({
                            title: 'Warning',
                            text: 'Error in creating new record.',
                            type: 'warning'
                        });
                    }
                },
                failure: function (data) { }
            });
        }
    }));
}



function ajaxDonePageLoad(data, myProp) {
    //common For all the ajax Done process.. [ Search Filter, All, Default, Delete action etc...]

    if (data.success == 1) { //Success
        var x = new Date();
        curDate = x.toUTCString();// changing the display to UTC string
        console.log(' success date : ' + curDate);
        //$(light_8).unblock();
        $('#usersTable').show();
        //$('#spinner-light-8').hide();
        //Steps for filling up the contents from the json response.
        //STEP 2 : Data Table
        var table = $('#usersTable').DataTable({ //.datatable-button-html5-basic
            destroy: true,
            //processing    :   true,
            //serverSide    :   true,
            //deferLoading  :   57,
            aaData: data.writeJSONData,
            selectAllPages: false,
            columns: [
                { "data": "SNo" },
                { "data": "Country Code" },
                { "data": "Country Name" },
                { "data": "Status" },
                { "data": "Action" },
            ],
            columnDefs: [
                { targets: 'no-sort', orderable: false }
            ],
            buttons:{
                dom:{
                    button:{
                        classname:'btn btn-light'
                    }
            },
            buttons:[{
                text: 'Add Country',
                className:'btn btn-primary',
                action:function(){
                      var myParams = 'hidReqType=formList&hidRecID=0';
                var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForRegionsAction';
                console.log('newURLLINK : ' + newURLLink);
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: myParams,
                    success: function (data) {
                        //doClear();
                        if (data.data == 'success') {
                          doOpenSwal('create',data.formCont);
                        }
                    },
                    error: function (data) { },
                });


                }
            }],
        },
            select: 'single',
            style: 'os',
            order: [[0, 'asc']],
            bFilter: false,
            bDestroy: true,
        });
        //STEP 2 : Data Table
        $('#usersTable_length').hide();
        $('#usersTable_length select').addClass('form-control form-input-styled');
        var scrollableDiv = $("div#usersTable_wrapper .dataTables_scrollBody");
        var divTableRows = $(scrollableDiv).find("table tbody tr");

        if (divTableRows.length < 5) {
            $(scrollableDiv).css("overflow", ""); //For controlling the Actions popup..
        }

        $('div .dt-buttons').css('float', 'left');
    }
}

//Edit and Delete
function doDTableActions() {
    $('#usersTable').on('click', '.logAction', (function () {
        var myAttr = $(this).attr('id').split('-');
        $('#hidRecID').val(myAttr[1]);

        switch (myAttr[0]) {
            case 'edit':
                var myParams = 'hidReqType=formList&hidRecID=' + myAttr[1];
                var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForRegionsAction';
                console.log('newURLLINK : ' + newURLLink);
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: myParams,
                    success: function (data) {
                        //doClear();
                        if (data.data == 'success') {
                          doOpenSwal('update',data.formCont);
                        }
                    },
                    error: function (data) { },
                });

                break;

            case 'del':
                //Get confirmation b4 deleting..
                    //alert('Thanks for confirming');
                    var myParams = 'hidReqType=dele&hidRecID=' + myAttr[1];
                    var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForRegionsAction';
                    console.log('newURLLINK : ' + newURLLink + myParams);
                    // return;
                   swal ({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover the deleted record!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    showLoaderOnConfirm: true,
                    preConfirm: function (email) {
                        return new Promise(function (resolve) {
                    $.ajax({
                        type: 'POST',
                        url: newURLLink,
                        dataType: 'json',
                        data: myParams,
                    })
                        .done(function (data) {
                            // Handles successful responses only
                            if (data.data == 'success') {
                                resolve();
                                preLoadData();
                            } else {
                                alert('error in Deleting');
                            }
                        })
                        .fail(function (reason) {
                            // Handles errors only
                            console.log('Handles errors only ' + reason);
                        })
                        .always(function (data, textStatus, response) { })
                        .then(function (data, textStatus, response) { });
                    });
        },
    });
break;
        }
    }
    ))
}

function doOpenSwal(myAction,formCont){

    swal({
        html:   formCont,
        showCancelButton: true,
        width:'60%',
        confirmButtonText: myAction,
        
        preConfirm: function(email){
            return new Promise(function (resolve){
                var myParams    =   '&hidReqType='+myAction;
                var newURLLink = $('#hidProjectURL').val()  +   'Settings/Regions/ajaxResultsForRegionsAction';
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: $('#frmCommon').serialize()   + myParams,
                })
                .done(function (data) {
                    // Handles successful responses only
                    if (data.data == 'success') {
                        // swal({
                        //     title: 'Information',
                        //     text: 'Record Updated Successfully!',
                        //     type: 'success'
                        //    });
                        resolve();
                        preLoadData();
                    } else {
                        alert('error in Updating');
                    }
                })
            })
        }

    })
}



/////STATES/////



function preLoadDataStates() {
    $(light_8).block({
        message: '<i class="icon-spinner11 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });



    //LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
    var myParams = 'limitRowsFirstTime=100';
    var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForStatesFiltersJSONGeneration' ;
    
    
    $.ajax({
        type: 'POST',
        url: newURLLink,
        data: myParams,
        dataType: 'json',
    })
        .done(function (data) {
            // Handles successful responses only
            console.log('Handles successful responses only' + data);
            ajaxDonePageLoadStates(data);
        })
        .fail(function (reason) {
            // Handles errors only
            console.log('Handles errors only ' + reason);
        })
        .always(function (data, textStatus, response) { })
        .then(function (data, textStatus, response) { });

        var newURLLink  =   $('#hidProject').val() + 'Settings/Regions/checkFieldExistsStates';
        

        $('.checkField').on('blur', (function () {
            console.log('checkFiled ID : ' + $(this).attr('id'));
            var myAttrID    =   $(this).attr('id');
            if ($(this).val() != '') {

                // ajax controller 
                $.ajax({
                    type: 'POST',
                    url: newURLLink ,
                    data: {'name':myAttrID, 'value':$(this).val()},
                    dataType: 'json',
                    success: function (data) {
                        var myErrorAttrID    =   myAttrID + '_error';
                        console.log('myErrorAttrID' + myErrorAttrID);
                        if (data.data == 'success') {
                            $('#'+myErrorAttrID).html('');
                            $('#'+myErrorAttrID).hide();
                        } else {
                            var myErrorMsg  =   '';

                            if(myAttrID == 'username') {
                                myErrorMsg  =   'User Name';
                            } else if(myAttrID == 'email') {
                                myErrorMsg  =   'Email';
                            }
                            $('#'+myErrorAttrID).show();
                            $('#'+myErrorAttrID).html(myErrorMsg + ' Already Exists');
                        }
                    },
                    failure: function (data) { }
                })    
            }
        }));
    
}


function btnActionsStates() {
    $('#btnReset').on('click', (function (e) {
        $('.form-control').each(function(){
            $(this).val('');
        });
    }));

    $('#confirm_pass').on('blur', function () {
        if ($('#pass').val() == $('#confirm_pass').val()) {
            $('#btnSave').attr("disabled", false);
            $('#message').hide();
        } else {
            $('#message').show();
            $("#btnSave").attr("disabled", true);
          $('#message').html('Not Matching').css('color', 'red');
        }
      });

        $('#btnSave').on('click', (function (e) {
        console.log('btn Value : ' + $(this).html());
        e.preventDefault();
        // if ($('#pass').val() == $('#confirm_pass').val()) {
        //     $('#message').hide();
        // } else {
        //     $('#message').show();
           
        //   $('#message').html('Not Matching').css('color', 'red');
        // }
        // alert('btnSave val : ' + $(this).html());
        var myForm  =   $('#frmUsers');
        if(myForm.valid()) {
            var myReqType = '';

            if ($(this).html().trim() == 'Save') {
                myReqType = 'create';
            } else if ($(this).html().trim() == 'Update') {
                myReqType = 'update';
            }
            $('#hidReqType').val(myReqType);
            var userForm  =   $('#frmUsers')[0];
            var form  =   new FormData(userForm);
            var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForStatesAction';
            // console.log('newURLLINK : ' + newURLLink);    +'&reqType=' + myReqType

            $.ajax({
                type: 'POST',
                url: newURLLink,
                dataType: 'json',
                processData: false,
                contentType: false,
                data: form,
                success: function (data) {
                    if (data.data == 'success') {
                        if($('#btnSave').val() == 'Save'){
                            swal({
                                title: 'Information',
                                text: 'Record Inserted successfully.',
                                type: 'success'
                            });
                        }else{
                            swal({
                                title: 'Information',
                                text: 'Record Updated successfully.',
                                type: 'success'
                            });
                        }
                        doClear();
                        $('#btnSave').html('Save');
                        preLoadDataStates();
                    } else {
                        swal({
                            title: 'Warning',
                            text: 'Error in creating new record.',
                            type: 'warning'
                        });
                    }
                },
                failure: function (data) { }
            });
        }
    }));
}








function ajaxDonePageLoadStates(data, myProp) {
    //common For all the ajax Done process.. [ Search Filter, All, Default, Delete action etc...]

    if (data.success == 1) { //Success
        var x = new Date();
        curDate = x.toUTCString();// changing the display to UTC string
        console.log(' success date : ' + curDate);
        //$(light_8).unblock();
        $('#statesTable').show();
        //$('#spinner-light-8').hide();
        //Steps for filling up the contents from the json response.
        //STEP 2 : Data Table
        var table = $('#statesTable').DataTable({ //.datatable-button-html5-basic
            destroy: true,
            //processing    :   true,
            //serverSide    :   true,
            //deferLoading  :   57,
            aaData: data.writeJSONData,
            selectAllPages: false,
            columns: [
                { "data": "SNo" },
                { "data": "State Code" },
                { "data": "State Name" },
                { "data": "Status" },
                { "data": "Action" },
            ],
            columnDefs: [
                { targets: 'no-sort', orderable: false }
            ],
            buttons:{
                dom:{
                    button:{
                        classname:'btn btn-light'
                    }
            },
            buttons:[{
                text: 'Add State',
                className:'btn btn-primary',
                action:function(){
                    console.log("Inside ADD Function");
                var myParams = 'hidReqType=formList&hidRecID=0';
                var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForStatesAction';
                console.log('newURLLINK : ' + newURLLink);
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: myParams,
                    success: function (data) {
                        //doClear();
                        if (data.data == 'success') {
                          doOpenSwalStates('create',data.formCont);
                        }
                    },
                    error: function (data) { },
                });


                }
            }],
        },
            select: 'single',
            style: 'os',
            order: [[0, 'asc']],
            bFilter: false,
            bDestroy: true,
        });
        //STEP 2 : Data Table
        $('#statesTable_length').hide();
        $('#statesTable_length select').addClass('form-control form-input-styled');
        var scrollableDiv = $("div#usersTable_wrapper .dataTables_scrollBody");
        var divTableRows = $(scrollableDiv).find("table tbody tr");

        if (divTableRows.length < 5) {
            $(scrollableDiv).css("overflow", ""); //For controlling the Actions popup..
        }

        $('div .dt-buttons').css('float', 'left');
    }
}





function doDTableActionsStates() {
    $('#statesTable').on('click', '.logAction', (function () {
        var myAttr = $(this).attr('id').split('-');
        $('#hidRecID').val(myAttr[1]);

        switch (myAttr[0]) {
            case 'edit':
                var myParams = 'hidReqType=formList&hidRecID=' + myAttr[1];
                var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForStatesAction';
                console.log('newURLLINK : ' + newURLLink);
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: myParams,
                    success: function (data) {
                        //doClear();
                        if (data.data == 'success') {
                          doOpenSwalStates('update',data.formCont);
                        }
                    },
                    error: function (data) { },
                });

                break;

            case 'del':
                //Get confirmation b4 deleting..
                    //alert('Thanks for confirming');
                    var myParams = 'hidReqType=dele&hidRecID=' + myAttr[1];
                    var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForStatesAction';
                    console.log('newURLLINK : ' + newURLLink + myParams);
                    // return;
                   swal ({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover the deleted record!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    showLoaderOnConfirm: true,
                    preConfirm: function (email) {
                        return new Promise(function (resolve) {
                    $.ajax({
                        type: 'POST',
                        url: newURLLink,
                        dataType: 'json',
                        data: myParams,
                    })
                        .done(function (data) {
                            // Handles successful responses only
                            if (data.data == 'success') {
                                resolve();
                                preLoadDataStates();
                            } else {
                                alert('error in Deleting');
                            }
                        })
                        .fail(function (reason) {
                            // Handles errors only
                            console.log('Handles errors only ' + reason);
                        })
                        .always(function (data, textStatus, response) { })
                        .then(function (data, textStatus, response) { });
                    });
        },
    });
break;
        }
    }
    ))
}

function doOpenSwalStates(myAction,formCont){

    swal({
        html:   formCont,
        showCancelButton: true,
        width:'60%',
        confirmButtonText: myAction,
        
        preConfirm: function(email){
            return new Promise(function (resolve){
                var myParams    =   '&hidReqType='+myAction;
                var newURLLink = $('#hidProjectURL').val()  +   'Settings/Regions/ajaxResultsForStatesAction';
                console.log('HHH'+newURLLink);
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: $('#frmCommonS').serialize()   + myParams,
                })
                .done(function (data) {
                    // Handles successful responses only
                    if (data.data == 'success') {
                        // swal({
                        //     title: 'Information',
                        //     text: 'Record Updated Successfully!',
                        //     type: 'success'
                        //    });
                        resolve();
                        preLoadDataStates();
                    } else {
                        alert('error in Updating');
                    }
                })
            })
        }

    })
}



/////CITIES/////


function preLoadDataCities() {
    $(light_8).block({
        message: '<i class="icon-spinner11 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });



    //LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
    var myParams = 'limitRowsFirstTime=100';
    var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForCitiesFiltersJSONGeneration' ;
    
    
    $.ajax({
        type: 'POST',
        url: newURLLink,
        data: myParams,
        dataType: 'json',
    })
        .done(function (data) {
            // Handles successful responses only
            console.log('Handles successful responses only' + data);
            ajaxDonePageLoadCities(data);
        })
        .fail(function (reason) {
            // Handles errors only
            console.log('Handles errors only ' + reason);
        })
        .always(function (data, textStatus, response) { })
        .then(function (data, textStatus, response) { });

        var newURLLink  =   $('#hidProject').val() + 'Settings/Regions/checkFieldExistsCities';
        

        $('.checkField').on('blur', (function () {
            console.log('checkFiled ID : ' + $(this).attr('id'));
            var myAttrID    =   $(this).attr('id');
            if ($(this).val() != '') {

                // ajax controller 
                $.ajax({
                    type: 'POST',
                    url: newURLLink ,
                    data: {'name':myAttrID, 'value':$(this).val()},
                    dataType: 'json',
                    success: function (data) {
                        var myErrorAttrID    =   myAttrID + '_error';
                        console.log('myErrorAttrID' + myErrorAttrID);
                        if (data.data == 'success') {
                            $('#'+myErrorAttrID).html('');
                            $('#'+myErrorAttrID).hide();
                        } else {
                            var myErrorMsg  =   '';

                            if(myAttrID == 'username') {
                                myErrorMsg  =   'User Name';
                            } else if(myAttrID == 'email') {
                                myErrorMsg  =   'Email';
                            }
                            $('#'+myErrorAttrID).show();
                            $('#'+myErrorAttrID).html(myErrorMsg + ' Already Exists');
                        }
                    },
                    failure: function (data) { }
                })    
            }
        }));
    
}


function btnActionsCities() {
    $('#btnReset').on('click', (function (e) {
        $('.form-control').each(function(){
            $(this).val('');
        });
    }));

    $('#confirm_pass').on('blur', function () {
        if ($('#pass').val() == $('#confirm_pass').val()) {
            $('#btnSave').attr("disabled", false);
            $('#message').hide();
        } else {
            $('#message').show();
            $("#btnSave").attr("disabled", true);
          $('#message').html('Not Matching').css('color', 'red');
        }
      });

        $('#btnSave').on('click', (function (e) {
        console.log('btn Value : ' + $(this).html());
        e.preventDefault();
        // if ($('#pass').val() == $('#confirm_pass').val()) {
        //     $('#message').hide();
        // } else {
        //     $('#message').show();
           
        //   $('#message').html('Not Matching').css('color', 'red');
        // }
        // alert('btnSave val : ' + $(this).html());
        var myForm  =   $('#frmUsers');
        if(myForm.valid()) {
            var myReqType = '';

            if ($(this).html().trim() == 'Save') {
                myReqType = 'create';
            } else if ($(this).html().trim() == 'Update') {
                myReqType = 'update';
            }
            $('#hidReqType').val(myReqType);
            var userForm  =   $('#frmUsers')[0];
            var form  =   new FormData(userForm);
            var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForCitiesAction';
            // console.log('newURLLINK : ' + newURLLink);    +'&reqType=' + myReqType

            $.ajax({
                type: 'POST',
                url: newURLLink,
                dataType: 'json',
                processData: false,
                contentType: false,
                data: form,
                success: function (data) {
                    if (data.data == 'success') {
                        if($('#btnSave').val() == 'Save'){
                            swal({
                                title: 'Information',
                                text: 'Record Inserted successfully.',
                                type: 'success'
                            });
                        }else{
                            swal({
                                title: 'Information',
                                text: 'Record Updated successfully.',
                                type: 'success'
                            });
                        }
                        doClear();
                        $('#btnSave').html('Save');
                        preLoadDataCities();
                    } else {
                        swal({
                            title: 'Warning',
                            text: 'Error in creating new record.',
                            type: 'warning'
                        });
                    }
                },
                failure: function (data) { }
            });
        }
    }));
}








function ajaxDonePageLoadCities(data, myProp) {
    //common For all the ajax Done process.. [ Search Filter, All, Default, Delete action etc...]

    if (data.success == 1) { //Success
        var x = new Date();
        curDate = x.toUTCString();// changing the display to UTC string
        console.log(' success date : ' + curDate);
        //$(light_8).unblock();
        $('#citiesTable').show();
        //$('#spinner-light-8').hide();
        //Steps for filling up the contents from the json response.
        //STEP 2 : Data Table
        var table = $('#citiesTable').DataTable({ //.datatable-button-html5-basic
            destroy: true,
            //processing    :   true,
            //serverSide    :   true,
            //deferLoading  :   57,
            aaData: data.writeJSONData,
            selectAllPages: false,
            columns: [
                { "data": "SNo" },
                { "data": "City Code" },
                { "data": "City Name" },
                { "data": "Status" },
                { "data": "Action" },
            ],
            columnDefs: [
                { targets: 'no-sort', orderable: false }
            ],
            buttons:{
                dom:{
                    button:{
                        classname:'btn btn-light'
                    }
            },
            buttons:[{
                text: 'Add City',
                className:'btn btn-primary',
                action:function(){
                    console.log("Inside ADD Function");
                var myParams = 'hidReqType=formList&hidRecID=0';
                var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForCitiesAction';
                console.log('newURLLINK : ' + newURLLink);
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: myParams,
                    success: function (data) {
                        //doClear();
                        if (data.data == 'success') {
                          doOpenSwalCities('create',data.formCont);
                        }
                    },
                    error: function (data) { },
                });


                }
            }],
        },
            select: 'single',
            style: 'os',
            order: [[0, 'asc']],
            bFilter: false,
            bDestroy: true,
        });
        //STEP 2 : Data Table
        $('#citiesTable_length').hide();
        $('#citiesTable_length select').addClass('form-control form-input-styled');
        var scrollableDiv = $("div#usersTable_wrapper .dataTables_scrollBody");
        var divTableRows = $(scrollableDiv).find("table tbody tr");

        if (divTableRows.length < 5) {
            $(scrollableDiv).css("overflow", ""); //For controlling the Actions popup..
        }

        $('div .dt-buttons').css('float', 'left');
    }
}





function doDTableActionsCities() {
    $('#citiesTable').on('click', '.logAction', (function () {
        var myAttr = $(this).attr('id').split('-');
        $('#hidRecID').val(myAttr[1]);

        switch (myAttr[0]) {
            case 'edit':
                var myParams = 'hidReqType=formList&hidRecID=' + myAttr[1];
                var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForCitiesAction';
                console.log('newURLLINK : ' + newURLLink);
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: myParams,
                    success: function (data) {
                        //doClear();
                        if (data.data == 'success') {
                          doOpenSwalCities('update',data.formCont);
                        }
                    },
                    error: function (data) { },
                });

                break;

            case 'del':
                //Get confirmation b4 deleting..
                    //alert('Thanks for confirming');
                    var myParams = 'hidReqType=dele&hidRecID=' + myAttr[1];
                    var newURLLink = $('#hidProjectURL').val() + 'Settings/Regions/ajaxResultsForCitiesAction';
                    console.log('newURLLINK : ' + newURLLink + myParams);
                    // return;
                   swal ({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover the deleted record!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    showLoaderOnConfirm: true,
                    preConfirm: function (email) {
                        return new Promise(function (resolve) {
                    $.ajax({
                        type: 'POST',
                        url: newURLLink,
                        dataType: 'json',
                        data: myParams,
                    })
                        .done(function (data) {
                            // Handles successful responses only
                            if (data.data == 'success') {
                                resolve();
                                preLoadDataCities();
                            } else {
                                alert('error in Deleting');
                            }
                        })
                        .fail(function (reason) {
                            // Handles errors only
                            console.log('Handles errors only ' + reason);
                        })
                        .always(function (data, textStatus, response) { })
                        .then(function (data, textStatus, response) { });
                    });
        },
    });
break;
        }
    }
    ))
}

function doOpenSwalCities(myAction,formCont){

    swal({
        html:   formCont,
        showCancelButton: true,
        width:'60%',
        confirmButtonText: myAction,
        
        preConfirm: function(email){
            return new Promise(function (resolve){
                var myParams    =   '&hidReqType='+myAction;
                var newURLLink = $('#hidProjectURL').val()  +   'Settings/Regions/ajaxResultsForCitiesAction';
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: $('#frmCommonC').serialize()   + myParams,
                })
                .done(function (data) {
                    // Handles successful responses only
                    if (data.data == 'success') {
                        // swal({
                        //     title: 'Information',
                        //     text: 'Record Updated Successfully!',
                        //     type: 'success'
                        //    });
                        resolve();
                        preLoadDataCities();
                    } else {
                        alert('error in Updating');
                    }
                })
            })
        }

    })
}

















