var light_8 = $('#spinner-light-8').closest('.card');
(function () {
    "use strict";
    preLoadData();
    settingsData();
    settingsAdd();
    doDTableActions();
})(jQuery);

function preLoadData() {
    $(light_8).block({
        message: '<i class="icon-spinner11 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });

    //LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
    var myParams = 'limitRowsFirstTime=100';
    var newURLLink = $('#hidProjectURL').val() + 'Settings/ajaxResultsForFiltersJSONGeneration';

    $.ajax({
        type: 'POST',
        url: newURLLink,
        data: myParams,
        dataType: 'json',
    })
        .done(function (data) {
            // Handles successful responses only
            console.log('Handles successful responses only' + data);
            ajaxDonePageLoad(data);
        })
        .fail(function (reason) {
            // Handles errors only
            console.log('Handles errors only ' + reason);
        })
        .always(function (data, textStatus, response) { })
        .then(function (data, textStatus, response) { });
}

function doClear() {
    $('#btnSave').html('<i class="icon-spinner4 mr-2"></i> Submit');
}

function settingsData() {
    $('#title').on('blur', (function () {
        if ($(this).val() == '') {
            //alert('User Name should not be blank');
            // $('#txtUserName').attr('autofocus', true);
            return false;
        } else {
            $('#title').focus();
        }
    }));

    $('#key').on('blur', (function () {
        if ($(this).val() == '') {
            return false;
        } else {
            $('#key').focus();
        }
    }));

    $('#description').on('blur', (function () {
        if ($(this).val() == '') {
            return false;
        }
        else {
            $('description').focus();
        }
    }));
    // $('#settingsData').on('blur', (function () {
    //     if ($(this).val() == '') {
    //         return false;
    //     }
    // }));
}
function settingsAdd() {
    $('#btnSave').on('click', (function (e) {
        console.log('btn Value : ' + $(this).html());
        // alert('click btn ? ');
        e.preventDefault();
        // alert('btnSave val : ' + $(this).html());
        var myReqType = '';

        if ($(this).html().trim() == 'SAVE') {
            myReqType = 'create';
        } else if ($(this).html().trim() == 'UPDATE') {
            myReqType = 'update';
        }

        if (($('#title').val() != '') && ($('#key').val() != '')
            && ($('#description').val() != '') && ($('#modalForm').modal() != '')) {
            var newURLLink = $('#hidProjectURL').val() + 'Settings/ajaxResultsForSettingsAction';
            // console.log('newURLLINK : ' + newURLLink);

            $.ajax({
                type: 'POST',
                url: newURLLink,
                dataType: 'json',
                data: $('#settings').serialize() + '&reqType=' + myReqType,
                success: function (data) {
                    if (data.data == 'success') {
                        // initialText = btn.data('initial-text');
                        // btn.html(initialText).removeClass('disabled');
                        // swal({
                        //     title: 'Information',
                        //     text: 'Record inserted successfully.',
                        //     type: 'info'
                        // });
                        // doClear();
                        $("#modalForm").modal('hide');
                        preLoadData();
                    } else {
                        // swal({
                        //     title: 'Warning',
                        //     text: 'Error in creating new record.',
                        //     type: 'warning'
                        // });
                        alert(data.data);
                    }
                },
                failure: function (data) { }
            });
        }

    }));
}

function ajaxDonePageLoad(data, myProp) {
    //common For all the ajax Done process.. [ Search Filter, All, Default, Delete action etc...]

    if (data.success == 1) { //Success
        var x = new Date();
        curDate = x.toUTCString();// changing the display to UTC string
        console.log(' success date : ' + curDate);
        //$(light_8).unblock();
        $('#clientsTable').show();
        //$('#spinner-light-8').hide();
        //Steps for filling up the contents from the json response.
        //STEP 2 : Data Table
        var table = $('#clientsTable').DataTable({ //.datatable-button-html5-basic
            destroy: true,
            //processing	:	true,
            //serverSide	:	true,
            //deferLoading	:	57,
            aaData: data.writeJSONData,
            selectAllPages: false,
            columns: [
                { "data": "SNo" },
                { "data": "Title" },
                { "data": "Key" },
                { "data": "Description" },
                { "data": "Action" },
            ],
            columnDefs: [
                { targets: 'no-sort', orderable: false }
            ],
            select: 'single',
            style: 'os',
            order: [[1, 'asc']],
            bFilter: false,
            bDestroy: true,
        });
        //STEP 2 : Data Table
        $('#clientsTable_length').hide();
        $('#clientsTable_length select').addClass('form-control form-input-styled');
        var scrollableDiv = $("div#clientsTable_wrapper .dataTables_scrollBody");
        var divTableRows = $(scrollableDiv).find("table tbody tr");

        if (divTableRows.length < 5) {
            $(scrollableDiv).css("overflow", ""); //For controlling the Actions popup..
        }

        $('div .dt-buttons').css('float', 'left');
    }
}

function doDTableActions() {
    $('#clientsTable').on('click', '.logAction', (function () {
        var myAttr = $(this).attr('id').split('-');
        $('#hidRecID').val(myAttr[1]);
        alert('$hideRecID');

        switch (myAttr[0]) {

            case 'edit':
                var myParams = '&reqType=edit&hideRecID=' + $('#hidRecID').val();
                if ($('#modalForm').modal() != '');
                var newURLLink = $('#hidProjectURL').val() + 'Settings/ajaxResultsForSettingsAction';
                console.log('newURLLINK : ' + newURLLink);
                $('#btnSave').html('UPDATE');

                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: $('#settings').serialize() + myParams,
                    success: function (data) {
                        //doClear();
                        if (data.data == 'success') {
                            //alert('data client ' + data.clientRecord.client_first_name);
                            $.each(data.clientRecord, (function (index, value) {
                                console.log(' index : ' + index + ' value : ' + value);
                                if (index == 'title') {
                                    $('#title').val(value);
                                }
                                if (index == 'key') {
                                    $('#key').val(value);
                                }
                                if (index == 'description') {
                                    $('#description').val(value);
                                }
                            }));
                            //clientAdd();
                        }
                    },
                    error: function (data) { },
                });

                break;

            case 'update':
                var myParams = "reqType=update&recId=" + myAttr[1];
                if ($('#modalForm').modal() != '');
                var newURLLink = $('#hidProjectURL').val() + 'Settings/ajaxResultsForSettingsAction';
                console.log('newURLLINK : ' + newURLLink);

                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: $('#settings').serialize() + myParams,
                    success: function (data) {
                        if (data.data == 'success') {
                            $("#modalForm").modal('hide');
                            preLoadData();
                        } else {
                            alert(data.data);
                        }
                    },
                    failure: function (data) { }
                });

                break;

        }
    }
    ))
}