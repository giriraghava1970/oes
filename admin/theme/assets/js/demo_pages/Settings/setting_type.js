var light_8 = $('#spinner-light-8').closest('.card');
(function () {
    "use strict";
    preLoadData();
    settingsTypeData();
    settingsTypeAdd();
    doReset();
    doDTableActions();
    doNavTabActions();
})(jQuery);

function preLoadData() {
    $(light_8).block({
        message: '<i class="icon-spinner11 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });

    //LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
    var myParams = 'limitRowsFirstTime=100&hidSettingTypeId=1'; // Default to 1
    var newURLLink = $('#hidProjectURL').val() + 'Settings/SystemSettings/ajaxResultsForFiltersJSONGeneration';

    $.ajax({
        type: 'POST',
        url: newURLLink,
        data: myParams,
        dataType: 'json',
    })
        .done(function (data) {
            // Handles successful responses only
            console.log('Handles successful responses only' + data);
            ajaxDonePageLoad(data);
        })
        .fail(function (reason) {
            // Handles errors only
            console.log('Handles errors only ' + reason);
        })
        .always(function (data, textStatus, response) { })
        .then(function (data, textStatus, response) { });
}


function doLoadFormData() {
    $('#settingTypesId').on('change', (function () {
        if ($(this).val() != '') {
            var myParams = 'limitRowsFirstTime=100&settingTypesId=' + $(this).val();
            var newURLLink = $('#hidProjectURL').val() + 'Common/ajaxResultsForSubSettingTypeDropBox';

            $.ajax({
                type: 'POST',
                url: newURLLink,
                data: myParams,
                dataType: 'json',
            })
                .done(function (data) {
                    // Handles successful responses only
                    console.log('Handles successful responses only' + data);
                    if (data.data == 'success') {
                        $('#settingsSubTypesId').empty();
                        var list = $('#settingsSubTypesId');
                        list.append('<option value="">--Select State -- </option>');

                        $.each(data.settingSubTypeId, function (index, value) {
                            list.append('<option value="' + value.id + '">' + value.setting_sub_type_name + '</option>');
                        });
                    }
                })
                .fail(function (reason) {
                    // Handles errors only
                    console.log('Handles errors only ' + reason);
                })
                .always(function (data, textStatus, response) { })
                .then(function (data, textStatus, response) { });
        }
    }));
};

function doNavTabActions() {
    $('.navbar-nav-link').on('click', (function (e) {
        e.preventDefault();
        settingAction($(this).attr('id'));
    }));
}

function settingAction(settingTypeId) {
    var mySettingID = settingTypeId.split('-');
    var myParams = 'limitRowsFirstTime=100' + '&hidSettingTypeId=' + mySettingID[2];
    var newURLLink = $('#hidProjectURL').val() + 'Settings/SystemSettings/ajaxResultsForFiltersJSONGeneration';

    $.ajax({
        type: 'POST',
        url: newURLLink,
        data: myParams,
        dataType: 'json',
    })
        .done(function (data) {
            // Handles successful responses only
            console.log('Handles successful responses only' + data);
            ajaxDonePageLoad(data);
        })
        .fail(function (reason) {
            // Handles errors only
            console.log('Handles errors only ' + reason);
        })
        .always(function (data, textStatus, response) { })
        .then(function (data, textStatus, response) { });
}


function doClear() {
    $('.form-control').each(function () {
        $(this).val('');
    });
}

function doReset() {
    $('#btnReset').on('click', (function (e) {
        console.log('btn Value : ' + $(this).html());
        e.preventDefault();
        $('.form-control').each(function () {
            $(this).val('');
        });
    }));
}


function settingsTypeData() {
    $('#settingName').on('blur', (function () {
        if ($(this).val() == '') {
            // alert('User Name should not be blank');
            // $('#txtUserName').attr('autofocus', true);
            return false;
        } else {
            $('#settingName').focus();
        }
    }));

    $('#settingValue').on('blur', (function () {
        if ($(this).val() == '') {
            return false;
        } else {
            $('#settingValue').focus();
        }
    }));
}

function settingsTypeAdd() {
    $('#btnSave').on('click', (function (e) {
        console.log('btn Value : ' + $(this).html());
        // alert('click btn ? ');
        e.preventDefault();
        // alert('btnSave val : ' + $(this).html());
        var myForm = $('#formCom');
        if (myForm.valid()) {
            var myReqType = '';

            if ($(this).html().trim() == 'SAVE') {
                myReqType = 'create';
            } else if ($(this).html().trim() == 'UPDATE') {
                myReqType = 'update';
            }

            if (($('#settingValue').val() != '') && ($('#settingValue').val() != '') && ($('#createdAt').val() != '')
                && ($('#createdBy').val() != '') && ($('#updatedAt').val() != '') && ($('#formCom').modal() != '')) {
                var newURLLink = $('#hidProjectURL').val() + 'Settings/SystemSettings/ajaxResultsForSettingsAction';
                console.log('newURLLINK : ' + newURLLink);

                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: $('#formCom').serialize() + '&reqType=' + myReqType,
                    success: function (data) {
                        if (data.data == 'success') {
                            // initialText = btn.data('initial-text');
                            // btn.html(initialText).removeClass('disabled');
                            // swal({
                            //     title: 'Information',
                            //     text: 'Record inserted successfully.',
                            //     type: 'info'
                            // });
                            doClear();
                            $("#formCom").modal('hide');
                            preLoadData();
                        } else {
                            // swal({
                            //     title: 'Warning',
                            //     text: 'Error in creating new record.',
                            //     type: 'warning'
                            // });
                            // alert(data.data);
                        }
                    },
                    failure: function (data) { }
                });
            }
        }
    }));
}

function ajaxDonePageLoad(data, myProp) {
    //common For all the ajax Done process.. [ Search Filter, All, Default, Delete action etc...]

    if (data.success == 1) { //Success
        var x = new Date();
        curDate = x.toUTCString();// changing the display to UTC string
        console.log(' success date : ' + curDate);
        //$(light_8).unblock();
        $('#clientsTable').show();
        //$('#spinner-light-8').hide();
        //Steps for filling up the contents from the json response.
        //STEP 2 : Data Table
        var table = $('#clientsTable').DataTable({ //.datatable-button-html5-basic
            destroy: true,
            //processing	:	true,
            //serverSide	:	true,
            //deferLoading	:	57,
            aaData: data.writeJSONData,
            selectAllPages: false,
            columns: [
                { "data": "SNo" },
                { "data": "Setting Type ID" },
                { "data": "Setting Sub Type ID" },
                { "data": "Setting Name" },
                { "data": "Setting Value" },
                { "data": "Action" },
            ],
            columnDefs: [
                { targets: 'no-sort', orderable: false }
            ],
            buttons: {
                dom: {
                    button: {
                        className: 'btn btn-light'
                    }
                },
                buttons: [
                    {
                        text: 'Create Account Overview',
                        className: 'btn bg-primary',
                        action: function () {
                            var myParams = '&reqType=formList&hidRecID=0';
                            var newURLLink = $('#hidProjectURL').val() + 'Settings/SystemSettings/ajaxResultsForSettingsAction';
                            console.log('newURLLINK : ' + newURLLink);

                            $.ajax({
                                type: 'POST',
                                url: newURLLink,
                                data: myParams,
                                dataType: 'json',
                            })
                                .done(function (data) {
                                    if (data.data == 'success') {
                                        doOpenSwal('create', data.formContent);
                                    }
                                })
                                .fail(function (reason) {
                                    // Handles errors only
                                    console.log('Handles errors only ' + reason);
                                })
                                .always(function (data, textStatus, response) { })
                                .then(function (data, textStatus, response) { });

                        }
                    },
                    {
                        extend: 'copyHtml5',
                        className: 'btn bg-grey',
                        title: $('#hidAppTitle').val() + ' - Academics Listing',
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        className: 'btn bg-grey',
                        title: $('#hidAppTitle').val() + ' - Academics Listing',
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    },
                    {
                        extend: 'csvHtml5',
                        className: 'btn bg-grey',
                        title: $('#hidAppTitle').val() + ' - Academics Listing',
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        className: 'btn bg-grey',
                        title: $('#hidAppTitle').val() + ' - Academics Listing',
                        exportOptions: {
                            columns: "thead th:not(.noExport)"
                        }
                    },
                ]
            },
            select: 'single',
            style: 'os',
            order: [[1, 'asc']],
            bFilter: false,
            bDestroy: true,
        });
        //STEP 2 : Data Table
        $('#clientsTable_length').hide();
        $('#clientsTable_length select').addClass('form-control form-input-styled');
        var scrollableDiv = $("div#clientsTable_wrapper .dataTables_scrollBody");
        var divTableRows = $(scrollableDiv).find("table tbody tr");

        if (divTableRows.length < 5) {
            $(scrollableDiv).css("overflow", ""); //For controlling the Actions popup..
        }

        $('div .dt-buttons').css('float', 'left');
    }
}


function doOpenSwal(myAction, formContent) {
    swal({
        title: myAction.toUpperCase() + ' ACCOUNT OVERVIEW',
        html: formContent,
        showCancelButton: true,
        width: '60%',
        confirmButtonText: myAction.toUpperCase(),
        showLoaderOnConfirm: true,
        allowOutsideClick: false,
        onOpen: function () {
            if (myAction == 'create') {
                doLoadFormData();
            }
        },
        preConfirm: function (email) {
            return new Promise(function (resolve) {
                var form = $('#formCom');

                if (form.valid()) {
                    var myParams = '&reqType=' + myAction;
                    var newURLLink = $('#hidProjectURL').val() + 'Settings/SystemSettings/ajaxResultsForSettingsAction';
                    console.log('newURLLINK : ' + newURLLink);

                    $.ajax({
                        type: 'POST',
                        url: newURLLink,
                        data: $('#formCom').serialize() + myParams,
                        dataType: 'json',
                    })
                        .done(function (data) {
                            if (data.data == 'success') {
                                resolve();
                                preLoadData();
                            }
                        })
                        .fail(function (reason) {
                            // Handles errors only
                            console.log('Handles errors only ' + reason);
                        })
                        .always(function (data, textStatus, response) { })
                        .then(function (data, textStatus, response) { });
                } else {
                    $('.btn-primary').prop('disabled', false);
                    $('.swal2-cancel').prop('disabled', false);
                    $('.btn-primary').removeAttr('style');
                    $('.swal2-actions').removeClass('swal2-loading');
                    $('#instituteName').focus();
                }
            });
        }
    });
}

function doDTableActions() {
    $('#clientsTable').on('click', '.logAction', (function () {
        var myAttr = $(this).attr('id').split('-');
        console.log('myAttr : ' + myAttr[0] + ' 1 : ' + myAttr[1] + ' 2 : ' + myAttr[2]);
        $('#hidRecID').val(myAttr[1]);

        switch (myAttr[0]) {

            case 'edit':
                var myParams = '&reqType=formList&hidRecID=' + myAttr[1] + '&hidSettingTypeId=' + myAttr[2];
                var newURLLink = $('#hidProjectURL').val() + 'Settings/SystemSettings/ajaxResultsForSettingsAction';
                console.log('newURLLINK : ' + newURLLink);

                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: myParams,
                })
                    .done(function (data) {
                        if (data.data == 'success') {
                            doOpenSwal('update', data.formContent);
                        }
                    })
                    .fail(function (reason) {
                        // Handles errors only
                        console.log('Handles errors only ' + reason);
                    })
                    .always(function (data, textStatus, response) { })
                    .then(function (data, textStatus, response) { });
                break;

            case 'del':
                var myParams = 'reqType=del&hidRecID=' + myAttr[1];
                var newURLLink = $('#hidProjectURL').val() + 'Settings/SystemSettings/ajaxResultsForSettingsAction';

                swal({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover the deleted record!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    showLoaderOnConfirm: true,
                    preConfirm: function (email) {
                        return new Promise(function (resolve) {
                            $.ajax({
                                type: 'POST',
                                url: newURLLink,
                                data: myParams,
                                dataType: 'json',
                            })
                                .done(function (data) {
                                    // Handles successful responses only
                                    if (data.data == 'success') {
                                        swal({
                                            title: 'Information',
                                            text: 'Record deleted successfully.',
                                            type: 'info'
                                        });

                                        // doClear();
                                        preLoadData();
                                    } else {
                                        alert('error in deleting');
                                    }
                                })
                                .fail(function (reason) {
                                    // Handles errors only
                                    console.log('Handles errors only ' + reason);
                                })
                                .always(function (data, textStatus, response) { })
                                .then(function (data, textStatus, response) { });
                        });
                    },
                });

                break;

        }
    }
    ))
}