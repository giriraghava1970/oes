	var light_prevData	=	$('#spinner-light-prevData').closest('.card');
	var myDataFiles	=	[];
	( function() {
	    "use strict";
		studentPrevLoadData();
		doPreviousDataBtnActions();
		doPrevDataActions();
		doShowProfileTabData();
		topNavLinks();
		doUploadPhoto();
		photoReload();

		if($('#hidFromWhere').val() == 'studView') { //for doc upload section
			$('#showProfile').hide();
			$('#showCourses').hide();
			$('#showAssessments').hide();
			$('#showAttendance').hide();
			$('#showDocuments').show();
			$('#showElectives').hide();
			$('#showAchievements').hide();
			$('#showLogs').hide();
			$('#showPrevData').hide();
		} else if($('#hidFromWhere').val() == 'achieve') { //for doc upload section
			$('#showProfile').hide();
			$('#showCourses').hide();
			$('#showAssessments').hide();
			$('#showAttendance').hide();
			$('#showDocuments').hide();
			$('#showElectives').hide();
			$('#showAchievements').show();
			$('#showLogs').hide();
			$('#showPrevData').hide();
		} else {
			$('#showProfile').show(); //Default is profile update..
			$('#showCourses').hide();
			$('#showAssessments').hide();
			$('#showAttendance').hide();
			$('#showDocuments').hide();
			$('#showElectives').hide();
			$('#showAchievements').hide();
			$('#showLogs').hide();
			$('#showPrevData').hide();
		}
	} )(jQuery);

	function topNavLinks(){
		$('.exportOption').on('click', (function(){
			if($(this).attr('id') == 'pdfExport') {
				$('#frmView').attr('target', '_new');
			}

			$('#frmView').attr('action', $(this).attr('data-target'));
			$('#frmView').submit();
		}));
	}

	function photoReload() {
		//LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
		var myParams	=	'limitRowsFirstTime=100&hidStudentID='+$('#hidStudentID').val();
		var newURLLink	=	'index.php?r=students/students/ajaxResultsForProfilePhotoUploadDisplay';

		$.ajax( {
	    	type	:	'POST',
	        url : newURLLink,
	        data : myParams,
	        dataType	: 'json',
	    } )
	        .done( function( data ) {
	        	if(data.data == 'success') {
	        		//$('#profilePhoto').empty();
	        		//$('#profilePhoto').html('<img id="profilePhoto" name="profilePhoto" src="'+data.destinationFile+'" alt="" title="">');
	        		$('#profilePhoto').removeAttr('src').attr('src', data.destinationFile);
	        	}
	        })
	        .fail( function( reason ) {
		            // Handles errors only
		        	console.log('Handles errors only ' + reason);
		        } )
		        .always( function( data, textStatus, response ) {} )
		        .then( function( data, textStatus, response ) {} );
	}

	function doUploadPhoto() {
		// fetch the existing photo file name for the student.
		// Upload New Photo Work Flow.
		$('#uploadProfilePhoto').on('click', (function(){
			//ajaxResultsForProfilePhotoUploadPOP
			//LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
			var myParams	=	'limitRowsFirstTime=100&hidStudentID='+$('#hidStudentID').val();
			var newURLLink	=	'index.php?r=students/students/ajaxResultsForProfilePhotoUploadPOP';

			$.ajax( {
		    	type	:	'POST',
		        url : newURLLink,
		        data : myParams,
		        dataType	: 'json',
		    } )
		        .done( function( data ) {
		        	if(data.success == 1) {
			            // Handles successful responses only
			        	console.log('Handles successful responses only' + data);
			        	swal({
					        title: '<i> Upload Student Profile Photo</i>',
					        html: data.formCont,
					        confirmButtonText: 'Upload',
					        showLoaderOnConfirm: true,
					        showCancelButton: true,
					        allowOutsideClick: false,
					        onOpen: function () {
					        	// Initialize
				                $('.form-input-styled').uniform({
				                    fileButtonClass: 'action btn bg-blue'
				                });
					        	$("input[type='file']").on("change", function () {
					    			// get the extension and if the extension doesnt meet the specified throw error
					    			var ext	=	$(this).val().split('.').pop().toLowerCase();

					    			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
					    				$('#response').html('<label id="photoData-error" class="validation-invalid-label" for="photoData">File Extension error.</label>');
					    				$(this).val(null);
					    				$(this).attr('required', 'required');
					    				//$('.filename').html('');
					    				$('#response').html('');
					    			}
						    			
					    			if(this.files[0].size > 1000000) {
					    				$('#response').html('<label id="photoData-error" class="validation-invalid-label" for="photoData">File size exceeds error.</label>');
					    				$(this).val(null);
					    				$(this).attr('required', 'required');
					    				$('.filename').html('');
						    		}
					    	    });
					        },
	                        preConfirm: function (email) {
	                            return new Promise(function (resolve) {
	                            	$('.swal2-confirm').html('Uploading...');
	                            	var form	=	$('#frmPhotoUpload')[0];
	                            	console.log('form : ' + form);
	                            	var formData	=	new FormData(form);
	                            	console.log(' process the formdata to ajax..' + formData);
	                            	alert('response cont : ' + $('#response').html());

	                            	if (formData) {
                            			var newURLLink	=	'index.php?r=students/students/ajaxProfilePhotoUploadActions';

                            			$.ajax( {
        							    	type	:	'POST',
        							        url : newURLLink,
        							        data	:	formData, //+ '&hidStudentID='+data.photoForStudID,
        							        processData: false,
        							        contentType: false,
        							        dataType	: 'json',
        							    } )
        							        .done( function( data ) {
        							            // Handles successful responses only
        							        	if(data.data == 'success') {
        							        		//$('#profilePhoto').removeAttr('src').attr('src', data.destinationFile);
        							        		$("#profilePhoto").attr("src", data.destinationFile+"?timestamp=" + new Date().getTime());
        							        		resolve();
        					   					} else {
        					   						swal({
        					   			                title: 'Information',
        					   			                text: 'Error in uploading profile photo.',
        					   			                type: 'warning'
        					   			            });
        					   					}
        							        } )
        							        .fail( function( reason ) {
        							            // Handles errors only
        							        	console.log('Handles errors only ' + reason);
        							        } )
        							        .always( function( data, textStatus, response ) {} )
        							        .then( function( data, textStatus, response ) {} );
	                            	}
	                            });
			        		},
						});
		        	}
		        } )
		        .fail( function( reason ) {
		            // Handles errors only
		        	console.log('Handles errors only ' + reason);
		        } )
		        .always( function( data, textStatus, response ) {} )
		        .then( function( data, textStatus, response ) {} );
			
		}));
	}

	function showUploadedItem (source) {
		var list	=	document.getElementById("image-list"),
		      li	=	document.createElement("li"),
		      img	=	document.createElement("img");
		    img.src	=	source;
		    li.appendChild(img);
		  list.appendChild(li);
	}

	function doValidateUpload() {
		 var url = 'index.php/?r=students/students/ajaxProfilePhotoUploadActions',
	        uploadButton = $('<button/>')
	            .addClass('btn btn-primary')
	            .prop('disabled', true)
	            .text('Processing...')
	            .on('click', function () {
	                var $this = $(this),
	                    data = $this.data();
	                $this
	                    .off('click')
	                    .text('Abort')
	                    .on('click', function () {
	                        $this.remove();
	                        data.abort();
	                    });
	                data.submit().always(function () {
	                    $this.remove();
	                });
	            });
	    $('#fileupload').fileupload({
	        url: url,
	        dataType: 'json',
	        autoUpload: false,
	        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
	        maxFileSize: 999000,
	        // Enable image resizing, except for Android and Opera,
	        // which actually support image resizing, but fail to
	        // send Blob objects via XHR requests:
	        disableImageResize: /Android(?!.*Chrome)|Opera/
	            .test(window.navigator.userAgent),
	        previewMaxWidth: 100,
	        previewMaxHeight: 100,
	        previewCrop: true
	    }).on('fileuploadadd', function (e, data) {
	        data.context	=	$('<div/>').appendTo('#files');
	        $.each(data.files, function (index, file) {
	            var node	=	$('<p/>')
	                    .append($('<span/>').text(file.name));
	            if (!index) {
	                node
	                    .append('<br>')
	                    .append(uploadButton.clone(true).data(data));
	            }
	            node.appendTo(data.context);
	        });
	    }).on('fileuploadprocessalways', function (e, data) {
	        var index = data.index,
	            file = data.files[index],
	            node = $(data.context.children()[index]);
	        if (file.preview) {
	            node
	                .prepend('<br>')
	                .prepend(file.preview);
	        }

	        if (file.error) {
	            node
	                .append('<br>')
	                .append($('<span class="text-danger"/>').text(file.error));
	        }

	        if (index + 1 === data.files.length) {
	            data.context.find('button')
	                .text('Upload')
	                .prop('disabled', !!data.files.error);
	        }
	    }).on('fileuploadprogressall', function (e, data) {
	        var progress	=	parseInt(data.loaded / data.total * 100, 10);
	        $('#progress .progress-bar').css(
	            'width',
	            progress + '%'
	        );
	    }).on('fileuploaddone', function (e, data) {
	        $.each(data.result.files, function (index, file) {
	            if (file.url) {
	                var link	=	$('<a>')
	                    .attr('target', '_blank')
	                    .prop('href', file.url);
	                $(data.context.children()[index])
	                    .wrap(link);
	            } else if (file.error) {
	                var error	=	$('<span class="text-danger"/>').text(file.error);
	                $(data.context.children()[index])
	                    .append('<br>')
	                    .append(error);
	            }
	        });
	    }).on('fileuploadfail', function (e, data) {
	        $.each(data.files, function (index) {
	            var error	=	$('<span class="text-danger"/>').text('File upload failed.');
	            $(data.context.children()[index])
	                .append('<br>')
	                .append(error);
	        });
	    }).prop('disabled', !$.support.fileInput)
	        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	}

	function doShowProfileTabData() {
		$('.nav-link').on('click', (function(){
			var myAttrID	=	$(this).attr('id');

			switch(myAttrID) {
				case 'myProfile':
					$('#showProfile').show();
					$('#showCourses').hide();
					$('#showAssessments').hide();
					$('#showAttendance').hide();
					$('#showDocuments').hide();
					$('#showElectives').hide();
					$('#showAchievements').hide();
					$('#showLogs').hide();
					$('#showPrevData').hide();
					$('#activeTab').html('<i class="icon-menu7 mr-2"></i> &nbsp;Profile');
					break;

				case 'showCourse':
					$('#showProfile').hide();
					$('#showCourses').show();
					$('#showAssessments').hide();
					$('#showAttendance').hide();
					$('#showDocuments').hide();
					$('#showElectives').hide();
					$('#showAchievements').hide();
					$('#showLogs').hide();
					$('#showPrevData').hide();
					$('#activeTab').html('<i class="icon-books mr-2"></i> &nbsp;Courses');
					break;

				case 'showAssess':
					$('#showProfile').hide();
					$('#showCourses').hide();
					$('#showAssessments').show();
					$('#showAttendance').hide();
					$('#showDocuments').hide();
					$('#showElectives').hide();
					$('#showAchievements').hide();
					$('#showLogs').hide();
					$('#showPrevData').hide();
					$('#activeTab').html('<i class="mi-open-in-new mr-2"></i> &nbsp;Assessments');
					break;

				case 'showAttendance':
					$('#showProfile').hide();
					$('#showCourses').hide();
					$('#showAssessments').hide();
					$('#showAttendance').show();
					$('#showDocuments').hide();
					$('#showElectives').hide();
					$('#showAchievements').hide();
					$('#showLogs').hide();
					$('#showPrevData').hide();
					$('#activeTab').html('<i class="icon-calendar3 mr-2"></i> &nbsp;Attendances');
					break;

				case 'showDoc':
					$('#showProfile').hide();
					$('#showCourses').hide();
					$('#showAssessments').hide();
					$('#showAttendance').hide();
					$('#showDocuments').show();
					$('#showElectives').hide();
					$('#showAchievements').hide();
					$('#showLogs').hide();
					$('#showPrevData').hide();
					$('#activeTab').html('<i class="icon-file-text2 mr-2"></i> &nbsp;Documents');
					break;

				case 'showElec':
					$('#showProfile').hide();
					$('#showCourses').hide();
					$('#showAssessments').hide();
					$('#showAttendance').hide();
					$('#showDocuments').hide();
					$('#showElectives').show();
					$('#showAchievements').hide();
					$('#showLogs').hide();
					$('#showPrevData').hide();
					$('#activeTab').html('<i class="icon-book mr-2"></i> &nbsp;Electives');
					break;

				case 'showAchieve':
					$('#showProfile').hide();
					$('#showCourses').hide();
					$('#showAssessments').hide();
					$('#showAttendance').hide();
					$('#showDocuments').hide();
					$('#showElectives').hide();
					$('#showAchievements').show();
					$('#showLogs').hide();
					$('#showPrevData').hide();
					$('#activeTab').html('<i class="icon-trophy2 mr-2"></i> &nbsp;Achievements');
					break;

				case 'showLogs':
					$('#showProfile').hide();
					$('#showCourses').hide();
					$('#showAssessments').hide();
					$('#showAttendance').hide();
					$('#showDocuments').hide();
					$('#showElectives').hide();
					$('#showAchievements').hide();
					$('#showLogs').show();
					$('#showPrevData').hide();
					$('#activeTab').html('<i class="icon-file-text mr-2"></i> &nbsp;Logs');
					break;
				case 'showPrevData':
					$('#showProfile').hide();
					$('#showCourses').hide();
					$('#showAssessments').hide();
					$('#showAttendance').hide();
					$('#showDocuments').hide();
					$('#showElectives').hide();
					$('#showAchievements').hide();
					$('#showLogs').hide();
					$('#showPrevData').show();
					$('#activeTab').html('<i class="fa fa-certificate mr-2"></i> &nbsp;Previous Data');
					break;
			}
		}));
	}

	function studentPrevLoadData(){
		$(light_prevData).block({
	        message: '<i class="icon-spinner11 spinner"></i>',
	        overlayCSS: {
	            backgroundColor: '#fff',
	            opacity: 0.8,
	            cursor: 'wait'
	        },
	        css: {
	            border: 0,
	            padding: 0,
	            backgroundColor: 'none'
	        }
	    });
		//LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
		var myParams	=	'limitRowsFirstTime=100&hidStudentID='+$('#hidStudentID').val();
		var newURLLink	=	'index.php?r=students/students/ajaxResultsForStudentPrevDataJSONGeneration';

		$.ajax( {
	    	type	:	'POST',
	        url : newURLLink,
	        data : myParams,
	        dataType	: 'json',
	    } )
	        .done( function( data ) {
	        	// Handles successful responses only
	        	console.log('Handles successful responses only' + data);
	        	studentPrevDataAjaxDonePageLoad(data);
	        } )
	        .fail( function( reason ) {
	            // Handles errors only
	        	console.log('Handles errors only ' + reason);
	        } )
	        .always( function( data, textStatus, response ) {} )
	        .then( function( data, textStatus, response ) {} );
	}

	/*function validateFilters(myProp) {
		var isOpenedTabValid	=	$("#frmCommon :input").valid();
		return isOpenedTabValid;
	}*/

	function doPreviousDataBtnActions() {
		$('#btnSave').on('click', (function() {
   			if ( $('.form-input-styled').is(':checked')) {
				$('#editstudentPrevData').val(1);
			} else {
				$('#editstudentPrevData').val(0);
			}

   			var newURLLink	=	'index.php?r=students/studentPrevData/ajaxResultsForstudentPrevData';

   			if ( ($('#txtstudentPrevDataName').val() != '') && $('#editstudentPrevData').val() != '' ){
	   			if($(this).html() == 'Submit') {
	   				$.ajax( {
				    	type	:	'POST',
				        url : newURLLink,
				        data	:	$('#frmLogCat').serialize() + '&reqType=create',
				        dataType	: 'json',
				    } )
				        .done( function( data ) {
				            // Handles successful responses only
				        	if(data.data == 'success') {
		   						swal({
		   			                title: 'Information',
		   			                text: 'Record inserted successfully.',
		   			                type: 'info'
		   			            });
		   						$('#frmCommon').trigger("reset");
		   						$('#btnSave').html('Submit');
				        		studentPrevLoadData();
		   					} else {
		   						alert('error in inserting');
		   					}
				        } )
				        .fail( function( reason ) {
				            // Handles errors only
				        	console.log('Handles errors only ' + reason);
				        } )
				        .always( function( data, textStatus, response ) {} )
				        .then( function( data, textStatus, response ) {} );
	   			}

	   			if($(this).html() == 'Update') {
	   				$.ajax( {
				    	type	:	'POST',
				        url : newURLLink,
				        data	:	$('#frmLogCat').serialize() + '&reqType=update',
				        dataType	: 'json',
				    } )
				        .done( function( data ) {
				            // Handles successful responses only
				        	if(data.data == 'success') {
		   						swal({
		   			                title: 'Information',
		   			                text: 'Record inserted successfully.',
		   			                type: 'info'
		   			            });
		   						$('#frmCommon').trigger("reset");
		   						$('#btnSave').html('Submit');
				        		studentPrevLoadData();
		   					} else {
		   						alert('error in inserting');
		   					}
				        } )
				        .fail( function( reason ) {
				            // Handles errors only
				        	console.log('Handles errors only ' + reason);
				        } )
				        .always( function( data, textStatus, response ) {} )
				        .then( function( data, textStatus, response ) {} );
	   			}
   			}//if mandatory fields are entered.
		}))
	}

	function studentPrevDataAjaxDonePageLoad(data, myProp) {
		//common For all the ajax Done process.. [ Search Filter, All, Default, Delete action etc...]

		if(data.success == 1) { //Success
			var x = new Date();
			curDate	=	x.toUTCString();// changing the display to UTC string
			console.log(' success date : ' + curDate);
			$(light_prevData).unblock();
	    	$('#studViewPrevTable').show();
	    	$('#spinner-light-prevData').hide();
	    	//Steps for filling up the contents from the json response.
	    	console.log('json output of prevData : ' + data.writeJSONData);
			//STEP 2 : Data Table
	        var table	=	$('#studViewPrevTable').DataTable({ //.datatable-button-html5-basic
	    		destroy :	true,
	    		aaData	:	data.writeJSONData,
	    		selectAllPages	:	false,
	         	columns: [
					{ "data": "SNo"},
					{ "data": "Institution" },
					{ "data": "Year" },
					{ "data": "Course" },
					{ "data": "Comments" },
					{ "data": "Actions" },
	         	],
	         	columnDefs: [
	     		  { targets: 'no-sort', orderable: false }
	     		],
	    		buttons: {
	    	        dom: {
	                    button: {
	                        className: 'btn btn-light'
	                    }
	                },
	                buttons: [
	                	{
	                        text: 'Add Previous Data',
	                        className: 'btn bg-primary',
	                        attr:  {
	                        	id	:	'sweet_warning',
	                        },
	                        enabled: true,
	                        action: function () {
	            	        	var myParams	=	'&limitRowsFirstTime=100&reqType=formListing&hidStudentID='+ $('#hidStudentID').val();
	            	        	var newURLLink	=	'index.php?r=students/students/ajaxResultsForStudentPrevDataFormJSONGeneration';
	            	        	console.log('myParams For Second Runner : ' + myParams);
	            	            $.ajax( {
	            	            	type	:	'POST',
	            	                url : newURLLink,
	            	                data : myParams,
	            	                dataType	: 'json',
	            	            } )
	            	            .done( function( data ) {
	            		            // Handles successful responses only
	            		        	console.log('Handles successful Form Loading responses only' + data);

	            		        	if(data.data == 'success') {
	            			        	swal({
	            					        title: '<i> Add Previous Data</i>',
	            					        html: data.formCont,
	            					        confirmButtonText: 'Add',
	            					        //showLoaderOnConfirm: true,
	            					        showCancelButton: true,
	            					        allowOutsideClick: false,
	            					        onOpen: function () {},
	            					        preConfirm: function (email) {
	            					            return new Promise(function (resolve) {
	            					            	var form	=	$('#frmCommon');
	            					            	var formValid	=	false;
	            					            	if(!(form.valid()) ) {
	            					            		$('.btn-primary').prop('disabled', false);
	            					            		$('.swal2-cancel').prop('disabled', false);
	            					            		$('.btn-primary').removeAttr('style');
	            					            		$('.swal2-actions').removeClass('swal2-loading');
	            					            		$('#txtInstitution').focus();
	            					            	} else {
	            					            		$('.swal2-actions').addClass('swal2-loading');
	            					            		$('.swal2-confirm').html('Please wait...');
	            								    	var myParams	=	'reqType=create';
	            								    	var newURLLink	=	'index.php?r=students/students/ajaxResultsForPrevDataActions';

	            								    	$.ajax( {
		            								    	type	:	'POST',
		            								    	url : newURLLink,
		            								    	data : $('#frmCommon').serialize()+ '&'+myParams,
		            								    	dataType	: 'json',
	            								    	} )
		            								    	.done( function( data ) {
		            								    		console.log('\r\n after Previous Data adding process done the data : ' + data.data);

		            								    		if(data.data == 'success') {
		            								    			// Handles successful responses only
		            								    			console.log('Handles successful previous data capturing process responses only' + data);
		            								    			//calling the preloadconfirm ...
		            								    			$('.swal2-confirm').html(' Previous Data capturing process completed successfully.');
		            								    			resolve();
		            								    			studentPrevLoadData();
		            								    			initialText	=	myProp.data('initial-text');
		            								    			myProp.html(initialText).removeClass('disabled');
		            								    		}
		            								    	} )
		            								    	.fail( function( reason ) {
		            								    		// Handles errors only
		            								    		console.log('Handles Approval process related errors only ' + reason);
		            								    	} )
		            								    	.always( function( data, textStatus, response ) {} )
		            								    	.then( function( data, textStatus, response ) {} );
	            					            	}
	            					            });
	            					        },
	            					    });
	            		        	}
	            		        } )
	            		        .fail( function( reason ) {
						    		// Handles errors only
						    		console.log('Handles Approval process related errors only ' + reason);
						    	} )
						    	.always( function( data, textStatus, response ) {} )
						    	.then( function( data, textStatus, response ) {} );
	                        }
	                    },
	            ]},
	            select	:	'single',
	            style: 'os',
	            order	: [[1, 'asc']],
	            bFilter	: false,
	            bDestroy	:	true,
		    });

	        //STEP 2 : Data Table
		   	$('#studViewPrevTable_length').hide();
	        $('#studViewPrevTable_length select').addClass('form-control form-input-styled');
	        var scrollableDiv	=	$("div#studViewPrevTable_wrapper .dataTables_scrollBody");
	        var divTableRows	=	$(scrollableDiv).find("table tbody tr") ;

	        if(divTableRows.length < 5){
	        	$(scrollableDiv).css("overflow", ""); //For controlling the Actions popup..
	        }

	        $('div .dt-buttons').css('float', 'left');
		}
	}

	function doPrevDataActions(){
		$('#studViewPrevTable').on('click', '.logAction', (function () {
		   	var myAttr	=	$(this).attr('id').split('-');
		   	$('#hidRecId').val(myAttr[1]);

		   	switch(myAttr[0]) {
		   		case 'edit' :
		   			var myParams	=	'&reqType=formListing&hidStudentID='+ $('#hidStudentID').val()+'&hidRecId='+$('#hidRecId').val();
    	        	var newURLLink	=	'index.php?r=students/students/ajaxResultsForStudentPrevDataFormJSONGeneration';
    	        	console.log('myParams For Second Runner : ' + myParams);
    	            $.ajax( {
    	            	type	:	'POST',
    	                url : newURLLink,
    	                data : myParams,
    	                dataType	: 'json',
    	            } )
    	            .done( function( data ) {
    		            // Handles successful responses only
    		        	console.log('Handles successful Form Loading responses only' + data);

    		        	if(data.data == 'success') {
    			        	swal({
    					        title: '<i> Edit Previous Data</i>',
    					        html: data.formCont,
    					        confirmButtonText: 'Update',
    					        //showLoaderOnConfirm: true,
    					        showCancelButton: true,
    					        allowOutsideClick: false,
    					        onOpen: function () {},
    					        preConfirm: function (email) {
    					            return new Promise(function (resolve) {
    					            	var form	=	$('#frmCommon');
    					            	var formValid	=	false;
    					            	if(!(form.valid()) ) {
    					            		$('.btn-primary').prop('disabled', false);
    					            		$('.btn-primary').removeAttr('style');
    					            		$('.swal2-actions').removeClass('swal2-loading');
    					            		$('#txtInstitution').focus();
    					            	} else {
    					            		$('.swal2-actions').addClass('swal2-loading');
    					            		$('.swal2-confirm').html('Please wait...');
    								    	var myParams	=	'reqType=update';
    								    	var newURLLink	=	'index.php?r=students/students/ajaxResultsForPrevDataActions';

    								    	$.ajax( {
    								    	type	:	'POST',
    								    	url : newURLLink,
    								    	data : $('#frmCommon').serialize()+ '&'+myParams,
    								    	dataType	: 'json',
    								    	} )
    								    	.done( function( data ) {
    								    		console.log('\r\n after Previous Data updating process done the data : ' + data.data);

    								    		if(data.data == 'success') {
    								    			// Handles successful responses only
    								    			console.log('Handles successful previous data capturing process responses only' + data);
    								    			//calling the preloadconfirm ...
    								    			$('.swal2-confirm').html(' Previous Data capturing process completed successfully.');
    								    			resolve();
    								    			studentPrevLoadData();
    								    			//initialText	=	myProp.data('initial-text');
    								    			//myProp.html(initialText).removeClass('disabled');
    								    		}
    								    	} )
    								    	.fail( function( reason ) {
    								    		// Handles errors only
    								    		console.log('Handles Approval process related errors only ' + reason);
    								    	} )
    								    	.always( function( data, textStatus, response ) {} )
    								    	.then( function( data, textStatus, response ) {} );
    					            	}
    					            });
    					        },
    					    });
    		        	}
    		        } )
    		        .fail( function( reason ) {
			    		// Handles errors only
			    		console.log('Handles Approval process related errors only ' + reason);
			    	} )
			    	.always( function( data, textStatus, response ) {} )
			    	.then( function( data, textStatus, response ) {} );

			   		break;

		   		case 'del' :
		   			swal({
                        title: 'Are you sure?',
                        text: 'You will not be able to recover the deleted record!',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes, delete it!',
                        showLoaderOnConfirm: true,
                        preConfirm: function (email) {
                            return new Promise(function (resolve) {
                            	$('.swal2-confirm').html('Deleting...');
                            	var myParams	=	'reqType=dele&hidStudentID='+$('#hidStudentID').val()+'&hidRecId='+myAttr[1];
						    	var newURLLink	=	'index.php?r=students/students/ajaxResultsForPrevDataActions';

						    	$.ajax( {
							    	type	:	'POST',
							        url : newURLLink,
							        data	:	myParams,
							        dataType	: 'json',
							    } )
							        .done( function( data ) {
							            // Handles successful responses only
							        	if(data.data == 'success') {
					   						swal({
					   			                title: 'Information',
					   			                text: 'Record deleted successfully.',
					   			                type: 'info'
					   			            });
			
					   						studentPrevLoadData();
					   					} else {
					   						swal({
					   			                title: 'Information',
					   			                text: 'Error in Deleting Previous Data.',
					   			                type: 'warning'
					   			            });
					   					}
							        } )
							        .fail( function( reason ) {
							            // Handles errors only
							        	console.log('Handles errors only ' + reason);
							        } )
							        .always( function( data, textStatus, response ) {} )
							        .then( function( data, textStatus, response ) {} );
                            })
                        },
		   			});

			   		break;
		   	}
	   	}));
	}