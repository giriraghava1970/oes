var light_8 = $('#spinner-light-8').closest('.card');
(function () {
    "use strict";
    preLoadData();
    btnActions();
    doDTableActions();
})(jQuery);

function preLoadData() {
    $(light_8).block({
        message: '<i class="icon-spinner11 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });

    //LOADING THE DEFAULT 100 RECORDS FOR STUDENTS FOR THE FIRST TIME WITHOUT ANY FILTERS
    var myParams = 'limitRowsFirstTime=100';
    var newURLLink = $('#hidProjectURL').val() + 'Questions/QuestionTypes/ajaxQuestionTypesResultsForFiltersJSONGeneration';

    $.ajax({
        type: 'POST',
        url: newURLLink,
        data: myParams,
        dataType: 'json',
    })
        .done(function (data) {
            // Handles successful responses only
            console.log('Handles successful responses only' + data);
            ajaxDonePageLoad(data);
        })
        .fail(function (reason) {
            // Handles errors only
            console.log('Handles errors only ' + reason);
        })
        .always(function (data, textStatus, response) { })
        .then(function (data, textStatus, response) { });
}

function doClear() {
    $('.form-control').each(function () {
        $(this).val('');
    });
}

function btnActions() {
    $('#btnSave').on('click', (function (e) {
        console.log('btn Value : ' + $(this).html());
        // alert('click btn ? ');
        e.preventDefault();
        //Check the Question Type already exists..
        if ($('#question_type').val() != '') {
            // ajax controller
            var newURLLink = $('#hidProject').val() + 'Questions/checkFieldExists';
            $.ajax({
                type: 'POST',
                url: newURLLink,
                data: { 'question_type': $('#question_type').val() },
                dataType: 'json',
            })
                .done(function (data) {
                    if (data.data == 'success') {
                        doSubmitForm();
                        $('#question_type_error').hide();
                    } else {
                        var myErrorMsg = '';
                        myErrorMsg = 'Question Type';
                        $('#question_type_error').show();
                        $('#question_type_error').html(myErrorMsg + ' Already Exists');
                    }
                })
                .fail(function (reason) {
                    // Handles errors only
                    console.log('Handles errors only ' + reason);
                })
                .always(function (data, textStatus, response) { })
                .then(function (data, textStatus, response) { });
        }
    }));
}

function doSubmitForm() {
    var myForm = $('#frmUsers');
    if (myForm.valid()) {
        var myReqType = '';

        if ($('#btnSave').html().trim() == 'Save') {
            myReqType = 'create';
        } else if ($('#btnSave').html().trim() == 'Update') {
            myReqType = 'update';
        }
        // alert('myReqType : ' + myReqType);
        var newURLLink = $('#hidProjectURL').val() + 'Questions/QuestionTypes/ajaxResultsForQuestionTypeAction';
        console.log('newURLLINK : ' + newURLLink);

        $.ajax({
            type: 'POST',
            url: newURLLink,
            dataType: 'json',
            data: $('#frmUsers').serialize() + '&reqType=' + myReqType,
        })
            .done(function (data) {
                if (data.data == 'success') {
                    if ($('#btnSave').html().trim() == 'Update') {
                        swal({
                            title: 'Information',
                            text: 'Record Updated successfully.',
                            type: 'success'
                        });
                    } else {
                        swal({
                            title: 'Information',
                            text: 'Record inserted successfully.',
                            type: 'success'
                        });
                    }
                    doClear();
                    $('#btnSave').html('Save');
                    preLoadData();
                } else {
                    swal({
                        title: 'Warning',
                        text: 'Error in creating new record.',
                        type: 'warning'
                    });
                }
            })
            .fail(function (reason) { })
            .always(function (data, textStatus, response) { })
            .then(function (data, textStatus, response) { });
    }
}

function ajaxDonePageLoad(data, myProp) {
    if (data.success == 1) { //Success
        var x = new Date();
        curDate = x.toUTCString();// changing the display to UTC string
        console.log(' success date : ' + curDate);
        //$(light_8).unblock();
        $('#usersTable').show();

        var table = $('#usersTable').DataTable({
            destroy: true,
            aaData: data.writeJSONData,
            selectAllPages: false,
            columns: [
                { "data": "SNo" },
                { "data": "Question Type" },
                { "data": "Status" },
                { "data": "Action" },
            ],
            columnDefs: [
                { targets: 'no-sort', orderable: false }
            ],
            buttons: {
                dom: {
                    button: {
                        classname: 'btn btn-light'
                    }
                },
                buttons: [{
                    text: 'Create Question Type',
                    className: 'btn btn-primary',
                    action: function () {
                        var myParams = 'reqType=formList&hidRecID=0';
                        var newURLLink = $('#hidProjectURL').val() + 'Questions/QuestionTypes/ajaxResultsForQuestionTypeAction';
                        console.log('newURLLINK : ' + newURLLink);
                        $.ajax({
                            type: 'POST',
                            url: newURLLink,
                            dataType: 'json',
                            data: myParams,
                        })
                            .done(function (data) {
                                if (data.data == 'success') {
                                    doOpenInsertSwal(data.formCont);
                                }
                            })
                            .fail(function (reason) {
                                // Handles errors only
                                console.log('Handles errors only ' + reason);
                            })
                            .always(function (data, textStatus, response) { })
                            .then(function (data, textStatus, response) { });

                    }
                }],
            },
            select: 'single',
            style: 'os',
            order: [[0, 'DESC']],
            bFilter: false,
            bDestroy: true,
        });

        $('#usersTable_length').hide();
        $('#usersTable_length select').addClass('form-control form-input-styled');
        var scrollableDiv = $("div#usersTable_wrapper .dataTables_scrollBody");
        var divTableRows = $(scrollableDiv).find("table tbody tr");

        if (divTableRows.length < 5) {
            $(scrollableDiv).css("overflow", "");
        }

        $('div .dt-buttons').css('float', 'left');
    }
}

//Edit and Delete
function doDTableActions() {
    $('#usersTable').on('click', '.logAction', (function () {
        var myAttr = $(this).attr('id').split('-');
        $('#hidRecID').val(myAttr[1]);

        switch (myAttr[0]) {
            case 'edit':
                //Call SWAL
                var myParams = 'reqType=formList&hidRecID=' + myAttr[1];
                var newURLLink = $('#hidProjectURL').val() + 'Questions/QuestionTypes/ajaxResultsForQuestionTypeAction';
                console.log('newURLLINK : ' + newURLLink);
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: myParams,
                })
                    .done(function (data) {
                        if (data.data == 'success') {
                            doOpenSwal(data.formCont);
                        }
                    })
                    .fail(function (reason) {
                        // Handles errors only
                        console.log('Handles errors only ' + reason);
                    })
                    .always(function (data, textStatus, response) { })
                    .then(function (data, textStatus, response) { });
                break;

            case 'del':
                var myParams = 'reqType=dele&hidRecID=' + myAttr[1];
                var newURLLink = $('#hidProjectURL').val() + 'Questions/QuestionTypes/ajaxResultsForQuestionTypeAction';
                console.log('newURLLINK : ' + newURLLink + myParams);
                // return;
                swal({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover the deleted record!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    showLoaderOnConfirm: true,
                    preConfirm: function (email) {
                        return new Promise(function (resolve) {
                            $.ajax({
                                type: 'POST',
                                url: newURLLink,
                                dataType: 'json',
                                data: myParams,
                            })
                                .done(function (data) {
                                    // Handles successful responses only
                                    if (data.data == 'success') {
                                        swal({
                                            title: 'Information',
                                            text: 'Record Deleted Successfully!',
                                            type: 'success'
                                        });
                                        preLoadData()
                                    } else {
                                        alert('error in Deleting');
                                    }
                                })
                                .fail(function (reason) {
                                    // Handles errors only
                                    console.log('Handles errors only ' + reason);
                                })
                                .always(function (data, textStatus, response) { })
                                .then(function (data, textStatus, response) { });
                        });
                    },
                });
                break;
        }
    }
    ))
}

function doOpenSwal(formCont) {
    //SWAL Holder
    swal({
        title: 'Update Question Type',
        html: formCont,
        showCancelButton: true,
        width: '60%',
        confirmButtonText: 'Update',
        showLoaderOnConfirm: true,
        preConfirm: function (email) {
            return new Promise(function (resolve) {
                //Form Action related Ajax Call
                var myParams = '&reqType=update';
                var newURLLink = $('#hidProjectURL').val() + 'Questions/QuestionTypes/ajaxResultsForQuestionTypeAction';
                console.log('newURLLINK : ' + newURLLink);
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: $('#frmCommon').serialize() + myParams,
                })

                    .done(function (data) {
                        doClear();
                        if (data.data == 'success') {
                            swal({
                                title: 'Information',
                                text: 'Record updated Successfully!',
                                type: 'success'
                            });
                            preLoadData();
                            // $('#btnSave').html('Update');


                            $.each(data.userRecord, (function (index, value) {
                                console.log(' index : ' + index + ' value : ' + value);
                                if (index == 'question_type') {
                                    $('#question_type').val(value);
                                }

                                if (index == 'status') {
                                    $('#status').val(value).change();
                                }
                            }));


                        }
                    })

                    .fail(function (reason) {
                        // Handles errors only
                        console.log('Handles errors only ' + reason);
                    })
                    .always(function (data, textStatus, response) { })
                    .then(function (data, textStatus, response) { });
            });
        },
    });
}

function doOpenInsertSwal(formCont) {
    //SWAL Holder
    swal({
        title: '',
        html: formCont,
        showCancelButton: true,
        //width: '60%',
        confirmButtonText: 'Save',
        showLoaderOnConfirm: true,
        preConfirm: function (email) {
            return new Promise(function (resolve) {
                //Form Action related Ajax Call
                var myParams = '&reqType=create';
                var newURLLink	=	$('#hidProjectURL').val() + 'Questions/QuestionTypes/ajaxResultsForQuestionTypeAction';
                console.log('newURLLINK : ' + newURLLink);
                $.ajax({
                    type: 'POST',
                    url: newURLLink,
                    dataType: 'json',
                    data: $('#frmCommon').serialize() + myParams,
                })

                    .done(function (data) {
                        doClear();
                        if (data.data == 'success') {
                            /*swal({ title: 'Information',
                                     text: 'Record insert Successfully!',
                                     type: 'success'
                                    });
                                     preLoadData();
                              // $('#btnSave').html('Update');*/
                            resolve();
                            preLoadData();
                        }
                    })

                    .fail(function (reason) {
                        // Handles errors only
                        console.log('Handles errors only ' + reason);
                    })
                    .always(function (data, textStatus, response) { })
                    .then(function (data, textStatus, response) { });
            });
        },
    });
}