/* ------------------------------------------------------------------------------
 *
 *  # Steps wizard
 *
 *  Demo JS code for form_wizard.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------
var formSubmittedAlready	=	false;
var questFormWizard	=	$('#spinner-light-questFormWizard').closest('.card');

var FormWizard = function() {
    //
    // Setup module components
    //

    // Wizard
    var _componentWizard = function() {
        if (!$().steps) {
            console.warn('Warning - steps.min.js is not loaded.');
            return;
        }

        // Wizard with validation
        // Stop function if validation is missing

        if (!$().validate) {
            console.warn('Warning - validate.min.js is not loaded.');
            return;
        }

        // Show form
        var form	=	$('.steps-validation').show();
        var light_8	=	$('#spinner-light-8').closest('.card');
        // Initialize wizard
        var count = 0;
    	$('.steps-validation').steps({
            headerTag: 'h6',
            bodyTag: 'fieldset',
            titleTemplate: '<span class="number">#index#</span> #title#',
            labels: {
                previous: '<i class="icon-arrow-left13 mr-2" /> Previous',
                next: 'Next <i class="icon-arrow-right14 ml-2" />',
                finish: 'Submit form <i class="icon-arrow-right14 ml-2" />'
            },
            transitionEffect: 'fade',
            autoFocus: true,
            saveState: true,
            onStepChanging: function (event, currentIndex, newIndex) {
                // Allways allow previous action even if the current form is not valid!
                if (currentIndex > newIndex) {
                    return true;
                }

                // Needed in some cases if the user went back (clean up)
                if (currentIndex < newIndex) {
                    // To remove error styles
                    form.find('.body:eq(' + newIndex + ') label.error').remove();
                    form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
                }

                form.validate().settings.ignore	=	':disabled,:hidden';
                return form.valid();
            },
            onFinishing: function (event, currentIndex) {
            	//alert('on Finishing! GIRISH HERE ? ');
            	form.validate().settings.ignore	=	':disabled';
                return form.valid();
            },
            onFinished: function (event, currentIndex) {
            	event.preventDefault();
            	count++;

            	if(count == 1) {
            		$('#hidReqType').val('create');
            		$('#questionForm').submit();
            		$(questFormWizard).block({
        		        message: '<i class="icon-spinner11 spinner"></i>',
        		        overlayCSS: {
        		            backgroundColor: '#fff',
        		            opacity: 0.8,
        		            cursor: 'wait'
        		        },
        		        css: {
        		            border: 0,
        		            padding: 0,
        		            backgroundColor: 'none'
        		        }
        		    });
				}
            }
        });

        // Initialize validation
        $('.steps-validation').validate({
            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
            errorClass: 'validation-invalid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },

            // Different components require proper error label placement
            errorPlacement: function(error, element) {
                // Unstyled checkboxes, radios
                if (element.parents().hasClass('form-check')) {
                    error.appendTo( element.parents('.form-check').parent() );
                } else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {// Input with icons and Select2
                    error.appendTo( element.parent() );
                } else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {// Input group, styled file input
                    error.appendTo( element.parent().parent() );
                } else {// Other elements
                    error.insertAfter(element);
                }
            },
            rules: {
                email: {
                    email: true
                },
                image: {
                    required: true,
                    extension: "jpg,jpeg",
                    filesize: 1,
                }
            }
        });
    };

    // Uniform
    var _componentUniform	=	function() {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-blue'
        });
    };

    //
    // Return objects assigned to module
    //

    return {
        init: function() {
           _componentWizard();
          _componentUniform();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    FormWizard.init();
    doQuestionFormPreLoadData();
});

function doQuestionFormPreLoadData() {
	//Retrieve Classes
	$('#lsCourses').on('change', (function(){
		if ($(this).val() != '') {
			var newURLLink	=	$('#hidProjectURL').val()+'Common/ajaxResultsForClassesDropBox';
	        var myParams	=	'reqType=fetchClassesByCourse&courseID='+ $(this).val();
			console.log('newURL : ' + newURLLink);

			$.ajax({
				type	:	'POST',
				url	:	newURLLink,
				dataType	:	'json',
				data	:	myParams,
			})
				.done(function (data) {
					if(data.data == 'success') {
						$("#lsClasses").empty(); //Emptying the modules every time when the Subject selection is altered.
						var list	=	$("#lsClasses");
						list.append('<option value="">--Choose One -- </option>');

						$.each(data.classDtls, function (index, value) {
							console.log('class id : ' + value.id + ' class name : ' + value.className);
							list.append('<option value="' + value.id+'">' + value.className+ '</option>');
						});
					}
                })
                .fail(function (reason) {
                    // Handles errors only
                    console.log('Handles errors only ' + reason);
                })
                .always(function (data, textStatus, response) { })
                .then(function (data, textStatus, response) { });
		} else {
			$('#lsClasses')
			    .find('option')
			    .remove()
			    .end()
			    .append('<option value="">--Choose One--</option>')
			    .val('');
		}
	}));

	//Retrieve Modules
	$('#lsSubjects').on('change', (function(){
		if ($(this).val() != '') {
			var newURLLink	=	$('#hidProjectURL').val()+'Common/ajaxResultsForChaptersDropBox';
	        var myParams	=	'reqType=fetchModulesBySubject&subjectID='+ $('#lsSubjects').val()+'&moduleID='+ $(this).val();
			console.log('newURL : ' + newURLLink);

			$.ajax({
				type	:	'POST',
				url	:	newURLLink,
				dataType	:	'json',
				data	:	myParams,
			})
				.done(function (data) {
					if(data.data == 'success') {
						$("#lsChapters").empty(); //Emptying the Chapters every time when the Subject and Modules selection are altered.
						var list	=	$("#lsChapters");
						list.append('<option value="">--Choose One -- </option>');

						$.each(data.chapterDtls, function (index, value) {
							console.log('chapter id : ' + value.id + ' chapter name : ' + value.chapterName);
							list.append('<option value="' + value.id+'">' + value.chapterName+ '</option>');
						});
					}
                })
                .fail(function (reason) {
                    // Handles errors only
                    console.log('Handles errors only ' + reason);
                })
                .always(function (data, textStatus, response) { })
                .then(function (data, textStatus, response) { });
		} else {
			$('#lsChapters')
			    .find('option')
			    .remove()
			    .end()
			    .append('<option value="">--Choose One--</option>')
			    .val('');
		}
	}));

	//Retrieve Topics
	$('#lsChapters').on('change', (function(){
		if ($(this).val() != '') {
			var newURLLink	=	$('#hidProjectURL').val()+'Common/ajaxResultsForTopicsDropBox';
	        var myParams	=	'reqType=fetchModulesBySubject&subjectID='+ $('#lsSubjects').val()+'&moduleID='+ $(this).val();
	        myParams	+=	'&chapterID='+$(this).val();
			console.log('newURL : ' + newURLLink);

			$.ajax({
				type	:	'POST',
				url	:	newURLLink,
				dataType	:	'json',
				data	:	myParams,
			})
				.done(function (data) {
					if(data.data == 'success') {
						$("#lsTopics").empty(); //Emptying the Chapters every time when the Subject and Modules selection are altered.
						var list	=	$("#lsTopics");
						list.append('<option value="">--Choose One -- </option>');

						$.each(data.topicDtls, function (index, value) {
							console.log('topic id : ' + value.id + ' topic name : ' + value.topicName);
							list.append('<option value="' + value.id+'">' + value.topicName+ '</option>');
						});
					}
                })
                .fail(function (reason) {
                    // Handles errors only
                    console.log('Handles errors only ' + reason);
                })
                .always(function (data, textStatus, response) { })
                .then(function (data, textStatus, response) { });
		} else {
			$('#lsTopics')
			    .find('option')
			    .remove()
			    .end()
			    .append('<option value="">--Choose One--</option>')
			    .val('');
		}
	}));

	$('#lsQuestionTypes').on('change', (function(){
		if($(this).val() != '') {
			var newURLLink	=	$('#hidProjectURL').val()+'Common/ajaxResultsForQuestionTypeOptions';
	        var myParams	=	'reqType=fetchQuestionOptions&questTypeID='+ $(this).val();
	        myParams	+=	'&chapterID='+$(this).val();
			console.log('newURL : ' + newURLLink);

			$.ajax({
				type	:	'POST',
				url	:	newURLLink,
				dataType	:	'json',
				data	:	myParams,
			})
				.done(function (data) {
					if(data.data == 'success') {
						$('#questionOptions').html(data.formCont);
						$('#questionOptions').show();
					}
                })
                .fail(function (reason) {
                    // Handles errors only
                    console.log('Handles errors only ' + reason);
                })
                .always(function (data, textStatus, response) { })
                .then(function (data, textStatus, response) { });
		} else {
			$('#questionOptions').html('');
		}
	}));
}