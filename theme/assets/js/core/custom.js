/* ------------------------------------------------------------------------------
 *
 *  # Custom JS code
 *
 *  Place here all your custom js. Make sure it's loaded after app.js
 *
 * ---------------------------------------------------------------------------- */
	var light_courseListing	=	$('#spinner-light-courseListing').closest('.card');
	( function() {
	    "use strict";
		preLoadMenuData();
		academicYearActions();
	} )(jQuery);

	function preLoadMenuData() {
		var path = window.location.href;//pathname
		//console.log('path : ' + path);
		path	=	path.replace(/\/$/, "");
		path	=	decodeURIComponent(path);
	    var url	=	window.location.pathname;

	    var activePage	=	stripTrailingSlash(path);
	    var navClass	=	'nav-sidebar';
		var navSubmenuClass	= 'nav-group-sub';
		var navSlidingSpeed	=	250;

		$('.' + navClass).each(function() {
		    $('.nav-item a').each(function() {
			    var currentPage	=	stripTrailingSlash($(this).attr('href'));
			    //console.log('currentPage : ' + currentPage + ' ==> ' + '?r='+ activePage);

			    if (activePage ==  currentPage) {
			    	$(this).addClass('active');
			    	//$(this).css('background-color', '#ec407a');
			    	$(this).css({backgroundColor: '#1B2529'});
			    	$(this).parents('li').addClass('nav-item-open'); 
					$(this).parents('li').children('.' + navSubmenuClass).slideDown(navSlidingSpeed);
			    }
			});
		});
	}

	function stripTrailingSlash(str) {
	    if(str.substr(-1) == '/') {
	      return str.substr(0, str.length - 1);
	    }

	    return str;
	}

	function academicYearActions() {
		$('.academic').on('click', (function(){
			var myAttrId	=	$(this).attr('id').split('-');

			if ( (myAttrId[0] == 'alter') && (myAttrId[1] == 'academicYear') 
				&& (myAttrId[2] != '')) {
				$('#hidSelectedAcademicYear').val(myAttrId[2]);
				$('#frmChangeAcademicYear').submit();
			}
		}))
	}