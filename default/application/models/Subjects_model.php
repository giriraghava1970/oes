<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Subjects_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get($subjectID = null){
        $this->db->select('subjects.*')->from('subjects');

        if($subjectID != null){
            $this->db->where('subjects.id', $subjectID);
        }

        $this->db->where('subjects.is_core', 1);
        $this->db->order_by('subjects.id', 'ASC');
        $query  =   $this->db->get();
        return $query->result();
    }

    public function getList($dataForFilter = array()){
        $this->db->select('*')->from('subjects');

        if(!(empty($dataForFilter))) {
            if($dataForFilter['subjectCode'] != null) {
                $this->db->where('subject_code LIKE "%'.$dataForFilter['subjectCode'].'%"');
            }

            if($dataForFilter['subjectName'] != null) {
                $this->db->where('subject_name LIKE "%'.$dataForFilter['subjectName'].'%"');
            }
        }

        $this->db->order_by('subject_name', 'ASC');
        $query  =   $this->db->get();
        return $query->result(); 
    }

    public function add($data) {
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('subjects', $data);
            return 1;
        } else {
            $this->db->insert('subjects', $data);
            return $this->db->insert_id();
        }
    }

    public function checkExists($fieldName, $fieldVal) {
        $this->db->select('*')->from('subjects');
        $this->db->where($fieldName, $fieldVal);
        $query  =   $this->db->get();
        return $query->num_rows();
    }

    public function delete($subjectID = null) {
        if($subjectID != null) {
            $this->db->where('id', $subjectID);
            $this->db->delete('subjects');
            return 1;
        }
    }
}