<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cities_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function add($data) {

    }

    public function getCitiesByStateID($stateId = null) {
        if($stateId != null) {
            $this->db->select('cities_mst.*, states_mst.state_name as stateName')->from('cities_mst');
            $this->db->join('states_mst', 'cities_mst.state_id = states_mst.id');
            $this->db->where('state_id', $stateId);
            $this->db->order_by('city_name', 'ASC');
            $query  =   $this->db->get();
            return $query->result();
        }
    }

    public function getcityByID($cityID = null) {
        if($cityID != null) {
            $this->db->select('*')->from('cities_mst');
            $this->db->where('id', $cityID);
            $query  =   $this->db->get();
            // echo "\r\n <Br/> getStateByID SQL : \r\n <br/><pre>"; print_r($this->db->last_query());
            return $query->result();
        }
    }

    public function get($id = null) {
        $this->db->select('*')->from('states_mst');

        if($id != null) {
            $this->db->where('id', $id);
        }

        $this->db->order_by('id', 'DESC');
        $query  =   $this->db->get();
        return $query->result();
    }
}