<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Chapters_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getChaptersBySubjectID($subjectID = null) {
        if ($subjectID != null) {
            $this->db->select('chapters.*, subjects.subject_name as subjectName')->from('chapters');
            $this->db->join('subjects', 'subjects.id = chapters.subject_id');
            $this->db->where('chapters.subject_id', $subjectID);
            $this->db->order_by('id', 'ASC');
            $query  =   $this->db->get();
            return $query->result();
        }
    }

    public function get($chapterID = null){
        $this->db->select('*')->from('chapters');

        if($chapterID != null){
            $this->db->where('id',$chapterID);
        }

        $this->db->order_by('id', 'ASC');
        $query  =   $this->db->get();
        return $query->result();
    }

    public function getList($dataForFilter = array()){
        $this->db->select('chapters.*,  subjects.id as subjectID, subjects.subject_name as subjectName')->from('chapters');
        $this->db->join('subjects', 'chapters.subject_id = subjects.id');

        if(!(empty($dataForFilter))) {
            if ($dataForFilter['subjectID'] != null) {
                $this->db->where('chapters.subject_id', $dataForFilter['subjectID']);
            }

            if($dataForFilter['chapterName'] != null) {
                $this->db->where('chapters.chapter_name LIKE "%' . $dataForFilter['chapterName'].'%"');
            }
        }

        $this->db->order_by('id', 'ASC');
        $query  =   $this->db->get();
        return $query->result(); 
    }

    public function add($data) {
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('chapters', $data);
            return 1;
        } else {
            $this->db->insert('chapters', $data);
            return $this->db->insert_id();
        }
    }

    public function checkExists($fieldName = null, $moduleID = null, $fieldVal = null) {
        if ( ($fieldName != null) && ($fieldVal != null) ) {
            $this->db->select('*')->from('chapters');
            //$this->db->where('module_id', $moduleID);
            $this->db->where($fieldName, $fieldVal);
            $query  =   $this->db->get();
            return $query->num_rows();
        }
    }

    public function delete($chapterID = null) {
        if($chapterID != null) {
            $this->db->where('id', $chapterID);
            $this->db->delete('chapters');
            return 1;
        }
    }
}