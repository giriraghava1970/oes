<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Topics_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getSubTopicsByTopicID($topicID = null) {
        if($topicID != null) {
            $this->db->select('*')->from('topics');
            $this->db->where('parentTopicID', $topicID);
            $this->db->order_by('id', 'ASC');
            $query  =   $this->db->get();
            return $query->result();
        }
    }

    public function getTopicsByChapterID($subjectID = null, $chapterID = null) {
        if ( ($subjectID != null) && ($chapterID != null) ) {
            $this->db->select('topics.*, subjects.id AS subjectID, subjects.subject_name AS subjectName, chapters.id AS chapterID,
                             chapters.chapter_name as chapterName')->from('topics');
            $this->db->join('chapters', 'chapters.id = topics.chapter_id');
            $this->db->join('subjects', 'subjects.id = topics.subject_id');
            $this->db->where('topics.subject_id', $subjectID);
            $this->db->where('topics.chapter_id', $chapterID);
            $this->db->order_by('id', 'ASC');
            $query  =   $this->db->get();
            return $query->result();
        }
    }

    public function get($topicID = null){
        $this->db->select('*')->from('topics');

        if($topicID != null){
            $this->db->where('id', $topicID);
        }

        $this->db->order_by('id', 'ASC');
        $query  =   $this->db->get();
        return $query->result();
    }

    public function getList($dataForFilter = array()){
        $this->db->select('topics.*, chapters.id as chapterID, chapters.chapter_name as chapterName, 
                           modules.id as moduleID, modules.module_name as moduleName, 
                           subjects.id as subjectID, subjects.subject_name as subjectName')->from('topics');
        $this->db->join('chapters', 'chapters.id= topics.chapter_id');
        $this->db->join('modules', 'topics.module_id= modules.id');
        $this->db->join('subjects', 'modules.subject_id = subjects.id');

        if(!(empty($dataForFilter))) {
            if ( ($dataForFilter['subjectID'] != null) && ($dataForFilter['moduleID'] != null) && ($dataForFilter['chapterID'] != null) ) {
                $this->db->where('topics.subject_id', $dataForFilter['subjectID']);
                $this->db->where('topics.module_id', $dataForFilter['moduleID']);
                $this->db->where('topics.module_id', $dataForFilter['chapterID']);
            }

            if($dataForFilter['topicName'] != null) {
                $this->db->where('topics.topic_name LIKE "%' . $dataForFilter['topicName'].'%"');
            }
        }

        $this->db->order_by('id', 'ASC');
        $query  =   $this->db->get();
        return $query->result(); 
    }

    public function add($data) {
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
            $this->db->update('topics', $data);
            return 1;
        } else {
            $this->db->insert('topics', $data);
            return $this->db->insert_id();
        }
    }

    public function checkExists($fieldName = null, $chapterID = null, $fieldVal = null) {
        if ( ($fieldName != null) && ($chapterID != null) && ($fieldVal != null) ) {
            $this->db->select('*')->from('topics');
            $this->db->where('module_id', $chapterID);
            $this->db->where($fieldName, $fieldVal);
            $query  =   $this->db->get();
            return $query->num_rows();
        }
    }

    public function delete($topicID = null) {
        if($topicID != null) {
            $this->db->where('id', $topicID);
            $this->db->delete('topics');
            return 1;
        }
    }
}