<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Countries_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function add($data) {

    }

    public function get($id = null) {
        $this->db->select('*')->from('countries_mst');

        if($id != null) {
            $this->db->where('id', $id);
        }

        $this->db->order_by('id', 'DESC');
        $query  =   $this->db->get();
        return $query->result();
    }

    public function getCountryName($id = null){
        $this->db->select('country_name')->from('countries_mst');
        $this->db->where('id', $id);
        return $this->db->get()->row()->country_name;
    }
}