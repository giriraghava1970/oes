<?php
class Generalfunctions {
    function __construct() {
        $this->CI	=	& get_instance();
        $this->ResultSet	=	array();
    }

    public function fetchMenus() {
        $menuDtls   =   '';
        return $menuDtls;
    }

    public function validateParams($methodName = '', $args = array()) {
        switch($methodName) {
            case 'subjects' :
                return 0;
                break;

            case 'userValidation'  :
                if(count($args) == 3)
                    return 3;
                else
                    return 0;

                break;

            case 'cities'  :
            case 'states'  :
            case 'chapters' :
            case 'subTopics' :
                if(count($args) == 1)
                    return 1;

               break;

            case 'topics' :
                if(count($args) == 2)
                    return 2;

                break;
        }
    }
    
    public function validateArguments($methodName = '', $args = array()) {
        $output    =   array();

        if(count($args) == $this->validateParams($methodName, $args)) {
            if(!(empty($args)) ) {
                foreach($args as $key => $val) {
                    if ( (isset($key)) && (($val == '') || ($val == null)) ) {
                        $output['errorCode']	=	'1';
                        $output['errorMessage']	=	'Failure';
                        $output['errorDescription']	=	$key .' should not be empty.';
                        $output['validationResult']	=	'failure';
                        break;
                    } else
                        $output['validationResult']	=	'true';
                }
            } else
                $output['validationResult']	=	'true';
        } else {
            $output['errorCode']	=	'1';
            $output['errorMessage']	=	'Failure';
            $output['errorDescription']	=	'Invalid parameters count';
            $output['validationResult']	=   'failure';
        }

        return $output;
    }
}?>