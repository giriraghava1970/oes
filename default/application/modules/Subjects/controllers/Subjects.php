<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Subjects extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Subjects_model');
    }

	public function index() {
		$data['pageName']	=	'Dashboard';
		$data['subjRes']    =   $this->Subjects_model->get();
		$this->load->template('Dashboard', 'templates/', 'Subjects/', 'subjectsList', $data);
	}
}