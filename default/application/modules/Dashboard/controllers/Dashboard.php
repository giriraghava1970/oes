<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Subjects_model');
    }

	public function index() {
		$data['pageName']	=	'Dashboard';
		$this->load->template('Dashboard', 'templates/', 'Dashboards/', 'layout', $data);
	}
}