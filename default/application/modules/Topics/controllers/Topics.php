<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Topics extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Subjects_model');
        $this->load->model('Chapters_model');
        $this->load->model('Topics_model');
    }

	public function index() {
	    $data  =   array();
	    $this->load->template('Topics', 'templates/', 'Topics/', 'topicsList', $data);
	}

	public function View() {
        $subjectID =   ($this->uri->segment(3)) ? $this->uri->segment(3) : '';
        $chapterID =   ($this->uri->segment(4)) ? $this->uri->segment(4) : '';

        if (($subjectID != null) && ($chapterID != null) ) {
	        $subjectID  =  base64_decode($subjectID);
	        $chapterID =   base64_decode($chapterID);
	        //$topicID   =   base64_decode($topicID);

	        $data['pageName']	=	'Topics';
	        $subjRes    =   $this->Subjects_model->get($subjectID);
	        $subjectName    =   '';

	        if(!(empty($subjRes))) {
	            foreach($subjRes as $s) {
	                $subjectName    =   $s->subject_name;
	            }
	        }

	        $chapterRes    =   $this->Chapters_model->get($chapterID);
	        $chapterName    =   '';

	        if(!(empty($chapterRes))) {
	            foreach($chapterRes as $c) {
	                $chapterName    =   $c->chapter_name;
	            }
	        }

	        //echo "\r\n <br/> subject id : " . $subjectID . " chapter : " . $chapterID;
	        $data['subjectID'] =   $subjectID;
	        $data['chapterID'] =   $chapterID;
	        $data['subjectName']    =   $subjectName;
	        $data['chapterName']    =   $chapterName;
	        $data['topicsRes']    =   $this->Topics_model->getTopicsByChapterID($chapterID);
	        $this->load->template('Topics', 'templates/', 'Topics/', 'topicsList', $data);
	    } else {
	        //echo "\r\n <br/> GIRISH HERE ? ";
	    }
	}
}