<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Chapters extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Subjects_model');
        $this->load->model('Chapters_model');
    }

	public function index() {
	    $data  =   array();
	    $this->load->template('Chapters', 'templates/', 'Chapters/', 'chaptersList', $data);
	}

	public function View($id) {
	    if($id) {
	        $subjectID  =  base64_decode($id);
	        $data['pageName']	=	'Subjects';
	        $subjRes    =   $this->Subjects_model->get($subjectID);
	        $subjectName    =   '';

	        if(!(empty($subjRes))) {
	            foreach($subjRes as $s) {
	                $subjectName    =   $s->subject_name;
	            }
	        }

	        $data['subjectID'] =   $subjectID;
	        $data['subjectName']    =   $subjectName;
	        $data['chaptersRes']    =   $this->Chapters_model->getChaptersBySubjectID($subjectID);
	        $this->load->template('Chapters', 'templates/', 'Chapters/', 'chaptersList', $data);
	    } else {
	        echo "\r\n <br/> GIRISH HERE ? ";
	    }
	}
}