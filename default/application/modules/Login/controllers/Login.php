<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
    function __construct() {
        parent::__construct();
    }

	public function index() {
		//$this->load->view('welcome_message');
		$data['pageName']	=	'Login';
		$this->load->template('Login', 'templates/', 'Login/', 'login', $data);
	}

	public function VerifyLogin() {
	    $login_post =   array(
	        'email' =>  $this->input->post('userName'),
	        'password'  =>  $this->input->post('userPassword') );
	    //$setting_result =   $this->Setting_model->get();
	    $result =   $this->Staff_model->checkLogin($login_post);

	    if ($result) {
	        if($result->is_active){
	            //$setting_result =   $this->setting_model->get();
	            $setting_result    =   null;
	            $session_data   =   array(
	                'id'    =>  $result->id,
	                'username'  =>  $result->name,
	                'email' =>  $result->email,
	                'roles' =>  $result->roles,
	                'date_format'   =>  $setting_result[0]['date_format'],
	                'currency_symbol'   =>  $setting_result[0]['currency_symbol'],
	                'start_month'   =>  $setting_result[0]['start_month'],
	                'school_name'   =>  $setting_result[0]['name'],
	                'timezone'  =>  $setting_result[0]['timezone'],
	                'sch_name'  =>  $setting_result[0]['name'],
	                'language'  =>  array('lang_id' => $setting_result[0]['lang_id'], 'language' => $setting_result[0]['language']),
	                'is_rtl'    =>  $setting_result[0]['is_rtl'],
	                'theme' =>  $setting_result[0]['theme'], );
	            
	            if(isset($result->roles['Teacher'])) {
	                $this->session->set_userdata('employee', $session_data);
	            } else {
	                $this->session->set_userdata('admin', $session_data);
	            }
	            
	            $role   =   $this->customlib->getStaffRole();
	            $role_name  =   json_decode($role)->name;
	            $this->customlib->setUserLog($this->input->post('username'), $role_name);
	            
	            if (isset($_SESSION['redirect_to']))
	                redirect($_SESSION['redirect_to']);
	                else {
	                    if(isset($result->roles['Teacher'])) {
	                        redirect('Employee/Dashboard');
	                    } else {
	                        redirect('Dashboard');
	                    }
	                }
	        }else{
	            $data['error_message']  =   'Your account is disabled please contact to administrator';
	            //redirect('Login', $data);
	        }
	    } else {
	        // Common for all users other than admin / Staff
	        $login_post =   array(
	            'username' =>  $this->input->post('userName'),
	            'password'  =>  $this->input->post('userPassword') );
	        $login_details  =   $this->User_model->checkLogin($login_post);
	        
	        if (isset($login_details) && !empty($login_details)) {
	            $user   =   $login_details[0];
	            
	            if ($user->is_active == "yes") {
	                if ($user->role == "student") {
	                    $result =   $this->User_model->read_user_information($user->id);
	                } else if ($user->role == "parent") {
	                    $result =   $this->User_model->checkLoginParent($login_post);
	                }
	                
	                if ($result != false) {
	                    $setting_result =   $this->Setting_model->get();
	                    
	                    if ($result[0]->role == "student") {
	                        $session_data   =   array(
	                            'id'    =>  $result[0]->id,
	                            'student_id'    =>  $result[0]->user_id,
	                            'role'  =>  $result[0]->role,
	                            'username'  =>  $result[0]->firstname . " " . $result[0]->lastname,
	                            'date_format'   =>  $setting_result[0]['date_format'],
	                            'currency_symbol'   =>  $setting_result[0]['currency_symbol'],
	                            'timezone'  =>  $setting_result[0]['timezone'],
	                            'sch_name'  =>  $setting_result[0]['name'],
	                            'language'  =>  array('lang_id' => $setting_result[0]['lang_id'], 'language' => $setting_result[0]['language']),
	                            'is_rtl'    =>  $setting_result[0]['is_rtl'],
	                            'theme' =>  $setting_result[0]['theme'],
	                            'image' =>  $result[0]->image, );
	                        $this->session->set_userdata('student', $session_data);
	                        $this->customlib->setUserLog($result[0]->username, $result[0]->role);
	                        redirect('Users/dashboard');
	                    } else if ($result[0]->role == "parent") {
	                        if ($result[0]->guardian_relation == "Father") {
	                            $image  =   $result[0]->father_pic;
	                        } else if ($result[0]->guardian_relation == "Mother") {
	                            $image  =   $result[0]->mother_pic;
	                        } else if ($result[0]->guardian_relation == "Other") {
	                            $image  =   $result[0]->guardian_pic;
	                        }
	                        
	                        $session_data   =   array(
	                            'id'    =>  $result[0]->id,
	                            'student_id'    =>  $result[0]->user_id,
	                            'role'  =>  $result[0]->role,
	                            'username'  =>  $result[0]->guardian_name,
	                            'date_format'   =>  $setting_result[0]['date_format'],
	                            'timezone'  =>  $setting_result[0]['timezone'],
	                            'sch_name'  =>  $setting_result[0]['name'],
	                            'currency_symbol'   =>  $setting_result[0]['currency_symbol'],
	                            'language'  =>  array('lang_id' => $setting_result[0]['lang_id'], 'language' => $setting_result[0]['language']),
	                            'is_rtl'    =>  $setting_result[0]['is_rtl'],
	                            'theme' =>  $setting_result[0]['theme'],
	                            'image' =>  $image, );
	                        $this->session->set_userdata('student', $session_data);
	                        $s  =   array();
	                        $user_id    =   ($result[0]->parent_id);
	                        $students_array =   $this->Student_model->read_siblings_students($user_id);
	                        $child_student  =   array();
	                        
	                        foreach ($students_array as $std_key => $std_val) {
	                            $child  =   array(
	                                'student_id'    =>  $std_val->id,
	                                'name'  =>  $std_val->firstname . " " . $std_val->lastname );
	                            $child_student[]    =   $child;
	                        }
	                        
	                        $this->session->set_userdata('parent_childs', $child_student);
	                        $this->customlib->setUserLog($result[0]->username, $result[0]->role);
	                        redirect('Parent/dashboard');
	                    }
	                } else {
	                    echo "\r\n <br/> GIRISH HERE ?";
	                    $data['error_message'] = 'Account Suspended';
	                    redirect('Login', $data);
	                }
	            } else {
	                $data['error_message'] =  'Your account is disabled please contact to administrator';
	                echo "\r\n <br/> GIRISH 1 HERE ";
	                redirect('Login', $data);
	            }
	        } else {
	            echo "\r\n <br/> GIRISH 2 HERE ";
	            redirect('Login');
	        }
	    }
	}
}