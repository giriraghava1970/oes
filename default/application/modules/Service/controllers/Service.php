<?php	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	require(APPPATH.'/libraries/REST_Controller.php');
    ini_set('display_errors','1');
    ini_set('error_reporting','E_ALL');

class Service extends REST_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Chapters_model');
		$this->load->model('Countries_model');
		$this->load->model('Cities_model');
		$this->load->model('States_model');
		$this->load->model('Subjects_model');
		$this->load->model("Users_model");
		$this->load->model("Userroles_model");
		$this->load->model('Topics_model');
		$this->generalFn    =   new GeneralFunctions();
	}

	private function authenticate($tokenParamValue = '') {
	    if ($this->input->post('token') != '') {
	        $token		=	$this->input->post('token');
	        return JWT::decode($token, $this->config->item('jwt_key'));
	    }  else if($tokenParamValue != '') {
	        $token		=	$tokenParamValue;
	        return JWT::decode($token, $this->config->item('jwt_key'));
	    }
	}

	public function getRegisteredDeviceTokens($appName){
	    /*$arrResult		= 	$this->devices_model->getAllDeviceDetails($appName);
	    $arrDeviceTokens	= 	array();

	    if(isset($arrResult) && !empty($arrResult)) {
	        for($i=0; $i<count($arrResult); $i++) {
	            $arrDeviceTokens[$i]	= 	$arrResult[$i]->device_token;
	        }
	    }

	    return $arrDeviceTokens;*/
	}

	public function users_get() {
	    $output    =   array();
	    $output['methodName']  =   'usersList';

	    try {
	        $usersList    =   $this->Users_model->get();
	        $results    =   array();
	        
	        if(!(empty($usersList))) {
	            $output['errorCode']   =   0;
	            $output['errorMessage']    =   'Success';
	            
	            foreach($usersList as $u) {
	                $results['id'] =   intVal($u->id);
	                $results['userName']    =   ucfirst($u->username);
	                $results['status'] =   $u->status;
	                $results['Action'] =   '<a href="javascript:void(0);" id="edit-'.$u->id.'" class="logAction">
                                                <span class="badge badge-primary"> Edit </span>
                                            </a>
                                            <a href="javascript:void(0);" id="del-'.$u->id.'" class="logAction">
                                                <span classs="badge badge-danger"> Delete </span>
                                            </a>';
	                //$results['formCont']   =   $this->getFormCont('Cities', 0);
	            }

	            $output['usersList']  =   $results;
	        }else {
	            $output['errorCode']   =   '1';
	            $output['errorMessage']    =   'Failure';
	            $output['errorDescription']    =   'Cities List does not exists. Please retry.';
	        }
	    }catch (Exception $e) {
	        $this->response(array('error' => $e->getMessage()), $e->getCode());
	    }

	    $data   =   $output;
	    
	    if($data)
            $this->response($data, 200); // 200 being the HTTP response code
        else {
            $output['errorCode']   =   '1';
            $output['errorMessage']    =   'Failure';
            $output['errorDescription']    =   'Users List does not exists. Please retry.';
        }
	}

	public function userValidation_post() {
	    $checkParams   =   $this->_post_args;
	    $output    =   array();
	    $output['methodName']  =   'userValidation';

	    if(!(empty($checkParams)) ) {
	        if($this->validateParams('userValidation', $checkParams) == 3) {
	            try {
	                $token =   array();
	                $validatedArgs =   $this->validateArguments('userValidation', $checkParams);

	                if (!(empty($validatedArgs)) ) {
	                    $checkValidation   =   $validatedArgs;

	                    if($checkValidation['validationResult'] == true) {
	                        unset($output['validationResult']);

	                        try {
	                            $userLogin =   $checkParams['userLogin'];
	                            $userPwd   =   $checkParams['userPwd'];
	                            $deviceId  =   $checkParams['deviceId'];
	                            $results   =   $this->Users_model->verifyLogin($userLogin, $userPwd);

	                            if($results != 0) {
	                                if ( (isset($results[0]->userId)) && ($results[0]->userId != 0) ) {
	                                    unset($results[0]->PWD);
	                                    $token['USER_ID']  =   $results[0]->userId;
	                                    $token['USER_GROUP_ID']    =   $results[0]->groupId;
	                                    $output['errorCode']   =   '-1';
	                                    $output['errorMessage']    =   'Success';
	                                    $output['userId']  =   $results[0]->userId;
	                                    $output['userLogin']   =   $userLogin;
	                                    $output['token']   =   JWT::encode($token, $this->config->item('jwt_key'));
	                                    $output['userName']    =   ucwords($results[0]->firstName . ' '. $results[0]->middleName. ' ' . $results[0]->lastName);
	                                    $output['groupId'] =   $results[0]->groupId;
	                                    $output['groupName']   =   $results[0]->groupName;
	                                }
	                            } else {
	                                $output['errorCode']   =   '1';
	                                $output['errorMessage']    =   'Failure';
	                                $output['errorDescription']    =       'User credentials does not match with the system. Retry.';
	                            }
	                        } catch (Exception $e) {
	                            $this->response(array('error' => $e->getMessage()), $e->getCode());
	                        }
	                    } else {
	                        $output['errorCode']   =   '1';
	                        $output['errorMessage']    =   'Failure';
	                        $output['errorDescription']    =   'More than one parametere value is empty. Retry';
	                    }
	                }
	            } catch (Exception $e) {
	                $this->response(array('error' => $e->getMessage()), $e->getCode());
	            }
	        } else {
	            $output['errorCode']   =   '1';
	            $output['errorMessage']    =   'Failure';
	            $output['errorDescription']    =   'Invalid parameters count';
	        }
	    } else {
	        $output['errorCode']   =   '1';
	        $output['errorMessage']    =   'Failure';
	        $output['errorDescription']    =   'Invalid parameters count';
	    }

	    $data   =   $output;

	    if($data)
	        $this->response($data, 200); // 200 being the HTTP response code
        else {
            $output['errorCode']   =   '1';
            $output['errorMessage']    =   'Failure';
            $output['errorDescription']    =   'User credentials does not match with the system. Retry.';
        }
	}

	public function countries_post() {
	    $output    =   array();
	    $output['methodName']  =   'countriesList';

	    try {
	        $countriesList    =   $this->Countries_model->get();
	        $results    =   array();

	        if(!(empty($countriesList))) {
	            $output['errorCode']   =   0;
	            $output['errorMessage']    =   'Success';
                $cnt    =   0;

	            foreach($countriesList as $c) {
	                $results[$cnt]['id'] =   intVal($c->id);
	                $results[$cnt]['countryCode']  =   $c->country_code;
	                $results[$cnt]['countryName']  =   $c->country_name;
	                $results[$cnt]['status'] =   $c->status;
	                $cnt++;
	            }

	            $output['countriesList']  =   $results;
	        }else {
	            $output['errorCode']   =   '1';
	            $output['errorMessage']    =   'Failure';
	            $output['errorDescription']    =   'Countries List does not exists. Please retry.';
	        }
	    }catch (Exception $e) {
	        $this->response(array('error' => $e->getMessage()), $e->getCode());
	    }

	    $data   =   $output;

	    if($data)
	        $this->response($data, 200); // 200 being the HTTP response code
        else {
            $output['errorCode']   =   '1';
            $output['errorMessage']    =   'Failure';
            $output['errorDescription']    =   'Cities List does not exists. Please retry.';
        }
	}

	public function states_post() {
	    $checkParams   =   $this->_post_args;
	    $output    =   array();
	    $output['methodName']  =   'statesList';

	    if(!(empty($checkParams)) ) {
	        if($this->generalFn->validateParams('states', $checkParams) == 1) {
	            try {
	                $token =   array();
	                $validatedArgs =   $this->generalFn->validateArguments('userValidation', $checkParams);

	                if (!(empty($validatedArgs)) ) {
	                    $checkValidation   =   $validatedArgs;

	                    if($checkValidation['validationResult'] == true) {
	                        unset($output['validationResult']);

                    	    try {
                    	        $statesList    =   $this->States_model->getStatesByCountryID($checkParams['countryID']);
                    	        $results    =   array();

                    	        if(!(empty($statesList))) {
                    	            $output['errorCode']   =   0;
                    	            $output['errorMessage']    =   'Success';
                                    $cnt    =   0;

                    	            foreach($statesList as $s) {
                    	                $results[$cnt]['id'] =   intVal($s->id);
                    	                $results[$cnt]['countryID']  =   intVal($s->country_id);
                    	                $results[$cnt]['countryName']  =  $s->countryName;
                    	                $results[$cnt]['stateCode']  =   $s->state_code;
                    	                $results[$cnt]['stateName']  =   $s->state_name;
                    	                $results[$cnt]['status'] =   $s->status;
                    	                $cnt++;
                    	            }

                    	            $output['statesList']  =   $results;
                    	        }else {
                    	            $output['errorCode']   =   '1';
                    	            $output['errorMessage']    =   'Failure';
                    	            $output['errorDescription']    =   'States List does not exists. Please retry.';
                    	        }
    	                    }catch (Exception $e) {
    	                        $this->response(array('error' => $e->getMessage()), $e->getCode());
    	                    }
    	                }else {
    	                    $output['errorCode']   =   '1';
    	                    $output['errorMessage']    =   'Failure';
    	                    $output['errorDescription']    =   'More than one parametere value is empty. Retry';
    	                }
    	            } else {
    	                
    	            }
    	        }catch (Exception $e) {
    	            $this->response(array('error' => $e->getMessage()), $e->getCode());
    	        }
    	    } else {
    	        $output['errorCode']   =   '1';
    	        $output['errorMessage']    =   'Failure';
    	        $output['errorDescription']    =   'Invalid parameters count';
    	    }
    	} else {
    	    $output['errorCode']   =   '1';
    	    $output['errorMessage']    =   'Failure';
    	    $output['errorDescription']    =   'Invalid parameters count';
    	}

	    $data   =   $output;

	    if($data)
	        $this->response($data, 200); // 200 being the HTTP response code
        else {
            $output['errorCode']   =   '1';
            $output['errorMessage']    =   'Failure';
            $output['errorDescription']    =   'States List does not exists. Please retry.';
        }
	}

	public function cities_post() {
	    $checkParams   =   $this->_post_args;
	    $output    =   array();
	    $output['methodName']  =   'citiesList';

	    if(!(empty($checkParams)) ) {
	        if($this->generalFn->validateParams('cities', $checkParams) == 1) {
	            try {
	                $token =   array();
	                $validatedArgs =   $this->generalFn->validateArguments('userValidation', $checkParams);

	                if (!(empty($validatedArgs)) ) {
	                    $checkValidation   =   $validatedArgs;

	                    if($checkValidation['validationResult'] == true) {
	                        unset($output['validationResult']);

                    	    try {
                    	        $citiesList    =   $this->Cities_model->getCitiesByStateID($checkParams['stateID']);
                                $results    =   array();

                        	    if(!(empty($citiesList))) {
                        	        $output['errorCode']   =   0;
                        	        $output['errorMessage']    =   'Success';
                                    $cnt    =   0;

                        	        foreach($citiesList as $c) {
                        	            $results[$cnt]['id'] =   intVal($c->id);
                        	            $results[$cnt]['stateID']    =   intVal($c->state_id);
                        	            $results[$cnt]['stateName']  =   $c->stateName;
                        	            $results[$cnt]['cityCode']  =   $c->city_code;
                        	            $results[$cnt]['cityName']  =   $c->city_name;
                        	            $results[$cnt]['status'] =   $c->status;
                        	            //$results['formCont']   =   $this->getFormCont('Cities', 0);
                        	            $cnt++;
                        	        }

                        	        $output['citiesList']  =   $results;
                        	    }else {
                        	        $output['errorCode']   =   '1';
                        	        $output['errorMessage']    =   'Failure';
                        	        $output['errorDescription']    =   'Cities List does not exists. Please retry.';
                        	    }
                    	    }catch (Exception $e) {
                    	        $this->response(array('error' => $e->getMessage()), $e->getCode());
                    	    }
	                    }else {
	                        $output['errorCode']   =   '1';
	                        $output['errorMessage']    =   'Failure';
	                        $output['errorDescription']    =   'More than one parametere value is empty. Retry';
	                    }
	                } else {	                    
	                }
	            }catch (Exception $e) {
	                $this->response(array('error' => $e->getMessage()), $e->getCode());
	            }
	        } else {
	            $output['errorCode']   =   '1';
	            $output['errorMessage']    =   'Failure';
	            $output['errorDescription']    =   'Invalid parameters count';
	        }
	    } else {
	        $output['errorCode']   =   '1';
	        $output['errorMessage']    =   'Failure';
	        $output['errorDescription']    =   'Invalid parameters count';
	    }

	    $data   =   $output;

	    if($data)
            $this->response($data, 200); // 200 being the HTTP response code
        else {
            $output['errorCode']   =   '1';
            $output['errorMessage']    =   'Failure';
            $output['errorDescription']    =   'Cities List does not exists. Please retry.';
        }
	}

	public function subjects_post() {
	    $checkParams   =   $this->_post_args;
	    $output    =   array();
	    $output['methodName']  =   'subjectsList';

        try {
            $token =   array();
            $validatedArgs =   $this->generalFn->validateArguments('subjects', $checkParams);

            if (!(empty($validatedArgs)) ) {
                $checkValidation   =   $validatedArgs;

                if($checkValidation['validationResult'] == true) {
                    unset($output['validationResult']);

                    try {
                        $subjList    =   $this->Subjects_model->get();
                        $results    =   array();

                        if(!(empty($subjList))) {
                            $output['errorCode']   =   0;
                            $output['errorMessage']    =   'Success';
                            $cnt    =   0;

                            foreach($subjList as $s) {
                                $results[$cnt]['id'] =   intVal($s->id);
                                $results[$cnt]['subjectName']    =   $s->subject_name;
                                $results[$cnt]['status'] =   $s->status;
                                $cnt++;
                            }

                            $output['subjectsList']  =   $results;
                        }else {
                            $output['errorCode']   =   '1';
                            $output['errorMessage']    =   'Failure';
                            $output['errorDescription']    =   'Subjects List does not exists. Please retry.';
                        }
                    }catch (Exception $e) {
                        $this->response(array('error' => $e->getMessage()), $e->getCode());
                    }
                }else {
                    $output['errorCode']   =   '1';
                    $output['errorMessage']    =   'Failure';
                    $output['errorDescription']    =   'More than one parametere value is empty. Retry';
                }
            } else {
            }
        }catch (Exception $e) {
            $this->response(array('error' => $e->getMessage()), $e->getCode());
        }

	    $data   =   $output;

	    if($data)
	        $this->response($data, 200); // 200 being the HTTP response code
        else {
            $output['errorCode']   =   '1';
            $output['errorMessage']    =   'Failure';
            $output['errorDescription']    =   'Subjects List does not exists. Please retry.';
        }
	}

	public function chapters_post() {
	    $checkParams   =   $this->_post_args;
	    $output    =   array();
	    $output['methodName']  =   'chaptersList';

	    if(!(empty($checkParams)) ) {
	        if($this->generalFn->validateParams('chapters', $checkParams) == 1) {
	            try {
	                $token =   array();
	                $validatedArgs =   $this->generalFn->validateArguments('chapters', $checkParams);

	                if (!(empty($validatedArgs)) ) {
	                    $checkValidation   =   $validatedArgs;

	                    if($checkValidation['validationResult'] == true) {
	                        unset($output['validationResult']);

	                        try {
	                            $chaptersList    =   $this->Chapters_model->getChaptersBySubjectID($checkParams['subjectID']);
	                            $results    =   array();

	                            if(!(empty($chaptersList))) {
	                                $output['errorCode']   =   0;
	                                $output['errorMessage']    =   'Success';
	                                $cnt    =   0;

	                                foreach($chaptersList as $c) {
	                                    $results[$cnt]['id'] =   intVal($c->id);
	                                    $results[$cnt]['subjectID']  =   intVal($c->subject_id);
	                                    $results[$cnt]['subjectName']  =  $c->subjectName;
	                                    $results[$cnt]['chapterName']  =   $c->chapter_name;
	                                    $topicRes  =   $this->Topics_model->getTopicsByChapterID($c->id);

	                                    $results[$cnt]['totalTopics']  =   count($topicRes);
	                                    $results[$cnt]['status'] =   $c->status;
	                                    $cnt++;
	                                }

	                                $output['chaptersList']	=	$results;
	                            }else {
	                                $output['errorCode']   =   '1';
	                                $output['errorMessage']    =   'Failure';
	                                $output['errorDescription']    =   'Chapters List does not exists. Please retry.';
	                            }
	                        }catch (Exception $e) {
	                            $this->response(array('error' => $e->getMessage()), $e->getCode());
	                        }
	                    }else {
	                        $output['errorCode']   =   '1';
	                        $output['errorMessage']    =   'Failure';
	                        $output['errorDescription']    =   'More than one parametere value is empty. Retry';
	                    }
	                } else {
	                    
	                }
	            }catch (Exception $e) {
	                $this->response(array('error' => $e->getMessage()), $e->getCode());
	            }
	        } else {
	            $output['errorCode']   =   '1';
	            $output['errorMessage']    =   'Failure';
	            $output['errorDescription']    =   'Invalid parameters count';
	        }
	    } else {
	        $output['errorCode']   =   '1';
	        $output['errorMessage']    =   'Failure';
	        $output['errorDescription']    =   'Invalid parameters count';
	    }

	    $data   =   $output;

	    if($data)
	        $this->response($data, 200); // 200 being the HTTP response code
        else {
            $output['errorCode']   =   '1';
            $output['errorMessage']    =   'Failure';
            $output['errorDescription']    =   'Chapters List does not exists. Please retry.';
        }
	}

	public function topics_post() {
	    $checkParams   =   $this->_post_args;
	    $output    =   array();
	    $output['methodName']  =   'topicsList';

	    if(!(empty($checkParams)) ) {
	        if($this->generalFn->validateParams('topics', $checkParams) == 2) {
	            try {
	                $token =   array();
	                $validatedArgs =   $this->generalFn->validateArguments('topics', $checkParams);

	                if (!(empty($validatedArgs)) ) {
	                    $checkValidation   =   $validatedArgs;

	                    if($checkValidation['validationResult'] == true) {
	                        unset($output['validationResult']);

	                        try {
	                            $topicsList    =   $this->Topics_model->getTopicsByChapterID($checkParams['subjectID'], $checkParams['chapterID']);
	                            $results    =   array();

	                            if(!(empty($topicsList))) {
	                                $output['errorCode']   =   0;
	                                $output['errorMessage']    =   'Success';
	                                $tnt    =   0;

	                                foreach($topicsList as $t) {
	                                    $results[$tnt]['id'] =   intVal($t->id);
	                                    $results[$tnt]['subjectID']  =   intVal($t->subject_id);
	                                    $results[$tnt]['subjectName']  =  $t->subjectName;
	                                    $results[$tnt]['chapterName']  =   $t->chapterName;
	                                    $subTopicRes  =   $this->Topics_model->getSubTopicsByTopicID($t->id);
	                                    
	                                    $results[$tnt]['totalSubTopics']  =   count($subTopicRes);
	                                    $results[$tnt]['status'] =   $t->status;
	                                    $tnt++;
	                                }

	                                $output['topicsList']	=	$results;
	                            }else {
	                                $output['errorCode']   =   '1';
	                                $output['errorMessage']    =   'Failure';
	                                $output['errorDescription']    =   'Topics List does not exists. Please retry.';
	                            }
	                        }catch (Exception $e) {
	                            $this->response(array('error' => $e->getMessage()), $e->getCode());
	                        }
	                    }else {
	                        $output['errorCode']   =   '1';
	                        $output['errorMessage']    =   'Failure';
	                        $output['errorDescription']    =   'More than one parametere value is empty. Retry';
	                    }
	                } else {

	                }
	            }catch (Exception $e) {
	                $this->response(array('error' => $e->getMessage()), $e->getCode());
	            }
	        } else {
	            $output['errorCode']   =   '1';
	            $output['errorMessage']    =   'Failure';
	            $output['errorDescription']    =   'Invalid parameters count G';
	        }
	    } else {
	        $output['errorCode']   =   '1';
	        $output['errorMessage']    =   'Failure';
	        $output['errorDescription']    =   'Invalid parameters count';
	    }

	    $data   =   $output;

	    if($data)
            $this->response($data, 200); // 200 being the HTTP response code
        else {
            $output['errorCode']   =   '1';
            $output['errorMessage']    =   'Failure';
            $output['errorDescription']    =   'Topics List does not exists. Please retry.';
        }
	}

	
}