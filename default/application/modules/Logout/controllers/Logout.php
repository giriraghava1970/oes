<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('Auth');
        $this->load->library('Enc_lib');
        $this->load->library('mailer');
        $this->load->config('ci-blog');
    }

	public function index() {
	    $admin_session =   $this->session->userdata('admin');
	    $student_session   =   $this->session->userdata('student');
	    $this->auth->logout();
	    redirect('Login');
	}
}
