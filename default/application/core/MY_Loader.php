<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";

class MY_Loader extends MX_Loader {
	public function template($templateCode = '', $templatePath = '', $templateModulePath = '', $template_name = '', $vars = array(), $return = FALSE) {
		//echo "\r\n <BR/> template code : "  . $templateCode . " path : " .$templatePath . " modulePath : " . $templateModulePath. " tempName :  " . $template_name . " vars : \r\n <Br/><pre>"; print_r($vars);
		//echo "\r\n <bR/> sess :\r\n<br/><pre>"; print_r($this->session);
		//exit();
	    if($templateCode == 'Login') {
	        $this->view($templatePath.$templateModulePath.$template_name, $vars);
	    } else {
	        $userLoginName  =   '';

	        /*if ($this->session->has_userdata('admin')) {
	            $admin  =   $this->session->userdata('admin');
	            $userLoginName  =   $admin['username'];
	        }else {
	            $student_session    =   $this->session->all_userdata();

	            if(!(empty($student_session))) {
	                if(isset($student_session['employee']['username'])) {
	                    $userLoginName  =   $student_session['employee']['username'];
	                } else {
	                    $userLoginName  =   $student_session['student']['username'];
	                }
	            }
	        }*/

	        $vars['userLoginName'] =   $userLoginName;
	        $this->view($templatePath.'common/header', $vars);
	        $this->view($templatePath.'common/leftNav', $vars);

	        switch($templateCode) {
	            case 'ClassTimings' :
	                break;
	        }

	        $this->view($templatePath.$templateModulePath.$template_name, $vars);
    		$this->view('templates/common/footer', $vars);
	    }
	}
}