<!DOCTYPE html>
<html lang="en">
    <head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    	<title>:: Online Examination System :: OES :: Login</title>
    	<!-- Global stylesheets -->
    	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    	<link href="<?php echo base_url();?>theme/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    	<link href="<?php echo base_url();?>theme/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    	<link href="<?php echo base_url();?>theme/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    	<link href="<?php echo base_url();?>theme/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    	<link href="<?php echo base_url();?>theme/assets/css/components.min.css" rel="stylesheet" type="text/css">
    	<link href="<?php echo base_url();?>theme/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    	<!-- /global stylesheets -->
    
    	<!-- Core JS files -->
    	<script src="<?php echo base_url();?>theme/assets/js/main/jquery.min.js"></script>
    	<script src="<?php echo base_url();?>theme/assets/js/main/bootstrap.bundle.min.js"></script>
    	<script src="<?php echo base_url();?>theme/assets/js/plugins/loaders/blockui.min.js"></script>
    	<!-- /core JS files -->

    	<!-- Theme JS files -->
    	<script src="<?php echo base_url();?>theme/assets/js/plugins/forms/styling/uniform.min.js"></script>
    	<script src="<?php echo base_url();?>theme/assets/js/app.js"></script>
    	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/login.js"></script>
    	<!-- /theme JS files -->
    </head>
    <body>
    	<!-- Page content -->
    	<div class="page-content">
    		<!-- Main content -->
    		<div class="content-wrapper">
    			<!-- Content area -->
    			<div class="content d-flex justify-content-center align-items-center">
    				<!-- Login card -->
    				<form class="login-form" method="post" action="<?php echo base_url().'Login/VerifyLogin';?>">
    					<div class="card mb-0">
    						<div class="card-body">
    							<div class="text-center mb-3">
    								<i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
    								<h5 class="mb-0">Login to your account</h5>
    								<span class="d-block text-muted">Your credentials</span>
    							</div>
    							<div class="form-group form-group-feedback form-group-feedback-left">
    								<input type="text" required data-mask="9999999999" tabindex="1" name="userLogin" id="userLogin" class="form-control" placeholder="9999999999">
    								<div class="form-control-feedback">
    									<i class="icon-mobile3 text-muted"></i>
    								</div>
    							</div>
    							<div class="form-group form-group-feedback form-group-feedback-left" style="display:none;">
    								<input type="password" class="form-control" id="userPwd" name="userPassword" placeholder="Enter OTP">
    								<div class="form-control-feedback">
    									<i class="icon-lock2 text-muted"></i>
    								</div>
    							</div>
    							<div class="form-group">
    								<button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
    							</div>
    							<span class="form-text text-center text-muted">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a> and <a href="#">Cookie Policy</a></span>
    						</div>
    					</div>
    				</form>
    				<!-- /login card -->
    			</div>
    			<!-- /content area -->
    		</div>
    		<!-- /main content -->
    	</div>
    	<!-- /page content -->
    </body>
</html>