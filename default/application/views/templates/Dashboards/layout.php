		<div class="card">
			<div class="card-body">
				Dashboard Contents Goes here
			</div>
		</div>
    	<script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/spin.min.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/plugins/buttons/ladda.min.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/demo_pages/components_buttons.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/demo_pages/dataTables.checkboxes.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/bootbox.min.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/plugins/ui/prism.min.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/plugins/extensions/jquery_ui/widgets.min.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/plugins/extensions/jquery_ui/effects.min.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/demo_pages/jqueryui_components.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/plugins/loaders/progressbar.min.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/plugins/notifications/sweet_alert.min.js"></script>
        <script src="<?php echo base_url();?>theme/assets/js/demo_pages/extra_sweetalert.js"></script>
        <!-- /Main Charts-->
    	<script src="<?php echo base_url();?>theme/assets/js/custom.js"></script>
    	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Dashboard/dashboard.js"></script>
    	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Dashboard/DashboardCalendarStyling.js"></script>
    	<script src="<?php echo base_url();?>theme/assets/js/demo_pages/Dashboard/dashboardListing.js"></script>